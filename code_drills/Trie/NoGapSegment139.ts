import {TopTrieUpperCaseChar, TrieUpperCaseChar, parseContinuousSegmentsFromLeftHeadRecur} from "./Trie"

// https://leetcode.com/problems/word-break/

// 139
// sample 1: s = "leetcode", wordDict = ["leet","code"]
const topTrie1 = new TopTrieUpperCaseChar()
topTrie1.insert("leet")
topTrie1.insert("code")

const s1 = "leetcode".toUpperCase()

console.log(`${s1} should be separated into 2 as :`)
console.log( parseContinuousSegmentsFromLeftHeadRecur(s1, topTrie1) )

// sample 2:  s = "applepenapple", wordDict = ["apple","pen"]
const topTrie2 = new TopTrieUpperCaseChar()
const s2 = "applepenapple".toUpperCase()
topTrie2.insert("apple")
topTrie2.insert("pen")

console.log(`${s2} should be separated into 3 as :`)
console.log( parseContinuousSegmentsFromLeftHeadRecur(s2, topTrie2) )

// sample 3: s = "catsandog", wordDict = ["cats","dog","sand","and","cat"]
const topTrie3 = new TopTrieUpperCaseChar()
topTrie3.insert("cats")
topTrie3.insert("dog")
topTrie3.insert("sand")
topTrie3.insert("and")
topTrie3.insert("cat")
const s = "catsandog".toUpperCase()

console.log(`${s} should be empty as :`)
console.log( parseContinuousSegmentsFromLeftHeadRecur(s, topTrie3) )
