import {TopTrieUpperCaseChar, TrieUpperCaseChar, parseContinuousSegmentsFromLeftHeadRecur} from "./Trie"

// s = "catsanddog", wordDict = ["cat","cats","and","sand","dog"]
const topTrie140 = new TopTrieUpperCaseChar()
topTrie140.insert("cat")
topTrie140.insert("cats")
topTrie140.insert("and")
topTrie140.insert("sand")
topTrie140.insert("dog")

const s140 = "catsanddog".toUpperCase()
console.log( parseContinuousSegmentsFromLeftHeadRecur(s140, topTrie140) )

// s = "pineapplepenapple", wordDict = ["apple","pen","applepen","pine","pineapple"]
const s140b = "pineapplepenapple".toUpperCase()
const topTrie140b = new TopTrieUpperCaseChar()
//"apple","pen","applepen","pine","pineapple"
topTrie140b.insert("apple")
topTrie140b.insert("pen")
topTrie140b.insert("applepen")
topTrie140b.insert("pine")
topTrie140b.insert("pineapple")
console.log( parseContinuousSegmentsFromLeftHeadRecur(s140b, topTrie140b) )


//s = "catsandog", wordDict = ["cats","dog","sand","and","cat"]
const s140c = "catsandog"
const topTrie140c = new TopTrieUpperCaseChar()
topTrie140c.insert("cats")
topTrie140c.insert("dog")
topTrie140c.insert("sand")
topTrie140c.insert("and")
topTrie140c.insert("cat")

console.log( parseContinuousSegmentsFromLeftHeadRecur(s140c, topTrie140c) )
