import {TopTrieUpperCaseChar, TrieUpperCaseChar, parseContinuousSegmentsFromLeftHeadRecur} from "./Trie"



function findWords(board: string[][], words: string[]): string[] {
    // Build Trie from words
    const topTrie = new TopTrieUpperCaseChar()
    for (let w of words) {
        topTrie.insert(w)
    }

    // Matrix crawl:
    // each crawl check use startWith() to detect  impossible and quit

    // let visited : Set<[number, number]> = new Set<[number, number]>()

    let wordsResults = Array<string>()

    for (let r = 0; r < board.length;r++) {
        for (let c = 0; c<board[0].length;c++) {

            let buf = board[r][c]
            // Each crawl thread must have its own visitied track, since function crawl is subfunction, we do not pass through crawl(...)
            let visited : Set<string> = new Set<string>()

            crawl(r,c, buf)
            visited.clear()

        }
    }

    function crawl(row1: number, col1: number, buf: string){
        const nbrs : Array<[number,number]> = getNbrs(row1, col1)

        if (nbrs.length > 0) {
            for (const nbr of nbrs) {
                let buflead = buf + board[nbr[0]][nbr[1]]

                console.log(`${board[row1][col1]} => ${nbr} ttto be added: ${board[nbr[0]][nbr[1]]} =>> ${buflead}`)

                // console.log(`lead string : ${buf}`)
                if (topTrie.startsWith(buflead)) {
                    console.log(`start with === : ${buflead}`)
                    visited.add(nbr[0] + "-" + nbr[1])

                    if (topTrie.search(buflead)){
                        console.log(`BRAVO!! ${buf}`)
                        wordsResults.push(buflead)
                        return // Here assume only one word possbile with potential bug of multi words found
                    }
                    
                    crawl(nbr[0], nbr[1],buflead)

                } else {     ///////
                    console.log(`${buf} not maching for start with`)
                }


            }
        }

    }
    
    function getNbrs(row1: number, col1: number): [number, number][] {
        visited.add(row1 + "-"+ col1)
        // console.log(`get nbr for : ${row1}, ${col1}, ${board[row1][col1]} <<<`)
        let rtn1 = Array<[number, number]>()

        let poses: Array<[number, number]>  = [
            [row1-1,col1],[row1+1,col1],
            [row1,col1-1],[row1,col1+1],
        ]


        for(const p of poses) {
        // for(let r2= row1-1;r2<row1+2;r2++) {
        //     for (let c2 = col1 -1; c2 < col1 + 2; c2++) {
                let r2 = p[0]
                let c2 = p[1]
                
                if ( visited.has(r2 + "-" + c2) ) {
                    // console.log(`finding existing ... ...: ${r2}, ${c2}`)
                    continue
                }

                if (r2 < 0 || c2<0 || r2 >= board.length || c2 >= board[0].length)
                    continue

                if (r2 == row1 && c2 == col1) {
                    continue
                }

                // console.log(`... ... Adding new : ${r2}, ${c2}`)

                rtn1.push([r2,c2])
                visited.add(r2 +"-"+c2)

            // }
        }


        if (rtn1.length > 0) {
            console.log(`rrrrrrrrrrrrrtttttttttnnnnnnnnnnnnnnnnnnnn: ${rtn1}`)
        }

        return rtn1
    }

    console.log(`wordsResults: ${wordsResults}`)
    return wordsResults
};

// case 1
// board = [["o","a","a","n"],["e","t","a","e"],["i","h","k","r"],["i","f","l","v"]], words = ["oath","pea","eat","rain"]

const board = [["o","a","a","n"],["e","t","a","e"],["i","h","k","r"],["i","f","l","v"]]

// const board = [
//     ["o","a"],
//     ["e","t"],
//     ["i","h"],
//     ["i","f"]
// ]


const words = ["oath","pea","eat","rain"]

// case 2
//board = [["a","b"],["c","d"]], words = ["abcb"]
// const board = [["a","b"],["c","d"]]
// const words = ["abcb"]
// const words = ["abcd"]


findWords(board, words)
