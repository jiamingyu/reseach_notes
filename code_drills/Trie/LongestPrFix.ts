import {TrieUpperCaseChar, TopTrieUpperCaseChar} from "./Trie"

// https://leetcode.com/problems/longest-common-prefix/
const top  = new TopTrieUpperCaseChar()

// const strs14 = ["flo","fr", "fli"]
const strs14 = ["flower","flow","floght"]

for (const str of strs14) {
    top.insert(str.toUpperCase())
}

// console.log(top)
// top.outputChildren()

console.log(top.longestPrefix14())
