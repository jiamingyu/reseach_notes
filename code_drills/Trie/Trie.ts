export abstract class Trie<T> {
    hasDictRecordMatchFromLeft(s: string) : string[] {
        throw new Error("Method not implemented.")
    }

    value : T = undefined
    isEndUnit: boolean = false
    citedCount = 0
    children: Trie<T>[] = new Array<Trie<T>>(26) 

    constructor() {
        // this.citedCount += 1
    }

    abstract insert(word: string): void

    abstract search(word: string): boolean

    abstract startsWith(prefix: string): boolean 

    longestPrefixCriteria14(wordsCount: number): string {
        throw new Error("Method not implemented.")
    }

}

// type UCC = 'A' | 'B' | 'C' | 'D' | 'E' | 'F' | 'G' | 'H' | 'I' | 'J' | 'K' | 'L' | 'M' | 'N' | 'O' | 'P' | 'Q' | 'R' | 'S' | 'T' | 'U' | 'V' | 'W' | 'X' | 'Y' | 'Z';

function calibIdx(word: string) : number {
    return word.toUpperCase().charCodeAt(0) - 'A'.charCodeAt(0)
}

export class TrieUpperCaseChar extends Trie<string> {

    // constructor(v: string) {
    //     super()
    //     this.value = v
    // }

    insert(word: string): void {
        if (word.length == 0)
            return

        if (this.value == word[0].toUpperCase() ){

        } else if (this.value == undefined)
            this.value = word[0].toUpperCase()

        this.citedCount += 1

        if (word.length < 2){
            this.isEndUnit = true
            return
        }

        const downstreamSubstr = word.slice(1)

        let idx =  downstreamSubstr.charCodeAt(0) - 'A'.charCodeAt(0)
        
        if (idx > 25) { // TODO
            throw new Error(`${idx} over supported range 2`)
        }

        // substr & call
        if (this.children[idx] == undefined) {
            this.children[idx] = new TrieUpperCaseChar()
        }
        this.children[idx].insert(downstreamSubstr)
        // console.log(this) //
    }

    override hasDictRecordMatchFromLeft(word: string) : string[] {
        let rtn  = new Array<string>()


        if (word[0] == this.value && this.isEndUnit) {
            rtn.push(word[0])

        }

        if (word.length == 1)
            return rtn

        // const rests : string[] = 
        this.children.filter(c => c != undefined).forEach(c=>{
            c.hasDictRecordMatchFromLeft(word.slice(1)).forEach( sc =>
                rtn.push( word[0] + sc )
            )
        })
        // rests.filter(r=> r != undefined).forEach(rs => rtn.concat(word[0] + rs))
        // console.log(`${this.value}::${word} :: ${rtn}`)
        return rtn
    }

    search(word: string): boolean {
        // console.log(`${this.value}:debuggg: ${word}`)

        if (word.length==1 &&  calibIdx(word) < 25) {
            if (word[0] == this.value && this.isEndUnit) {
                return true
            } 
            else return false;
        }

        if (word[0] == this.value && this.children.filter(c => c.search(word.slice(1))).length > 0 ) {
            
            return true
        }

        return false

    }
    startsWith(prefix: string): boolean {
        if (prefix.length ==0){
            throw new Error("start with call with empty input")
        }

        if (prefix[0] != this.value)
            return false

        if (prefix.length == 1)
            return true

        return this.children.filter(c=>c.startsWith(prefix.slice(1))).length > 0

        
            
    }

    override longestPrefixCriteria14(countCriteria: number) : string {
        
        if (countCriteria != this.citedCount)
            return ""

        let lead : string = undefined
        for (const c of this.children) {
            if (c == undefined)
                continue
            const x = c.longestPrefixCriteria14(countCriteria)

            if (lead == undefined)
                lead = x
            else if (x.length > lead.length){
                lead = x
            }
        }
        // console.log(`${this.citedCount}::${this.value}::${lead}`)
        // console.log(`returning: ${this.value + lead}`)
        return this.value + lead
    }



}

export class TopTrieUpperCaseChar extends TrieUpperCaseChar {
    wordsCount = 0

    build() : TrieUpperCaseChar {
        return new TrieUpperCaseChar()
    }

    constructor() {
        super()

    }

    outputChildren() {
        // console.log(`starting presenting children:`)

        for (const c  of this.children) {
            console.log(c)

        }

        console.log(`ending presenting children:`)
    }

    longestPrefix14(): string {
        let maxs : string = ''

        for (const c of this.children) {
            if (c == undefined || c.citedCount < this.wordsCount)
                continue

            const prefixLead = c.longestPrefixCriteria14(this.wordsCount)
            // console.log(`prefixLead: ${prefixLead}`)
            
            if (prefixLead.length > maxs.length){
                maxs = prefixLead
            }
        }

        // console.log(`max is ${maxs}`)
        return maxs
    }

    override hasDictRecordMatchFromLeft(s: string) : string[] {
        

        for (const c of this.children) {
            if (c == undefined)
                continue

            let x : string[] = c.hasDictRecordMatchFromLeft(s.toUpperCase())
            if (x.length > 0) {
                return x
            }
        }

        return []
    }

    override startsWith(prefix: string): boolean {

        if (prefix.length == 0)
            throw new Error(`input is empty, not valid`)

        return this.children.filter(c=>c.startsWith(prefix.toUpperCase())).length > 0

    }

    override insert(word: string): void {
        word = word.toUpperCase()
        if (word.length == 0)
            return

        let idx =  word.charCodeAt(0) - 'A'.charCodeAt(0)
        
        if (idx > 25) { // TODO
            throw new Error(`${idx} over supported range 1`)
        }

        // substr & call
        this.wordsCount += 1
        if (this.children[idx] == undefined) {
            this.children[idx] = new TrieUpperCaseChar()
        }
        this.children[idx].insert(word)
    }

    search(word: string): boolean {
        return this.children.filter(c => c.search(word.toUpperCase())).length > 0
    }
}

export function parseContinuousSegmentsFromLeftHeadRecur(s: string, topTrie: TopTrieUpperCaseChar): string[][] {
    
    let rtn = Array<Array<string>>()
    const cuts: string[] = topTrie.hasDictRecordMatchFromLeft(s.slice(0))

    for (const sc of cuts) {
        // console.log(`${sc}|${s}`)
        if (sc == s){
            rtn.push([s])
            continue
        }

        const tailsLeads : string[][] = parseContinuousSegmentsFromLeftHeadRecur(s.slice(sc.length), topTrie)
        
        // console.log(`${sc} related tails: ${tailsLeads}`)

        for(const tailslead of tailsLeads) {
            let tmp = new Array<string>()
            tmp.push(sc)
            tmp = tmp.concat(tailslead) //
            rtn.push(tmp)
        }
    }
    return rtn
}


// const topTrie = new TopTrieUpperCaseChar()

// const words = ["oath","pea","eat","rain"]
// for(let w of words) {
//     topTrie.insert(w)
// }

// console.log(topTrie.search("oaa"))

// console.log(topTrie.search("oath"))

