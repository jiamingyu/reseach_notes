import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class NumberTheoryDrill {

    @Test
    public void testModularMultiInverse(){

        int A = 3; int M = 11;
        int X = modularMultiInverse(A, M);
        System.out.println(X);
    }

    /**
     * A * X = 1 MOD M (A*X % M = 1, X < M)
     * @param a
     * @param m
     * @return
     */
    private int modularMultiInverse(int a, int m) {
        for(int i = 1; i < m; i++)
            if((a * i) % m == 1)
                return i;

        return -1;

    }

    //TODO: Least Common Multiple of n number

    @Test
    public void testSegmentedSievePrimeNumber() {
        segmentedSievePrimeNumber(10);
        // TODO: what if limit almost overflow

    }

    /**
     * https://www.geeksforgeeks.org/segmented-sieve/
     *
     * @param limit
     * @return
     */
    public List<Integer> segmentedSievePrimeNumber(int limit) {
        List<Integer> r = new ArrayList<>();
        boolean[] primes = new boolean[limit];
        Arrays.fill(primes, true);

        for(int i = 2; i < primes.length; i++) {
            if (!primes[i])
                continue;

            if (calibAsNonePrime(i) ){
                primes[i] = false;
            }

            for(int j = 2; j < i; j++) {
                if (j * i < primes.length) {
                    primes[j * i] = false;
                }
            }

        }

        for(int i = 1; i < primes.length; i++)
            if (primes[i]){ r.add(i); System.out.println(i);}

            return r;
    }

    private boolean calibAsNonePrime(int num) {
        for(int i = 2; i < num; i++) {
            if (num % i == 0)
                return true;
        }
        return false;
    }


}
