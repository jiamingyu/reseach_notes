import org.junit.jupiter.api.Test;

public class GreedyAlgorithmDrill {


    /**
     * Although greedy category, it is kind of sliding way
     *
     *     https://www.geeksforgeeks.org/minimum-number-platforms-required-railwaybus-station-set-2-map-based-approach/
     */
    @Test
    public void testMinStations(){
        int arr[] = { 900, 940, 950, 1100, 1500, 1800 };
        int dep[] = { 910, 1200, 1120, 1130, 1900, 2000 };

        // cur both point to start
        int maxOverlap = 0;
        int arrCurL = 0;int arrCurR = 0;
        int earlestDptTime = dep[0];

        while(arrCurR < arr.length) {
            // next arrive b4 earliest departure
            if(arrCurR + 1 < arr.length && earlestDptTime > arr[arrCurR+1]) {
                arrCurR+=1;
                earlestDptTime = calibEarlestDptTimes(dep, arrCurL, arrCurR);
                continue;
            }

            // Calib max
            int currentCount = (arrCurR) - arrCurL + 1;
            maxOverlap = maxOverlap > currentCount ? maxOverlap : currentCount;

            // left move
            arrCurL++;
            arrCurR = arrCurR >= arrCurL ? arrCurR : arrCurL;

            // update latesttime on
            if (arrCurR + 1 < arr.length) {
                earlestDptTime = calibEarlestDptTimes(dep, arrCurL, arrCurR);
            }
        }

        System.out.println("Minimum Number of " +
                "Platforms Required = " + maxOverlap);
    }

    public int calibEarlestDptTimes(int[] dep, int arrCurL, int arrCurR) {
        int earlestDptTime = Integer.MAX_VALUE;
        for(int i = arrCurL; i < arrCurR + 1; i++) {
            earlestDptTime = Math.min(earlestDptTime, dep[i]);
        }
        return earlestDptTime;
    }

}
