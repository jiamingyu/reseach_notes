import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class DijkstraSimple {

    final int V = 9;

    int graph[][]
            = new int[][] { { 0, 4, 0, 0, 0, 0, 0, 8, 0 },
            { 4, 0, 8, 0, 0, 0, 0, 11, 0 },
            { 0, 8, 0, 7, 0, 4, 0, 0, 2 },
            { 0, 0, 7, 0, 9, 14, 0, 0, 0 },
            { 0, 0, 0, 9, 0, 10, 0, 0, 0 },
            { 0, 0, 4, 14, 10, 0, 2, 0, 0 },
            { 0, 0, 0, 0, 0, 2, 0, 1, 6 },
            { 8, 11, 0, 0, 0, 0, 1, 0, 7 },
            { 0, 0, 2, 0, 0, 0, 6, 7, 0 } };

    /**
     * Below was made Aug 2023:
     *
     * https://www.geeksforgeeks.org/dijkstras-shortest-path-algorithm-greedy-algo-7/
     */
    @Test
    public void drillDijskstra() {
        List<Integer> r =  makeDijkstraSecond(0);
        System.out.println(r);
    }

    private List<Integer> makeDijkstraSecond(int i) {
        List<Integer> selected = new ArrayList<>();
        List<String> addingPath = new ArrayList<>();
        selected.add(i);
        Integer[] shortestPathTrack = new Integer[graph.length];
        shortestPathTrack[i] = 0;

        while(selected.size() < graph.length) {

            int roundMinDist = Integer.MAX_VALUE;
            int roundMinDistVtx = -1;
            int roundMinDistStartingSingle = -1;

            for( Integer single : selected) {
                for (int j =0 ;j < graph.length; j++) {
                    if (graph[single][j] != 0 && !selected.contains(j)) {
                        int shortestLead = shortestPathTrack[single] + graph[single][j];
                        if (roundMinDist > shortestLead ) {
                            roundMinDist = shortestLead;
                            roundMinDistVtx = j;
                            roundMinDistStartingSingle = single;
                        }

                    }
                }
            }

            selected.add(roundMinDistVtx);
            shortestPathTrack[roundMinDistVtx] =  roundMinDist;
            addingPath.add( roundMinDistStartingSingle + " -> " + roundMinDistVtx);
        }
        return selected;
    }

}
