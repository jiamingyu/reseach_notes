import org.junit.jupiter.api.Test;

import java.util.*;


/**
 * https://www.geeksforgeeks.org/boggle-find-possible-words-board-characters/
 *
 * 
 */
public class BoggleDrill {

    @Test
    public void testFindWordInBoardofChars() {
        char boggle[][] = { { 'G', 'I', 'Z' },
                { 'U', 'E', 'K' },
                { 'Q', 'S', 'E' } };

        String dictionary[] = { "GEEKS", "FOR", "QUIZ", "GUQ", "EE" };

        List<String> r = findWordInBoardofChars(boggle, dictionary);
        r.stream().forEach(s -> System.out.println(s));


    }

    private List<String> findWordInBoardofChars(char[][] boggle, String[] dictionary) {
        List<String> rtn = new ArrayList<>();

        Map<Character, String> c2pos = new HashMap<>();
        for(int r =0 ; r < boggle.length; r++) {
            for(int c =0; c < boggle[0].length; c++) {
                c2pos.putIfAbsent(boggle[r][c], r + "-" + c);
            }
        }

        for(int i = 0 ; i < dictionary.length; i++) {
            char[] chars = dictionary[i].toCharArray();
            if (!c2pos.containsKey(Character.valueOf(chars[0])))
                continue;

            Set<String> visited = new HashSet<>();
            visited.add(c2pos.get(chars[0]));
            String pStr = c2pos.get(chars[0]);

            String[] xNy = pStr.split("-");
            int row = Integer.parseInt(xNy[0]);
            int col = Integer.parseInt(xNy[1]);

            boolean completeMatch = false;

            //single String check
            single_str_check:
            for(int cidx = 1;cidx < chars.length;cidx++) {
                String posStrNext = neighbourHasChar(row, col, chars[cidx], boggle);
                if ( posStrNext != null) {
                    visited.add(posStrNext);

                    String[] xNy2 = posStrNext.split("-");
                    row = Integer.parseInt(xNy2[0]);
                    col = Integer.parseInt(xNy2[1]);

                    if(cidx == chars.length-1)
                        completeMatch = true;

                } else {
                    break single_str_check;
                }
            }
            if (completeMatch)
                rtn.add(dictionary[i] + ">>" + String.join(":", visited));
        }

        return rtn;
    }

    private String neighbourHasChar(int row, int col, char aChar, char[][] boggle) {
        String posStr = null;

        for(int r = row-1; r< row+2; r++) {
            if (r<0 || r > boggle.length-1)
                continue;
            for(int c = col-1; c< col+2; c++) {
                if (c<0 || c > boggle.length-1)
                    continue;

                if (c==col && r == row)
                    continue;

                if (boggle[r][c] == aChar)
                    posStr = r + "-" + c;
            }
        }
        return posStr;
    }

}
