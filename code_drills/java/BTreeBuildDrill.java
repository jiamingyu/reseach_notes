import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;

public class BTreeBuildDrill {
    /**
     * https://www.geeksforgeeks.org/construct-tree-from-given-inorder-and-preorder-traversal/
     */
    @Test
    public void testBuildBTByPreNInOrderTraversal() {


        int in[] = new int[] { 9, 8, 4, 2, 10, 5, 10, 1, 6, 3, 13, 12, 7 };
        int pre[] = new int[] { 1, 2, 4, 8, 9, 5, 10, 10, 3, 6, 7, 12, 13 };

        TreeNode root = buildBTByPreNInOrderTraversal(in, pre);

    }

    class ItrsContainer {
        int inOrderIdxCur = -1;
        int preOrderIdxCur = -1;
        int postOrderIdxCur = -1;


        Map<Integer, TreeNode> v2AncestorParents = new HashMap<>();
        TreeNode tree =null; // first of pre is root

        public boolean allowRedo(int[] in, int[] pre) {
            return inOrderIdxCur < in.length && preOrderIdxCur < pre.length;
        }
    }

    private TreeNode buildBTByPreNInOrderTraversal(int[] in, int[] pre) {
        Stack<TreeNode> preOrderLeftArmStack = new Stack<>();
        TreeNode rootOfTree = new TreeNode(pre[0]);

        ItrsContainer container = new ItrsContainer();
        container.inOrderIdxCur=0; container.preOrderIdxCur = 0;

        container.tree = new TreeNode(pre[0]);
        preOrderLeftArmStack.add(rootOfTree);
        container.v2AncestorParents.put(pre[0], rootOfTree);


        do {
            traverseLeftHoldLowestParent(container, in, pre, preOrderLeftArmStack);

            if (container.preOrderIdxCur < pre.length - 1) {

                TreeNode pop = null;
                while (!container.v2AncestorParents.isEmpty()
                        && container.v2AncestorParents.containsKey(in[container.inOrderIdxCur])
                        && preOrderLeftArmStack.peek().val != in[container.inOrderIdxCur]
                ) { // new left-arm component should connect to related parent node, current stack should pop until that parent
                    preOrderLeftArmStack.pop();
                    container.v2AncestorParents.remove(in[container.inOrderIdxCur]);

                    container.inOrderIdxCur++; //in-order should also add since for case: continuous none-right arm parent node
                }

                // Now end of stack, if exist  should be parent of right arm
                TreeNode nextLeftRoot = new TreeNode(pre[++container.preOrderIdxCur]);

                TreeNode parentNode4Rt = preOrderLeftArmStack.pop();

                parentNode4Rt.right = nextLeftRoot;
                container.v2AncestorParents.remove(parentNode4Rt);

                preOrderLeftArmStack.add(nextLeftRoot);
                container.v2AncestorParents.put(nextLeftRoot.val, nextLeftRoot);
            }
        } while(container.allowRedo(in, pre));


        return rootOfTree;

    }

    private void traverseLeftHoldLowestParent(ItrsContainer container, int[] in, int[] pre, Stack<TreeNode> preOrderLeftArmStack) {
        TreeNode localRootNode = preOrderLeftArmStack.peek();

        Stack<TreeNode> localStack = new Stack<>();
        localStack.push(localRootNode);


        while (pre[container.preOrderIdxCur] != in[container.inOrderIdxCur] && container.preOrderIdxCur < pre.length-1) {
            int preValue = pre[++container.preOrderIdxCur];
            TreeNode node = new TreeNode(preValue);


            localStack.add(node);
            localRootNode.left = node;
            localRootNode = node;
        }

        // now leftlower node
//        localRef.left = new TreeNode(++container.preOrderIdxCur);

        TreeNode lastPopped = null;
        while( !localStack.isEmpty() && in[container.inOrderIdxCur] == localStack.peek().val ) {
            lastPopped = localStack.pop();
//            System.out.println(lastPopped.val + "   :   " + in[container.inOrderIdxCur]);
            container.inOrderIdxCur++; // inorder itr move to next to ready for next round
        }
        // last popped is root to fork right, so push back to stack
        //!!!!!!!!
        Queue<TreeNode> q = new LinkedList<>();
        while (lastPopped != null && lastPopped != preOrderLeftArmStack.peek()) {
            q.add(lastPopped);
            lastPopped = localStack.pop();
        }

        while(!q.isEmpty())
            preOrderLeftArmStack.push(q.poll());



//        if (lastPopped != null && lastPopped != preOrderLeftArmStack.peek())
//            preOrderLeftArmStack.push(lastPopped);



        // inOrderIdxCur is one-step ahead
    }


    /**
     *
     * https://www.geeksforgeeks.org/full-and-complete-binary-tree-from-given-preorder-and-postorder-traversals/
     */
    @Test
    public void buildBTByPreNPostOrderTraversal() {

        int pre[] = { 1, 2, 4, 8, 9, 5, 3, 6, 7 };
        int post[] = { 8, 9, 4, 5, 2, 6, 7, 3, 1 };

    }

    public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;
        TreeNode(int x) { val = x; }
    }

}
