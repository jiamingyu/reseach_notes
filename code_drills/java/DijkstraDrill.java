import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DijkstraDrill {

    /**
     * Find Shortest path between Nodes on Graph/edge.
     * Reading on https://www.baeldung.com/java-dijkstra, disagree W. the solution because you can not predict
     *  shortest path approach based on path info already visited.
     *
     *  - Vote by submitted complete crawls is my approach
     *  - Hacky static DijkstraPath.globalMinDistanceBar avoid unnecessary crawling if path already longer than existing full crawling
     *  - Optional usage & comparison W. Scala version
     *
     *  - Later, optimize by crawl node with short edge as priority, which captures the idea in paper above & some forum
     */
    @Test
    public void testDijkstra() {
        Node nodeA = new Node('A');
        Node nodeB = new Node('B');
        Node nodeC = new Node('C');
        Node nodeD = new Node('D');
        Node nodeE = new Node('E');
        Node nodeF = new Node('F');

        nodeA.addDestination(nodeB, 10);
        nodeA.addDestination(nodeC, 15);

        nodeB.addDestination(nodeD, 12);
        nodeB.addDestination(nodeF, 15);

        nodeC.addDestination(nodeE, 10);

        nodeD.addDestination(nodeE, 2);
        nodeD.addDestination(nodeF, 1);

        nodeF.addDestination(nodeE, 5);

        Graph graph = new Graph();

        graph.addNode(nodeA);
        graph.addNode(nodeB);
        graph.addNode(nodeC);
        graph.addNode(nodeD);
        graph.addNode(nodeE);
        graph.addNode(nodeF);

        DijkstraPath dijkstraPath = calculateShortestPathFromSource(graph, nodeA, nodeE);
        System.out.println(dijkstraPath.distanceFromSource);
        System.out.println(dijkstraPath);
    }




    public DijkstraPath calculateShortestPathFromSource(Graph graph, Node source, Node dest) {
        source.setDistance(0);

        ArrayList<Character> characters = new ArrayList<>();
        characters.add(source.name);
        Optional<DijkstraPath> dijkstraPath = voteShortestPerSingleNode(graph, new DijkstraPath(0, characters), dest);
        return  dijkstraPath.orElse(new DijkstraPath());
    }

    private Optional<DijkstraPath> voteShortestPerSingleNode(Graph graph, DijkstraPath dijkstraPath, Node dest) {

        if (dijkstraPath.distanceFromSource > DijkstraPath.globalMinDistanceBar){
            return Optional.empty();
        }

        Optional<Character> lastCharacter = dijkstraPath.lastNodeName();

        if (!lastCharacter.isPresent()){
            return Optional.empty();
        }

        if ( lastCharacter.get() == dest.name ) {
            dijkstraPath.validPath = true;

            // check update lower bar
            if (DijkstraPath.globalMinDistanceBar > dijkstraPath.distanceFromSource)
                DijkstraPath.globalMinDistanceBar = dijkstraPath.distanceFromSource;

            return Optional.of(dijkstraPath);
        }

        Node lastCharNode = graph.getNodeByName(lastCharacter.get());//TODO: what if not exist by name

        Optional<DijkstraPath> rtn = Optional.empty();

        // optimization: prioritize node with short edge : check impl in sourceDistAscendedNodes()
        for (Pair<Integer, Node> dist2Node : lastCharNode.sourceDistAscendedNodes()) {
            int processedDistance = dist2Node.left + dijkstraPath.distanceFromSource;
            Node adjNode = dist2Node.right;
            ArrayList<Character> processedCharacters = new ArrayList<>(dijkstraPath.path);

            if (processedCharacters.contains(adjNode.name)) //block dup crawl.
                continue;

            processedCharacters.add(adjNode.name);
            Optional<DijkstraPath> dijkstraPathCalculated = voteShortestPerSingleNode(graph, new DijkstraPath(processedDistance, processedCharacters), dest);
            // dijkstraPathCalculated.isEmpty() vs isPresent()
            if (!rtn.isPresent() || (!dijkstraPathCalculated.isEmpty() && dijkstraPathCalculated.get().distanceFromSource < rtn.get().distanceFromSource)) {
                rtn = dijkstraPathCalculated;
            }
        }
        return rtn;
    }


    class Graph {

        private Map<Character, Node> name2Node = new HashMap<>();

        public void addNode(Node nodeA) {
            name2Node.put(nodeA.name, nodeA);
        }

        public Node getNodeByName(Character name) {
            return  name2Node.get(name);
        }
    }

    class Pair<T, N> {
        public Pair(T left, N right) {
            this.left = left;
            this.right = right;
        }

        public T left;
        public N right;

        public T getLeft() {
            return left;
        }
    }

    class Node {

        public Character name;

        private List<Node> shortestPath = new LinkedList<>();

        private Integer distance = Integer.MAX_VALUE;

        Map<Node, Integer> adjacentNodes = new HashMap<>();

        public List<Pair<Integer, Node>> sourceDistAscendedNodes() {

            Pair<String, String> x = new Pair("", "");

            Stream<Pair<Integer, Node>> pairStream = adjacentNodes.entrySet().stream().map(ent -> new Pair(ent.getValue(), ent.getKey()));
            List<Pair<Integer, Node>> edgeWNodes = pairStream.collect(Collectors.toList());


            edgeWNodes.sort(Comparator.comparingInt(Pair::getLeft));
            return edgeWNodes;
        }

        public void addDestination(Node destination, int distance) {
            adjacentNodes.put(destination, distance);
        }

        public Node(Character name) {
            this.name = name;
        }

        public void setDistance(Integer distance) {
            this.distance = distance;
        }
    }

    class DijkstraPath {

        public static int globalMinDistanceBar = Integer.MAX_VALUE;
        List<Character> path = new ArrayList<>();
        public int distanceFromSource = Integer.MAX_VALUE;

        public boolean validPath = false;
        public DijkstraPath() {
        }

        public DijkstraPath(int distanceFromSource, List<Character> path) {
            this.distanceFromSource = distanceFromSource;
            this.path = path;
        }

        public Optional<Character> lastNodeName() {
            return path.isEmpty()? Optional.empty() : Optional.of(path.get(path.size()-1));
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            path.stream().forEach(c-> sb.append(c));

            return sb.toString();
        }
    }

}
