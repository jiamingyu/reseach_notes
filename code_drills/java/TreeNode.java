public class TreeNode {

    public TreeNode(int val, TreeNode left, TreeNode right) {
        this.val = val;
        this.left = left;
        this.right = right;
    }

    public TreeNode(int val, int leftVal, int rightVal) {
        this.val = val;
        this.left = new TreeNode(leftVal, null, null);
        this.right = new TreeNode(rightVal, null, null);
    }

    public TreeNode(int i, int left, TreeNode rightNode) {
        this.val = i;
        this.left = new TreeNode(left, null, null);
        this.right = rightNode;
    }

    public TreeNode(int val) {
        this.val = val;
    }

    int val = Integer.MIN_VALUE;
    TreeNode left;
    TreeNode right;


}
