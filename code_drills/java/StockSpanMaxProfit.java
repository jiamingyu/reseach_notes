import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.stream.Collectors;

public class StockSpanMaxProfit {

    /**
     * Solution using greedy algo W. recur call
     *
     * https://leetcode.wang/leetcode-122-Best-Time-to-Buy-and-Sell-StockII.html
     * Notice: I understand sell date fund not available for next buy, so it has to be next day as earliest re-purchase
     */
    @Test
    public void testStockMaxProfit() {
//        int price[] = { 10, 4, 5, 90, 120, 80 };
        int price[] = { 4, 10,  5, 90, 120, 80 };
        new ArrayList<>();
        ArrayList<CalbResult> calbResults =  stockMaxProfit(price);
        System.out.println(calbResults);

    }

    private ArrayList<CalbResult> stockMaxProfit(int[] prices) {
        return doCalibMaxMargins(prices, 0);
    }


    /**
     * Solution using stack
     */
    @Test
    public void testStockProfitMaxMarginWStack() {

        int[] dailyPrices = {1, 5, 2, 8, 3, 10};
        int transactionLimit = 1;

        int maxProfit = stockProfitMaxMarginWStack(dailyPrices, transactionLimit);

        System.out.println( maxProfit);

    }

    private int stockProfitMaxMarginWStack(int[] dailyPrices, int transactionLimit) {
        int r = -1;

        Stack<Integer> leftTrackLowerStack = new Stack();
        List<CalbResult> calbResults = new ArrayList<>();

        assert dailyPrices.length > 2;
        leftTrackLowerStack.add(0);

        for (int i = 1 ; i < dailyPrices.length;i ++) {
            int maxProfit = Integer.MIN_VALUE;
            int maxProfitLowerIdx = -1;

            while ( !leftTrackLowerStack.isEmpty() && dailyPrices[i] > dailyPrices[leftTrackLowerStack.peek()]) {
                int profitLead = dailyPrices[i] - dailyPrices[leftTrackLowerStack.peek()];

                maxProfitLowerIdx = maxProfit < profitLead ? leftTrackLowerStack.peek() : maxProfitLowerIdx;
                maxProfit = maxProfit < profitLead ? profitLead : maxProfit;

                leftTrackLowerStack.pop();
            }
            if (maxProfit != Integer.MIN_VALUE)
                calbResults.add(new CalbResult(maxProfit, maxProfitLowerIdx,i));

            leftTrackLowerStack.add(i);
        }
        return calbResults.stream().map(x->x.profit).reduce((a,b)->a + b).get();
    }


    class CalbResult implements Comparable {
        public CalbResult() {
        }

        public CalbResult(int profit, int startIdx, int endIdx) {
            this.profit = profit;
            this.startIdx = startIdx;
            this.endIdx = endIdx;
        }

        int profit = -1;
        int startIdx = -1;
        int endIdx = -1;

        @Override
        public int compareTo(Object o) {
//            return this.profit - ((CalbResult)o).profit  ;
            return ((CalbResult)o).profit -this.profit  ; //Desc for priorityQ
        }
    }

    private ArrayList<CalbResult> doCalibMaxMargins(int[] prices, int headIdxOnOriginalArray) {
        final ArrayList<CalbResult> calbResults = new ArrayList<>();

        final CalbResult buffer = new CalbResult();
        List<Integer> greaterThanHead = new ArrayList<>();

        for(int i =1; i< prices.length;i ++)  {
            if (prices[i] > prices[0])
                greaterThanHead.add(i);
        };

        greaterThanHead.stream().filter(x-> x + 1 < prices.length).forEach( gtIdx -> {

            System.out.println(gtIdx);

                    int headMargin = prices[gtIdx] - prices[0];
                    int[] nextRange = Arrays.copyOfRange(prices, gtIdx+1, prices.length);
                    ArrayList<CalbResult> calbResultsLocal = doCalibMaxMargins(nextRange, headIdxOnOriginalArray + gtIdx + 1);
                    Integer cumMargin = calbResultsLocal.stream().map(x -> x.profit).reduce(0, (a, b) -> a + b);//;

                    if (headMargin + cumMargin > buffer.profit){
                        buffer.profit = headMargin  + cumMargin;

                        calbResults.clear();
                        if (!calbResultsLocal.isEmpty()) {
                            calbResults.addAll(calbResultsLocal);
                        }
                        calbResults.add(new CalbResult(headMargin, headIdxOnOriginalArray, headIdxOnOriginalArray + gtIdx));
                    }
                }
        );

        return  calbResults;
    }

    /**
     * https://leetcode.wang/leetcode-121-Best-Time-to-Buy-and-Sell-Stock.html
     *
     * https://leetcode.wang/leetcode-123-Best-Time-to-Buy-and-Sell-StockIII.html
     */
    @Test
    public void testStockProfitWLimitedTrnx() {
        int[] dailyPrices = {1, 5, 2, 8, 3, 10};
        int transactionLimit = 2;

        int maxProfit = stockProfitWTrnxLimit(dailyPrices, transactionLimit);

        System.out.println( maxProfit);
    }

    private int stockProfitWTrnxLimit(int[] dailyPrices, int transactionLimit) {
        assert transactionLimit < dailyPrices.length/2;
        int r = 0;

        Map<Integer, List<Integer>> price2Idxes = new HashMap<>();
        for(int i=0; i< dailyPrices.length; i++) {
            List<Integer> idxSet = price2Idxes.computeIfAbsent(dailyPrices[i], k -> new ArrayList<Integer>());
            idxSet.add(i);
        }

        List<Integer> valuesAsc = price2Idxes.keySet().stream().sorted().collect(Collectors.toList());
        int count = 0;
        int lowerKeyItr =0;
        int upperKeyItr =valuesAsc.size()-1;
        int lowerSetSubcount =0;
        int upperSetSubcount =0;

        while(count < transactionLimit) {
            if (lowerSetSubcount >= price2Idxes.get(valuesAsc.get(lowerKeyItr)).size() ){
                lowerKeyItr+=1;
                lowerSetSubcount=0;
            }

            if (upperSetSubcount >= price2Idxes.get(valuesAsc.get(upperKeyItr)).size() ){
                upperKeyItr -= 1;
                upperSetSubcount = 0;
            }

            lowerSetSubcount+=1;
            upperSetSubcount+=1;
            r += valuesAsc.get(upperKeyItr) - valuesAsc.get(lowerKeyItr);

            count += 1;
        }

        return r;
    }


    /**
     * https://www.geeksforgeeks.org/the-stock-span-problem/
     */
    @Test
    public void testStockSpan(){
//        int price[] = { 10, 4, 5, 90, 120, 80 };
        int price[] = { 100, 80, 60, 70, 60, 75, 85 };
        int span[] = calculateSpanRightNextGreater(price);
    }

    private int[] calculateSpanRightNextGreater(int[] price) {
        int span[] = new int[price.length];
        Arrays.fill(span, -1);
        span[0] = 1;

        Stack<Integer> idxStack = new Stack<>();
        idxStack.add(0);

        for(int i =1; i< price.length;i++) {
            if (idxStack.isEmpty() || price[idxStack.peek()] > price[i]) {
                span[i] = 1;
                idxStack.add(i);
                continue;
            }

            while(!idxStack.isEmpty() && price[idxStack.peek()] < price[i] ){

                idxStack.pop();
            }
            span[i] = i - idxStack.peek();
            idxStack.add(i);
        }

        return span;

    }

}
