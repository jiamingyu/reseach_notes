import org.junit.jupiter.api.Test;

import java.util.*;

public class SlidingAlgorDrill {

    /**
     * https://www.geeksforgeeks.org/equilibrium-index-of-an-array/
     */
    @Test
    public void testEquilibriumIndex()
    {
        int arr[] = { -7, 1, 5, 2, -4, 3, 0 };
        int arr_size = arr.length;

        // Function call
        System.out.println(equilibrium(arr));
    }

    List<Integer> equilibrium(int[] arr) {
        List<Integer> equilibrIdxes = new ArrayList<>();

        int cur = 0;
        int sum = Arrays.stream(arr).sum();
        int lSum = 0;

        while(cur+1 < arr.length-1) {
            lSum += arr[cur];

            cur++;
            int rSum = sum - arr[cur] - lSum;
            if (lSum == rSum)
                equilibrIdxes.add(cur);
        }
        return equilibrIdxes;
    }

    /**
     * https://www.geeksforgeeks.org/print-all-subarrays-with-0-sum/
     * The solution here is to demo when 0->n sum identical, means in-between delta is zero, and that is the subarray we are looking for
     */
    @Test
    public void testFindZeroSumSubArrays()
    {

        // Given array
        int[] arr = {6, 3, -1, -3, 4, -2, 2, 4, 6, -12, -7};
//        int arr = {0, 1, 2,   3, 4, 5,  6, 7, 8, 9,    0};
        int n = arr.length;

        // Function Call
        ArrayList<Pair> out = findZeroSumSubArrays(arr);

        // if we didn’t find any subarray with 0 sum,
        // then subarray doesn’t exists
        if (out != null && out.size() == 0)
            System.out.println("No subarray exists");
        else
            System.out.print(out);
    }

    ArrayList<Pair> findZeroSumSubArrays(int[] arr) {
        ArrayList<Pair> r = new ArrayList<>();

        int[] sums = new int[arr.length];
        Map<Integer, List<Integer>> sum2Idxes = new HashMap<>();

        int i = 0;
        while(i < arr.length) {
            sums[i] = i ==0 ? arr[i] : sums[i-1] + arr[i];
            List<Integer> indexes = sum2Idxes.computeIfAbsent(sums[i], x -> new ArrayList<>());
            indexes.add(i);
            i++;
        }

        for(Map.Entry<Integer, List<Integer>> e : sum2Idxes.entrySet()) {
            if(e.getValue().size() > 2) { // first index should not be part of 0 sum subarray
                System.out.println(e.getKey());
                System.out.println(e.getValue());
                r.add(new Pair(e.getValue().get(1), e.getValue().get(2))); //many assert here skipped
            }
        }
        return r;
    }

    class Pair
    {
        int left, right;
        Pair(int a, int b)
        {
            left = a;
            right = b;
        }

        @Override
        public String toString() {
            return left + "->" + right;
        }
    }

    /**
     * https://www.geeksforgeeks.org/longest-subsequence-of-the-form-010-in-a-binary-string/
     * Published solution on above not intuitive although correct. I use FSM to achieve O(n)
     *
     */
    @Test
    public void testLongestZeroOneZero() {
        //        String s = "000011100000";
        String s = "10000111000001";
        System.out.println(longestSubseq2(s));
    }


    public static int longestSubseq2(String s)
    {
        int max = 0;
        int track = 0;
        CurStatus status = CurStatus.INIT;

        char[] chars = s.toCharArray();

        for(int i =0; i < chars.length;i++) {
            int numericChar = Character.getNumericValue(chars[i]);
            assert(numericChar == 1  || numericChar == 0);

            switch (status) {
                case INIT: {
                    if(numericChar==0){
                        status = CurStatus.START_ZERO;
                        track =1;
                    }

                    break;
                }
                case START_ZERO: {
                    if(numericChar==1){
                        status = CurStatus.ZERO_ONE;
                    }
                    track +=1;
                    break;
                }
                case ZERO_ONE: {
                    if(numericChar==0){
                        status = CurStatus.ONE_ZERO;
                    }
                    track +=1;
                    break;
                }
                case ONE_ZERO: {
                    if(numericChar==1){
                        status = CurStatus.INIT;
                        max = max > track ? max : track;
                        track =1;
                    } else {
                        track +=1;
                    }
                    break;
                }
            }
        }

        //valid until last index scenario
        max = max > track ? max : track;
        return max;
    }

    enum CurStatus {
        INIT, START_ZERO, ZERO_ONE, ONE_ZERO
    }

    /**
     * https://www.geeksforgeeks.org/largest-subarray-with-equal-number-of-0s-and-1s/
     * A. -1 represent 0 value B. Any equal cum means delta change zero inbetween.
     */
    @Test
    public void testSubArrayWEqualZeroNOne() {
        int arr[] = { 1, 0, 0, 1, 0, 1, 1 };
        int n = arr.length;
        int brr[] = subArrayWEqualZeroNOne(arr, n);
        Arrays.stream(brr).forEach(x -> System.out.print(x + ","));
        System.out.println(brr);
    }

    private int[] subArrayWEqualZeroNOne(int[] arr, int n) {
        int[] cum = new int[arr.length];
        Map<Integer, List<Integer>> v2Idxes = new HashMap<>();

        cum[0] = arr[0]==0?-1:1;
        v2Idxes.computeIfAbsent(cum[0], x -> new ArrayList<>()).add(0);

        for(int i = 1;i < arr.length; i++) {
            int i1 = arr[i] == 0 ? -1 : 1;
            cum[i] = cum[i-1] + i1;
            List<Integer> idxes = v2Idxes.computeIfAbsent(cum[i], x -> new ArrayList<>());

            idxes.add(i);
        }

        int l = 0; int r = cum.length - 1;

        int maxDelta = 0;
        for(List<Integer> idxes : v2Idxes.values())  {
            if(idxes.get(idxes.size()-1) - idxes.get(0) > maxDelta) {
                maxDelta = idxes.get(idxes.size()-1) - idxes.get(0);
                l = idxes.get(0); r = idxes.get(idxes.size()-1);
            }
        }
        System.out.println(l + "<+ index L(Not included):index R =>" + r);

        return Arrays.copyOfRange(arr, l+1, r+1);

    }

}
