import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.stream.Collectors;

public class TarjanBridgeFinder {

    @Test
    public void testTarjanGraphBridgeA() {

        Graph<Integer> g1 = new Graph<>();
        g1.addEdge(1, 0);
        g1.addEdge(0, 2);
        g1.addEdge(2, 1);
        g1.addEdge(0, 3);
        g1.addEdge(3, 4);

        tarjanGraphBridge(g1, 0);
    }

    @Test
    public void testTarjanGraphBridgeB() {

        Graph<Integer> g2 = new Graph<>();
        g2.addEdge(0, 1);
        g2.addEdge(1, 2);
        g2.addEdge(2, 3);

        tarjanGraphBridge(g2, 0);
    }


    @Test
    public void testTarjanGraphBridgeC() {

        Graph<Integer> g3 = new Graph<>();

        g3.addEdge(0, 1);
        g3.addEdge(1, 2);
        g3.addEdge(2, 0);
        g3.addEdge(1, 3);
        g3.addEdge(1, 4);
        g3.addEdge(1, 6);
        g3.addEdge(3, 5);
        g3.addEdge(4, 5);

        tarjanGraphBridge(g3, 0);
    }
    public <T> void tarjanGraphBridge(Graph<T> g1, T start) {
        if (!g1.vertix2Adjancents.containsKey(start))
            throw new IllegalArgumentException("No edge defined W. start value");

        TarjanContainer<T> container = new TarjanContainer<>(start);
        stackCheckBridge(start, g1, container);

        for(T vertValue:  g1.vertix2Adjancents.keySet().stream().filter(v -> !container.visited(v)).collect(Collectors.toList())) {
            stackCheckBridge(vertValue, g1, container);
        }

        container.collectBridges();

    }

    private <T> void stackCheckBridge(T hub, Graph<T> g1, TarjanContainer<T> tarjanContainer) {
        List<T> adjs = g1.vertix2Adjancents.get(hub);
        if (adjs ==null)
            return;

        for(T adj : adjs) {

            // Loop vert adjs
            // (visited but vert is NOT parent of adj.): vert's earlestTick can reduce by adj. earlestTick

            // Not yet visited:
            // 1. markParentNInitialTick,
            // 2. stack call stackCheckBridge(adj, ...
            // 3. above stack back here might impact vertValue earlestTick by adj's earlestTick update

            //4. !!!: now if  adj's vertex2EarlestTickAdj is not itself && mapped initial Tick < vertvalue.InitialTick
            // <<<< IT IS A BRIDGE >>>>: tarjanContainer.detectBridge(vertValue, adj)


            if (tarjanContainer.visited(adj) && !tarjanContainer.isParentOfAdj(hub, adj)) {
                boolean updateHappen = tarjanContainer.tryReduceHubTickByNoneChildAdj(hub, adj);
                continue;
            }

            if (!tarjanContainer.visited(adj)) {
                tarjanContainer.markParentWCountChildInitialTick(hub, adj);
                stackCheckBridge(adj, g1, tarjanContainer);
                
                tarjanContainer.checkUpdateParentEarlistTickCountPerChild(hub, adj);
            }
        }
    }

    class TarjanContainer<T>{
        int tickCount = 0;
        Map<T, Integer> vertex2InitialTickCount = new HashMap<>();
        Map<T, T> vertexEarlestTickDerivedByAdj = new HashMap<>();
        Map<T, T> child2Parent = new HashMap<>();

        public TarjanContainer(T start) {
            tickCount++;
            vertex2InitialTickCount.put(start, tickCount);
            vertexEarlestTickDerivedByAdj.put(start, start);

        }

        public void markParentWCountChildInitialTick(T parent, T child) {
            tickCount++;
            vertex2InitialTickCount.put(child, tickCount);
            vertexEarlestTickDerivedByAdj.put(child, child);

            // for head only
            if (vertex2InitialTickCount.containsKey(parent)){
                child2Parent.put(child, parent);
            }
        }

        public void checkUpdateParentEarlistTickCountPerChild(T parent, T current) {
            if (vertex2InitialTickCount.get(vertexEarlestTickDerivedByAdj.get(parent)) > vertex2InitialTickCount.get(vertexEarlestTickDerivedByAdj.get(current))) {
                vertexEarlestTickDerivedByAdj.put(parent, vertexEarlestTickDerivedByAdj.get(current));
            }
        }

        /**
         *
         * @return true if new bridge is detected
         */
        public boolean detectBridge(T parentLead, T current) {
            return false;
        }

        public boolean isParentOfAdj(T hub, T adj) {
            return child2Parent.containsKey(adj) && child2Parent.get(adj) == hub;
        }

        public boolean visited(T adj) {
            return vertex2InitialTickCount.containsKey(adj);
        }

        public boolean tryReduceHubTickByNoneChildAdj(T hub, T adjShouldNoSon) {
            if (!vertex2InitialTickCount.containsKey(hub) || !vertex2InitialTickCount.containsKey(adjShouldNoSon))
                return false;
            if ( !isParentOfAdj(adjShouldNoSon, hub) && vertex2InitialTickCount.get(adjShouldNoSon) < vertex2InitialTickCount.get(hub)) {
                vertexEarlestTickDerivedByAdj.put(hub, adjShouldNoSon);
                return true;
            }
            return false;
        }

        public void collectBridges() {
            child2Parent.forEach((c, p) -> {
                System.out.println(
                       c + "|" + vertexEarlestTickDerivedByAdj.get(c) + "|" + vertex2InitialTickCount.get(vertexEarlestTickDerivedByAdj.get(c))
                                + " : " + vertex2InitialTickCount.get(p) + "|" + p
                );
                if (vertex2InitialTickCount.get(vertexEarlestTickDerivedByAdj.get(c)) > vertex2InitialTickCount.get(p) ) //??????
                    System.out.println(c + " is child of " + p + " W. bridge connection");
            });
        }
    }
    class Graph<T> {
        Map<T, List<T>> vertix2Adjancents = new HashMap<>();

        void addEdge(T k, T v) {
            vertix2Adjancents.computeIfAbsent(k, s -> new ArrayList<T>()).add(v);
            vertix2Adjancents.computeIfAbsent(v, s -> new ArrayList<T>()).add(k);
        }
    }

}
