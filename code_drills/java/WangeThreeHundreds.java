import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class WangeThreeHundreds {

    class NumArray {
        int UNDETERMINED = -1;
        int[][] seSum;
        int[] nums;

        public NumArray(int[] nums) {
            this.nums = nums;
            seSum = new int[nums.length][nums.length];
            for(int i = 0; i  < seSum.length; i++)
                Arrays.fill(seSum[i], Integer.MIN_VALUE);

            seSum[0][0] = nums[0];

            exeSum(0, nums.length-1, UNDETERMINED);


        }

        private void exeSum(int sidx, int eidx, int impactedIdx) {
            assert sidx <= eidx;
            assert sidx >= 0;
            assert eidx < nums.length;

            for(int i = sidx; i < eidx+1; i++) {
                for (int j = i; j < eidx + 1; j++) {
                    if (impactedIdx != UNDETERMINED && (impactedIdx < sidx || impactedIdx > eidx))
                        continue;

                    if (i ==j){
                        seSum[i][j] = nums[i];
                        continue;
                    }


                    if (i > 0 && seSum[i-1][j] != Integer.MIN_VALUE) {
                        seSum[i][j] = seSum[i-1][j] - nums[i-1];
                        continue;
                    }

                    if (j > 0 && seSum[i][j-1] != Integer.MIN_VALUE) {
                        seSum[i][j] = seSum[i][j-1] + nums[j];
                        continue;
                    }

                }
            }
        }

        public void update(int index, int val) {
            nums[index] = val;

            exeSum(0, nums.length-1, index);


        }

        public int sumRange(int left, int right) {

            return seSum[left][right];

        }
    }


    /**
     * https://zhuanlan.zhihu.com/p/176925249
     *
     * https://leetcode.com/problems/range-sum-query-mutable/
     *
     */
    @Test
    public void testRangeSumQueryMutable() {
        NumArray na = new NumArray(new int[]{1,3,5});
        na.update(0,2);
        System.out.println(na.sumRange(0, 2));


    }


    /**
     *
     * https://zhuanlan.zhihu.com/p/143278873
     * https://leetcode.com/problems/remove-invalid-parentheses/
     */
    @Test
    public void testRemoveInvalidParentheses() {
        String s = "()())()";
        //              |
        List<String> r = correctInvalidRtParentheses(s);
        HashSet<String> rset = new HashSet<>(r);
        rset.stream().forEach(x -> System.out.println(x));
    }

    //TODO multi showing scenarios "()())()()())()"
    //                                 |      |

    //TODO  Left overflow parentheses secnarios

    //TODO: simple good case : ()()

    private List<String> correctInvalidRtParentheses(String s) {
        // assert only '(' or  ')'
        int track = 0;
        int trackEndIdx = 0;
        List<Integer> rtIdxes = new ArrayList<>();
        for(int i = 0 ; i < s.length(); i++) {
            int trackOld = track;
            track = s.charAt(i)== '(' ? track+1 : track-1;

            // right parenthesis collecting
            if (s.charAt(i)== ')') {
                rtIdxes.add(i);
            }

            if (trackOld < 0) { // last round already ')' overflows
                if (trackOld < track){ // scenario: this round is '(', finally not going to the wrong more
                    // end of cum over )
                    // TODO: review, review & review
                    track = trackOld;
                    trackEndIdx = i - 1;
                    break;
                }
            }
        }
        List<String> r = null;
        if (track < 0) {
            r = calibPermutation("", s.substring(0, trackEndIdx+1), rtIdxes, track * (-1), s.substring(trackEndIdx+1, s.length()));
        }
        return r;

    }

    private List<String> calibPermutation(String headSubstring, String s, List<Integer> leftIdxes, int track, String tailSubstring) {

        List<String> r = new ArrayList<>();
        List<List<Integer>> vaildRtIdxesCol = getReducedSubseq(leftIdxes, track);

        for(List<Integer> idxes : vaildRtIdxesCol) {
            StringBuilder sb = new StringBuilder();
            for(int i=0; i < s.length(); i++) {
                if (s.charAt(i) == ')' && !idxes.contains(i)) // Only add idxes allowed ')'
                    continue;
                sb.append(s.charAt(i));
            }
            r.add(sb.append(tailSubstring).toString());
        }

        return r;
    }

    private List<List<Integer>> getReducedSubseq(List<Integer> dl, int countDown) {
        List<List<Integer>> r = new ArrayList();


        if (countDown <= 0) {
            r.add(dl);
            return r;
        }


        if (dl.size() < countDown) //either way no way to get result
            return r;

        r.addAll(getReducedSubseq(dl.subList(1, dl.size()), countDown-1));// head is the one removed

        List<List<Integer>> tails = getReducedSubseq(dl.subList(1, dl.size()), countDown);//head kept, all removes are from the rest
        List<List<Integer>> prefixed = new ArrayList<>();
        for(List<Integer> tail : tails) {
            List<Integer> x = new ArrayList<>();
            prefixed.add(x);

            x.add(dl.get(0));
            x.addAll(tail);
        }
        r.addAll(prefixed);

        return r;
    }

    @Test
    public void testPickSubSeq() {

        Integer[] d = { 1, 3, 4, 5};
        List<Integer> dl = Arrays.asList(d);

        List<List<Integer>> r = getReducedSubseq(dl, 2);
        r.stream().forEach(sr -> System.out.println(sr));
    }


    /**
     * https://leetcode.wang/leetcode-306-Additive-Number.html
     *
     * https://leetcode.com/problems/additive-number/
     *
     * - What we are approaching is to find a solution and exit, instead of comprehensive multiple solutions.
     * - Dynamic programming is the fundamental approach.
     * - Sum or 2nd number should NOT be less than 1st (length also).
     * - Lately, the zero starting position scenario added to.
     *
     */
    class AdditiveResult{
        boolean good;
        String nums;

        public AdditiveResult(boolean b, String concatResult) {
            good = b;
            nums = concatResult;
        }
    }
    @Test
    public void testAdditiveNums() {
        String numbStr = "112358";

        AdditiveResult additiveResult = existsAdditiveNumsComb(null, null, numbStr, "");
        if(additiveResult != null && additiveResult.good)
            System.out.println(additiveResult.nums);
        else
            System.out.println("Additive number does not exist");

    }

    @Test
    public void testAdditiveNumsHandleZero() {
        String numbStr = "0112358";

        AdditiveResult additiveResult = existsAdditiveNumsComb(null, null, numbStr, "");
        if(additiveResult != null && additiveResult.good)
            System.out.println(additiveResult.nums);
        else
            System.out.println("Additive number does not exist");
    }

    @Test
    public void testAdditiveNumsHandleZeroB() {
        String numbStr = "1123508";

        AdditiveResult additiveResult = existsAdditiveNumsComb(null, null, numbStr, "");
        if(additiveResult != null && additiveResult.good)
            System.out.println(additiveResult.nums);
        else
            System.out.println("Additive number does not exist");
    }

    @Test
    public void testAdditiveNumsfalse() { //false
        String numbStr = "112359";
        AdditiveResult additiveResult = existsAdditiveNumsComb(null, null, numbStr, "");
        if(additiveResult != null && additiveResult.good)
            System.out.println(additiveResult.nums);
        else
            System.out.println("Not additive numbers");

    }

    @Test
    public void testAdditiveNums2B() {
        String numbStr = "199100199";
        AdditiveResult additiveResult = existsAdditiveNumsComb(null, null, numbStr, "");
        if(additiveResult != null && additiveResult.good)
            System.out.println(additiveResult.nums);
    }

    private AdditiveResult existsAdditiveNumsComb(Integer firstInt, Integer secondInt, String numbStr, String concatResult) {
        if(numbStr.isEmpty() || numbStr == "") // Recur exit w. good
            return new AdditiveResult(true, concatResult);

        // Zero @ 0 idx does not deserve further crawling
        if (numbStr.startsWith("0")) {
            return null;
        }


        if (firstInt == null) {
            for(int i=1; i < numbStr.length()/2 + 1; ++i) {
                AdditiveResult r = existsAdditiveNumsComb(Integer.parseInt( numbStr.substring(0, i) ), null, numbStr.substring(i), concatResult.concat(numbStr.substring(0, i)).concat(","));
                if ( r != null && r.good)
                    return r;
            }
            return null;
        } else {
            if (secondInt == null) {
                for(int i = 1; i < numbStr.length();i++) {

                    AdditiveResult r = existsAdditiveNumsComb(firstInt, Integer.parseInt( numbStr.substring(0, i) ), numbStr.substring(i), concatResult.concat(numbStr.concat(",").substring(0, i)));
                    if (r != null && r.good)
                        return r;
                }
                return null;
            }

            // get sum number
            for(int i = 1; i <= numbStr.length(); i++) {
                int sum = Integer.parseInt(numbStr.substring(0, i));
                if (sum == firstInt + secondInt){
                    AdditiveResult additiveResult = existsAdditiveNumsComb(secondInt, sum, numbStr.substring(i), concatResult.concat(",").concat(numbStr.substring(0, i)));
                    if (additiveResult != null && additiveResult.good)
                        return additiveResult;
                }
            }
            return null;
        }
    }

}
