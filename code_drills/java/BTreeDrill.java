import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

/**
 * Whenever working on BTree, pay attention it is just BTree or B. Search Tree
 */
public class BTreeDrill {

    /**
     * https://www.geeksforgeeks.org/find-minimum-depth-of-a-binary-tree/
     *
     * My solution: A. BFS prefer to DFS as only care first leaf level. B. Use kind of poison pill idea to mark new level in Q
     */
    @Test
    public void testMinDepth() {
        Node tree = new Node(1);
        tree.left = new Node(2);
        tree.right = new Node(3);

        tree.left.left = new Node(4);
        tree.left.right = new Node(5);

        tree.right.left = new Node(6);
        tree.right.right = new Node(7);

        tree.right.left.right = new Node(8);

        int minDpth = findMinDepthBFS(tree);

        System.out.println(minDpth);

    }

    private int findMinDepthBFS(Node tree) {
        Node cur = null;
        Queue<Node> q = new ArrayBlockingQueue<>(100);
        int level = 0;

        q.add(tree);
        q.add(new MarkNode(level));

        while (q.peek() != null) {
            cur = q.poll();

            if (cur instanceof MarkNode) {
                level ++;
                q.add(new MarkNode(level));
            } else if (cur.left != null && cur.right != null){

                q.add(cur.left); q.add(cur.right);
            } else {
                break;
            }

        }

        return level;
    }

    public class MarkNode extends  Node{
        int level;
        public MarkNode(int level){
            this.level = level;
        }
    }


    /**
     * Helper class for all BTree tests
     */
    public class Node {
        public Node(){}

        public Node(int val, Node left, Node right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }

        public Node(int val, int leftVal, int rightVal) {
            this.val = val;
            this.left = new Node(leftVal, null, null);
            this.right = new Node(rightVal, null, null);
        }

        public Node(int val) {
            this.val = val;
        }

        public int val = Integer.MIN_VALUE;
        Node left;
        Node right;
    }

    public class CharNode {
        public CharNode(){}

        public CharNode(char val) {
            this.val = val;
        }

        public Character val;
        CharNode left;
        CharNode right;
    }

    @Test
    public void testValidatePreOrder() {
        int[] pre1 = new int[]{40, 30, 35, 80, 100}; //valid
//        int[] pre1 = new int[]{40, 30, 35, 80, 32, 100}; // invalid
        StackRtn stackRtn = validatePreOrder(pre1);
        System.out.println(stackRtn.isValid + " at index "+ stackRtn.crawlIdx);
    }

    /**
     * This is solution ref. Instead of using stack instance, recur taking the role for my approach. Initially looking easy one turns out very tricky
     * https://www.geeksforgeeks.org/check-if-a-given-array-can-represent-preorder-traversal-of-binary-search-tree/
     * @param pre1
     * @return
     */
    private StackRtn validatePreOrder(int[] pre1) {
        int curIdx = 0;

        StackRtn stackRtn = validatePreOrderLeftChildStackCall(curIdx, pre1, -1);
        while(stackRtn.isValid && stackRtn.crawlIdx < pre1.length-1) {
            stackRtn = validatePreOrderLeftChildStackCall(stackRtn.crawlIdx, pre1, stackRtn.bottomValIdx);
        }
        return stackRtn;
    }

    private StackRtn validatePreOrderLeftChildStackCall(int curIdx, int[] ary, int bottomValIdx) {
        // End of crawl
        if (curIdx == ary.length - 1) // last element,  end of stack
            return new StackRtn(curIdx, true, curIdx);
        assert ary[curIdx] != ary[curIdx+1]; //atomic W. above guarantee not out of index

        // value lower than lower bar => invalid
        if (bottomValIdx != -1 && ary[curIdx] < ary[bottomValIdx])
            return new StackRtn(curIdx, false, bottomValIdx);

        int crawlIdx = curIdx+1;
        // crawlIdx lower than bottom value => invalid
        if (bottomValIdx != -1 && ary[crawlIdx] < ary[bottomValIdx] ) {
            return new StackRtn(bottomValIdx, false, crawlIdx);
        }

        if (ary[curIdx] < ary[crawlIdx]) { //kill current stack, bring crawlIdx to parent, maybe parent will assign this to current cur's right arm
            int lowerValueIdx = curIdx;
            int crawlingIdx = curIdx + 1;
            return new StackRtn(lowerValueIdx, true,crawlingIdx); // stack up W. bottom value
        }

        /**
         * Now left child approach
         */
        StackRtn stackRtn = validatePreOrderLeftChildStackCall(curIdx + 1, ary, curIdx + 1);

        if (!stackRtn.isValid)
            return stackRtn;


        StackRtn reviewedStackRtn;
        if(ary[stackRtn.crawlIdx] < ary[curIdx])
        {
            /**Note:
             * Observe from bottom up, crawl v. is left of cur but right of harvested stack v,
             * meaning valid BTree under crawl should all under current stack
             * Only Exception: reviewedStackRtn.crawlIdx $V in provided array is larger than curIdx V. meaning scale to upper scale
             *
             * Most complicated scenario!!
             * This step took me longer to weigh as I struggled to figure out it won't be more recur call as BTree under crawlIdx value should all < cur stack value
             */
            reviewedStackRtn = validatePreOrderLeftChildStackCall(stackRtn.crawlIdx, ary, stackRtn.bottomValIdx);
            //If still ary[stackRtn1.incrementalIdx] > ary[curIdx], definitely won't be valid under current stack,
            // Only valid possibility is to update W. current Idx as lower Value then stack up for further check.
            if ( ary[reviewedStackRtn.crawlIdx] > ary[curIdx] ){
                reviewedStackRtn.bottomValIdx = curIdx;
            }
        } else // scenario: current node not big enough to hold incrementalIdx as right child, stack up W. current node value as bottom value
            reviewedStackRtn = new StackRtn(curIdx, true, stackRtn.crawlIdx);

        return reviewedStackRtn;
    }

    class StackRtn {
        int crawlIdx;
        int bottomValIdx;
        boolean isValid;

        public StackRtn(int bottomValIdx, boolean isValid, int crawlIdx) {
            this.bottomValIdx = bottomValIdx;
            this.isValid = isValid;
            this.crawlIdx = crawlIdx;
        }
    }


    /**
     *
     * Ref : https://www.geeksforgeeks.org/bottom-view-binary-tree/
     *
     *                                      20
     *                                      /\
     *                   8                                  22
     *                /         \                     /             \
     *              5           3                   4               25
     *                          /   \
     *                      10      14
     */
    @Test
    public void testBottomViewOnTree() {
        Node root = new Node(20);
        root.left = new Node(8);
        root.right = new Node(22);
        root.left.left = new Node(5);
        root.left.right = new Node(3);
        root.right.left = new Node(4);
        root.right.right = new Node(25);
        root.left.right.left = new Node(10);
        root.left.right.right = new Node(14);

        directPrintBottomLeafView(root);
    }

    private void directPrintBottomLeafView(Node root) {
        doPrintDFS(root);
    }

    private void doPrintDFS(Node root) {
        if(isLeafNPrint(root))
            return;

        if (root.left != null) {
            doPrintDFS(root.left);
        }
        if (root.right != null)
            doPrintDFS(root.right);

    }

    private boolean isLeafNPrint(Node rt) {
        if (rt.left == null && rt.right == null){
            System.out.println("leaf v: " + rt.val);
        }
        return rt.left == null && rt.right == null;
    }

    @Test
    public void testBuildBSearchTreeByInNPreOrderArray() {
        char in[] = new char[] { 'D', 'B', 'E', 'A', 'F', 'C' };
        char pre[] = new char[] { 'A', 'B', 'D', 'E', 'C', 'F' };

        CharNode rootCharNode = buildBSearchTreeByInNPreOrderArray(in, pre);
        //TODO: print tree structure

    }

    /**
     * Ref:
     * https://www.geeksforgeeks.org/construct-tree-from-given-inorder-and-preorder-traversal/
     * https://www.geeksforgeeks.org/check-binary-tree-subtree-another-binary-tree-set-2/ [Partial]
     *
     * @param in
     * @param pre
     * @return
     */
    private CharNode buildBSearchTreeByInNPreOrderArray(char[] in, char[] pre) {
        Map<Character, Integer> inorderChar2Idx  = new HashMap<>();
        int idx = 0;
        for(char inChar : in) {
            inorderChar2Idx.put(inChar, idx++);
        }
        CharNode currentCharNode = doBuildBSearchTreeByInNPreOrderArray(inorderChar2Idx, pre, 0,0, in.length-1);
        return currentCharNode;
    }

    private CharNode doBuildBSearchTreeByInNPreOrderArray(Map<Character, Integer> inorderChar2Idx, char[] pre, int preOrderCurIdx, int inOrderStartIdx, int inOrderEndIdx) {
        CharNode curCharNode = new CharNode(pre[preOrderCurIdx]);
        int preOrderCurMappedInorderIdx = inorderChar2Idx.get(pre[preOrderCurIdx]);

        // find left 1st element in inorders
        for(int i = preOrderCurIdx+1; i < pre.length;i++) {
            int firstChildPreOrderCurMappedInorderIdxLft = inorderChar2Idx.get(pre[i]);
            if ( firstChildPreOrderCurMappedInorderIdxLft < preOrderCurMappedInorderIdx ) {
                //inOrderStartIdx must >= inOrderEndIdx
                CharNode leftChild = doBuildBSearchTreeByInNPreOrderArray(
                        inorderChar2Idx, pre, i, inOrderStartIdx, firstChildPreOrderCurMappedInorderIdxLft > inOrderStartIdx? firstChildPreOrderCurMappedInorderIdxLft-1: inOrderStartIdx);
                curCharNode.left = leftChild;
                break;
            }
        }
        for(int i = preOrderCurIdx+1; i < pre.length;i++) {
            int firstChildPreOrderCurMappedInorderIdxRt = inorderChar2Idx.get(pre[i]);
            if ( firstChildPreOrderCurMappedInorderIdxRt > preOrderCurMappedInorderIdx ) {
                //inOrderStartIdx must >= inOrderEndIdx
                CharNode rightChild = doBuildBSearchTreeByInNPreOrderArray(
                        inorderChar2Idx, pre, i, firstChildPreOrderCurMappedInorderIdxRt < inOrderEndIdx ? firstChildPreOrderCurMappedInorderIdxRt + 1: inOrderEndIdx, inOrderEndIdx);
                curCharNode.right = rightChild;
                break;
            }
        }
        return curCharNode;
    }

}
