import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class DynamicProgDrill {
    @Test
    public void testShortestDistStrings() {
        String str1 = "sunday";
        String str2 = "saturday";

        int step = shortestEditStepsA2B(str1, str2);
        System.out.println(step);
    }

    private int shortestEditStepsA2B(String str1, String str2) {

        return countStep(str1, str2, 0);
    }

    /**
     * https://www.geeksforgeeks.org/edit-distance-dp-5/
     *
     * Hehe, it is a brain concussion
     *
     * @param str1
     * @param str2
     * @param stepCount
     * @return
     */
    private int countStep(String str1, String str2, int stepCount) {
        if (str2.length()==0 || str1.length()==0)
            return stepCount;

        //eq
        if( str1.charAt(0)==str2.charAt(0))
            return countStep(str1.substring(1), str2.substring(1), stepCount);

        //remove
        int removeCt = countStep(str1.substring(1), str2.substring(0), ++stepCount);

        //insert
        int insertCt = countStep(str1.substring(1), str2.substring(0), ++stepCount);

        //default: replace
        int replaceCt = countStep(str1.substring(1), str2.substring(1), ++stepCount);

        int minPathCount = Math.min(Math.min(removeCt, insertCt), replaceCt);

        return minPathCount;
    }

    @Test
    public void testSubsetSum() {
        int set[] = { 3, 34, 4, 12, 5, 2 };
        int sum = 9;

        boolean existed = getSubsetSum(set, sum);
        System.out.println(existed);
    }

    private boolean getSubsetSum(int[] set, int sumRef) {
        // TODO: stack end check
//        if (set.length == 0)
//            return 0;

        if (set.length == 1 ) {
            if (sumRef == set[0])
                return true;
            else return false;
        }

        boolean sumWOHead = getSubsetSum(Arrays.copyOfRange(set, 1, set.length), sumRef);
        if (set[0] > sumRef)
            return sumWOHead;

        boolean sumWHeadTail = getSubsetSum(Arrays.copyOfRange(set, 1, set.length), sumRef-set[0]);

        if (sumWOHead || sumWHeadTail) {
            System.out.println(sumWOHead + "||" + sumWHeadTail);
        }
        return sumWOHead || sumWHeadTail;

    }

    @Test
    public void testLongestIncreaseSubseq() {
        int arr[] = { 10, 22, 9, 33, 21, 50, 41, 60 };

        longestIncreaseSubseq(arr);
    }

    /**
     * https://www.geeksforgeeks.org/longest-increasing-subsequence-dp-3/
     * @param arr
     */
    void longestIncreaseSubseq(int[] arr) {
        int startIdx =0;
        int endIdx= arr.length-1;
        int maxSize = 0;

        System.out.println(trackLongestLIS(startIdx, endIdx, arr, maxSize));
    }

    private int trackLongestLIS(int startIdx, int endIdx, int[] arr, int maxSize) {
        //TODO: exit condition
        if(startIdx == endIdx)
            return maxSize;

        int bufRtV = Integer.MAX_VALUE;
        int sizeCount = 1; //self as first one
        for(int i = endIdx; i >=0; i--) {
            if (arr[i] < bufRtV){
                bufRtV = arr[i];
                sizeCount += 1;
            }
        }

        maxSize = maxSize > sizeCount ? maxSize : sizeCount;

        return trackLongestLIS(++startIdx, endIdx, arr, maxSize);

    }

    @Test
    public void testLongestCommonSubsequence() {
        String str1 = "AGGTAB";
        String str2 = "GXTXAYB";
        String lcs = longestCommonSubsequence(str1, str2);
        System.out.println(lcs + ": Length of LCS is "
                + lcs.length());
    }


    class CommonSubseqRtn {
        int curIdx;
        String maxStr;

        public CommonSubseqRtn(int curIdx, String maxStr) {
            this.curIdx = curIdx;
            this.maxStr = maxStr;
        }
    }

    /**
     * https://www.geeksforgeeks.org/longest-common-subsequence-dp-4/
     * @param str1
     * @param str2
     * @return
     */
    private String longestCommonSubsequence(String str1, String str2) {
        String maxStr = "";
        int curIdxStr1 = 0;
        CommonSubseqRtn rtn = null;

        //Note: return str1 cursor will help skip those dup cur1 over-visit
        while(curIdxStr1 < str1.length()) {
            rtn = calibFirstMachedChar(curIdxStr1, 0, str1, str2);
            curIdxStr1 = rtn.curIdx;
        }


        return rtn.maxStr;

    }

    private CommonSubseqRtn calibFirstMachedChar(int str1CurIdx, int str2Idx, String str1, String str2) {
        // end stack
        StringBuilder sb = new StringBuilder();

        if (str1CurIdx>=str1.length()-1 && str2Idx>=str2.length()-1)
            return new CommonSubseqRtn(str1CurIdx, "");

        while(str2Idx < str2.length()) {
            if (str1.charAt(str1CurIdx) == str2.charAt(str2Idx)) {
                CommonSubseqRtn commonSubseqRtn = calibFirstMachedChar(str1CurIdx + 1, str2Idx + 1, str1, str2);
                sb.append(str1.charAt(str1CurIdx));
                if(commonSubseqRtn.maxStr != null)
                    sb.append(commonSubseqRtn.maxStr);
                return new CommonSubseqRtn(commonSubseqRtn.curIdx, sb.toString());
            }
            str2Idx++;
        }
        return new CommonSubseqRtn(str1CurIdx, sb.toString());
    }

    /**
     * - When recur apply for A/B scenario, Bit ops should be considered as well.
     * - With help of element-capacity matrix, complexity is NumberOfElement * numberical_capacity, while no need to repeatedly calculating.
     *
     * https://www.geeksforgeeks.org/partition-a-set-into-two-subsets-such-that-the-difference-of-subset-sums-is-minimum/
     */
    @Test
    public void testMinDiffBtwnTwoPartitions() {
        int arr[] = { 3, 1, 4, 2, 2, 1 };

        int delta = minDiffBtwnTwoPartitionsRecur(arr);
        System.out.println(delta);

        int deltaInDinary = minDiffBtwnTwoPartitionsBinaryOps(arr);
        System.out.println(deltaInDinary);

        int deltaWMatrix = minDiffBtwnTwoPartitionsWCache(arr);
        System.out.println(deltaWMatrix);
    }

    private int minDiffBtwnTwoPartitionsWCache(int[] arr) {
        int sum = Arrays.stream(arr).sum();
        int sumHalfPlus = sum/2 + 1;
        int[][] profitHalfSumMatrix = new int[arr.length][sum];
        int selectedBucketSum = Integer.MAX_VALUE;

        int lastRound = Integer.MIN_VALUE;
        Arrays.fill(profitHalfSumMatrix[0], 0);
        for(int idxPlusOne =1;idxPlusOne < arr.length; idxPlusOne++) {
            int rowElementValue = arr[idxPlusOne-1];
            profitHalfSumMatrix[idxPlusOne][rowElementValue] = rowElementValue;

            for(int sumCur = 0; sumCur < sum;sumCur++){
                if (profitHalfSumMatrix[idxPlusOne - 1][sumCur] != 0) {
                    int sumCurPlusRowElementVal = sumCur + rowElementValue;
                    profitHalfSumMatrix[idxPlusOne][sumCurPlusRowElementVal] = sumCurPlusRowElementVal; //TODO: boolean instead of dup val, or like Str append Idx?

                    // Check MIN( delta, provision +next_value)
                    if (lastRound != Integer.MIN_VALUE && sumCurPlusRowElementVal > sumHalfPlus) {
                        if(sumHalfPlus - lastRound < sumCurPlusRowElementVal - sumHalfPlus) {
                            selectedBucketSum = selectedBucketSum > lastRound ? lastRound : selectedBucketSum;
                        } else {
                            selectedBucketSum = selectedBucketSum > sumCurPlusRowElementVal ? sumCurPlusRowElementVal : selectedBucketSum;
                        }
                        break;
                    }
                    lastRound = sumCurPlusRowElementVal;
                }
            }
        }


        return Math.abs(sum - selectedBucketSum * 2); // (sum - selectedBucketSum) - selectedBucketSum

    }


    private int minDiffBtwnTwoPartitionsBinaryOps(int[] arr) {
        int permutationCount = 1 << arr.length;
        int minDelta = Integer.MAX_VALUE;
        int total = Arrays.stream(arr).sum();

        for(int i=0; i < permutationCount; i++) {
            int bucketSum = 0;
            for(int idxItr = 0; idxItr < arr.length; idxItr++) {
                int positiveIdx = i >> idxItr & 1;
                if (positiveIdx == 1)
                    bucketSum += arr[positiveIdx];
            }

            int delta = total - bucketSum -bucketSum;
            minDelta = minDelta > delta ? delta : minDelta;
        }

        return minDelta;
    }

    private int minDiffBtwnTwoPartitionsRecur(int[] arr) {
        int idxItr = 0;
        int sum = 0;

        for(int i =0; i < arr.length;i++) {
            sum += arr[i];
        }

        return stackTrackTwoPartitionDelta(sum, -1,  arr, idxItr, "");

    }

    private int stackTrackTwoPartitionDelta(int sum, int firstBucketSum, int[] arr, int idxItr, String firstIdxsStr4Debug) {
        idxItr+=1;

        if (idxItr == arr.length) {
            int delta = (sum - firstBucketSum) -firstBucketSum;
            return Math.abs(delta);
        }

        int sumOf1stBucket = stackTrackTwoPartitionDelta(sum, firstBucketSum + arr[idxItr],arr,idxItr, firstIdxsStr4Debug + Integer.toString(idxItr) + ":");
        int sumOf2ndBucket = stackTrackTwoPartitionDelta(sum, firstBucketSum, arr, idxItr, firstIdxsStr4Debug);

        return Math.min(sumOf1stBucket, sumOf2ndBucket);

    }

    @Test
    public void testKnapsack() {
        int profits[] = new int[] { 60, 100, 120 };
        int weights[] = new int[] { 10, 20, 30 };
        int weightMax = 50;

        int maxProfitonKnapsack = maxProfitonKnapsack(weightMax, profits, weights);
        System.out.println(maxProfitonKnapsack);

    }

    private int maxProfitonKnapsack(int weightMax, int[] profits, int[] weights) {

        //max can be readable for the right most matrix input ??
        int rtnMax = Integer.MIN_VALUE;

        int weightSum = Arrays.stream(weights).sum();

        int[][]  profitNWtSum2ProfitCum = new int[profits.length + 1][weightSum + 1];

        for (int idxPlus = 1; idxPlus < profits.length + 1; idxPlus++) {
            // single val assign Str and fill
            int rowPrimeWeight = weights[idxPlus - 1];
            profitNWtSum2ProfitCum[idxPlus][rowPrimeWeight] = profits[idxPlus-1];

            // filter W. weightMax
            if (rowPrimeWeight > weightMax)
                continue;

            // crawl n identify last row !null n calib sum
            for(int cumCur = 0; cumCur < weightSum; cumCur++) {
                if (profitNWtSum2ProfitCum[idxPlus-1][cumCur] != 0){
                    int sumLead = cumCur + rowPrimeWeight;
                    // filter W. weightMax
                    if(sumLead > weightMax)
                        break; // inner loop

                    profitNWtSum2ProfitCum[idxPlus][sumLead] = profitNWtSum2ProfitCum[idxPlus-1][cumCur] + profits[idxPlus-1];

                }
            }
        }

        int descIdx = weightMax;
        while(rtnMax == Integer.MIN_VALUE) {
            for (int idxPlus = 1; idxPlus < profits.length + 1; idxPlus++){
                if (profitNWtSum2ProfitCum[idxPlus][descIdx] != 0)
                    rtnMax = profitNWtSum2ProfitCum[idxPlus][descIdx];
            }
                descIdx--;
        }
        return rtnMax;
    }

    //TODO: https://www.geeksforgeeks.org/boolean-parenthesization-problem-dp-37/

}
