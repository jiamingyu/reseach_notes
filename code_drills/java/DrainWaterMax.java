import org.junit.jupiter.api.Test;

import java.util.*;

public class DrainWaterMax {
    //TODO: Practice 2 curs to define L/R for each bar:      https://www.geeksforgeeks.org/trapping-rain-water/

    @Test
    public void testContainerWMostWater() {
        int[] bars = new int[]{0,1,0,2,1,0,1,3,2,1,2,1};
        int holdVolumn = trapCalibByStack(bars);
        System.out.println(holdVolumn);
    }

    private int trapCalibByStack(int[] bars) {
        Stack<Integer> descendBarStack = new Stack<>();

        int curIdx = 1;
        descendBarStack.push(0);
        int totalDrains = 0;
        while(curIdx < bars.length) {
            if (bars[curIdx] < bars[descendBarStack.peek()]) {
                descendBarStack.push(curIdx);
                curIdx++;
                continue;
            }

            int leftHightIdx = Integer.MIN_VALUE;
            while(!descendBarStack.isEmpty() && bars[descendBarStack.peek()] < bars[curIdx]) {
                leftHightIdx = descendBarStack.pop();
            }
            descendBarStack.push(curIdx);

            if(leftHightIdx != Integer.MIN_VALUE)
                totalDrains += bars[leftHightIdx] * (curIdx - leftHightIdx );

            curIdx++;
        }

        int rightBankIdx = Integer.MIN_VALUE;
        int leftBankIdx = Integer.MIN_VALUE;

        if(!descendBarStack.isEmpty()) {
            rightBankIdx = descendBarStack.pop();
        }
        while(!descendBarStack.isEmpty()) {
            // right bar is lower side
            leftBankIdx = descendBarStack.pop();
            totalDrains += bars[rightBankIdx] * (rightBankIdx - leftBankIdx);
            rightBankIdx = leftBankIdx;

        }
        return totalDrains;
    }

}
