import org.junit.jupiter.api.Test;

import java.util.Stack;

/**
 * https://www.geeksforgeeks.org/largest-rectangular-area-in-a-histogram-using-stack/
 */
public class MaxRecByStack {
    @Test
    public void testMaxRecByStack() {

        int hist[] = { 6, 2, 5, 4, 5, 1, 6 };

        // 12
        System.out.println("Maximum area is "
                + maxRecByStack(hist));
    }

    private int maxRecByStack(int[] hist) {
        int maxArea = 0;
        Stack<Integer> ascIdxes = new Stack<>();

        for(int idx =0; idx < hist.length;idx++) {
            if (ascIdxes.isEmpty() || hist[ascIdxes.peek()] < hist[idx]) {
                ascIdxes.push(idx);
                continue;
            }

            // see lower bars
            while(!ascIdxes.isEmpty() && hist[ascIdxes.peek()] > hist[idx]) {
                int barHeight = hist[ascIdxes.pop()];
                //now peek() reflect position of left corner
                int leftMarginIdx = ascIdxes.isEmpty() ? -1:ascIdxes.peek();
                int width = idx - leftMarginIdx - 1;
                int areaLead = width * barHeight;
                maxArea = maxArea < areaLead ? areaLead : maxArea;
            }
            ascIdxes.push(idx);
        }

        return maxArea;
    }


}
