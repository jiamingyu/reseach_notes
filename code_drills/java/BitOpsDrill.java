import org.junit.jupiter.api.Test;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

public class BitOpsDrill {

    /**
     * https://www.geeksforgeeks.org/given-a-number-find-next-sparse-number/
     */
    @Test
    public void testSparseNumber() {
        nextGreaterSparseNum(6);
    }

    public int nextGreaterSparseNum(int n) {

        int r = n;
        System.out.println("Displaying number incrementally:");
        while(!isSparseNum(r))
            r++;
        return r;
    }

    boolean isSparseNum(int n) {
        System.out.println( n + " bit displaying: " +Integer.toString(n , 2));

        int lastOneIdx = -1;
        boolean isSparse = true;

        for(int i =0;i < 32;i++) {
            if ((n >> i & 1) == 1) {
                if ( lastOneIdx!=-1 && i == lastOneIdx + 1 ) {
                    isSparse = false; break;
                }
                lastOneIdx = i;
            }
        }
        return isSparse;
    }

    /**
     * https://www.geeksforgeeks.org/count-number-of-bits-to-be-flipped-to-convert-a-to-b/
     */
    @Test
    public void testCountA2BFlipped() {
        int count = countA2BFlipped(3, 6);
        System.out.println(count);
    }

    private int countA2BFlipped(int a, int b) {
        int count = 0;
        System.out.println(  Integer.toString(a, 2) + " vs " + Integer.toString(b, 2) );

        for(int i =0 ; i < 32; i++) {
            int aidx = a >> i & 1;
            int bidx = b >> i & 1;

            if ((aidx ^ bidx) ==1)
                count ++;


        }


        return count;
    }

    @Test
    void testSumBitDiffAmgPairs() {
        int[] arr = {1, 3, 5};
//        AtomicInteger atomicSum = sumBitDiffAmgPairs(arr, true);
        AtomicInteger atomicSum = sumBitDiffAmgPairs(arr, false);
        System.out.println(atomicSum.get());
    }

    AtomicInteger sumBitDiffAmgPairs(int[] arr, boolean countDup) {
        AtomicInteger sum = new AtomicInteger(0);

        // Dup version
        IntStream.range(0, arr.length).forEach( aidx -> {
            int dynbStartIdx = countDup ? 0 : aidx;

            IntStream.range(dynbStartIdx, arr.length).forEach(b -> {
                        countBitDiff(arr[aidx], arr[b], sum);
                    });
                }

        );
        return sum;
    }

    private void countBitDiff(int a, int b, AtomicInteger sum) {
        int x = a ^ b;
        int count = 0;
        while(x > 0) {
            count ++;
            x = x & (x-1);
        }
        sum.set(sum.get() + count );
    }

    @Test
    void testRotateBit() {
        int n = 13;
        int r = 2;
        System.out.println(" initial: "+Integer.toString(n, 2));

        // left
        int left = (n << r) | n >>> (32-r);
        System.out.println(Integer.toString(left, 2));

        //right
        // Note : https://www.geeksforgeeks.org/rotate-bits-of-an-integer/ given right rotate I don't agree, and test fail
        int chopped = n >>> r;
//        System.out.println(Integer.toString(chopped, 2)); //debug
        int toAddLeftChopped = (chopped << r) ^ n;
        int toAddLeftReady = toAddLeftChopped << r ;
        System.out.println(Integer.toString(toAddLeftReady | chopped, 2));


        // prove g4g code fail
        int test = (n >>> r) |n << (32 - r);
        System.out.println("g4g right rot : " + Integer.toString(test, 2));
    }

    //TODO:
    //https://www.geeksforgeeks.org/find-the-element-that-appears-once/
    //https://www.geeksforgeeks.org/find-the-element-that-appears-once/


}
