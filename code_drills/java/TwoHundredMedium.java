import org.junit.jupiter.api.Test;

import java.util.*;

public class TwoHundredMedium {

    /**
     * https://leetcode.com/problems/game-of-life/
     * https://leetcode.wang/leetcode-289-Game-of-Life.html
     */

    /**
     * This solution is tweaked based on post above. Here assign prime number to 2nd life status.
     * Then it has each value carry the information of prev_status + current_status + (if stale or new)
     */
    final int DEAD = 3;
    final int ALIVE = 5;

    final int ALIVE_TN_DEAD = 1 + DEAD;//4
    final int DEAD_TN_ALIVE = 0 + ALIVE;//5
    final int ALIVE_TN_ALIVE = 1 + ALIVE;//6
    final int DEAD_TN_DEAD = 0 + DEAD;//3
    @Test
    public void testGameOfLifeInPlace() { //O(n * n * 9)
        int[][] d = {{0,1,0},{0,0,1},{1,1,1},{0,0,0}};


        for(int r =0; r < d.length; r++) {
            for (int c = 0; c < d[0].length; c++) {
                calibInPlace(d, r, c);
            }
        }

        System.out.println(Arrays.deepToString(d));

        for(int r =0; r < d.length; r++) {
            for (int c = 0; c < d[0].length; c++) {
                switch(d[r][c]){
                    case ALIVE_TN_DEAD :
                    case DEAD_TN_DEAD:
                        System.out.print("0,");break;
                    case ALIVE_TN_ALIVE:
                    case DEAD_TN_ALIVE:
                        System.out.print("1,");break;
                }
            }
            System.out.println();

        }

    }
    private void calibInPlace(int[][] d, int r, int c) {

        int aliveNbrCount = 0;
        int r1 = r>0 ? r - 1 : r;
        int r2 = r < d.length-1 ? r+1 : r;

        int c1 = c > 0 ? c-1 : c;
        int c2 = c < d[0].length - 1 ? c+1 : c;

        for( int ri = r1; ri <= r2; ri++ ) {
            for (int ci = c1; ci <= c2; ci++) {
                if (ri==r && ci == c)
                    continue;
                aliveNbrCount += getOriginalStatusByDiffertiantePhase(d[ri][ci]);
            }
        }

        switch (getOriginalStatusByDiffertiantePhase(d[r][c])) {
            case 0: {
                if (aliveNbrCount == 3)
                    d[r][c] = DEAD_TN_ALIVE;
                else
                    d[r][c] = DEAD_TN_DEAD;
                break;
            }
            case 1 : {
                if (aliveNbrCount == 2 || aliveNbrCount == 3) { // + 3
                    d[r][c] = ALIVE_TN_ALIVE;
                } else {
                    d[r][c] = ALIVE_TN_DEAD;
                }
                break;
            }
        }
    }

    private int getOriginalStatusByDiffertiantePhase(int v) {
        switch (v) {
            case 0:
            case DEAD_TN_ALIVE :
            case DEAD_TN_DEAD:
                return 0;

            case 1:
            case ALIVE_TN_DEAD :
            case ALIVE_TN_ALIVE:
                return 1;
        }

        return Integer.MIN_VALUE;
    }

    @Test
    public void testGameOfLife() {
        /**
         * eight neighbors
         *

         *
         * border line cells count follow same
         */
        int[][] d = {{0,1,0},{0,0,1},{1,1,1},{0,0,0}};

        int[][] aliveNbrs = new int[d.length][d[0].length];
        int[][] dbuffer = new int[d.length][d[0].length];

        for(int r =0; r < d.length; r++) {
            for(int c =0; c < d[0].length; c++) {
                aliveNbrs[r][c] = countAlive(r, c, d);
            }
        }

        for(int r =0; r < d.length; r++) {
            for (int c = 0; c < d[0].length; c++) {
                if(d[r][c] == 1)
                    dbuffer[r][c] = calibNextStatus4Live(aliveNbrs, r, c);
                else
                    dbuffer[r][c] = calibNextStatus4Dead(aliveNbrs, r, c);
            }
        }

        System.out.println(Arrays.deepToString(dbuffer));
    }

    /**
     * Any dead cell with exactly three live neighbors becomes a live cell, as if by reproduction. ==3 + DEAD
     */
    private int calibNextStatus4Dead(int[][] aliveNbrs, int r, int c) {
        if (aliveNbrs[r][c] == 3)
            return 1;
        else
            return 0;
    }

    /**
     * Any live cell with fewer than two live neighbors dies, as if caused by under-population. <2
     * Any live cell with two or three live neighbors lives on to the next generation. == 2,3
     * Any live cell with more than three live neighbors dies, as if by over-population..  >3
     *
     *
     */
    private int calibNextStatus4Live(int[][] aliveNbrs, int r, int c) {
        switch (aliveNbrs[r][c] ) {
            case 2, 3 : return 1;
            default: return 0;
        }


    }

    private int countAlive(int r, int c, int[][] d) {
        int sum = 0;

        int r1 = r>0 ? r - 1 : r;
        int r2 = r < d.length-1 ? r+1 : r;

        int c1 = c > 0 ? c-1 : c;
        int c2 = c < d[0].length - 1 ? c+1 : c;

        for( int ri = r1; ri <= r2; ri++ ) {
            for (int ci = c1; ci <= c2; ci++) {
                if (ri==r && ci == c)
                    continue;
                sum += d[ri][ci];
            }
        }
        return sum;
    }

    @Test
    public void testFindSingleNumber(){
//        int[] d = {1,2,1,3,2,5};
//        int countFlag =1;

        int[] d = {1,2,1,3,2,5,-2};
        int countFlag =1;

        Arrays.stream(findIntWithSpecifiedCounts(d, countFlag)).forEach(v->System.out.println(v));

    }

    /**
     * Covered topics:
     *
     * https://leetcode.com/problems/single-number-iii/
     * https://leetcode.wang/leetcode-260-Single-NumberIII.html
     *
     * https://leetcode.wang/leetcode-287-Find-the-Duplicate-Number.html
     *
     * // Below can be covered following single-number3 above
     * https://leetcode.wang/leetcode-137-Single-NumberII.html
     *
     * https://leetcode.wang/leetcode-136-Single-Number.html
     *
     *
     * This solution handles negtive int by double `index reflected value` array by size
     * @param nums
     * @param specifiedCount
     * @return
     */
    public int[] findIntWithSpecifiedCounts(int[] nums, int specifiedCount) {

        int limitV = Arrays.stream(nums).reduce(0, (l, r) -> Math.max(l, Math.abs(r)) );
        int[] valueIndexedCount = new int[limitV * 2];

        for(int i=0; i< nums.length;i++) {
            if (nums[i] > 0)
                valueIndexedCount[nums[i]]= valueIndexedCount[nums[i]]==0 ? 1:valueIndexedCount[nums[i]] + 1;
            else
                valueIndexedCount[nums.length + nums[i]*(-1)]
                        = valueIndexedCount[nums.length + nums[i]*(-1)]==0 ? 1:valueIndexedCount[nums.length + nums[i]*(-1)] + 1;
        }

        List<Integer> r = new ArrayList();
        for(int i=0;i<valueIndexedCount.length/2;i++) {
            if (valueIndexedCount[i] == specifiedCount)
                r.add(i);
        }
        for(int i=valueIndexedCount.length/2; i< valueIndexedCount.length; i++) {
            if (valueIndexedCount[i] == specifiedCount)
                r.add((i- nums.length) * -1);
        }

        return r.stream().mapToInt(i->i).toArray();
    }




    /**
     * https://leetcode.wang/leetcode-287-Find-the-Duplicate-Number.html
     *
     * Curious why always meet and meet again always on the same position even there are 2 identical value position
     */
    @Test
    public void testFindDupNumberWFastSlowPointer() {
        int[] d = new int[]{4,2,3,2,6,1,5};

        int slow = d[0];
        int fast = d[d[0]];

        int ct = 0;
        while( slow != fast || ct++ <  d.length * 100 ) {
            slow = d[slow];
            fast = d[d[fast]];
        }
        System.out.println("Meet 1st at idx:" + slow);

        while( slow != fast || ct++ <  d.length * 100 ) {
            slow = d[slow];
            fast = d[d[fast]];
        }
        System.out.println("Meet 2nd at idx:" + slow);

    }


    /**
     * https://leetcode.wang/leetcode-279-Perfect-Squares.html
     *
     */
    @Test
    public void testMinCountSqrsSum() {//TODO: impl cache, result track
        int d = 12;

        System.out.println(minSqrCountSum(d));
    }

    private int minSqrCountSum(int d) {
        int ctMin = d;
        for(int a = 1; a * a < d; a++) {
            ctMin = Math.min(ctMin, minSqrCountSum(a));
        }
        return ctMin + 1;
    }


    /**
     https://leetcode.wang/leetcode-274-H-Index.html
     https://leetcode.wang/leetcode-275-H-IndexII.html
     */
    @Test
    public void testNIndexNum() {
        int[] d = new int[]{0, 1, 3, 5, 6};
        int idxInclude = d.length - 1;

        int[] cum = new int[d.length + 1];
        for(int i=0; i< d.length; i++) {
            if (d[i] < idxInclude) {
                cum[i] = 1;
            } else {
                cum[i] =  0;
                cum[d.length] += 1;
            }
        }

        if (cum[d.length] > idxInclude){
            System.out.println("Full house");
            System.exit(0);
        }

        int sumCum = cum[d.length];
        for(int i = idxInclude; i >= 0; i--) {
            sumCum += cum[i];
            if (sumCum > i) {
                System.out.println("H number is: " + (idxInclude-i+1));
                break;
            }

        }


    }

    /**
     * https://leetcode.wang/leetcode-264-Ugly-NumberII.html
     */
    @Test
    public void testGetNthUglyNumber() { // 2,3,5 composite only
        int cap = 100;
        int n = 6;

        boolean[] ug = new boolean[100];

        int x = cap / 2;
        int itr = 0;
        while (++itr < x) {

            if( itr * 3 < cap && !ug[itr * 3])
                ug[itr * 3] = true;
            if( itr * 5 < cap && !ug[itr * 5])
                ug[itr * 5] = true;
            if(!ug[itr * 2])
                ug[itr * 2] = true;
        }

        int count = 0;
        for(int i  =1 ; i < cap; i++) {
            if (ug[i])
                count++;
            if (count == n) {
                System.out.println( n + "th ugly number:" + i);
            }
        }



    }


    /**
     * https://leetcode.wang/leetcode-236-Lowest-Common-Ancestor-of-a-Binary-Tree.html
     */
    @Test
    public void testLowestAncestorBTree() {
        int[] d = {3,5,1,6,2,0,8,Integer.MIN_VALUE,Integer.MIN_VALUE,7,4};
//        int p = 5; int q = 1;
        int p = 7; int q = 4;
        TreeNode rootNode = polulateBFSOnArray(d);

        TreeNode lowestParent = findLowestParent(p, q, d, rootNode);
        System.out.println(lowestParent.val);

    }

    private TreeNode findLowestParent(int p, int q, int[] d, TreeNode rn) {
        if (rn == null || rn.val == p || rn.val == q) {
            return rn;
        }

        TreeNode tnl = findLowestParent(p, q, d, rn.left);
        TreeNode tnr = findLowestParent(p, q, d, rn.right);

        if (tnl == null && tnr == null) {
            System.out.println("None of value found under " + rn.val);
            return null;
        }

        if (tnl != null && tnr == null) {
            return tnl;
        } else if (tnl == null && tnr != null) {
            return tnr;
        } else
            return rn;
    }


    /**
     * https://leetcode.wang/leetcode-234-Palindrome-Linked-List.html
     *
     * Dev Note:
     * It took me quite a while to get solution, esp. for scenario like: (1, 2, 2, 6, 6, 2, 2, 1). When d[2] == d[1], it kick off pali match.
     * However, a[3] != a[0]. At this time shall we a) consider Not a Pali b) reconsider all visited as left side and keep trying further?
     * Unless end of string, it should be b).
     *
     * While Stack can be used to buffer visited for later matching, it calls List to store and dump mirrored right part to stack.
     */

    @Test
    public void testPalindrome4LinkedList() {
//        LinkedList<Integer> d = new LinkedList(Arrays.asList(1, 2, 2, 1));//even
//        LinkedList<Integer> d = new LinkedList(Arrays.asList(1, 2, 2, 6, 6, 2, 2, 1));// Tricky one proved needs 2 stacks to buffer
        LinkedList<Integer> d = new LinkedList(Arrays.asList(1, 2, 3, 2, 1));//odd

        //None Palid cases:
//        LinkedList<Integer> d = new LinkedList(Arrays.asList(1, 3, 2, 2, 1));//even


        ListIterator<Integer> itr = d.listIterator();
        Stack<Integer> s = new Stack<>();
        Stack<Integer> s2 = new Stack<>();

        int last2nd = Integer.MIN_VALUE;
        boolean flagPali = false;

        while(itr.hasNext()) {
            Integer v = itr.next();

            if (!s.isEmpty() && (v == s.peek() || v == last2nd)) {
                flagPali = true;

                // start possible palid
//                q.add(v);
                if (v == s.peek())
                    s2.add(s.pop()); //even overall 22
                else if ( s2.isEmpty() ){ //odd 212, & kick off only
                    s2.add(s.pop());
                    s2.add(s.pop());
                }

                // scenario: graceful exit on validated
                if (s.isEmpty() && !itr.hasNext()) {
                    System.out.println("Bravo ! Validated Pali.");
                    System.exit(8);
                } else
                    continue;
            }
            // end of build Pali

            if (flagPali) {

                flagPali = false;

                // q dump 2 stack first, and stack to stack buffer
                List<Integer> ls = new ArrayList<>();
                while(!s2.isEmpty()) {
                    Integer pop = s2.pop();
                    ls.add(pop);
                    s.add(pop);
                }
                s.addAll(ls);
            }
            if (!s.isEmpty()) {
                last2nd = s.peek();
            }
            s.add(v);
        }
        System.out.println(d + "verified NOT Plaidrom");
    }


    /**
     * https://leetcode.wang/leetcode-230-Kth-Smallest-Element-in-a-BST.html
     *
     * Here instead of using Morris traverse, just use stack
     * Some thinking after impl:
     * Stack based solution like this normally falls into the dilemma : Like this case, need to track both count cum and value at nth
     * It has to count on static var for count cum and identify specific rtn (like Int.MIN) to handle when deep down branch has nothing to return.
     *
     * You can see post solution just print out result at right stack to avoid the issue.
     *
     */
    @Test
    public void testKthSmallestInBST() {
        int[] d = new int[]{3, 1, 4, Integer.MIN_VALUE, 2}; //min rep null
        int k =3;

        TreeNode rn = polulateBFSOnArray(d);
        int count = 0;

        int r = findNthVByStack(rn, k);
        System.out.println(r);
    }

    static int nthCount = 0;
    private int findNthVByStack(TreeNode rn, int k) {
//        if (rn != null && nthCount == k)
//            return rn.val;

        // l or r
        if (rn.left != null) {
            int x = findNthVByStack(rn.left,  k);
            if(x != Integer.MIN_VALUE)
                return x;
        }

        nthCount++; //rn counted
        if (nthCount == k)
            return rn.val;

        if (rn.right != null) {
            int x = findNthVByStack(rn.right, k);
            // current node as plus one
            if(x != Integer.MIN_VALUE)
                return x;
        }

        // Now only as value node
        return Integer.MIN_VALUE;

    }


/**
 * https://leetcode.wang/leetcode-229-Majority-ElementII.html
 *
 * https://leetcode.wang/leetcode-169-Majority-Element.html   easy level
 *
 * Ref: https://en.wikipedia.org/wiki/Boyer%E2%80%93Moore_majority_vote_algorithm
 *
 * Note: Here we are talking about "majority" (not just mean more than half), in other words "not minority"
 */
//TODO: Impl & tweak the algorithm

    @Test
    public void testMajorityElement() {

    }

    /**
     * https://leetcode.wang/leetcode-228-Summary-Ranges.html
     */
    //This is the first approach which is not very happy and neat
    @Test
    public void testSummarizeRangeOfOrderedArray(){
        int[] d = new int[] {0, 1,2,4,5,7};
        summarizeRangeOfOrderedArray(d, 0, d.length-1);
    }

    private void summarizeRangeOfOrderedArray(int[] d, int s, int e) {
        if (s == e) {
            System.out.println("(" + s + ")");
            return;
        }

        if ( s > e )
            return;

        int l =  s + (e-s)/2;
        int r =  s + (e-s)/2;

        while(r < d.length-1) {
            if(d[r+1] - d[r] == 1) {
                r+=1;
            } else
                break;
        }

        while(l > 0) {
            if ( d[l] - d[l-1] == 1 ) {
                l-=1;
            } else
                break;
        }

        summarizeRangeOfOrderedArray(d, s, l);

        if( l !=  (e-s)/2 || r != (e-s)/2)
            System.out.println(l + " -> " + r);

        summarizeRangeOfOrderedArray(d, r+1, e);
    }

    /**
     * Note: above imp is always O(n) as it is crawling l/r one by one
     * below imp is using dyna program with O(lg(n)). Also display better
     * Nice rethink and retry after a good sleep !
     */
    @Test
    public void testSummarizeRangeOfOrderedArray2(){
//        int[] d = new int[] {0, 1,2,4,5,7};
        int[] d = new int[] {0,2,3 ,4,6,8,9};
        summarizeRangeOfOrderedArray2(d, 0, d.length-1);
    }

    private void summarizeRangeOfOrderedArray2(int[] d, int sidx, int eidx) {
        // others
        if (sidx == eidx){
            System.out.println(sidx);
            return;
        }
        if (eidx - sidx == 1){
            System.out.println(sidx + ", "+ eidx);
            return;
        }

        int cur = sidx + (eidx - sidx)/2;
        int status = 0;
        if (d[cur] - d[sidx] == cur - sidx){
            status = 1;
        }
        if (d[eidx] - d[cur+1] == eidx - cur - 1) {
            status = 2;
        }
        if (d[cur] - d[sidx] == cur - sidx && d[eidx] - d[cur+1] == eidx - cur - 1)
            status = 3;

        switch (status) {
            case 0: {
                summarizeRangeOfOrderedArray2(d, sidx, cur);
                summarizeRangeOfOrderedArray2(d, cur + 1, eidx);
                break;}
            case 1: { // left range
                if (sidx == cur)
                    System.out.println(sidx);
                else
                    System.out.println(sidx+"->"+ cur);
                summarizeRangeOfOrderedArray2(d, cur + 1, eidx);
                break;}
            case 2: {
                summarizeRangeOfOrderedArray2(d, sidx, cur);
                if (cur+1 == eidx)
                    System.out.println(eidx);
                else
                    System.out.println((cur+1) +"->"+ eidx);
                break;
            } //right range
            case 3:
                System.out.println(sidx +"->"+ eidx);
                break;

        }

    }


/**
 * https://leetcode.wang/leetcode-227-Basic-CalculatorII.html
 */

/**
 * https://leetcode.wang/leetcode-226-Invert-Binary-Tree.html
 */

    /**
     * https://leetcode.wang/leetcode-223-Rectangle-Area.html
     *
     *
     */
    @Test
    public void testCompute2RecArea(){
//        int ax1 = -3; int ay1 = 0; int ax2 = 3; int ay2 = 4; int bx1 = 0; int by1 = -1; int bx2 = 9; int by2 = 2;
        int ax1 = -2; int ay1 = -2; int ax2 = 2; int ay2 = 2; int bx1 = -2; int by1 = -2; int bx2 = 2; int by2 = 2;
        int r = computeArea( ax1,  ay1,  ax2,  ay2,  bx1,  by1,  bx2,  by2);
        System.out.println(r);
    }

    private int computeArea(int ax1, int ay1, int ax2, int ay2, int bx1, int by1, int bx2, int by2) {
        boolean ax2Ltbx1 =  bx1 - ax2 > 0;
        boolean ay2Ltby1 =  by1 - ay2 > 0;

        int s = (ax2-ax1) * (ay2 - ay1) + (bx2-bx1) * (by2 - by1);

        //none over-lap
        if (ax2Ltbx1 && ay2Ltby1){
            return s;
        } else {
            // Lower left should get larger
            int cx1 = Math.max(ax1, bx1);
            int cy1 = Math.max(ay1, by1);

            // Upper right should get smaller
            int cx2 = Math.min(ax2, bx2);
            int cy2 = Math.min(ay2, by2);

            int overlap = (cy2 - cy1) * (cx2 - cx1);

            return s - overlap;
        }

    }


/**
 * https://leetcode.wang/leetcode-222-Count-Complete-Tree-Nodes.html
 */

    /**
     * https://leetcode.wang/leetcode-221-Maximal-Square.html
     *
     * After reviewing all post, It comes 2 things in my view:
     * A. Must use info from processed position (regardless direction)
     * B. For square, data of p[r-1][c-1], combining rowContinueCount & colContinueCount of (r,c)
     * should give enough info on square of r,c.
     *
     */
    @Test
    public void testMaxSquare() {
        char[][] m = {
                {'1','0','1','0','0'},
                {'1','0','1','1','1'},
                {'1','1','1','1','1'},
                {'1','0','0','1','0'}
        };
        int[][] rowContinueCount = new int[m.length][m[0].length];
        int[][] colContinueCount = new int[m.length][m[0].length];
        int[][] s2 = new int[m.length][m[0].length];
        s2[0][0] = m[0][0] == '1' ? 1 : 0;
        s2[1][0] = m[0][0] == '1' ? 1 : 0;
        s2[0][1] = m[0][0] == '1' ? 1 : 0;

        int maxR = -1;
        int maxC = -1;
        int maxS = -1;

        for(int r = 0; r < m.length; r++) {
            for(int c =0 ; c < m[0].length; c++) {
                if (m[r][c] != '1'){
                    rowContinueCount[r][c] = 0;
                    colContinueCount[r][c] = 0;
                    s2[r][c] = 0;
                    continue;
                }

                rowContinueCount[r][c] = r == 0 ? 1 : rowContinueCount[r-1][c] + 1;
                colContinueCount[r][c] = c == 0 ? 1 : colContinueCount[r][c-1] + 1;

                if(r > 0 && c > 0
                        && rowContinueCount[r][c] !=0 && colContinueCount[r][c] != 0
                ) {
                    if (s2[r-1][c-1] > 0) {
                        int l = Math.min(rowContinueCount[r][c], colContinueCount[r][c]);
                        if(l >= s2[r-1][c-1] + 1 ){
                            s2[r][c] = s2[r-1][c-1] + 1;
                         }

                    } else {
                        s2[r][c] = 1;
                    }

                }

                if (s2[r][c] > maxS) {
                    maxR = r;
                    maxC = c;
                    maxS = s2[r][c];
                }

            }
        }

        System.out.println( "r:" + maxR + " c: " + maxC + " area: " + maxS*maxS);
    }

    /**
     * https://leetcode.wang/leetcode-220-Contains-DuplicateIII.html
     */
    @Test
    public void testContainsNearbyAlmostDuplicate() {
//        int[] d = {1,2,3,1}; int indexDiff = 3;int valueDiff = 0;
        int[] d = {1,5,9,1,5,9}; int indexDiff = 2;int valueDiff = 3;

        boolean r = containsNearbyAlmostDuplicate(d, indexDiff, valueDiff);
        System.out.println(r);
    }

    private boolean containsNearbyAlmostDuplicate(int[] d, int indexDiff, int valueDiff) {

        for(int i = 0; i< d.length - indexDiff; i++) {
            for(int j = i+1; j < i+indexDiff+1; j++) {
                if (valueDiff >= Math.abs(d[i] - d[j]))
                    return true;
            }
        }


        return false;
    }

    /**
     * https://leetcode.wang/leetcode-215-Kth-Largest-Element-in-an-Array.html
     */
    @Test
    public void testKthLargestOfArray() {
        int[] d = {3,2,1,5,6,4}; int kth = 2;
//        int[] d = {3,2,3,1,2,4,5,5,6}; int kth = 4;
        KStack stk = new KStack(d.length);

        for(int sd : d) {
            stk.add(sd);
        }

        System.out.println(stk.existsKth(kth));
    }

    class KStack { // default desending
        int[] d = null;

        public KStack(int m) {
            d = new int[m+1];
            Arrays.fill(d, Integer.MIN_VALUE);
        }

        /**
         *
         * @param x
         * @return
         * False if a) exists b)
         */
        public boolean add(int x) {
            boolean r = false;
            int lastIdx = d.length-1;
            if(x >= d[0]) {
                d[lastIdx] = d[0];
                d[0] = x;
            } else {
                d[lastIdx] = x;
            }

            for(int i =0 ;i < lastIdx-1; i++) {
                if (d[lastIdx] >= d[i+1] && d[lastIdx] <= d[i]){ //equals necessary to handle dup entry
                    r= true;
                    int buf = d[i+1];
                    d[i+1] = d[lastIdx];
                    d[lastIdx] = buf;
                }
            }
            // So far not clear [n+1]
            return r;
        }

        public int existsKth(int k) {
            if(k-1 < d.length-1 && d[k-1] != Integer.MIN_VALUE)
                return d[k-1];
            else
                return Integer.MIN_VALUE;
        }

        public boolean contains(int x) {
            return false;
        }

    }

    /**
     * 198 W.213, exercising dynamic prog W. cache data. 213 diff from 198 only in considering start/end index when recur structure well defined
     */

    /**
     * Warming up practice prior to 213
     * https://leetcode.com/problems/house-robber/
     */
    @Test
    public void testHouseRobWOCircle() {
//        int[] h = {1,2,3,1};
        int[] h = {2,7,9,3,1};

        int[] lastNMax = new int[h.length];

        System.out.println( maxUp(0, h, 0, h.length-1, lastNMax));
    }

    private int maxUp(int sum, int[] h, int s, int e, int[] lastNMax) {
        // exit
        if (s > e)
            return sum; // like idx + 2
        if (s==e)
            return sum + h[e];

        if (lastNMax[s] == 0) { // For debug purpose, put return separately
            int considered = maxUp(sum+h[s], h, s+2, e, lastNMax);
            int unConsidered = maxUp(sum, h, s+1, e, lastNMax);

            lastNMax[s] = Math.max(considered, unConsidered);
            return lastNMax[s];

        } else {
            return lastNMax[s];
        }
    }

/**
 * https://leetcode.wang/leetcode-213-House-RobberII.html
 */
@Test
public void testHouseRobWithCircle() {
//        int[] h = {1, 2,3};
//        int[] h = {2,3,2};
//        int[] h = {1,2,3,1};
    int[] h = {2,7,9,3,1}; //now 11 instead of 12 when none-circle

    int[] lastNMax = new int[h.length];

    System.out.println( maxUpWCircle(0, h, 0, h.length-1, lastNMax));
}


    private int maxUpWCircle(int sum, int[] h, int s, int e, int[] lastNMax) {
        // exit
        if (s > e)
            return sum; // like idx + 2
        if (s==e)
            return sum + h[e];

        if (lastNMax[s] == 0) { // For debug purpose, put return separately
            int considered = maxUpWCircle(sum+h[s], h, s+2, e-1, lastNMax);
            int unConsidered = maxUpWCircle(sum, h, s+1, e, lastNMax);

            lastNMax[s] = Math.max(considered, unConsidered);
            return lastNMax[s];

        } else {
            return lastNMax[s];
        }
    }

/**
 * https://leetcode.wang/leetcode-211-Add-And-Search-Word-Data-structure-design.html
 */

    /**
     * https://leetcode.wang/leetcode-209-Minimum-Size-Subarray-Sum.html
     *
     * Very smart solution when using sum array + binary search !!
     *
     */
    @Test
    public void testMinSubarraySum() {
        int[] d = {2,3,1,2,4,3};
        int t = 7;

        int l = minSubarraySum(d, t);
        System.out.println("min length is:  " + l);
    }

    private int minSubarraySum(int[] d, int t) {
        int r = d.length;

        // cum[]
        int[] cum = new int[d.length];
        int sumBuf = 0;
        for(int i=0; i < d.length; i++) {
            sumBuf += d[i];
            cum[i] = sumBuf;
        }

        // Btree search
        int minLengthBelongedIdx = -1;
        for(int i=0; i < d.length; i++) {
            int x = binarySearchAboveTargetVLength2(t, cum, i, i,d.length-1);
            if (x < r) {
                r = x; minLengthBelongedIdx = i;
                System.out.println("Updating: " + i + "::" + x);
            }
        }

        System.out.println("Final calib result Idx: " + minLengthBelongedIdx + " with Length " + r);

        return r;
    }

    private int binarySearchAboveTargetVLength2(int t, int[] cum, int i, int startIdx, int endIdx) {
        if (cum[endIdx] - cum[i] < t)
            return Integer.MAX_VALUE;

        if (startIdx==endIdx && cum[startIdx] - cum[i] >= t)
            return startIdx - i;

        int midIdx = (endIdx + startIdx) / 2;
        int midVal = cum[midIdx] - cum[i];

        if (midVal == t)
            return  midIdx - i + 1;

        if (midVal > t) { // explore first half
            return binarySearchAboveTargetVLength2(t, cum, i , startIdx, midIdx);
        } else {
            return binarySearchAboveTargetVLength2(t, cum, i, midIdx + 1, endIdx);
        }

    }

/**
 * https://leetcode.wang/leetcode-208-Implement-Trie-Prefix-Tree.html
 */




/**
 * https://leetcode.wang/leetcode-205-Isomorphic-Strings.html
 */
@Test
public void testIsomorphic() {
//    String s = "egg"; String t = "add";
    String s = "egd"; String t = "add";
    System.out.println( isIsomorphic(s, t) );

}


public boolean isIsomorphic(String s, String t) {
    if (s.length() != t.length())
        return false;

    int sct = 0;
    int tct = 0;
    Map<Character, Integer> c2IS = new HashMap<>();
    Map<Character, Integer> c2IT = new HashMap<>();

    for(int i=0 ; i<s.length();i++) {
        char cs = s.charAt(i);
        char ct = t.charAt(i);
        int si = -1; int ti = -1;
        if (c2IS.keySet().contains(cs))
            si = c2IS.get(cs);
        else {
            c2IS.put(cs, sct++);
            si = c2IS.get(cs);

        }

        if (c2IT.keySet().contains(cs))
            ti = c2IT.get(ct);
        else {
            c2IT.put(ct, tct++);
            ti = c2IT.get(ct);

        }
        if (si != ti)
            return false;
    }
    return true;
}


/**
 * https://leetcode.wang/leetcode-233-Number-of-Digit-One.html
 *
 * Note: This is hard level topic
 */


// https://leetcode.com/problems/number-of-digit-one/solutions/64382/JavaPython-one-pass-solution-easy-to-understand/
@Test
public void testCountOne() {
    countDigitOne(213);
}

public int countDigitOne(int n) {

    if (n <= 0) return 0;
    int q = n, x = 1, ans = 0;
    do {
        int digit = q % 10;
        q /= 10;
        ans += q * x;
        if (digit == 1) ans += n % x + 1;
        if (digit >  1) ans += x;
        x *= 10;
    } while (q > 0);
    return ans;

}



    /**
     * Util funcs shared in tests
     */
    // Util to read BSF BT from array
    @Test
    public void testPolulateBFSOnArray() {

//        int[] d = new int[]{1,2,3,4,5,6,7,8,9};
//        int[] d = new int[]{1,2,Integer.MIN_VALUE,4,5,6,7,8,9};
        int[] d = {3,5,1,6,2,0,8,Integer.MIN_VALUE,Integer.MIN_VALUE,7,4};

        TreeNode rootNode = polulateBFSOnArray(d);
        int x = 0;
    }

    private TreeNode polulateBFSOnArray(int[] d) {

        Queue<TreeNode> q = new LinkedList();

        int idx =0;
        TreeNode rootNode = new TreeNode(d[idx]);
        q.add(rootNode);

        boolean keepGoing = true;

        while(keepGoing) {
            TreeNode poll = q.poll();

            if (idx + 1 < d.length) {
                poll.left = d[++idx] == Integer.MIN_VALUE ? null : new TreeNode(d[idx]);
                if (poll.left != null)
                    q.add(poll.left);
            }
            if (idx + 1 < d.length) {
                poll.right = d[++idx] == Integer.MIN_VALUE ? null : new TreeNode((d[idx]));
                if (poll.right != null)
                    q.add(poll.right);
            }

            if (idx == d.length - 1)
                keepGoing = false;
        }
        return rootNode;
    }

}
