import java.util.*;

import org.junit.jupiter.api.Test;

public class BTreeTraversals {
    @Test
    public void testPostOrderTraverse() {
        TreeNode bt00 = new TreeNode(50, 25, 75);
//        bt00.left.left = new TreeNode(12,null,new TreeNode(18,15,22));
//        bt00.left.left = new TreeNode(12,new TreeNode(11, null, null), new TreeNode(18,15,22));
        TreeNode twentyTwoNode = new TreeNode(22, null,new TreeNode(23, null, null));

//        bt00.left.left = new TreeNode(12,new TreeNode(11, null, null), new TreeNode(18,15,22));
        bt00.left.left = new TreeNode(12,new TreeNode(11, null, null), new TreeNode(18,15,twentyTwoNode));


        bt00.left.right = new TreeNode(38, new TreeNode(32),new TreeNode(45));
        bt00.right.left = new TreeNode(62,56,68);

//        postorderTraversalGFGVersion(bt00);
        postorderTraversalSeparateLinkFrmCollect(bt00);
    }

    public List<Integer> postorderTraversalGFGVersion(TreeNode root) {
        List<Integer> r = new ArrayList<>();

        TreeNode cur = new TreeNode(Integer.MIN_VALUE, root, null);

        while (cur != null) {

            if (cur.left != null) {
                TreeNode leftChildRightEndRecon = cur.left;

                Stack<Integer> rtStack = new Stack<>();
                boolean keepCrawlRight = true;
                while(keepCrawlRight) {

                    TreeNode priorRecon = leftChildRightEndRecon;
                    rtStack.add(leftChildRightEndRecon.val);//root of possible right arm to be added

                    leftChildRightEndRecon = leftChildRightEndRecon.right;

                    // leftChildRightEndRecon @ end of right:
                    // A. connect right -> cur
                    // B. cur move to next left &&  START ALLOVER !!
                    if (leftChildRightEndRecon == null) {
                        rtStack.clear(); // yes, dump it
                        priorRecon.right = cur;
                        cur = cur.left;   //   <<<<<<<<<<<<<<<<
                        keepCrawlRight = false;
                    }

                    // Collection phase only, This is the 2nd time recon visit the above priorRecon node
                    if (leftChildRightEndRecon == cur) {
                        cur = cur.right; // >>>>>>>>>

                        while(!rtStack.isEmpty())
                            r.add(rtStack.pop());
                        keepCrawlRight = false;
                    }
                }
            }
            else {
                // Scenario 1: When traversing down & reach left end, try move right one step and work as root of sub-left
                // Scenario 2: This should be path to move cur upper above to its Leftmost parent when tring to traverse backabove
                cur = cur.right;  // >>>>>>>>>
            }
        }
        return r;
    }

    /**
     * Under Construction
     * @param root
     * @return
     */
    public List<Integer> postorderTraversalSeparateLinkFrmCollect(TreeNode root) {
        List<Integer> r = new ArrayList<>();

        TreeNode cur = new TreeNode(Integer.MIN_VALUE, root, null);
        leftChildRightArmEnd2ParentRecur(cur);

        //
        Stack<TreeNode> rightExpandRootNodeStack = new Stack<>();
        while (cur != null) {
            /**
             *  cur just passed pseudo-root node right child and point back to pseudo-node now. Will fall-back to left and open endless traverse
             *
             *    pseudo <-------
             *    /             | [added right ref of root_data]
             *    root_data ----
             *    /     \
             *    lft   rt
             *
             */
            if (cur.val == Integer.MIN_VALUE && !rightExpandRootNodeStack.isEmpty()) {
                // stack should now: pseudo, root, root_right_child
                while(rightExpandRootNodeStack.size() > 1) //collect all none-pseudo from stack
                    r.add(rightExpandRootNodeStack.pop().val);
                break; //Exit point
            }

            if (cur.left != null) {
                rightExpandRootNodeStack.add(cur); //curStack add Scenario 1.
                cur = cur.left;
                continue;
            }

            /**
             *       cur
             *     /    \
             *  null    other_nodes
             */
            Stack<Integer> localNoneRootNodeStack = new Stack<>();
            localNoneRootNodeStack.add(cur.val); // Now localNoneRootNodeStack added leftnodes. see "Collecting to result": B
            cur = cur.right;

            /**
             *                25      <-- [added ref], pointing to left side grand-parent
             *            12/   \??     |
             *          11/  \18        |
             *            15/  \22      |
             *                    \23   |
             */

            // right cur eventuall will point to left side grand-parent then stop
            while( !rightExpandRootNodeStack.contains(cur) ) {
                if (cur.left != null) {//sc3
                    rightExpandRootNodeStack.add(cur);  // start a new rightExpandRootNodeStack traverse
                    break;//go back to  `while (cur != null) { ...     `
                }
                // right child only, adding to local stack
                localNoneRootNodeStack.add(cur.val);
                cur = cur.right;
            }
            // Now cur should point to its left side grand-parent

            /**
             * Collecting to result splits into 2:
             */
            // A. Collect None-root Nodes on right end
            while(!localNoneRootNodeStack.isEmpty()) {
                r.add(localNoneRootNodeStack.pop());
            }
            // B. Collect rootNodes until cur pointed on stack (rest of those on right arm)
            // Notice: this also cover left child only scenario as it will be top of the rightExpandRootNodeStack
            while(rightExpandRootNodeStack.peek() != cur) {
                r.add(rightExpandRootNodeStack.pop().val);
            }
            // Ready to move to last root cur's right node
            cur = cur.right;
        }
        return r;
    }

    private void leftChildRightArmEnd2ParentRecur(TreeNode curOrigin) {

        TreeNode leftTraverseRootCur = curOrigin;
        while (leftTraverseRootCur.left != null) {
            TreeNode rightArmEndExplorer = leftTraverseRootCur.left;
            while(rightArmEndExplorer.right != null) {
                if(rightArmEndExplorer.right.left != null)
                    leftChildRightArmEnd2ParentRecur(rightArmEndExplorer.right);

                rightArmEndExplorer = rightArmEndExplorer.right;
            }
            // now leftChildRightEndRecon.right == null, link right arm to leftTraverseRootCur
            rightArmEndExplorer.right = leftTraverseRootCur;
            leftTraverseRootCur = leftTraverseRootCur.left;   //   <<<<<<<<<<<<<<<<
        }

    }

    /**
     * TODO: InOrder also good if just using Stack for leftnode
     *
     */


    /**
     * InOrder Traversal Morris
     *
     * https://www.cnblogs.com/AnnieKim/archive/2013/06/15/morristraversal.html
     *
     */
    @Test
    public void testInOrderTraverse() {
        TreeNode bt00 = new TreeNode(50, 25, 75);
//        bt00.left.left = new TreeNode(12,null,new TreeNode(18,15,22));
//        bt00.left.left = new TreeNode(12,new TreeNode(11, null, null), new TreeNode(18,15,22));
        TreeNode twentyTwoNode = new TreeNode(22, null, new TreeNode(23, null, null));

//        bt00.left.left = new TreeNode(12,new TreeNode(11, null, null), new TreeNode(18,15,22));
        bt00.left.left = new TreeNode(12, new TreeNode(11, null, null), new TreeNode(18, 15, twentyTwoNode));


        bt00.left.right = new TreeNode(38, new TreeNode(32), new TreeNode(45));
        bt00.right.left = new TreeNode(62, 56, 68);

        List<Integer> r = morrisTraversal(bt00);
    }

    /**
     * Mine
     * @param bt00
     * @return
     */
    private List<Integer> morrisTraversal(TreeNode bt00) {
        List<Integer> r = new ArrayList<>();

        TreeNode ex = new TreeNode(Integer.MIN_VALUE, bt00, null);
        TreeNode p = ex;

        while(p != null) {
            if (p.left != null) {
                TreeNode plc = p.left;

                while(plc.right != null && plc.right != p) {
                    plc = plc.right;
                }
                if (plc.right == null) {
                    plc.right = p; //plc.right==null   + morris link
                    p = p.left;//yes, p move to end node
                }
                else { // plc.right == p
                    r.add(p.val);
                    plc.right = null;

                    p= p.right;

                }
            } else {
                r.add(p.val);
                p = p.right; //when morris ref added, this will bring p back to its parent, and then it will detect circle
            }
        }



        return r;
    }

    @Test
    public void testPreOrderTraverse() {
        TreeNode bt00 = new TreeNode(50, 25, 75);
//        bt00.left.left = new TreeNode(12,null,new TreeNode(18,15,22));
//        bt00.left.left = new TreeNode(12,new TreeNode(11, null, null), new TreeNode(18,15,22));
        TreeNode twentyTwoNode = new TreeNode(22, null, new TreeNode(23, null, null));

//        bt00.left.left = new TreeNode(12,new TreeNode(11, null, null), new TreeNode(18,15,22));
        bt00.left.left = new TreeNode(12, new TreeNode(11, null, null), new TreeNode(18, 15, twentyTwoNode));


        bt00.left.right = new TreeNode(38, new TreeNode(32), new TreeNode(45));
        bt00.right.left = new TreeNode(62, 56, 68);

        List<Integer> r = morrisTraversalPreorder(bt00);
    }

    private List<Integer> morrisTraversalPreorder(TreeNode bt00) {
//        TreeNode ex = new TreeNode(Integer.MAX_VALUE, bt00, null);
        TreeNode p = bt00;
        List<Integer> r = new ArrayList<>();

        while(p != null) {
            if (p.left == null) {
                r.add(p.val);
                p = p.right;
            } else { // has left
                TreeNode lc =  p.left;
                while(lc.right != null && lc.right != p) {
                    lc = lc.right;
                }

                if (lc.right == null) {
                    r.add(p.val);
                    lc.right = p;
                    p = p.left;
                }else {// only left lc.right == p
                    p = p.right; // will null if p back to ex
                    lc.right = null;
                }

            }
        }
        return r;
    }


    /**
     * *********************************************************************************
     *
     * *********************************************************************************
     */

    /**
     * G4G
     */
    @Test
    public void testInOrderTraverseGG() {
        tNode bt00 = new tNode(50, 25, 75);
//        bt00.left.left = new TreeNode(12,null,new TreeNode(18,15,22));
//        bt00.left.left = new TreeNode(12,new TreeNode(11, null, null), new TreeNode(18,15,22));
        tNode twentyTwoNode = new tNode(22, null, new tNode(23, null, null));

//        bt00.left.left = new TreeNode(12,new TreeNode(11, null, null), new TreeNode(18,15,22));
        bt00.left.left = new tNode(12, new tNode(11, null, null), new tNode(18, 15, twentyTwoNode));


        bt00.left.right = new tNode(38, new tNode(32), new tNode(45));
        bt00.right.left = new tNode(62, 56, 68);

        MorrisTraversal(bt00);

    }

    void MorrisTraversal(tNode root)
    {
        tNode current, pre;

        if (root == null)
            return;

        current = root;
        while (current != null)
        {
            if (current.left == null)
            {
                System.out.print(current.data + " ");

                current = current.right;
            }
            else {
                /* Find the inorder
                    predecessor of current
                 */
                pre = current.left;
                while (pre.right != null
                        && pre.right != current)
                    pre = pre.right;

                /* Make current as right
                   child of its
                 * inorder predecessor */
                if (pre.right == null) {
                    pre.right = current;
                    current = current.left;
                }

                /* Revert the changes made
                   in the 'if' part
                   to restore the original
                   tree i.e., fix
                   the right child of predecessor*/
                else
                {
                    pre.right = null;
                    System.out.print(current.data + " ");
                    current = current.right;
                } /* End of if condition pre->right == NULL
                 */

            } /* End of if condition current->left == NULL*/

        } /* End of while */
    }


    class tNode {
        int data;
        tNode left, right;

        tNode(int item)
        {
            data = item;
            left = right = null;
        }

        public tNode(int val, tNode left, tNode right) {
            this.data = val;
            this.left = left;
            this.right = right;
        }

        public tNode(int val, int leftVal, int rightVal) {
            this.data = val;
            this.left = new tNode(leftVal, null, null);
            this.right = new tNode(rightVal, null, null);
        }

        public tNode(int i, int left, tNode rightNode) {
            this.data = i;
            this.left = new tNode(left, null, null);
            this.right = rightNode;
        }
    }


    /**
     * https://www.geeksforgeeks.org/morris-traversal-for-preorder/
     */

}
