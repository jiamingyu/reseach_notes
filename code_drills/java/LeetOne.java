import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.stream.Collectors;

public class LeetOne {
    /**
     * https://leetcode.com/problems/number-of-islands/description/
     */
    @Test
    public void testCountIslands() {
        char[][] grid = {
                {'1','1','1','1','0'},
                {'1','1','0','1','0'},
                {'1','1','0','0','0'},
                {'0','0','0','0','0'}
        };

//        char[][] grid = {
//                {'1', '1', '0', '0', '0'},
//                {'1', '1', '0', '0', '0'},
//                {'0', '0', '1', '0', '0'},
//                {'0', '0', '0', '1', '1'}
//        };

        boolean[][] visited = new boolean[4][5];

        int count = 0;
        for (int r = 0; r < 4; r++) {
            for (int c = 0; c < 5; c++) {
                if (visited[r][c] || grid[r][c] == '0')
                    continue;

                // Here must be a new island since spot of visited land has been flagged
                count++;
                exploreLandUnvisited(r, c, visited, grid);
            }
        }
        System.out.println(count);
    }

    private void exploreLandUnvisited(int r, int c, boolean[][] visited, char[][] grid) {
        if (r < 0 || r >= grid.length || c < 0 || c >= grid[0].length || visited[r][c])
            return;

        if (grid[r][c] == '1') {
            visited[r][c] = true;
            exploreLandUnvisited(r + 1, c, visited, grid);
            exploreLandUnvisited(r - 1, c, visited, grid);
            exploreLandUnvisited(r, c + 1, visited, grid);
            exploreLandUnvisited(r, c - 1, visited, grid);
        }

    }


    /**
     * https://leetcode.com/problems/3sum/description/
     *
     * This solution is derived following solutions of
     * coding question: https://leetcode.com/explore/interview/card/top-interview-questions-medium/111/dynamic-programming/809/
     */
    @Test
    public void testSumZeroDynaProgram() {
        int[] d = {-1, 0, 1, 2, -1, -4};
        //          0  1  2  3  4    5

        dynaSumZero(d, 0, new ArrayList<>(), 0);
    }

    private void dynaSumZero(int[] d, int headIdx, List<Integer> rIdxBuf, int sumbuf) {

        // rtn
        if (sumbuf == 0 && rIdxBuf.size() == 3){
            System.out.println(rIdxBuf);
            return;
        }
        if (headIdx > d.length -1)
            return;

        if (rIdxBuf.size() > 3)
            return;

        int sumbuf2 = sumbuf + d[headIdx];
        ArrayList<Integer> rIdxBuf2 = new ArrayList<>(rIdxBuf);
        rIdxBuf2.add(headIdx);

        dynaSumZero(d, headIdx+1, rIdxBuf2, sumbuf2); //head as part of r
        dynaSumZero(d, headIdx + 1, rIdxBuf, sumbuf); // head pass

    }

    /**
     * https://leetcode.com/problems/3sum/description/
     *
     * This is 2nd solution 1 week later when review the topic. However,it needs new Set allocations in memory for each stack
     */
    @Test
    public void testSumZeroW3NumbersDyna() {
        int[] d = {-1, 0, 1, 2, -1, -4};

        HashSet<Integer> ids = new HashSet<>();
        Set<Set<Integer>> solutions = new HashSet<>();
        sumSubArrayXclude(0, ids, d, solutions);
        solutions.forEach(ls -> System.out.println("index set is:  " + ls));
    }

    private void sumSubArrayXclude(int sum, Set<Integer> ids, int[] d, Set<Set<Integer>> idsLs) {
        if (sum == 0 && ids.size() == 3) {
            idsLs.add(ids);
            return;
        }

        if (ids.size() > 3)
            return;

        for(int i=0; i < d.length; i++) {
            if(ids.contains(i)) {
                continue;
            }
            Set<Integer> ids2 = new HashSet<>(ids); // Clone to new Set here
            ids2.add(i);
            sumSubArrayXclude(sum + d[i] * (-1), ids2, d, idsLs);
        }
    }

    /**
     * https://leetcode.com/explore/interview/card/top-interview-questions-medium/103/array-and-strings/777/
     */
    @Test
    public void testSetMtrxZeros() {
        int[][] matrix = {{1, 1, 1}, {1, 0, 1}, {1, 1, 1}};
        List<String> zeros = new ArrayList<>();

        for (int r = 0; r < matrix.length; r++) {
            for (int c = 0; c < matrix[0].length; c++) {
                if (matrix[r][c] == 1)
                    continue;

                zeros.add(r + "-" + c);

            }
        }

        for (String s : zeros) {
            String[] xy = s.split("-");
            for (int v = 0; v < matrix.length; v++) {
                matrix[v][Integer.parseInt(xy[1])] = 0;
                matrix[Integer.parseInt(xy[0])][v] = 0;
            }
        }

        int x = 0;

    }

    /**
     * 1 week later, I came to use visited[].
     */
    @Test
    public void testSetMtrxZerosMarkVisited() {
        int[][] m = {{1, 1, 1}, {1, 0, 1}, {1, 1, 1}};
//        int[][] m = {{1, 1, 1}, {1, 0, 1}, {1, 1, 0}};
        boolean[][] v = new boolean[3][3];

        for (int r = 0; r < m.length; r++) {
            for (int c = 0; c < m[0].length; c++) {
                if (m[r][c] == 1 || v[r][c])
                    continue;
                else {
                    Arrays.fill(v[r], true);
                    Arrays.fill(m[r], 0);

                    for(int i=0; i<3;i++) {
                        v[i][c] = true;
                        m[i][c] = 0;

                    }
                }
            }
        }
        System.out.println(Arrays.deepToString(m));
    }

    /**
     * If we ssign [0, 1] to new status as [3, 5] => [0+3, 0+5, 1 + 3, 1 + 5] (very generic methodology when thinking about track status without extra space)
     * Further research showing here we only care 1 -> 0, so only below will happen.
     * 1 -> 1+3
     * 0 -> 0 + 3
     * If we leave 0 untouched, we only care about units updated to 4
     */
    @Test
    public void testSetMtrxZerosMarkVisitedAsPrimaryNum() {
//        int[][] m = {{1, 1, 1}, {1, 0, 1}, {1, 1, 1}};
        int[][] m = {{1, 1, 1}, {1, 0, 1}, {1, 1, 0}};


        for (int r = 0; r < m.length; r++) {
            for (int c = 0; c < m[0].length; c++) {
                if (m[r][c] == 1 ||  m[r][c] == 4)
                    continue;

                for(int i=0; i < m[0].length;i++) {//col
                    if (m[r][i] == 1 )
                        m[r][i] += 3;
                }

                for(int i=0; i < m.length;i++)    //row
                    if (m[i][c] == 1 )
                        m[i][c] += 3;

            }
        }

        for (int r = 0; r < m.length; r++) {
            for (int c = 0; c < m[0].length; c++) {
                if (m[r][c] == 4)
                    m[r][c] = 0;

            }
        }
        System.out.println(Arrays.deepToString(m));
    }

    /**
     * https://leetcode.com/problems/group-anagrams/description/
     */

    @Test
    public void testAnagramGroupWPrimeNum() {
        String[] strs = {"eat", "tea", "tan", "ate", "nat", "bat"};

        List<Integer> primeNumbs = genPrimeNumAtWilling(2, 200);
        assert primeNumbs.size() >= 26;
        int arraySize = 26 * (primeNumbs.get(0) + primeNumbs.get(25))/2;
        Set<String>[] angms = new Set[arraySize];

        for(String s : strs) {
            char[] cs = s.toCharArray();
            int sum = 0;
            for(char c : cs) {
                int idx = c - 'a';
                Integer primeNum = primeNumbs.get(idx);
                sum += primeNum;
            }
            if (angms[sum] == null)
                angms[sum] = new HashSet<>();

            angms[sum].add(s);
        }

        for(int i = 0; i < arraySize; i++) {
            if (angms[i] != null) {
                System.out.println( i + "th one has:  " + angms[i]);
            }
        }


    }

    List<Integer> genPrimeNumAtWilling( int lowerLimit, int upperLimit) {
        assert lowerLimit > 1;

        boolean[] p = new boolean[upperLimit];
        Arrays.fill(p, true);
        p[0] = false; // hacky
        p[1] = false; // hacky

        for(int i =lowerLimit ; i * i < upperLimit; i++) {
            if (p[i]) {
                for(int j = i * i; j < upperLimit; j+=i){
                    p[j] = false;
                }
            }
        }

        List<Integer> r = new ArrayList<>();
        for(int i=0; i < upperLimit; i++) {
            if (p[i])
                r.add(i);
        }
        return r;
    }

    /**
     https://www.geeksforgeeks.org/java-program-for-sieve-of-eratosthenes/
     */
    @Test
    public void testGenPrimeNumAtWilling() {
        System.out.println ( genPrimeNumAtWilling(2, 100) );
    }

    /**
     * https://leetcode.com/explore/interview/card/top-interview-questions-medium/103/array-and-strings/779/
     *
     * Initial impl use Set to track Char only W. l/r itr handling both moving and other check. It brings lots of by-flow considerations in impl, turns out not good. Deleted
     */
    @Test
    public void testLongestUniqueCharSubstrMap() { // Impl 10/12/23
//        String s = "bbb";
//        String s = "abcabcbb";
//        String s  = "pwwkew";
        String s  = "pwkeabc";

        // assert all lower case
        int l = 0;
        int r = 0;
        char[] cs = s.toCharArray();
        Map<Character, Integer> c2Idx = new HashMap<>();
        c2Idx.put(cs[l], l);

        int maxL = -1;
        int maxR = -2;

        while (r < s.length() - 1) {// last one won't miss since it will be recon by "r+1"
            if (!c2Idx.keySet().contains(cs[r + 1])) {
                c2Idx.put(cs[++r], r);
                continue;
            }

            Integer dupCIdx = c2Idx.get(cs[r + 1]);
            if (r-l > maxR - maxL) {

                System.out.println("intermediate SCenarios:" + maxL + ": " + maxR + "=>" + (maxR - maxL + 1));
            } else if (r-l == maxR - maxL)
                System.out.println("EQ SCenarios:" + l + ": " + r + "=>" + (maxR - maxL + 1));

            // old states, compare with Max
            maxL= l;
            maxR = r;
            //now update l/r and mapped
            l = dupCIdx + 1; // should already tracked on c2Idx
            c2Idx.put(cs[++r], r);
        }

        if (l==r && maxR == maxL)
            System.out.println("Complete identical chars in String");
        else if (l==0 && r == cs.length -1){         // none dup ever
            maxL = l;
            maxR = r + 1;
            System.out.println("Whole string none dup:  "+ maxL + ": " + maxR + "=>" + (maxR - maxL + 1));
        }
        System.out.println("final SCenarios:" + maxL + ": " + maxR + "=>" + (maxR - maxL + 1));
    }

    /**
     * back to back impl comparing to solution above
     *
    rewrite w. light solution after review post on G4G
     */
    @Test
    public void testLongestUniqueCharSubstr2() {
//        String s = "abcabcbb";
        //          01234567
        String s  = "pwwkew";
        //           123456

        char[] cs = s.toCharArray();
        Map<Character, Integer> c2i = new HashMap<>();
        int l = 0; int r = 0;
        int llead=0;
//        int rlead=0;
        for(int i=0; i < s.length();i++) {
            if (c2i.containsKey(cs[i])){
                llead = c2i.get(cs[i]) + 1;
                if (i - llead > r-l) { // new max
                    //l update to l+1
                    l = llead;
                    r=i;
                    // update c->i
                }
            }
            // in any case as last step to update c->idx
            c2i.put(cs[i], i);
        }

        System.out.println(l + ":"  + r);
    }

    /**
     * https://leetcode.com/explore/interview/card/top-interview-questions-medium/103/array-and-strings/780/
     */
    @Test
    public void testLongestPalindrom() {
//        String s = "babad";
        String s = "ABABABA";
        //       A#B#A#B#A#B#A
        //       0123456789012
        char[] cs = new char[2 * s.length() - 1];

        int[] palnTrk = new int[2 * s.length() - 1];

        for (int i = 0; i < s.length(); i++) {
            cs[2 * i] = s.charAt(i);
            if (i != s.length() - 1)
                cs[2 * i + 1] = '#';
        }

        int max = 0;
        int maxI = -1;

        for (int i = 0; i < cs.length; i++) {
            int ml = calibPali4Idx(i, cs, palnTrk);
            if (max < ml) {
                max = ml;
                maxI = i;
            }
        }

        System.out.println(max + " & " + maxI);
    }

    private int calibPali4Idx(int i, char[] cs, int[] palnTrk) {
        if (cs[i] == '#')
            return -1;

        // start paln l that was filled by prior exploring
        int l = palnTrk[i] == 0 ? 0 : palnTrk[i];

        while (l + 1 + i < cs.length && i - l - 1 > 0) {
            if (cs[l + 1 + i] == cs[i - l - 1]) {
                l++;
            } else {
                break;
            }
        }
        palnTrk[i] = l;

        int lidx=0;
        while ( lidx < l) {
            if (palnTrk[i - lidx] > 0 && lidx + i < cs.length) {
                int lftPalnV = palnTrk[i - lidx];
                // Left paired Paln value could be too long when applied to right
                palnTrk[lidx + i] = lftPalnV + i < cs.length ? palnTrk[i - lidx] : cs.length - i;
            }
            lidx++;
        }

        return l;
    }

    /**
     * https://leetcode.com/explore/interview/card/top-interview-questions-medium/103/array-and-strings/781/
     */
    @Test
    public void testAscValCovarIdxTriplet() {
//        int[] d = {2,1,5,0,4,6};
        //         0 1 2 3 4 5

        int[] d = {2, 1, 3, 5, 0, 4,};
        //         0  1  2  3 4 5

        // Check smaller exists from left
        int[] minIdxs = new int[d.length];
        Arrays.fill(minIdxs, -1); // default -1 assuming no small left entry
        int minIdx = 0;
        for(int i= 0 ; i< d.length; i++) {

            if (d[minIdx] >= d[i]) {
                minIdx = i;
            } else
                minIdxs[i] = minIdx;
        }

        // Check larger exists and break if found minidxs[i] != 1
        int[] maxIdxs = new int[d.length];
        Arrays.fill(maxIdxs, -1);
        int maxIdx = d.length-1;
        for (int i = d.length-1; i> -1; i--) {
            if (d[maxIdx] <= d[i]){ // max idx is current idx, leave maxidxs[i] untouched
                maxIdx = i;
            } else {
                maxIdxs[i] = maxIdx;

                if (minIdxs[i] != -1) {
                    System.out.println(minIdxs[i] + ":" + i + ":" + maxIdxs[i]);
                    // Per requirement, it can break here since only ask for existing check

                }
            }
        }
    }

    /**
         * https://leetcode.com/explore/interview/card/top-interview-questions-medium/103/array-and-strings/4153/
         */
    @Test
    public void testCountNSay() {
        int lmt = 6;

        int count = 0;
        List<Integer> d = new ArrayList<>();//TODO: next to replace number
        d.add(1);

        while (++count <= lmt) {
            System.out.println(count + " (Debug) rounds To count and say  :" + d);

            List<Integer> d2 = readNSay(d);
            System.out.println(count + ": count debug :" + d2);
            d = d2;
        }
    }

    private List<Integer> readNSay(List<Integer> ds) {
        List<Integer> countDigitPairs = new ArrayList<>();

        Integer buf = ds.get(0);
        int count = 1;
        for( int i = 1; i < ds.size(); i++) {
            if (buf != ds.get(i)) {
                addCountDigitRecs(countDigitPairs, buf, count);
                count = 1;

            } else {
                count += 1;
            }
            buf = ds.get(i);
        }
        //last
        addCountDigitRecs(countDigitPairs, buf, count);
        return countDigitPairs;
    }

    private void addCountDigitRecs(List<Integer> countDigitPairs, Integer digitTarget, int count) {
        if (count < 10)
            countDigitPairs.add(count);
        else {
            Stack<Integer> s = new Stack<>();
            while(count > 0) {
                s.add( count % 10 );
                count = count / 10;
            }
            while(!s.isEmpty())
                countDigitPairs.add(s.pop());
        }

        countDigitPairs.add(digitTarget);
    }

    @Test
    public void testAddCountDigitRecs() {
        ArrayList<Integer> integers = new ArrayList<>();
        addCountDigitRecs( integers, 3, 12);
        System.out.println( integers );
    }

    @Test
    public void tt() { // to verify case with 10^n as count
        parseIntegerToDigitList(100);
    }

    private List<Integer>  parseIntegerToDigitList(int n) {
        List<Integer> countDigitPairs = new ArrayList<>();

        if (n < 10) {
            countDigitPairs.add(1);
            countDigitPairs.add(n);
            return countDigitPairs;
        }

        int bf = n;
        Stack<Integer> s = new Stack<>();
        while (bf / 10 != 0) {
            s.add(bf % 10);
            bf = bf / 10;
        }

        s.add(bf % 10);

        int countTarget = s.pop();
        int count=1;
        while (!s.isEmpty()) {
            assert s.peek() < 10;
            if (s.peek() == countTarget) {
                s.pop();
                count++;
            } else {
                countDigitPairs.add(count); countDigitPairs.add(countTarget);
                countTarget = s.pop();
                count = 1;
            }
            if (s.isEmpty()) {
                countDigitPairs.add(count); countDigitPairs.add(countTarget);
            }
        }

        return countDigitPairs;
    }

    // This is the 2nd solution 1 week after above.
    /**
     * Motivation: When using List structure, it is hard to handle the counted digit just equal to next count amount highest digit:
     * eg : ..2 followed by 21 of 3
     *
     * If using String/chars, that one will be easy
     */
    @Test
    public void testCountNSayChars() {
        int upperXclude = 5;
//        int upperXclude = 11;

        String r = "1";
        int count = 0;

        while(++count < upperXclude) {
            System.out.println(r);

            char[] cs = r.toCharArray();
            int ct =1;
            StringBuilder sb = new StringBuilder();
            for(int i =0 ;i < cs.length;i++) {
                if (i < cs.length-1 && cs[i] == cs[i+1]) {
                    ct++;
                    if (i == cs.length-2) { //i+1 reach end corner case
                        sb.append(ct).append(cs[i]);
                        break; // last 2 identical
                    }
                } else {
                    sb.append(ct).append(cs[i]);
                    ct = 1;
                }

            }
            r = sb.toString();

        }


    }

    // Back to back solution with right above.
    @Test
    public void testCountNSayChar() {
        int lmt = 6;

        int count = 0;
        String v = "1";
        while (++count <= lmt) {
            System.out.println(count + " (Debug) rounds To count and say  :" + v);

            v = countNSayString(v);
            System.out.println(count + ": count debug :" + v);
        }
    }

    private String countNSayString(String v) {
        char[] cs = v.toLowerCase().toCharArray();

        char lastC = 'a'; //start flag
        int count = 1;

        StringBuffer sb = new StringBuffer();

        for(char c : cs) {
            if (lastC == 'a'){
                lastC = c;
                continue;
            }


            if (lastC != c){
                sb.append(count); sb.append(lastC);
                count = 1;
            } else {
                count += 1;

            }
            lastC = c;
        }
        // end count
        sb.append(count); sb.append(lastC);


        return sb.toString();

    }


    /**
     * https://leetcode.com/explore/interview/card/top-interview-questions-medium/107/linked-list/783/
     *
     * This solution has covered scenarios of val > 9, which is beyond question constrain, and increases complexity
     *
     * Good to think when look@ constrains: begine val is 0, diff size of the 2 Linked List
     *
     */
    @Test
    public void testLinkedLstReverseOrderSum() {
//        LinkedList<Integer> l1= new LinkedList(Arrays.asList(2,4,3));
//        LinkedList<Integer> l2= new LinkedList(Arrays.asList(5,6,4));

        // last round sum > 9
//        LinkedList<Integer> l1= new LinkedList(Arrays.asList(2,4,8));
//        LinkedList<Integer> l2= new LinkedList(Arrays.asList(5,6,4));

//        LinkedList<Integer> l1= new LinkedList(Arrays.asList(2,4,3,6));
//        LinkedList<Integer> l2= new LinkedList(Arrays.asList(5,6,4));

        // last round sum > 9
//        LinkedList<Integer> l1= new LinkedList(Arrays.asList(2,4,3,9));
//        LinkedList<Integer> l2= new LinkedList(Arrays.asList(5,6,8));

        LinkedList<Integer> l1 = new LinkedList(Arrays.asList(5, 6, 8));
        LinkedList<Integer> l2 = new LinkedList(Arrays.asList(2, 4, 3, 9));

        ListIterator<Integer> it1 = l1.listIterator();
        ListIterator<Integer> it2 = l2.listIterator();


        LinkedList<Integer> r = new LinkedList<>();
        int digitCount = 0;

        int plus = 0;
        while (it1.hasNext() && it2.hasNext()) {
            int s = it1.next() + it2.next() + plus;
            if (s > 9) {
                r.add(s % 10);
                plus = 1;
            } else {
                r.add(s);
                plus = 0;
            }
            digitCount++;
        }

        if (l1.size() != l2.size()) {
            ListIterator<Integer> itx = l1.size() > l2.size() ? it1 : it2;
            while (itx.hasNext()) {
                int s = itx.next() + plus;
                if (s > 9) {
                    r.add(s % 10);
                    plus = 1;

                } else {
                    r.add(s);
                    plus = 0;
                }
            }
        }

        if (plus == 1) // last round is > 9
            r.add(1);

        System.out.println(r);
    }

    /**
     * https://leetcode.com/explore/interview/card/top-interview-questions-medium/107/linked-list/784/
     */
    @Test
    public void testOddEvenLLNodeRegroup() {
        ListNode second = new ListNode(2, new ListNode(3, new ListNode(4, new ListNode(5))));
        ListNode head = new ListNode(1, second);

        ListNode oddtail = head;
        ListNode eventail = second;

        while (eventail.next != null) {
            ListNode odd = eventail.next;
            eventail.next = odd.next;

            odd.next = oddtail.next;
            oddtail.next = odd;

            oddtail = oddtail.next;
            if (eventail.next != null)
                eventail = eventail.next;
            else
                break;
        }
        System.out.println(head);
    }

    public class ListNode {
        int val;
        ListNode next;

        ListNode() {
        }

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }

        public String toString() {
            StringBuffer sb = new StringBuffer();
            ListNode b = this;
            while (b.next != null) {
                sb.append(b.val + "||");
                b = b.next;
            }
            sb.append(b.val + "||");
            return sb.toString();

        }
    }

    /**
     * https://leetcode.com/explore/interview/card/top-interview-questions-medium/109/backtracking/797/
     * Map solution is overkill. grid access should become standard when coming into those questions.
     */
    @Test
    public void testWordSearch() {
        char[][] board = {{'A', 'B', 'C', 'E'}, {'S', 'F', 'C', 'S'}, {'A', 'D', 'E', 'E'}};
        String word = "SEE";

        System.out. println(existOnBoard(board, word));
    }

    public boolean existOnBoard(char[][] board, String w) {
        char[] cs = w.toCharArray();

        for (int r = 0; r < board.length; r++) {
            for (int c = 0; c < board[0].length; c++) {
                if (board[r][c] == cs[0]) {
                    boolean[][] v = new boolean[board.length][board[0].length];


                    if(checkExist2(v, board,r,c, w, 0))
                        return true;
                }
            }
        }



        return false;
    }

    private boolean checkExist2(boolean[][] v, char[][] board, int r, int c, String w, int i) {
        // exist t
        if (w.length() == i)
            return true;

        if (r > board.length -1  || c> board[0].length -1  || r < 0 || c < 0)
            return false;

        // char ==
        if (w.charAt(i) != board[r][c])
            return false;

        v[r][c] = true;
        boolean r1 = checkExist2(v, board,r,c+1, w, i+1);
        boolean r2 = checkExist2(v, board,r,c-1, w, i+1);
        boolean r3 = checkExist2(v, board,r+1,c, w, i+1);
        boolean r4 = checkExist2(v, board,r-1,c, w, i+1);

        boolean b = r1 || r2 || r3 || r4;
        if(!b)
            v[r][c] = false;

        return b;
    }


    /**
     * https://leetcode.com/explore/interview/card/top-interview-questions-medium/109/backtracking/797/
     * Word search with array only
     * Ref
     * https://www.geeksforgeeks.org/check-if-a-word-exists-in-a-grid-or-not/
     */
    @Test
    public void testWordSearchExists() {

        char[][] bd = {{'A', 'B', 'C', 'E'}, {'S', 'F', 'C', 'S'}, {'A', 'D', 'E', 'E'}};
        String word = "SEE";

        boolean[][] v = new boolean[bd.length][bd[0].length];
        for (int i = 0; i < v.length; i++) {
            Arrays.fill(v[i], false);
        }

        char[] cs = word.toCharArray();
        for (int r = 0; r < bd.length; r++) {
            for (int c = 0; c < bd[0].length; c++) {

                if (bd[r][c] == cs[0] && doMatch(0, cs, bd, r, c, v)) {
                    System.out.println("working : " + r + "-" + c);
                    break;
                }

            }
        }

    }


    private boolean doMatch(int i, char[] cs, char[][] bd, int r, int c, boolean[][] v) {
        // A: Success case & Exit
        if (i == cs.length) // complete match words when coming here
            return true;

        // B: False cases
        if (r < 0 || r > bd.length - 1 || c < 0 || c > bd[0].length - 1) //boundary
            return false;

        if (v[r][c]) //visited
            return false;

        // Yes this is a dup check for cs[0], but still ok for recur
        if (bd[r][c] != cs[i]) //char val
            return false;

        //C.Continue recur
        v[r][c] = true;
        // We need this structure for debugging
        // clone: necessary to block data noise on each other
        boolean a = doMatch(i + 1, cs, bd, r + 1, c, v.clone());
        boolean b = doMatch(i + 1, cs, bd, r - 1, c, v.clone());
        boolean f = doMatch(i + 1, cs, bd, r, c + 1, v.clone());
        boolean d = doMatch(i + 1, cs, bd, r, c - 1, v.clone());

        return a || b || f || d;

    }

    /**
     * https://leetcode.com/explore/interview/card/top-interview-questions-medium/110/sorting-and-searching/798/
     */
    @Test
    public void testColorsClusterAsc() {
        int[] b = {2, 0, 2, 1, 1, 0};

//        clusteringAscBubble(b); // O n!
        countingSortAsc(b); //O n

        Arrays.stream(b).forEach(x -> System.out.println(x));
        ;
    }

    private void countingSortAsc(int[] d) {
        int[] idxValCount = new int[d.length]; //constraint: here b[i] value must < b.length

        for (int i = 0; i < d.length; i++) {
            idxValCount[d[i]] += 1;
        }


        for (int i = 0; i < idxValCount.length; i++) {

            int startIdx = i == 0 ? 0 : idxValCount[i - 1]; // idxValCount[i - 1] has been cumulated in last loop
            idxValCount[i] = i == 0 ? idxValCount[i] : idxValCount[i - 1] + idxValCount[i];

            for (int j = startIdx; j < idxValCount[i]; j++)
                d[j] = i;
        }
    }


    /**
     * Similiar to above, how about count sorting chars?
     * <p>
     * Hint:  char has 8 bits (256)
     */
    @Test
    public void testCountSortChars() {
        char arr[] = {'g', 'e', 'e', 'k', 's', 'f', 'o',
                'r', 'g', 'e', 'e', 'k', 's'};
        countSortChars(arr);
        System.out.println(String.valueOf(arr));
    }

    private void countSortChars(char[] arr) {
        int cl = 1 << 8;
        int[] cct = new int[cl];
        int[] cum = new int[cl];
        for(int i=0; i< arr.length; i++) {
            int cint = arr[i];
            cct[cint]+=1;
        }


        for(int i=1; i < cct.length; i++) {
            cum[i] = cum[i-1] + cct[i];
        }

        // assert all lower char
        int startIdx = 0;
        int endIdxXld = 0;

        for(int i = 'a'; i<= 'z'; i++) {
            if (cum[i] == 0) {
                continue;
            }
            endIdxXld = cum[i];
            for(int j = startIdx; j < endIdxXld; j++) {
                arr[j] = (char)i;
            }
            startIdx = endIdxXld;
        }

        System.out.println(Arrays.toString(arr));

    }


    /**
     * https://leetcode.com/explore/interview/card/top-interview-questions-medium/110/sorting-and-searching/801/
     */
    @Test
    public void testFindPeaks() {
//        int[] d = {1,2,3,1};
        int[] d = {1,2,1,3,5,6,4};
        int l =0; int r = d.length;
        int mp = (r-l)/2;

        while(true) {
            if (d[mp-1] < d[mp] && d[mp] > d[mp+1]){
                System.out.println(mp);
                break;
            }
            if (d[mp]-1 > d[mp]) {
                mp = (mp-l)/2;
                r = mp;
            }
            if (d[mp] < d[mp] + 1) {
                mp = mp + (r - mp)/2;
                l = mp;
            }

        }
    }

    /**
     * https://leetcode.com/explore/interview/card/top-interview-questions-medium/110/sorting-and-searching/802/
     */
    @Test
    public void testSearch4Range() {
        int[] d = {5, 7, 7, 8, 8, 10};
        //         0 1 2 3 4 5
        int target = 8;

        int i = d.length / 2;

        // find i with d[i] = target
        boolean ok = true;
        while (ok) {
            if (d[i] < target) {
                i = (i + d.length) / 2;
            } else if (d[i] < target) {
                i = (i) / 2;
            } else {
                ok = false;
            }
        }

        // expand l/r from i for all idx value == target
        int l = 0;
        int r = 0;
        while ((i - l - 1 > 0 && d[i - l - 1] == target) || (i + r + 1 < d.length && d[i + r + 1] == target)) {
            if (i - l - 1 > 0 && d[i - l - 1] == target)
                l--;
            if (i + r + 1 < d.length && d[i + r + 1] == target)
                r++;
        }

        System.out.println(i - l);
        System.out.println(i + r);

    }

    /**
     * https://www.geeksforgeeks.org/minimum-halls-required-for-class-scheduling/
     */
    @Test
    public void testMinRooms4MeetingsConcurrent() {

        int s[] = {0, 1, 1};
        int f[] = {5, 2, 10};

        int[] time = new int[12];
        int maxM = 0;

        for (int i = 0; i < s.length; i++) {
            int it = s[i];
            while (it < f[i] + 1) {
                time[it] += 1;  // increment count within meeting[i] range
                maxM = maxM > time[it] ? maxM : time[it];
                it++;
            }

        }

        System.out.println(Arrays.toString(time));
        System.out.println(maxM);


    }

    @Test
    public void testMeetingsConcurrent() {  // Matrix way: meeting as X, clock time as Y
        int[] s = {1, 3, 0, 5, 8, 5};
        int[] f = {2, 4, 6, 7, 9, 9};

        boolean[][] m = new boolean[s.length][12];

        for (int i = 0; i < s.length; i++) {
            int start = s[i];
            int end = f[i];
            int it = start;
            while (it < end + 1) {
                m[i][it] = true;
                it++;
            }

        }

        Set<Set<Integer>> ms = new HashSet<>();
        for (int c = 0; c < 12; c++) {
            Set<Integer> meets = new HashSet<>();
            for (int r = 0; r < s.length; r++) {
                if (m[r][c]) {
                    meets.add(r + 1);
                }
            }
            if (!meets.isEmpty())
                ms.add(meets);
        }
        System.out.println(ms);
    }


    /**
     * ???
     */
    @Test
    public void testSearchMatrix() {
        int[][] matrix = {{1, 4, 7, 11, 15}, {2, 5, 8, 12, 19}, {3, 6, 9, 16, 22}, {10, 13, 14, 17, 24}, {18, 21, 23, 26, 30}}; //false
        int target = 20;//false
//        int target = 23;//true

        System.out.println(searchMatrix(matrix, target));
    }

    public boolean searchMatrix(int[][] m, int target) {
        int l = -2;
        int h = m.length;
        for (int i = 0; i < m.length; i++) {
            if (target > m[i][i] && l < i - 1)
                l = i - 1; //Note: initial 'i' instead of 'i-1' leads to bug for 23
            if (target < m[i][i]) {
                h = i;
            }
        }

        if (l == h) {
            System.out.println(m[l][l]);
            return false;
        } else {
            for (int r = l; r < h + 1; r++) {
                for (int c = l; c < h + 1; c++) {
                    System.out.println(m[r][c]);
                    if (target == m[r][c])
                        return true;
                }
            }

        }
        return false;
    }

    /**
     * https://leetcode.com/explore/interview/card/top-interview-questions-medium/109/backtracking/793/
     * ref: (Q or 2 Stack swap as Q)
     * https://www.geeksforgeeks.org/iterative-letter-combinations-of-a-phone-number/
     */
    @Test
    public void testWordsBehindPhoneNumber() {
        String digits = "23";
        String[] table = {"0", "1", "abc", "def", "ghi",
                "jkl", "mno", "pqrs", "tuv", "wxyz"};

        List<String> words = wordsBehindPhoneNumber(digits, table);
        System.out.println(words);
    }

    private List<String> wordsBehindPhoneNumber(String digits, String[] table) {
        char[] ds = digits.toCharArray();
        Stack<String> a = new Stack<>();
        a.add("");//flag start point
        Stack<String> b = new Stack<>();

        for (char d : ds) { //digit ch -> int
            int di = Integer.parseInt(Character.toString(d));

            // idxed String of table -> c[]
            String s = table[di]; //key's c
            char[] css = s.toCharArray();

            Stack<String> stk = a.isEmpty() ? b : a;
            Stack<String> stk2 = a.isEmpty() ? a : b;
            while (!stk.isEmpty()) {
                String pop = stk.pop();
                // stack itrs swap for each css
                for (char singc : css) {
                    stk2.add(pop + singc);
                }

            }
        }
        return a.isEmpty() ? b : a;
    }

    /**
     * Solution 2.
     *
     * Recur avoid headach of swap btwn 2 stacks
     */
    @Test
    public void testWordsBehindPhoneNumberRecur() {
        String digits = "23";
        String[] table = {"0", "1", "abc", "def", "ghi",
                "jkl", "mno", "pqrs", "tuv", "wxyz"};

        Stack<String> strs = new Stack<>();
        strs.add(""); // seed to guarantee generating in recur
        Stack<String> words = wordsBehindPhoneNumberRecur(digits, table, strs);
        System.out.println(words);
    }

    private Stack<String> wordsBehindPhoneNumberRecur(String digits, String[] table, Stack<String> stk) {

        if (digits.isEmpty())
            return stk;

        int i = Integer.parseInt(Character.toString(digits.toCharArray()[0]));
        String strs = table[i];
        char[] cs = strs.toCharArray();

        Stack<String> alt = new Stack<>();

        while (!stk.isEmpty()) {
            String pop = stk.pop();
            for (char cc : cs)
                alt.add(pop + cc);
        }
        return wordsBehindPhoneNumberRecur(digits.substring(1), table, alt);
    }

    /**
     * https://leetcode.com/explore/interview/card/top-interview-questions-medium/109/backtracking/794/
     */
    @Test
    public void testParenthesisCombGivenLimitIncludeDFS() {
        int limitInclude = 3;

        printParenthesisCombGivenLimitIncludeDFS(0, 0, limitInclude, "1");
//        printParenthesisCombGivenLimitIncludeDFS(1, 0, limitInclude, "(");//working too

    }

    private void printParenthesisCombGivenLimitIncludeDFS(int lc, int rc, int limitInclude, String s) {
        // exit
        if (lc==limitInclude && rc== limitInclude) {
            System.out.println(s);
            return; //IMPORTNAT!!
        }
        // dfs : (
        if (lc < limitInclude){
//            lc += 1; // This is a trap: pass by value concept + value at this level left untouched
            String s2 = s +"(";
            printParenthesisCombGivenLimitIncludeDFS(lc + 1, rc, limitInclude, s2);

        }

        if (rc < lc) {
//            rc += 1;
            String s2 = s +")";
            printParenthesisCombGivenLimitIncludeDFS(lc, rc + 1, limitInclude, s2);
        }
    }

    @Test
    public void testParenthesisCombGivenLimit() {
        int limit = 3;
        Set<String> ss = new HashSet<>(); // think about why Set instead of Stack or Q
        ss.add("");
        Set<String> rs = parenthesisCombGivenLimitRecur(limit, ss);
        System.out.println(rs);

    }

    private Set<String> parenthesisCombGivenLimitRecur(int ct, Set<String> sts) {
        if (ct == 0)
            return sts;

        Set<String> sts2 = new HashSet<>();
        for (String s : sts) {
            sts2.add("()" + s);
            sts2.add(s + "()");
            sts2.add("(" + s + ")");
        }
        return parenthesisCombGivenLimitRecur(--ct, sts2);

    }


    /**
     * [()(), ()(), (()), ()(), ()(), (()), ()(), ()(), (())]
     *
     * Wrong impl
     *
     */
    @Test
    public void testParenthesisCombGivenLimitStack() {
        int limitXlude = 3;
        Stack<String> ss = new Stack<>();
        ss.add("");
        Stack<String> rs = parenthesisCombGivenLimitRecurStack(limitXlude, ss, 0);
        System.out.println(rs);

    }

    private Stack<String> parenthesisCombGivenLimitRecurStack(int limitXclude, Stack<String> ss, int stackIdx) {
        if (++stackIdx == limitXclude )
            return ss;

        Stack<String> b = new Stack<>();
        while(!ss.isEmpty()){
            String pop = ss.pop();
            b.add("()" + pop);
            b.add( pop + "()" );
            b.add("(" + pop + ")");
        }

        return parenthesisCombGivenLimitRecurStack(limitXclude, b, stackIdx);
    }


    /**
     * https://leetcode.com/explore/interview/card/top-interview-questions-medium/109/backtracking/795/
     * https://leetcode.com/explore/interview/card/top-interview-questions-medium/109/backtracking/796/
     * <p>
     *                  Permutation                Subset
     * size         fixed, full size            0 -> fullsize
     * order matter     yes                     no(bit op works)
     */

    @Test
    public void testPermutate() {
//        int[] d = {1, 2, 3};
        int[] d = new int[3];
        Arrays.setAll(d, x-> x);
//        int[] d = {0, 1, 2};
        ArrayList<int[]> ints = new ArrayList<>();
        permuteCol(d, 0, ints);
        ints.stream().forEach(xx -> System.out.println(Arrays.toString(xx)));
    }

    private void permuteCol(int[] d, int i, List<int[]> r) {
        if (i == d.length) { //control flow makes this 1st & $length instead of $length-1
            r.add(d.clone()); //clone is necessary
            return;

        }

        /**
         * Total Permutation of i leave position unchanged (i == j) + Total Permutation of i swapped to other positions
         */
        for (int j = i; j < d.length; j++) {
//        for (int j = i + 1; j < d.length; j++) { //Note: think about why not this, ref comments above
            swapValsOfIndexes(j, i, d);
            permuteCol(d, i + 1, r); // here should be i+1 instead of j+1(wrong), because it leave further solution for subcollection without head element.
            swapValsOfIndexes(j, i, d); // swap back to keep original
        }
    }

    private void swapValsOfIndexes(int j, int i, int[] d) {
        int bf = d[j];
        d[j] = d[i];
        d[i] = bf;
    }

    @Test
    public void testSubSet() {
        int[] d = new int[]{7, 8, 9};
        int x = 1 << d.length;

        for (int i = 0; i < x; i++) {
            System.out.println("***********");

            for (int s = 0; s < d.length; s++) {
                if ((i >> s & 1) == 1)
                    System.out.print(d[s] + ",");
            }
        }

    }


    /**
     * https://leetcode.com/explore/interview/card/top-interview-questions-medium/111/dynamic-programming/807/
     */
    @Test
    public void testJumpGame() {
//        int[] d = new int[]{2,3,1,1,4};
        int[] d = new int[]{3, 2, 1, 0, 4};

        int extra = 0;
        for (int i = d.length - 1; i >= 0; i--) {
            if (d[i] == 0) {
                extra += 1;
                continue;
            }
            if (d.length - i + extra < d[i])
                extra = 0;

        }
        System.out.println(extra);

    }

    /**
     * https://leetcode.com/explore/interview/card/top-interview-questions-medium/111/dynamic-programming/808/
     *
     * Attn: the question asks for unique path, not only efficient path. However, the solution sample given is
     * to use efficient way. So the solution give below should be in align with what it asks for at this time.
     */
    @Test
    public void testUnqPath() {
//        int rlmt = 3; int clmt = 2;//limit is size of row/col
        int rlmt = 3;
        int clmt = 7;//limit is size of row/col
        CountHolder countHolder = new CountHolder();

        findLowerRight(0, 0, "", rlmt, clmt, countHolder);
        System.out.println("Total path count:" + countHolder.count);
    }

//    static int countX = 0;
    // PlaceHolder to replace static var, pass by ref feature of function call
    class CountHolder {
        int count = 0;
    }

    private void findLowerRight(int r, int c, String s, int rlmt, int clmt, CountHolder countHolder) {
        if (r == rlmt - 1 && c == clmt - 1) {
            System.out.println(s);
             countHolder.count += 1;
            return;
        }

        //May ask: Why Not considering visited???: Here each stack we clearly split the right/down approach, therefore no need to track visited
        if (r < rlmt - 1) {
            findLowerRight(r + 1, c, s + " down, ", rlmt, clmt, countHolder);
        }
        if (c < clmt - 1) {
            findLowerRight(r, c + 1, s + " right, ", rlmt, clmt, countHolder);
        }
    }

    /**
     * https://leetcode.com/explore/interview/card/top-interview-questions-medium/111/dynamic-programming/809/
     * <p>
     * Note: This is diff from comb of coins for amount using DP listed right below
     *
     * Oct 6, 23 rethinking: Below solution working because there is 1 cent coin to fill the last gap.
     * Or we can say last 2 value is one odd one even that is possbile to fill the gap.
     */
    //TODO: challenging scenario: if min val is not 1 & 2, but like 5, 7 (Meaning no tools to fill in last 1 or 2), what will be the solutions?

    @Test
    public void testFewestCoinChanges() {
//        int[] coins = new int[] {1,2,5}; //asc
        int[] coins = new int[]{1, 2, 3}; //asc
        int amount = 11;
        int cv = amount * 100;
        int count = 0;

        for (int i = coins.length - 1; i >= 0; i--) {
            count += cv / coins[i];
            cv = cv % coins[i];
        }

        System.out.println(count);

    }

    /**
     * https://www.geeksforgeeks.org/coin-change-dp-7/
     */
    // This using   coin[n][sum] matrix
    @Test
    public void testCoinChange5() {
        int n = 3;
        int sum = 5;
        int[] coins = { 1, 2, 3 };
        int[][] sumIdxCache = new int[sum + 1][n];
        for(int[] x : sumIdxCache) {
            Arrays.fill(x, -1);
        }

        int count = countCoinChangeWSumIdxMatrix(sum, coins, n-1, sumIdxCache);
        System.out.println(count);
    }

    private int countCoinChangeWSumIdxMatrix(int sum, int[] coins, int indexesConsidered, int[][] sumIdxCache) {
        if (indexesConsidered < 0)
            return 0;

        //
        if (sum == 0) // valid case, contribute 1 count
            return 1;

        if (sum < 0 )
            return 0;

        // now only sum > 0 for futhrer calibrations
        if (sumIdxCache[sum][indexesConsidered] != -1) {
            return sumIdxCache[sum][indexesConsidered];
        }

        int c = countCoinChangeWSumIdxMatrix(sum - coins[indexesConsidered], coins, indexesConsidered, sumIdxCache) // take one & still consider as another one to be considered
        + countCoinChangeWSumIdxMatrix(sum, coins, indexesConsidered -1, sumIdxCache); //excluded completely

        sumIdxCache[sum][indexesConsidered] = c;

        return c;
    }


    @Test
    public void testCoinChange() {
        int[] coins = {1, 2, 5};
        int ttl = 11;
        
        int count = coinChange(coins, ttl, 0, 0);

    }

    private int coinChange(int[] coins, int ttl , int subttl, int cct) {
        int headCoinCt = 1;
        while( headCoinCt * coins[0] + subttl <= ttl ){
            int[] subCoins = Arrays.copyOfRange(coins, 1, coins.length);
            coinChange(subCoins, ttl, subttl + coins[0], ++cct);
            headCoinCt += 1;
        }

        return 1;
    }


    @Test
    public void testCoinChangeDyna() {
        int[] coins = {1, 2, 5};
        int ttl = 11;

        int solutionCounts = findCoinChange(coins, ttl, "");
        System.out.println("Total solutions: " + solutionCounts);
    }

    private int findCoinChange(int[] coins, int ttl, String s) {
        // good
        if (ttl == 0) {
            System.out.println(s);
            return 1;
        }

        // xption
        if (coins.length == 0)
            return 0;

        if (ttl < 0)
            return 0;


        int s1 = findCoinChange(coins, ttl - coins[0], s  + coins[0]+ ", ");//consider head
        int s2 = findCoinChange(Arrays.copyOfRange(coins, 1, coins.length), ttl, s);//not consider

        return s1 + s2;
    }

    /**
     * https://leetcode.com/explore/interview/card/top-interview-questions-medium/111/dynamic-programming/810/
     *
     * Notice: it is sequence, not continuous seq. In other words, just order matters
     */
    @Test
    public void testLongestAscConseq() { // Nov 2023, sometimes go out for a cup of coffee & rework will make sense
        int[] d = new int[]{10,9,2,5,3,7,101,18};//4

        Map<Integer, List<Integer>> resultCache = new HashMap<Integer, List<Integer>>();
        ArrayList<Integer> ascIdxes = new ArrayList<>(); ascIdxes.add(0);
        int maxCount = calibAscConseq2(resultCache, d, ascIdxes, 1);
        System.out.println("count:" + maxCount);

    }

    private int calibAscConseq2(Map<Integer, List<Integer>> resultCache, int[] d, List<Integer> ascIdxes, int headIdx) {
        if (headIdx == d.length) {
            if (!resultCache.containsKey(ascIdxes.get(0))) {
                System.out.println("candidate collection: " + ascIdxes);
                resultCache.put(ascIdxes.get(0), ascIdxes);
            }

            return ascIdxes.size();
        }

        int count = -1;

        if ( d[headIdx] > d[ascIdxes.get(ascIdxes.size()-1)] ) { // head is ascending, included and move on
            ascIdxes.add(headIdx);
            count = calibAscConseq2(resultCache, d, ascIdxes, headIdx + 1);
        } else { // head is smaller, create new track taking head as smallest/1st element, comparing with skipping head approach
            List<Integer> newAscIdxes = new ArrayList<Integer>();
            newAscIdxes.add(headIdx);
            int countAsNew = resultCache.containsKey(headIdx) ? resultCache.get(headIdx).size() : calibAscConseq2(resultCache, d, newAscIdxes, headIdx + 1);
            int countSkipHead = resultCache.containsKey(ascIdxes.get(0)) ? resultCache.get(ascIdxes.get(0)).size() : calibAscConseq2(resultCache, d, ascIdxes, headIdx + 1);
            count = Math.max(countAsNew, countSkipHead);
        }

        return count;
    }

    /**
     * https://leetcode.com/explore/interview/card/top-interview-questions-medium/112/design/813/
     */
    @Test
    public void testRadomCRD() {
        RandomSet rd = new RandomSet(100);
        rd.insert(23);
        rd.insert(2);
        rd.insert(13);
        int random = rd.getRandom();
        System.out.println(random);

        rd.remove(13);


    }

    class RandomSet {
        int limit = 25;
        Map<Integer, Set<Integer>> v2idxs = new HashMap();
        int[] d;
        int dcount = 0;
        Random random = new Random();

        public RandomSet(int limit) {
            this.limit = limit;
            d = new int[limit];
        }

        public boolean insert(int val) {
            if (dcount == limit)
                return false;

            v2idxs.computeIfAbsent(val, x -> new HashSet<Integer>()).add(dcount);
            d[dcount++] = val;// val stored in "pre adding idx"

            return true;
        }

        public boolean remove(int v) {
            if (!v2idxs.containsKey(v))
                return false;

            Set<Integer> idxs = v2idxs.get(v);
            int rIdx = idxs.stream().toList().get(0); //TODO: order guaranteed?

            if (rIdx == dcount - 1) { //last idx
                v2idxs.get(d[rIdx]).remove(rIdx);
                if (v2idxs.get(d[rIdx]).isEmpty())
                    v2idxs.remove(d[rIdx]);
                return true;
            }

            // swap w last, cover map //TODO: where is the swap ??
            Set<Integer> ins = v2idxs.get(rIdx);
            ins.remove(rIdx);
            if (ins.isEmpty()) {
                v2idxs.remove(d[rIdx]);
            }
            v2idxs.get(d[d.length - 1]).remove(d.length - 1); //remove is fine TODO: del k


            d[rIdx] = d[d.length - 1]; //it is remove right?

            return true;
        }

        public int getRandom() {

            int rIdx = random.nextInt(dcount);
            return d[rIdx];
        }
    }

    /**
     * https://leetcode.com/explore/interview/card/top-interview-questions-medium/113/math/815/
     */
    @Test
    public void testHappyNumber() {
        int d = 19; //happy
//        int d = 2; // none-happy

        boolean r = isHappyNum(d);
        System.out.println(r);

    }

    private boolean isHappyNum(int d) {
        Set<Integer> rs = new HashSet<>();
        int t = Integer.MIN_VALUE;

        while (!rs.contains(1) && !rs.contains(t)) { // 1 or loop
            List<Integer> l = new ArrayList<>();
            while (d != 0) { //parse digit * dig , sum
                int sd = d % 10;
                l.add(sd * sd);
                d = d / 10;
            }
            d = l.stream().reduce(0, Integer::sum);

            if (rs.contains(d)){
                break;
            } else
                rs.add(d);

        }
        if (rs.contains(1))
            return true;
        else return false;
    }

    /**
     * https://leetcode.com/explore/interview/card/top-interview-questions-medium/113/math/816/
     */
    @Test
    public void testFactorialTrailZero() { // Trail mean tail
//        int d = 3;
        int d = 5;

        int f = 1;

        while (d > 1) {
            f *= d--;
        }

        int ct = 0;
        while (f != 0) {
            int t = f % 10;
            if (t == 0) {
                ct++;
                f = f / 10;
            } else
                break;
        }

        System.out.println(ct);
    }

    /**
     * https://leetcode.com/explore/interview/card/top-interview-questions-medium/113/math/817/
     */
    @Test
    public void testXlsRecur() {
//        String d = "AB";
        String d = "zY";

        System.out.println ( XlsNumbRec(d.toUpperCase(), 0) );


    }

    private String XlsNumbRec(String dstr, int v) {
        if (dstr.isEmpty())
            return Integer.toString(v);

        int nb = dstr.charAt(0) - 'A' + 1;
        int nextV = (v) * 26 + nb;

        return XlsNumbRec(dstr.substring(1), nextV);
    }

    @Test
    public void testXls() {

//        String d = "AB";
        String d = "ZY";

        char[] chars = d.toCharArray();
        int sm = 0;
        List<Integer> rs = new ArrayList<>();


        /** Trap: see what is wrong is we do this
         for(int i=0; i < chars.length;  i++) {
         //ascii int: v - 'A'
         int delta = 1 + chars[i] - 'A';
         sm += (chars.length-1-i)*26 + delta;
         }
         System.out.println(sm);
         */


        //itr chars lft to rt
        for (int i = 0; i < chars.length; i++) {

            //ascii int: v - 'A'
            int delta = 1 + chars[i] - 'A';

            // Each existing(left one) reset by  * 26
            for (int ii = 0; ii < rs.size(); ii++) {
                rs.set(ii, rs.get(ii) * 26);
            }
            // add new
            rs.add(delta);
        }

        //sum
        for (int e : rs) {
            sm += e;
        }
        System.out.println(sm);
    }

    /**
     * https://leetcode.com/explore/interview/card/top-interview-questions-medium/113/math/818/
     */
    @Test
    public void testPow() {
        double x = 2.00000;
        int n = 10;
        double r = 1;

        for (int i = 0; i < Math.abs(n); i++) {
            r *= x;
        }


        if (n < 0) {
            System.out.println(1 / r);
        } else
            System.out.println(r);

    }

    /**
     * https://leetcode.com/explore/interview/card/top-interview-questions-medium/113/math/819/
     */
    @Test
    public void testFindSqtUsingBinarySearch() {
        /**
         * Math basic: n*n indicates sqt(n*n) <= n*n/2, so we find the upbounds
         */
//        int d = 4;
        int d = 33;

        if (d==1){
            System.out.println(d);
            System.exit(0);
        }

        double x = d/2;

        // lower bounds
        while (x * x > d) {
            // round down
            x = x/2;
        }

        // crawl btwn lower : upper bounds
        for(long i = Math.round(x); i < x*2; i++) {
            if (i * i <= d && (i+1) * (i+1) > d) {
                System.out.println("result is " + i);
                break;
            }
        }
    }

    /**
     * https://leetcode.com/explore/interview/card/top-interview-questions-medium/113/math/820/
     *
     * https://www.geeksforgeeks.org/divide-two-integers-without-using-multiplication-division-mod-operator/
     */
    //TODO: try binary solution of g4g
    @Test
    public void testDivideInteger() {
        int dividend = 10;
        int divisor = 3;

//        int dividend = 7;
//        int divisor = -3;

        int sign = Math.abs(divisor * dividend) / (divisor * dividend);
        int dividendabs = Math.abs(dividend);
        int divisorabs = Math.abs(divisor);

        int r = 1;
        int bf = divisorabs;

        while (dividendabs > bf) {
            r++;
            bf += divisorabs;
        }// requires less than dividend
        r--;

        System.out.println(r * sign);

    }


    /**
     * https://leetcode.com/explore/interview/card/top-interview-questions-medium/113/math/821/
     * <p>
     * Ref: https://www.geeksforgeeks.org/find-recurring-sequence-fraction/
     * <p>
     * Gap: the initial desc ask return fraction, but sample result showing result in decimal format.
     *
     *
     * Math basic: when identical number happening in remainder, the repeating exsits
     */
    @Test
    public void testFractionDisplay() {
//        int n = 1; int dn = 2;
//        int n = 4; int dn = 3;
        int n = 4; int dn = 333; //0.(012)

        List<Integer> divResults = new ArrayList<>();
        Map<Integer, Integer> rem2DrsIdx = new HashMap<>(); //Under repeat, repeating start idx can be identified by this
        StringBuilder sb = new StringBuilder();

        // Decide whole number part display
        if (n / dn > 0) {
            sb.append(n / dn + ".");
        } else
            sb.append("0.");

        //Now all below on right digit p, which will be rem*10/(%)dn
        int rem = n % dn;
        while (rem != 0 && !rem2DrsIdx.containsKey(rem)) { //When identical remainder show up, it is recuring
            rem2DrsIdx.put(rem, divResults.size()); //mapped to index

            // prep
            rem *= 10;

            int divResult = rem / dn;//cover 0.000123
            divResults.add(divResult);

            //renew rem from dr
            rem = rem % dn;

        }
        // now rem is the first repeated remainder

        if (rem == 0) {
            String ss = String.join("", divResults.stream().map(it -> it.toString()).collect(Collectors.toList()));

            sb.append(ss);
            System.out.println("No recuring: " + sb);
        } else {
            int lsIdx = rem2DrsIdx.get(rem);
            List<Integer> integers = divResults.subList(0, lsIdx);
            String noneRptStr = String.join("", integers.stream().map(it -> it.toString()).collect(Collectors.toList()));
            List<Integer> integersRpt = divResults.subList(lsIdx, divResults.size());

            sb.append("(");
            String rptStr = String.join("", integersRpt.stream().map(it -> it.toString()).collect(Collectors.toList()));
            sb.append(noneRptStr).append(rptStr);
            sb.append(")");
            System.out.println("recuring :  " + sb);
        }
    }

    @Test
    public void testValidateSudoku() {
        // Trick: here careful intp[][] or char[][]
         char[][] b = {
         {'5','3','.','.','7','.','.','.','.'}
         ,{'6','.','.','1','9','5','.','.','.'}
         ,{'.','9','8','.','.','.','.','6','.'}
         ,{'8','.','.','.','6','.','.','.','3'}
         ,{'4','.','.','8','.','3','.','.','1'}
         ,{'7','.','.','.','2','.','.','.','6'}
         ,{'.','6','.','.','.','.','2','8','.'}
         ,{'.','.','.','4','1','9','.','.','5'}
         ,{'.','.','.','.','8','.','.','7','9'}
         };

        /*
        char[][] b = {
                {'8', '3', '.', '.', '7', '.', '.', '.', '.'}
                , {'6', '.', '.', '1', '9', '5', '.', '.', '.'}
                , {'.', '9', '8', '.', '.', '.', '.', '6', '.'}
                , {'8', '.', '.', '.', '6', '.', '.', '.', '3'}
                , {'4', '.', '.', '8', '.', '3', '.', '.', '1'}
                , {'7', '.', '.', '.', '2', '.', '.', '.', '6'}
                , {'.', '6', '.', '.', '.', '.', '2', '8', '.'}
                , {'.', '.', '.', '4', '1', '9', '.', '.', '5'}
                , {'.', '.', '.', '.', '8', '.', '.', '7', '9'}
        };*/

        Set<Character>[] rs = new Set[9];
        Set<Character>[] cs = new Set[9];
        Set<Character>[][] b2 = new Set[3][3];


//        Arrays.fill(rs, new HashSet<Character>());// Tick: This is wrong since all ele points to same obj. Look@api

        for(int ct =0; ct < 3;ct++) {
            for (int ct2 =0; ct2<3;ct2++) {
                b2[ct][ct2] = new HashSet<>();
                rs[ct*3 + ct2] = new HashSet(); cs[ct*3 + ct2] = new HashSet<>();
            }
        }

        oloop:
        for (int r = 0; r < 9; r++) {
            for (int c = 0; c < 9; c++) {
                char x = b[r][c];
                if (x == '.')
                    continue;

                boolean aa = rs[r].add(x);
                boolean bb = cs[c].add(x);
                Set<Character> cs2 = b2[r/3][c/3];
                boolean cc = cs2.add(x);

//                if ( rs[r].add(x) && cs[c].add(x) && b2[r/3][c/3].add(r + "-" + c))
                if (aa && bb && cc)
                {}
                else {
                    System.out.println("Not valid @:" + r + "-" + c);
                    break oloop;

                }
            }
        }
        System.out.println("Vaide Sudoku");


    }


}
