import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

/**
 * https://www.geeksforgeeks.org/auto-complete-feature-using-trie/
 *
 *
 */
public class AutoCompleteTrieSolution {

    @Test
    public void testAutoCompleteTrie() {
        List<String> words = List.of("hello", "dog", "hell", "cat", "a", "hel","help","helps","helping");
        CharTrieNode rootNode = new CharTrieNode();

        for(int i =0; i < words.size();i++) {
            rootNode.mergeString(words.get(i), false);
        }

        List<String> indicates = rootNode.findIndicates("hel");
        System.out.println(indicates); //bug: missing couple of like "hel"
    }

}
