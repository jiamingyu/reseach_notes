import org.junit.jupiter.api.Test;

public class CharTrieNodeDrill {
    //TODO: word break dict in  Trie solutions: https://www.geeksforgeeks.org/word-break-problem-trie-solution/



    @Test
    public void testLongestCommonPrefix() {
        String[] sts = new String[]{"geeksforgeeks", "geeks", "geazer" };
        CharTrieNode root = new CharTrieNode(sts[0]);

        for(int i=1; i < sts.length;i++) {
            String s = sts[i];

            char[] scs = s.toCharArray();
            CharTrieNode cur = root;
            if (scs[0] != root.nodeValue)
                System.out.println(s + " shows first uncommon char idx is 0");


            for(int ci=1;ci< scs.length; ci++) {
                if (cur != null && cur.getChild(scs[ci]) != null  ) {
                    cur = cur.getChild(scs[ci]);
                } else {
                    System.out.println(s + " shows first uncommon char idx is " + ci);
                    break;
                }
            }
            System.out.println( "Bravo ! " + s + " shows all common chars");

        }
    }

    @Test
    public void testFindShortestUniquePrefix() {
        String arr[] = {"zebra", "dog", "duck", "dot"};
        int n = arr.length;
        findShortestUniquePrefix(arr, n);
    }

    //TODO: This is just need a mutation of CharTrieNode
    // https://www.geeksforgeeks.org/find-all-shortest-unique-prefixes-to-represent-each-word-in-a-given-list/
    private void findShortestUniquePrefix(String[] arr, int n) {
        CharTrieNode root = new CharTrieNode(arr[0]);



    }


}


