import org.junit.jupiter.api.Test;

public class TrieStructureDrill {

    /**
     * A trie (pronounced as "try") or prefix tree is a tree data structure used to efficiently store and retrieve keys in a dataset of strings. There are various applications of this data structure, such as autocomplete and spellchecker.
     *
     * Implement the Trie class:
     *
     * Trie() Initializes the trie object.
     * void insert(String word) Inserts the string word into the trie.
     * boolean search(String word) Returns true if the string word is in the trie (i.e., was inserted before), and false otherwise.
     * boolean startsWith(String prefix) Returns true if there is a previously inserted string word that has the prefix prefix, and false otherwise.
     *
     * Test A :
     * Trie trie = new Trie();
     * trie.insert("apple");
     * trie.search("apple");   // return True
     * trie.search("app");     // return False
     * trie.startsWith("app"); // return True
     * trie.insert("app");
     * trie.search("app");     // return True
     *
     */

    @Test
    public void testTrieA() {
        CharTrie trie = new CharTrie();
        trie.insert("apple");
        System.out.println(trie.search("apple", false));   // return True
        System.out.println(trie.search("applx", false));   // return F
        System.out.println(trie.search("applex", false));   // return F
        System.out.println(trie.search("app", false));     // return False
        System.out.println(trie.startsWith("app")); // return True
        trie.insert("app");
        System.out.println( trie.search("app", false) );     // return True
    }

    class CharTrie {
        CharTrieNode[] root = new CharTrieNode[26];
        public boolean insert(String s){
            assert !s.isEmpty();

            char c = s.toLowerCase().charAt(0);
            int idx = c - 'a';
            root[idx] = new CharTrieNode(c);
            root[idx].insert(s.substring(1));

            return false;
        }
        public boolean search(String s, boolean enableStartWith){
            assert !s.isEmpty();

            int idx = s.charAt(0) - 'a';
            if (root[idx] == null)
                return false;
            else
                return root[idx].search(s.substring(1), enableStartWith);
        }
        public boolean startsWith(String s){
            return search(s, true);
        }

    }

    class CharTrieNode extends TrieNode<Character> {

        public CharTrieNode(Character nodeVal) {
            super(nodeVal);
        }

        @Override
        protected void initChildren() {
            children = new CharTrieNode[TRI_SIZE];
        }

        @Override
        public boolean insert(String s) {
            assert !s.isEmpty();

            char[] cs = s.toLowerCase().toCharArray();

            int idx = s.toLowerCase().charAt(0) - 'a';

            if (children[idx] == null) {
                children[idx] = new CharTrieNode(cs[0]);
            }
            // last char
            if (s.length() == 1) {
                if ( children[idx].existsEnd )
                    return false;
                else {
                    children[idx].existsEnd = true;
                    return true;
                }
            }
            // below only can be length > 1
            return children[idx].insert(s.substring(1));
        }

        @Override
        public boolean search(String s, boolean enableStartWith) {
            int idx = s.charAt(0) - 'a';

            if (children[idx] == null) {
                return false;
            } else {
                // last char
                if (s.length() == 1) {
                    if ( children[idx].existsEnd  )
                        return true;
                    else if (enableStartWith) { // s find match only here but not complete to the pre-inserted words down the trie way
                        return true;
                    } else
                        return false;
                }
                return children[idx].search(s.substring(1), enableStartWith);
            }
        }
    }

    abstract class TrieNode<T> {
        T nodeVal;
        int TRI_SIZE = 64; //2expo6
        TrieNode<T>[] children;

        boolean existsEnd = false;


        public TrieNode(T nodeVal) {
            this.nodeVal = nodeVal;
            initChildren();
        }

        protected abstract void initChildren();

        public abstract boolean insert(String substring);

        public abstract boolean search(String substringboolean, boolean enableStartWith);
    }

}
