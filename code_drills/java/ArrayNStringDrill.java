import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.stream.IntStream;

public class ArrayNStringDrill {

    @Test
    public void testMaxMagin() {
        int price[] = { 100, 180, 260, 310, 40, 535, 695 };
//        int price[] = { 10, 180, 20, 30, 40, 5, 95 };
        System.out.println(maxMagin(price));
    }

    /**
     * Solution high level desc:
     * Post mentioned solution gives some insights. Based on this, I did:
     * A. all considered transactions should be lower buy and higher sell, reflected by derived ProfitMargin
     * B. Comb of transaction chain only focus on how to combine those ProfitMargins
     * (if market profiting can be predictable like this case, it will not be so attractive anymore)
     *
     * https://www.geeksforgeeks.org/stock-buy-sell/
     *
     * @param prices
     * @return
     */
    private MaxMarginRtn maxMagin(int[] prices) {
        Map<Integer, List<ProfitMargin>> start2ProfitMargins = new HashMap<>();

        for(int i =0; i<prices.length; i++) {
            end_pt:
            for (int j = i; j < prices.length; j++) {
                if (prices[i] >= prices[j])
                    continue end_pt;
                List<ProfitMargin> profitMargins = start2ProfitMargins.computeIfAbsent(i, x -> new ArrayList<>());

                profitMargins.add(new ProfitMargin(i, j));

            }
        }

        MaxMarginRtn max = calibMaxMargin(0, start2ProfitMargins, prices);
        return max;
    }

    private MaxMarginRtn calibMaxMargin(int sIdx, Map<Integer, List<ProfitMargin>> start2ProfitMargins, int[] prices) {

        if (sIdx == prices.length) //TODO End condition
            return null;

        MaxMarginRtn max = null;
        String maxLeadStr = null;

        for(int firstTrade =sIdx; firstTrade<prices.length; firstTrade++) {
            if (!start2ProfitMargins.containsKey(firstTrade))
                continue;
            margin_loop:
            for(ProfitMargin margin : start2ProfitMargins.get(firstTrade)) {
//                if (margin.endDay + 1 == prices.length) //TODO: This cause bug
//                    continue margin_loop;

                MaxMarginRtn stackLevelMaxLead = calibMaxMargin(margin.endDay + 1, start2ProfitMargins, prices);

                boolean isHeadStack = sIdx == 0;
                if ( stackLevelMaxLead == null) {
                    if(!isHeadStack ) {
                        if (max == null ||  (max.max < prices[margin.endDay] - prices[margin.startDay])) {
                            max = new MaxMarginRtn(prices[margin.endDay] - prices[margin.startDay], prices[margin.startDay]  + "-" + prices[margin.endDay]+ ":");
                            maxLeadStr = margin.toString();
                        }
                    }
                } else {
                    if(max == null ||  max.max < stackLevelMaxLead.max + margin.calibMargin(prices)){
                        max = stackLevelMaxLead;
                        max.max = stackLevelMaxLead.max + margin.calibMargin(prices);
                        maxLeadStr = margin.toString();
                    }
                }

            }

        }

        if (max != null)
            max.tradeRecordsSb.append(maxLeadStr);
        return max;
    }


    @Test
    public void testLongestContinuousSubArray() {
        int arr[] = {1, 56, 58, 57, 90, 92, 94, 93, 91, 45};
        int[] r = longestContinuousSubArrayRegardlessAscDesc(arr);
        Arrays.asList(r).forEach(x->System.out.println(x + ","));
    }

    /** This solution is NOT the exact solution to this, but variant here as defining asc/desc, instead of contiguous
     * https://www.geeksforgeeks.org/length-largest-subarray-contiguous-elements-set-1/
     * @param arr
     * @return
     */
    private int[] longestContinuousSubArrayRegardlessAscDesc(int[] arr) {
        int startIdx =0;
        int endIdx = 1;
        int lastDelta = 0;
        int maxContiguousLen = 0;

        assert arr.length > 1;

        while(endIdx < arr.length - 1) {
            switch (lastDelta) {
                case -1, 1 -> {
                    if (Math.abs(arr[endIdx] - arr[endIdx+1]) == 1) {
                        continue;
                    }else
                        lastDelta = 0;
                    break;
                }
                default ->  {
                    lastDelta = arr[endIdx + 1] - arr[endIdx];
                }
            }

            if(lastDelta != 1 && lastDelta !=-1) {
                // update max
                if (endIdx - startIdx + 1 >+ maxContiguousLen) {
                    System.out.println( startIdx + " -> " + endIdx );
                }
                maxContiguousLen = Math.max(maxContiguousLen, endIdx - startIdx + 1);
                //reset
                startIdx = endIdx;
                endIdx=startIdx+1;
            } else { // expand
                endIdx++;
            }

        }

        System.out.println(maxContiguousLen);

        int[] subarray = new int[endIdx - startIdx + 1];
        System.arraycopy(arr, startIdx, subarray, 0, subarray.length);

        return subarray;
    }

    @Test
    public void testComb2SortedArrayBitSolution() {
        int a[] = { 10, 15, 25 };
        int b[] = { 5, 20, 30 };
        List<List<Integer>> comb = comb2SortedArrayBitSolution(a, b);

        comb.forEach(
                s->{
                    s.forEach(sn -> {System.out.print(sn + ":");
                    });
                    System.out.println();
                }
        );
    }

    /**
     * https://www.geeksforgeeks.org/generate-all-possible-sorted-arrays-from-alternate-elements-of-two-given-arrays/
     *
     * @param a
     * @param b
     * @return
     */
    private List<List<Integer>> comb2SortedArrayBitSolution(int[] a, int[] b) {
        List<List<Integer>> extractedA = extracted(a);
        List<List<Integer>> extractedB = extracted(b);

        List<List<Integer>> comb = new ArrayList<>();

        extractedA.stream().filter(acs->!acs.isEmpty()).forEach(acs ->{
            extractedB.stream().filter(bcs->!bcs.isEmpty()).forEach(bcs->{
                List<Integer> a2b = new ArrayList<>();
                comb.add(a2b);
                int ai =0; int bi =0;boolean takeA =true;
                while(ai < acs.size() && bi < bcs.size()) {
                    if(takeA && acs.get(ai) <= bcs.get(bi)) {
                        a2b.add(acs.get(ai));ai++;takeA=false;
                    } else {
                        a2b.add(bcs.get(bi));bi++;takeA=true;
                    }
                }
                if (ai < acs.size()) {
                    a2b.addAll(acs.subList(ai+1, acs.size()));
                }
                if (bi < bcs.size()) {
                    a2b.addAll(bcs.subList(bi+1, bcs.size()));
                }
            });
        });


        return comb;
    }

    List<List<Integer>> extracted(int[] a) {
        List<List<Integer>> totalSubSet = new ArrayList();
        IntStream.range(0,1<< a.length).forEach(avar-> {
            List<Integer> aSubSet = new ArrayList();
            for(int acur = 0; acur < a.length; acur++) {
                if((avar >> acur & 1) ==1)
                    aSubSet.add(a[acur]);
            }
//            aSubSet.forEach(x->System.out.print(x + "::"));
//            System.out.println("==============");
            totalSubSet.add(aSubSet);
        });
        return totalSubSet;
    }

    /**
     * https://www.geeksforgeeks.org/reverse-a-string-without-affecting-special-characters/
     */
    @Test
    public void testReverseStrWOTouchSpecialChars() {
        String s = new String("a,b$c"); //c,b$a”

        char[] chars = s.toCharArray();

        StringBuilder sb = new StringBuilder();

        Map<Integer, Character> initPos2SpecialChar = new HashMap<>();
        for(int i =0; i < s.length();i++) {
            if ( !Character.isAlphabetic(chars[i]) ){
                initPos2SpecialChar.putIfAbsent(i, chars[i]);
            }
        }

        for(int i = s.length()-1;i>=0;i--) {
            while (initPos2SpecialChar.containsKey(sb.length()-i))
                sb.append(initPos2SpecialChar.get(i));
            sb.append(chars[i]);
        }
        System.out.println(sb.toString());
    }

    @Test
    public void testReverseStrWOTouchSpecialCharsBigO1() { // Key part is detect unassigned char var by CharType
        String s = new String("a,b$c"); //c,b$a”
        char[] chars = s.toCharArray();

        char[] r = new char[s.length()];
        int rIdx = chars.length-1;



        for(int i=0;i<chars.length;i++) {
            if (!Character.isAlphabetic(chars[i])) {
                r[i] = chars[i];
                rIdx--;
                continue;
            }
            if (Character.getType(r[i]) == Character.CONTROL) {
                System.out.println(i);
            }
            r[rIdx] = chars[i];
            rIdx--;
        }

        System.out.println(Arrays.toString(r));

    }

}
// End of test suites

class ProfitMargin {
    int startDay;
    int endDay;

    public ProfitMargin(int startDay, int endDay) {
        this.startDay = startDay;
        this.endDay = endDay;
    }

    int calibMargin(int dailyPrices[]) {
        //TODO: assert
        return dailyPrices[endDay] - dailyPrices[startDay];
    }

    public String toString() {
        return startDay + "->"+ endDay+":";
    }
}

class MaxMarginRtn {
    int max;
    StringBuffer tradeRecordsSb = new StringBuffer();

    public MaxMarginRtn(int max, String tradeStr) {
        this.max = max;
        tradeRecordsSb.append(":" + tradeStr + ":");
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(max + ":\n");
        sb.append(tradeRecordsSb.toString());
        return sb.toString();
    }

}
