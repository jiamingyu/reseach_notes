import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

public class CharTrieNode {
    static int CHAR_SIZE = 26;

    char nodeValue;
    CharTrieNode[] chars = new CharTrieNode[CHAR_SIZE];

    public CharTrieNode() {
    }
    public CharTrieNode(String s) {
        mergeString(s, true);
    }

    public CharTrieNode(char nodeValue) {
        this.nodeValue = nodeValue;
    }

    boolean addChild(char c) {
        int idx = c - 'a';
        if (chars[idx] != null) {
            return false;
        } else {
            chars[idx] = new CharTrieNode(c);
            return true;
        }
    }

    public CharTrieNode getChild(char c) {
        int idx = c - 'a';
        if (chars[idx] != null) {
            return chars[idx];
        }
        return null;
    }


    public void mergeString(String s, boolean asParent) {
        char[] cs = s.toCharArray();
        CharTrieNode cur = this;

        if (asParent) {
            nodeValue = cs[0];
            for(int i = 1; i < cs.length; i++) {
//                int idx = cs[i] - 'a';
//                chars[idx] = new CharTrieNode(cs[i]);
                cur.addChild(cs[i]);
                cur = cur.getChild(cs[i]);
            }
        } else {
            for(int i = 0; i < cs.length; i++) {
//                int idx = cs[i] - 'a';
//                chars[idx] = new CharTrieNode(cs[i]);
                cur.addChild(cs[i]);
                cur = cur.getChild(cs[i]);
            }
        }

    }

    public List<String> findIndicates(String s) {
        List<String> rs = new CopyOnWriteArrayList<>();


        if (!s.isEmpty() && chars[s.charAt(0)-'a'] != null) {
            List<String> indicates = chars[s.charAt(0) - 'a'].findIndicates(s.substring(1));
            if (nodeValue != '\u0000') {
                indicates.stream().forEach(ss -> {
                        rs.add(Character.toString(nodeValue) + ss);
                });
            } else {
                indicates.stream().forEach(ss -> rs.add(ss));
            }

        } else  {
            List<String> charsArrayUnder = findCharArrayUnder();
            charsArrayUnder.stream().forEach(ss -> rs.add(Character.toString(nodeValue) + ss));
        }
//        rs.stream().forEach(x-> rs.add(Character.toString(nodeValue) + x));
        return rs;
    }

    private List<String> findCharArrayUnder() {
        List<String> rs = new ArrayList<>();
        if (Arrays.stream(chars).filter(x-> x != null).toList().isEmpty()){
            rs.add(Character.toString(nodeValue));
            return rs;
        }


        // collect all under current node
        List<CharTrieNode> indicates = Arrays.stream(chars).filter(x->x!=null).collect(Collectors.toList());
        if (!indicates.isEmpty()) {
            indicates.stream().forEach(single -> {
                List<String> charArrayUnder = single.findCharArrayUnder();
                List<String> readys = charArrayUnder.stream().map(s -> Character.toString(nodeValue) + s).collect(Collectors.toList());
                rs.addAll(readys);
            });
        } else // no direct children at all
            rs.add(Character.toString(nodeValue));
        return rs;
    }
}