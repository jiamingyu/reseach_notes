package main

import (
	"fmt"
)

func removeStones(stones [][]int) int {
	doubleStoneCt := len(stones) * 2
	gap := len(stones)
	parents := make([]int, doubleStoneCt)
	// idx assign to values
	for i := range parents {
		parents[i] = i
	}

	// union/find
	for _, s := range stones {
		pid := gap + s[1]
		parents[findParent(parents, s[0])] = findParent(parents, pid)
	}

	// count island by idx identity
	islandCt := len(stones)
	for idx, _ := range stones {
		if parents[idx] != idx {
			islandCt -= 1
		}

	}

	return len(stones) - islandCt
}
func findParent(p []int, c int) int {
	switch {
	case p[c] == c:
		return c
	default:
		{
			return findParent(p, p[c])

		}

	}
}

func main() {
	// stones := [][]int{{0, 0}, {0, 1}, {1, 0}, {1, 2}, {2, 1}, {2, 2}}
	stones := [][]int{{0, 0}, {0, 2}, {1, 1}, {2, 0}, {2, 2}}

	fmt.Print(removeStones(stones))

}
