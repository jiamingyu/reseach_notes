# from typing import List
import queue

class Solution:
    def openLock(self, deadends: list[str], target: str) -> int:
      s = ""
      
      visited = set()
      q = queue.Queue()
      count = 1 //next count happen after processing the first rounds
      
      q.put('0000')
      q.put('dummy')
      visited.add('0000')
      
      # ls2 = [0,0,0,0]
      # lsj = "".join(str(ls2))
      # if '0000' in visited:
      #   print('in there')
      # return 1
      
      while not q.empty():
        s = q.get()
        if s == "dummy":
          print("dummy")
          q.put("dummy")
          count += 1
          continue
        
        ls = list(s)
        
        for i, c in enumerate(ls):
          x=''
          y=''
          v1 = (int(c) + 1) % 10
          v2 = (int(c) + 9) % 10
          if i == len(ls)-1:
            x = s[0:i] + str(v1)
            y = s[0:i] + str(v2)
          else:
            x = s[0:i] + str(v1) + s[i+1:]
            y = s[0:i] + str(v2) + s[i+1:]
            
          # print(x)
          # print(y)
            
          
          
          if x == target or y == target:
            print(f"found target at level {count}")
            return count
        
          if not x in visited and not x in deadends:
            visited.add(x)
            q.put(x)
          if not y in visited and not y in deadends:
            visited.add(y)
            q.put(y)
        

      
      
      return count
      

s = Solution()
# print( s.findDiagonalOrder([[]]))
mat =  ["0201","0101","0102","1212","2002"]
target = "0202"
print( s.openLock(mat, target))
