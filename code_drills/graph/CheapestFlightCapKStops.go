package main

import (
	"fmt"
)

// https://leetcode.com/problems/cheapest-flights-within-k-stops/description/

func findCheapestPrice(n int, flights [][]int, src int, dst int, stopCap int) int {
	s2dPrice := make([][]int, n)
	for idx := 0; idx < len(s2dPrice); idx++ {
		s2dPrice[idx] = make([]int, n)
	}
	for _, sed := range flights {
		s2dPrice[sed[0]][sed[1]] = sed[2]
	}

	idxBasedStopCt := make([]int, n)

	pQ := PriorityQ{data: make(map[int]int, 0)}
	// init Q with src
	idxBasedStopCt[src] = -1
	for i := 0; i < n; i++ {
		if i == src {
			continue
		}
		if s2dPrice[src][i] > 0 {
			pQ.push(i, s2dPrice[src][i])
			idxBasedStopCt[i] = 1
		}
	}

	srcBuf := src

	for srcBuf != dst {

		srcBuf, priceBuf := pQ.poll()

		if srcBuf == -1 && priceBuf == -1 {
			break
		}

		if idxBasedStopCt[srcBuf]-1 > stopCap { //reached stop count limit
			continue
		}

		for i := 0; i < n; i++ {
			if idxBasedStopCt[i] != 0 { //visited
				continue
			}

			price := s2dPrice[srcBuf][i]
			if price > 0 {
				priceIncreased := price + priceBuf
				pQ.push(i, priceIncreased)
				idxBasedStopCt[i] = idxBasedStopCt[srcBuf] + 1
			}
		}

	}

	v, ok2 := pQ.data[dst]
	if ok2 {
		return v
	} else {
		return -1
	}
}

func main() {
	// Venilla case, should be 700
	flights := [][]int{{0, 1, 100}, {1, 2, 100}, {2, 0, 100}, {1, 3, 600}, {2, 3, 200}}

	// Should indicate stop cap reached
	// flights := [][]int{{0, 1, 100}, {1, 2, 100}, {2, 0, 100}, {2, 3, 200}}

	src := 0
	dst := 3
	cap := 1

	calib := findCheapestPrice(4, flights, src, dst, cap)

	if calib == -1 {
		fmt.Println("No route under stop cap exists")
	} else {
		fmt.Println("route under stop cap exists with price:", calib)

	}
}

/*
*
PriorityQ Module
*/
type PriorityQ struct {
	data      map[int]int
	lowestKey int
}

// func main() {
// Verify PriorityQ
// pmp := PriorityQ{data: make(map[int]int, 0)}
// pmp.push(2, 101)
// fmt.Println(pmp.poll())
// }

func (mp *PriorityQ) poll() (int, int, bool) {
	v, ok :=apackage main

	import (
		"fmt"
	)
	
	// https://leetcode.com/problems/cheapest-flights-within-k-stops/description/
	
	func findCheapestPrice(n int, flights [][]int, src int, dst int, stopCap int) int {
		s2dPrice := make([][]int, n)
		for idx := 0; idx < len(s2dPrice); idx++ {
			s2dPrice[idx] = make([]int, n)
		}
		for _, sed := range flights {
			s2dPrice[sed[0]][sed[1]] = sed[2]
		}
	
		idxBasedStopCt := make([]int, n)
	
		pQ := PriorityQ{data: make(map[int]int, 0)}
		// init Q with src
		idxBasedStopCt[src] = -1
		for i := 0; i < n; i++ {
			if i == src {
				continue
			}
			if s2dPrice[src][i] > 0 {
				pQ.push(i, s2dPrice[src][i])
				idxBasedStopCt[i] = 1
			}
		}
	
		srcBuf := src
	
		for srcBuf != dst {
	
			srcBuf, priceBuf, ok := pQ.poll()
	
			if !ok {
				break
			}
	
			if idxBasedStopCt[srcBuf]-1 > stopCap { //reached stop count limit
				continue
			}
	
			for i := 0; i < n; i++ {
				if idxBasedStopCt[i] != 0 { //visited
					continue
				}
	
				price := s2dPrice[srcBuf][i]
				if price > 0 {
					priceIncreased := price + priceBuf
					pQ.push(i, priceIncreased)
					idxBasedStopCt[i] = idxBasedStopCt[srcBuf] + 1
				}
			}
	
		}
	
		v, ok2 := pQ.data[dst]
		if ok2 {
			return v
		} else {
			return -1
		}
	}
	
	func main() {
		// Venilla case, should be 700
		// flights := [][]int{{0, 1, 100}, {1, 2, 100}, {2, 0, 100}, {1, 3, 600}, {2, 3, 200}}
	
		// Should indicate stop cap reached
		flights := [][]int{{0, 1, 100}, {1, 2, 100}, {2, 0, 100}, {2, 3, 200}}
	
		src := 0
		dst := 3
		cap := 1
	
		calib := findCheapestPrice(4, flights, src, dst, cap)
	
		if calib == -1 {
			fmt.Println("No route under stop cap exists")
		} else {
			fmt.Println("route under stop cap exists with price:", calib)
	
		}
	}
	
	/*
	*
	PriorityQ Module
	*/
	type PriorityQ struct {
		data      map[int]int
		lowestKey int
	}
	
	// func main() {
	// Verify PriorityQ
	// pmp := PriorityQ{data: make(map[int]int, 0)}
	// pmp.push(2, 101)
	// fmt.Println(pmp.poll())
	// }
	
	func (mp *PriorityQ) poll() (int, int, bool) {
		v, ok := mp.data[mp.lowestKey]
		if ok {
			delete(mp.data, mp.lowestKey)
			b4DelLowestKey := mp.lowestKey
			mp.updateLowest()
			return b4DelLowestKey, v, true
		}
		return -1, -1, false
	}
	
	func (mp *PriorityQ) push(k int, v int) (int, int) {
		mp.data[k] = v
	
		if len(mp.data) == 1 {
			mp.lowestKey = k
			return k, v
		}
	
		mp.updateLowest()
		return mp.lowestKey, mp.data[mp.lowestKey]
	}
	
	func (mp *PriorityQ) updateLowest() {
		for k2, v2 := range mp.data {
			if v2 < mp.data[mp.lowestKey] {
				mp.lowestKey = k2
			}
		}
	}
	 mp.data[mp.lowestKey]
	if ok {
		delete(mp.data, mp.lowestKey)
		b4DelLowestKey := mp.lowestKey
		mp.updateLowest()
		return b4DelLowestKey, v, true
	}
	return -1, -1, false
}

func (mp *PriorityQ) push(k int, v int) (int, int) {
	mp.data[k] = v

	if len(mp.data) == 1 {
		mp.lowestKey = k
		return k, v
	}

	mp.updateLowest()
	return mp.lowestKey, mp.data[mp.lowestKey]
}

func (mp *PriorityQ) updateLowest() {
	for k2, v2 := range mp.data {
		if v2 < mp.data[mp.lowestKey] {
			mp.lowestKey = k2
		}
	}
}
