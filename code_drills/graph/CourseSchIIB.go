package main

import (
	"fmt"
)

const CURRENT = 1
const VISITED = 2

func findOrder(numCourses int, prerequ2wanted [][]int) []int {

	prereq2after := make([][]int, numCourses) // index is key to course id

	for _, pv := range prerequ2wanted {
		if prereq2after[pv[1]] == nil {
			prereq2after[pv[1]] = make([]int, 0)
		}
		prereq2after[pv[1]] = append(prereq2after[pv[1]], pv[0])
	}

	lead := make([][]int, 10)
	for idx := 0; idx < numCourses; idx++ {

		v := make([]int, numCourses)
		q := make([]int, numCourses)
		for idx := range q {
			q[idx] = -1
		}
		leads := crawlDfs(prereq2after, v, idx)

		checkV := make([]int, numCourses)
		for idx := range checkV {
			checkV[idx] = 0
		}
		for _, l := range leads {
			for _, lv := range l {
				checkV[lv] = 1
			}
		}
		fmt.Println("checkV: ", checkV)

		pass := true
		for _, cv := range checkV {
			if cv == 0 {
				pass = false
				break
			}
		}
		fmt.Println("checkV2: ", checkV, "pass:", pass)

		if pass {
			fmt.Println("lllllllleads:  ", leads)

		}

	}

	return lead[0] // Here only return one if there exists multi-solutions

}

func crawlDfs(g [][]int, visited2 []int, cid int) [][]int {
	if visited2[cid] == VISITED {
		return make([][]int, 0)
	}
	if visited2[cid] == CURRENT {
		fmt.Println("cycle detected")
		return make([][]int, 0) //TODO:
	}
	if g[cid] == nil {
		visited2[cid] = VISITED
		return [][]int{{cid}}
	}

	visited2[cid] = CURRENT

	rtn := make([][]int, 0)
	for _, gv := range g[cid] {
		fmt.Println("gv:", gv)
		if visited2[gv] == VISITED {
			continue
		}

		visited := make([]int, len(visited2))
		copy(visited, visited2)

		subQs := crawlDfs(g, visited, gv)
		fmt.Println("subQs:  ", subQs)
		for idx, subQ := range subQs {
			subQs[idx] = append(subQ, cid)
		}
		rtn = append(rtn, subQs...) //TODO: ...

	}
	visited2[cid] = VISITED //TODO: bug

	return rtn
}

func main() {
	// count := 2
	// prerequisites := [][]int{{1, 0}, {0, 1}}
	// prerequisites := [][]int{{0, 1}}

	count := 4
	prerequisites := [][]int{{1, 0}, {2, 0}, {3, 1}, {3, 2}} //This is tricky & will be impl later
	// prerequisites := [][]int{{1, 0}, {2, 1}, {3, 2}}

	fmt.Println(findOrder(count, prerequisites))

}
