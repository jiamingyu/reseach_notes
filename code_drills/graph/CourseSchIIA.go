package main

import (
	"fmt"
)

const CURRENT = 1
const VISITED = 2

func findOrder(numCourses int, prerequ2wanted [][]int) []int {

	prereq2after := make([][]int, numCourses) // index is key to course id

	for _, pv := range prerequ2wanted {
		if prereq2after[pv[1]] == nil {
			prereq2after[pv[1]] = make([]int, 0)
		}
		prereq2after[pv[1]] = append(prereq2after[pv[1]], pv[0])
	}

	lead := make([][]int, 10)
	for idx := 0; idx < numCourses; idx++ {

		v := make([]int, numCourses)
		q := make([]int, numCourses)
		for idx := range q {
			q[idx] = -1
		}
		crawlDfs(prereq2after, v, idx, lead, q)

		validPath := true
		for idx2, v := range q {
			if idx2+1 < len(q) && v == -1 {
				validPath = false
			}
			if idx2+1 == len(q) {
				q[idx2] = idx
			}
		}
		if validPath {
			fmt.Println("valid path is:    ", q)
		}

	}

	return lead[0] // Here only return one if there exists multi-solutions

}

func crawlDfs(g [][]int, v []int, cid int, lead [][]int, q []int) {
	if v[cid] == VISITED {
		return
	}
	if v[cid] == CURRENT {
		fmt.Println("cycle detected")
		return
	}
	if g[cid] == nil {
		v[cid] = VISITED
		return
	}

	v[cid] = CURRENT
	for _, gv := range g[cid] {
		if v[gv] == VISITED {
			continue
		}
		crawlDfs(g, v, gv, lead, q)
		for idx, v := range q {
			if v == -1 {
				q[idx] = gv
				break
			}
		}
	}

	v[cid] = VISITED

}

func main() {
	// count := 2
	// prerequisites := [][]int{{1, 0}, {0, 1}}
	// prerequisites := [][]int{{0, 1}}

	count := 4
	prerequisites := [][]int{{1, 0}, {2, 0}, {3, 1}, {3, 2}} //This is tricky & will be impl later
	// prerequisites := [][]int{{1, 0}, {2, 1}, {3, 2}}

	fmt.Println(findOrder(count, prerequisites))

}
