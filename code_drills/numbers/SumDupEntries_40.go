/*
*
Notice: Input data is unique, the outcome result can be dupicate data sum
*/
package main

import "fmt"

func main() {

	cs := []int{10, 1, 2, 7, 6, 1, 5}
	target := 8
	r := combinationSumReplicate(cs, target)
	printOutcome(r)
}

func combinationSumReplicate(candidates []int, target int) [][]int {

	return doCal(candidates, target, 0, make([]int, 0))

}

func doCal(candidates []int, subTarget int, curIdx int, leadIdxes []int) [][]int {

	if len(candidates) <= curIdx+1 {
		//fmt.Printf("reached collection end !!! %d:\n", leadIdxes)
		return nil
	}

	rtn := make([][]int, 0)
	// exit condition
	if subTarget < 0 {
		return nil
	}

	if subTarget == 0 {

		rtn = append(rtn, leadIdxes)
		return rtn
	}

	// considered, should first be
	//if candidates[curIdx] <= subTarget {
	leadIdxesAdded := append(leadIdxes, curIdx)
	rtn = concat2DArray(rtn, doCal(candidates, subTarget-candidates[curIdx], curIdx+1, leadIdxesAdded))
	//}
	//not considered
	rtn = concat2DArray(rtn, doCal(candidates, subTarget, curIdx+1, leadIdxes))

	return rtn

}

func concat2DArray(a [][]int, b [][]int) [][]int {

	for _, vs := range b {
		a = append(a, vs)
	}
	return a
}
func printOutcome(rs [][]int) {
	for _, r := range rs {
		fmt.Println(r)
	}

}
