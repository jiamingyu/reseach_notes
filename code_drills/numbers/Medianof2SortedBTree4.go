package main

import (
	"fmt"
	"sort"

	"math"
)

var wip bool = true

func main() {
	// nums1 := []int{1}
	// nums2 := []int{3}

	// nums1 := []int{1, 2}
	// nums2 := []int{3}

	// nums1 := []int{1}
	// nums2 := []int{2, 4}

	// even count
	// nums1 := []int{1, 2}
	// nums2 := []int{3, 4}

	// nums1 := []int{1, 3}
	// nums2 := []int{2, 4}

	// nums1 := []int{1, 3}
	// nums2 := []int{2, 4, 5, 6}

	// nums1 := []int{1, 3, 6}
	// nums2 := []int{2, 4, 5}

	// nums1 := []int{1, 2, 3}
	// nums2 := []int{4, 5, 6}

	// nums1 := []int{1}
	// nums2 := []int{2, 3, 4, 5, 6}

	// nums1 := []int{2}
	// nums2 := []int{1, 3, 4}

	/** odd cases */

	// nums1 := []int{1, 2, 3, 7}
	// nums2 := []int{4, 5, 6}

	// nums1 := []int{1, 2, 3}
	// nums2 := []int{4, 5, 6, 7}

	// nums1 := []int{3, 4}
	// nums2 := []int{1, 2, 5}

	nums1 := []int{2, 4}
	nums2 := []int{1, 3, 5}

	// nums1 := []int{4}
	// nums2 := []int{1, 2, 3, 5}

	fmt.Println(findMedianSortedArrays(nums1, nums2))

}

func findMedianSortedArrays(nums1 []int, nums2 []int) float64 {
	ttlCount := (len(nums1) + len(nums2))
	if ttlCount == 0 {
		return float64(math.MinInt)
	}
	if ttlCount == 1 {
		if len(nums1) == 1 {
			return float64(nums1[0])
		} else {
			return float64(nums2[0])
		}
	}
	cmb := append(nums1, nums2...)

	if ttlCount == 2 {
		return (float64(cmb[0]) + float64(cmb[1])) / 2
	}

	if ttlCount == 3 {
		sort.Ints(cmb)
		return float64(cmb[1])
	}

	cur1 := 0
	cur2 := 0
	lftCount := ttlCount / 2

	// move curs to tot/2
	// for !(lftCount <= (cur1+1)+(cur2+1)) || (cur1 == 0 && cur2 == 0 && lftCount > 1) { // when odd: <, when even: ==
	for lftCount > cur1+1+cur2+1 { // when odd: <, when even: ==

		delta := lftCount - (cur1 + cur2 + 1) // size to size perspective
		rightPace := delta / 2
		if rightPace == 0 {
			rightPace = 1
		}
		lead1 := cur1 + rightPace //? delta odd
		lead2 := cur2 + rightPace //? delta odd

		// check reach limit
		if !(lead1 < len(nums1)) {
			cur2 = cur2 + delta - (len(nums1) - cur1) //eg: cur idx 0,count as 1

			//Odd: cur2 = cur2 + delta - (len(nums1) - 1 - cur1) //eg: cur idx 0,count as 1

			cur1 = len(nums1) - 1
			fmt.Printf("%d, %d, loop C: %d, %d:\n", delta, lftCount, cur1, cur2)

			continue
		}
		if !(lead2 < len(nums2)) {
			cur1 = cur1 + delta - (len(nums2) - 1 - cur2)
			cur2 = len(nums2) - cur2
			fmt.Printf("%d, loop D: %d, %d:\n", lftCount, cur1, cur2)

			continue
		}

		// vanilla: Principle is, the lower of cur must be part of the left of median
		if nums1[lead1] < nums2[lead2] {
			cur1 = lead1
			fmt.Printf("%d, loop A: %d, %d:\n", lftCount, cur1, cur2)

		} else {
			cur2 = lead2
			fmt.Printf("%d, loop B: %d, %d:\n", lftCount, cur1, cur2)

		}

		// fmt.Printf("lead  1,2: %d, %d  \n", lead1, lead2)
		// fmt.Printf("1,2: %d, %d  \n", cur1, cur2)
		// break

	}
	fmt.Println(cur1)
	fmt.Println(cur2)

	// calib med
	if (len(nums1)+len(nums2))%2 == 0 { //even
		fmt.Printf("%d, %d:\n", cur1, cur2)

		var r float64

		// edge case 1:
		// nums1 := []int{1, 2}
		// nums2 := []int{3, 4}
		if cur1*cur2 == 0 && cur1+1+cur2+1 > lftCount {
			return (math.Max(float64(nums1[cur1]), float64(nums2[cur2])) + math.Min(float64(nums1[cur1]), float64(nums2[cur2]))) / 2
		}

		l := math.Max(float64(nums1[cur1]), float64(nums2[cur2]))

		if cur1 < len(nums1)-1 && cur2 < len(nums2)-1 && nums1[cur1] > nums2[cur2] {
			r = math.Min(float64(nums1[cur1+1]), float64(nums2[cur2+1]))
		} else {
			if cur1 == len(nums1)-1 {
				r = float64(nums2[cur2+1])
			} else {
				r = float64(nums1[cur1+1])
			}
		}

		return (l + r) / 2
		// }

	} else { //odd
		// edge case: still none move, has the possbility of neither cur touch mid

		// if cur1 < len(nums1)-1 && cur2 < len(nums2)-1 && nums1[cur1] > nums2[cur2] {
		return math.Max(float64(nums1[cur1]), float64(nums2[cur2]))
		// } else {
		// 	if len(nums1)-1 == cur1 {
		// 		return float64(nums2[cur2+1])
		// 	} else {
		// 		return float64(nums1[cur1+1])
		// 	}
		// }

	}

}
