package main

// https://leetcode.com/problems/multiply-strings/description/

import (
	"fmt"
	"strconv"
)

func multiply(num1 string, num2 string) []byte {

	l1 := len(num1)
	l2 := len(num2)
	cs := make([]byte, l1+l2)

	for i1, c1 := range num1 {
		for i2, c2 := range num2 {
			fv1, _ := strconv.Atoi(string(c1))
			fv2, _ := strconv.Atoi(string(c2))

			v := fv1 * fv2

			if v >= 10 {
				higherPosV := int(cs[i1+i2]) + v/10

				cs[i1+i2+1] = byte(v%10 + int(cs[i1+i2+1]))

				cs[i1+i2] = byte(higherPosV)

			} else {
				cs[i1+i2+1] = byte(v)

			}
		}

	}
	return cs

}

func main() {

	num1 := "54"
	num2 := "31"
	/**
	  		0  1  2  3
	  		5  4
	  		3  1
	-------------------------
	    *)  1  5
	  	 	   1  2
	  	          5
	  	             4



	*/

	v := multiply(num1, num2)
	fmt.Println("val: ", v)

}
