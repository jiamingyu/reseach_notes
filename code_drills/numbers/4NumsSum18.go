package main

import (
	"fmt"
	"sort"
)


var rs = [][]int{}
var countConst = 4 //Changing this as count of numbers in sum will meet req of other N count sum, W. some tune up

func main() {

	nums := []int{1, 0, -1, 0, -2, 2}
	sort.Ints(nums)

	target := 0
	fourSumClosest(nums, target)

	fmt.Println(rs)

}

func fourSumClosest(nums []int, target int) int {
	//filter to Set
	ms := nums[0:]

	//recur
	lead := make([]int, 0)
	buildSum(ms, lead, target)

	return 1
}

var dbg = 0

func buildSum(m []int, lead []int, target int) {
	dbg += 1
	if dbg == 888 {
		panic("shit")

	}

	sum := 0
	for _, v := range lead {
		sum += v
	}

	if len(lead) == countConst && sum == target {
		rs = append(rs, lead)
		return
	}

	switch {

	case len(m) == 0:
		// Must have this to mark end of stack calib
		// fmt.Printf("exhausting crawling: %d \n", lead)
	default:
		{

			leadPlus := append(lead, m[0])
			mLess := m[1:]
			buildSum(mLess, leadPlus, target)

			leadCopy := lead
			buildSum(mLess, leadCopy, target)

		}
	}

}
