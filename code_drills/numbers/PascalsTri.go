package main

// https://leetcode.com/problems/multiply-strings/description/

import (
	"fmt"
)

func generate(numRows int) [][]int {
	rtn := make([][]int, numRows)

	// rtn[0] = make([]int, 1)
	// rtn[0][0] = 1

	for r := 0; r < numRows; r++ {
		rtn[r] = make([]int, r+1)
		rtn[r][0] = 1
		rtn[r][r] = 1

		// Solution 1: current row perspective
		// for c := 1; c < r; c++ {
		// 	rtn[r][c] = rtn[r-1][c-1] + rtn[r-1][c]
		// }

		// Solution 2: last row perspective
		lastr := r - 1
		for lastc := 0; lastc < lastr; lastc++ {
			rtn[r][lastc+1] = rtn[lastr][lastc] + rtn[lastr][lastc+1]
		}
	}

	return rtn

}

func main() {
	fmt.Println(generate(3))
	fmt.Println(generate(5))

}
