import org.junit.jupiter.api.Test;

public class MidianOf2SortedArray4 {

    @Test
    public void testFour(){
//        int[] y = {1,2};
//        int[] x = {3,4};

//            int[] x = {1,3};
//            int[] y = {2,4};

//        int[] x = {1,3};
//        int[] y = {2,4,5,6};

//        int[] x = {1,3,6};
//        int[] y = {2,4,5};

//            int[] x = {1,2,3};
//            int[] y = {4,5,6};

//        int[] x = {1};
//        int[] y = {2,3,4,5,6};


/** odd cases */

//        int[] x = {1,2,3,7};
//        int[] y = {4,5,6};

//        int[] x = {1,2,3};
//        int[] y = {4,5,6,7};

//        int[] x = {3, 4};
//        int[] y = {1,  2,5};


//        int[] x = {2, 4};
//        int[] y = {1, 3,5};

//        int[] y = { 4}; 
//        int[] x = {1,  2, 3,5};

//        int[] x = {2};
//        int[] y = {1,  3,4};

//        int[] y = {1};
//        int[] x = {2,6};
        System.out.println(findMedianSortedArraysYu(x, y));
    }

    public double findMedianSortedArraysYu(int[] nums1,int[] nums2) {
        int n = nums1.length;
        int m = nums2.length;

        if (m+n==0) {
            throw new IllegalArgumentException("Both empty colleciton ");
        }

        return convergeKthCount(nums1,  0, nums2, 0); //odd
    }

    /**
     *
     *
     * @param nums1
     * @param i1
     * @param nums2
     * @param i2
     * @return
     */
    private double convergeKthCount(int[] nums1, int i1, int[] nums2, int i2) {
        int countAll = nums1.length + nums2.length;

        // leads work as recon curs to check all scenarios b4 finally decide who move to where
        int lead1 = i1 + (countAll/2-i1-i2)/2;
        int lead2 = i2 + (countAll/2-i1-i2)/2;


        // Exit path/s
        if (countAll % 2 == 1 && i1+1+i2+1 == countAll/2 + 1) { //odd
            return nums1[i1] > nums2[i2] ? nums1[i1] : nums2[i2];
        }
        if (countAll % 2 ==0 && i1+1+i2+1 == countAll/2  ) { //even
            int r;
            int l = nums1[i1] > nums2[i2] ? nums1[i1] : nums2[i2];

            enum EventRightMedianScenario {
                BothAdvanceOk, OneEnd, TwoEnd
            }
            EventRightMedianScenario rightMedianScenario = EventRightMedianScenario.BothAdvanceOk;
            if (i1 + 1 == nums1.length)
                rightMedianScenario = EventRightMedianScenario.OneEnd;
            if (i2+1 == nums2.length)
                rightMedianScenario = EventRightMedianScenario.TwoEnd;

            switch (rightMedianScenario) {
                case OneEnd: r = Math.min(nums2[i2+1], nums2[i2+1]);break;
                case TwoEnd:  r=Math.min(nums1[i1+1], nums1[i1+2]);break;
                default : r=Math.min(nums1[i1+1] , nums2[i2+1]);

            }

            return (double) (l+r)/2;
        }
        enum Scenario {
            ONE_FULL, TWO_FULL, NEITHER_FULL
        }

        Scenario scenario;
        if (lead1 < nums1.length && lead2 < nums2.length){
            scenario = Scenario.NEITHER_FULL;
        } else if (lead1 < nums1.length){
            scenario = Scenario.TWO_FULL;
        } else
            scenario = Scenario.ONE_FULL;

        // Decide either reach ends, or which lead smaller therefore be sure to move
        switch (scenario) {
            case NEITHER_FULL:{
                return nums1[lead1] < nums2[lead2] ? convergeKthCount(nums1, lead1, nums2, i2) : convergeKthCount(nums1, i1, nums2, lead2);
            }
            case ONE_FULL:{
                lead1 = nums1.length-1;
                lead2 =  countAll /2 - nums1.length-1;//lead is index
                return convergeKthCount(nums1, lead1, nums2, lead2);
            }
            case TWO_FULL: {
                lead2 = nums2.length-1;
                lead1 =  countAll /2 - nums2.length;
                return convergeKthCount(nums1, lead1, nums2, lead2);
            }
            default: return Double.MIN_VALUE;
        }

    }

}
