/*
*
Notice: Input data is unique, the outcome result can be dupicate data sum
*/
package main

import "fmt"

func main() {
	target := 7
	cs := []int{2, 3, 6, 7}

	printOutcome(combinationSumUnique(cs, target))
}

func combinationSumUnique(candidates []int, target int) [][]int {

	r := combinationSumUniqueRecur(candidates, target, &[]int{}, 0)

	return r
}

func combinationSumUniqueRecur(candidates []int, targetOrig int, lead *[]int, dbg int) [][]int {
	rtn := make([][]int, 0)

	if targetOrig == 0 {
		return append(rtn, *lead)
	}

	for i, v := range candidates {
		target := targetOrig

		candidates2 := make([]int, len(candidates)-i-1)
		copy(candidates2, candidates[i+1:len(candidates)])

		buf := make([]int, len(*lead)) //buf: local v dup made collection
		copy(buf, *lead)

		for target-v >= 0 {

			// append another current $v
			buf = append(buf, v)
			target = target - v

			// lead2: specifice v counts, must happend after buf/target above
			lead2 := make([]int, len(buf))
			copy(lead2, buf)
			subrtn := combinationSumUniqueRecur(candidates2, target, &lead2, dbg+1)
			for _, sub := range subrtn {
				rtn = append(rtn, sub)
			}

		}
	}
	return rtn
}

func printOutcome(rs [][]int) {
	for _, r := range rs {
		fmt.Println(r)
	}

}
