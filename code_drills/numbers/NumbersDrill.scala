
import org.scalatest.flatspec.AnyFlatSpec

import scala.annotation.tailrec
import scala.collection.mutable.ListBuffer

class NumbersDrill extends AnyFlatSpec{
  "118, 119  Pascal triangle" should " 1,(1,1),(1,2,1),..." in {

    @tailrec
    def pascalTri(tierCt : Int, untilCap: Int, oldLst: List[Int]): List[Int] = {
      val lb = ListBuffer[Int]()
      lb.addOne(1)

      tierCt match {
        case 0 => {pascalTri(tierCt+1, untilCap,lb.toList)}
        case 1 => { lb.addOne(1);pascalTri(tierCt+1, untilCap,lb.toList)}
        case _ if tierCt < untilCap => {
          for (i <- 0 until oldLst.length-1) {
            lb.addOne(oldLst(i)+oldLst(i+1))
          }
          lb.addOne(1)
          pascalTri(tierCt+1, untilCap, lb.toList)
        }
        case _ if tierCt == untilCap => oldLst
        case _ => {List()}
      }
    }

    val x = pascalTri(0, 5, List())
    x.foreach(println)
  }

}
