
def findMedianSortedArrays(nums1: Array[Int], nums2: Array[Int]): Option[Double] = {

	val n1 = nums1.length; val n2 = nums2.length

	val isEven = (n1 + n2) % 2 match {
		case 0 => true
		case 1 => false
	}

	var c1 =0; var c2=0
	var subVal = Integer.MIN_VALUE

	while(c1+c2 < (n1+n2-1)/2) { // last round will have c1+c2 == (n1+n2)/2 + 1
		// println(s"xption: ${c1}, ${c2}")
		nums1(c1) - nums2(c2) match {
			case x if x < 0 && c1 < n1 - 1 => {
				subVal = nums1(c1)
				c1 = c1 + 1
			}

			case x if x < 0 && c1 == n1 - 1 => {
				subVal = nums2(c2)
				c2 = c2 + 1
			}

			case x if x >= 0 && c2 < n2 - 1=> {
				subVal = nums2(c2)
				c2 = c2 + 1
			}

			case x if x >= 0 && c2 == n2 - 1 => {
				subVal = nums1(c1)
				c1 = c1 + 1
			}

			case _ => println(s"should not reach: ${c1}, ${c2}")
		}

		// println(s"${nums1(c1)}, ${nums2(c2)}, ${subVal}")

	}

	(n1+n2)%2 match {
		case 0 if subVal != Integer.MIN_VALUE =>  Some(  (Math.max(nums1(c1), nums2(c2)) + subVal) / 2.0 )
		case 0 if subVal == Integer.MIN_VALUE => {val dv : Int = nums1(c1) + nums2(c2);Some( dv/2.0) }
		case _ => Some( Math.min(nums1(c1), nums2(c2)) )
	}

}

// val nums1 = Array(2, 6, 7)
// val nums2 = Array(1,3);

val nums1 = Array(2, 8)
val nums2 = Array(1, 9);


val r = findMedianSortedArrays(nums1, nums2)
println(r)
