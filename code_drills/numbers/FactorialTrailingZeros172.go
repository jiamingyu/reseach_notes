package main

// https://leetcode.com/problems/factorial-trailing-zeroes/

import (
	"fmt"
	"math"
)

func trailingZeroes(n int) int {
	trailingSum := 0

	// detect 5's exponent level
	e := 1
	fmt.Println(trailingSum)

	for math.Pow(5, float64(e-1)) < float64(n) { // use 'e-1' to make sure last round still being able to exec
		fmt.Println("toadd: ", n/int(math.Pow(5, float64(e))))
		trailingSum += n / int(math.Pow(5, float64(e)))
		e += 1
	}

	return trailingSum
}

func main() {
	// fmt.Println(trailingZeroes(3))
	// fmt.Println(trailingZeroes(5))
	// fmt.Println(trailingZeroes(25))
	fmt.Println(trailingZeroes(26))

	// fmt.Println(trailingZeroes(13))

}
