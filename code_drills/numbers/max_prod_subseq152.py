
def maxProduct(nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        l = 0
        r = 1
        buf = nums[l] * nums[r] #sys.minint
        max = buf
        while(l < r and r < len(nums)-1):
        
            print("buf is {3},l is {0}, r is {1}, max is {2}".format(l, r, max, buf))
            
            #  
            if buf*nums[r+1] > max:
                r +=1
                max = buf*nums[r]
                
            else :
                buf = buf / nums[l]
                l += 1
                if (l==r and r < len(nums)-1): # corner case
                    r+=1
                
                max = buf if buf > max else max
                

# nums = [2,3,-2,4] # should 6
nums = [-2,0,-1] # should 0

print(maxProduct(nums))
