package net.jyu.scalademo

import org.scalatest.flatspec.AnyFlatSpec

import scala.collection.mutable
import scala.util.control.Breaks.break

class NSumDrill extends  AnyFlatSpec{
  trait TwoSum {
    val d2count = mutable.Map[Int, Int]()
    var sortedSetOpt : Option[Array[Int]] = None
    def add(d: Int)(implicit resort: Boolean = false)
    def find(sum: Int): Boolean
    def readAll(ds: Array[Int])(implicit isSorted: Boolean = false)

  }

  case class TwoSumWOSorted() extends  TwoSum {

    override def add(d: Int)(implicit resort: Boolean = false): Unit = {
      d2count.put(d, d2count.getOrElseUpdate(d, +1))
      if (resort)
        sortedSetOpt = Some(d2count.keySet.toArray.sorted)
    }


    override def find(sum: Int): Boolean = {

      sortedSetOpt match {
        case None => return false
        case _ => {}
      }
      var idx = sortedSetOpt.get.size - 1
      var loop = true

      while(loop && idx > 0 && sortedSetOpt.get(idx) > sum) {
        idx match {
          case idx2 if sortedSetOpt.get(sortedSetOpt.get.size - 1) <= sum => return false
          case idx2 if sortedSetOpt.get(0) >= sum => false
          case idx2 if sortedSetOpt.get((idx + 1) / 2) > sum  => {
            idx = idx / 2
          }
          case idx2 if sortedSetOpt.get((idx + 1) / 2) <= sum => {
            loop = false
          }

          case _ => {
            println(s"should not reach: $idx")
            break()
          }
        }
      }

      for (d <- sortedSetOpt.get.slice(0, idx + 1)) {
//      for(d <- d2count.keySet) {//If key not pre-sorted:
        val delta = sum - d
        delta match {
          case d2 if d2 <= 0 => {}
          case d2 if d2==d && d2count.get(d).get > 1 => {println(s"$d & $d");return true}
          case d2 if d2count.keySet.contains(d2) => {println(s"$d & $d2"); return true}
          case _ => {}
        }
      }
      false
    }

    override def readAll(ds: Array[Int])(implicit isSorted: Boolean=false): Unit = {
      for(d <- ds) {
        add(d)
      }
      sortedSetOpt = if (isSorted) Some(ds) else Some(d2count.keySet.toArray.sorted)
    }
  }

  "1 unsorted input for Two Sum: (1,5,3) asking for 4 & 7" should " return true & false" in {
    val tss = TwoSumWOSorted()
    tss.add(3)(true)
    tss.add(1)(true)
    tss.add(5)(true)
    println(tss.find(4))
//    println(tss.find(7))
  }

  "1 unsorted input for Two Sum: (1,5,3) asking for  7" should " return false" in {
    val tss = TwoSumWOSorted()
    tss.add(3)(true)
    tss.add(1)(true)
    tss.add(5)(true)
    println(tss.find(7))
  }


  "167 Sorted input for Two Sum: (1,3,5) asking for 4 & 7" should " return true & false" in {
    val tss = TwoSumWOSorted()
    tss.readAll(Array(1,3,5))(true)
    println(tss.find(4))
    println(tss.find(7))

  }

  "167 Sorted input for Two Sum: (2,7,11,15) asking for 9" should " return true" in {
    val tss = TwoSumWOSorted()
    tss.readAll(Array(2,7,11,15))(true)
    println(tss.find(9))

  }

  "167 Sorted input for Two Sum: (2,3,4) asking for 6" should " return true " in {
    val tss = TwoSumWOSorted()
    tss.readAll(Array(2, 3, 4))(true)
    println(tss.find(6))
  }

}
