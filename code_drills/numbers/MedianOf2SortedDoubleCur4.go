package main

import (
	"fmt"
)

var wip bool = false

func main() {
	nums1 := []int{1, 3} //[]int{1} also pass
	nums2 := []int{2, 4}
	fmt.Println(findMedianSortedArrays(nums1, nums2))

}

func findMedianSortedArrays(nums1 []int, nums2 []int) float64 {
	return doubleCursFindMed(nums1, nums2, 0, 0, 0)
}

func doubleCursFindMed(nums1 []int, nums2 []int, i1, i2 int, dbg int) float64 { // leave dbg here to demo safe guard recur dev
	fmt.Printf("idxes: %d, %d:  \n", i1, i2)

	if wip && dbg > 5 {
		panic("wrong impl")
	}

	if i1+i2+1 >= (len(nums1)+len(nums2))/2 { //index should plus 1, instead of 2 since lens comb divided into half
		f1 := float64(nums1[i1])
		f2 := float64(nums2[i2])
		if (len(nums1)+len(nums2))%2 == 1 {
			if f1 > f2 {
				return f2
			} else {
				return f1
			}
		} else {
			return (f1 + f2) / 2
		}
	}

	if nums1[i1] < nums2[i2] {
		return doubleCursFindMed(nums1, nums2, i1+1, i2, dbg+1)
	} else {
		return doubleCursFindMed(nums1, nums2, i1, i2+1, dbg+1)
	}
}
