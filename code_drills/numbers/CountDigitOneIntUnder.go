package main

https://leetcode.wang/leetcode-233-Number-of-Digit-One.html

import (
	"fmt"
)

func countDigitOne(n int) int {
	m := make(map[int]int)

	c := 1
	for c < n {
		fmt.Println(c)
		switch {
		case c*10 >= n:
			{
				if n-n%c == c { // starting w. left most as "1"
					m[c] = c/10 + n%c + m[c/10]
					/**
					eg: for 1300..
					1000.., 1100..., 1200..., 1300... =>   n%c
					under 1100.., each get exta "1" => c/10
					cached counts under (0)100.. => m[c/10]
					*/
					
				}
				break
			}
		case c > 1 && c/10 > 1 && c*10 < n:
			m[c] = 10*m[c/100] + m[c/10]
		default:
			m[1] = 1
		}
		c *= 10
	}
	fmt.Println(m)

	return 1

}

func main() {
	fmt.Println(countDigitOne(13))

}
