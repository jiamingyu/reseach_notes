package main

// https://leetcode.com/problems/maximum-product-subarray/

import (
	"fmt"
)

func maxProductSubArray(nums []int) int {
	if len(nums) == 1 {
		return nums[0]
	}

	if len(nums) == 0 {
		return 0
	}

	l := 0
	rtn := -1

	for l < len(nums)-1 {
		buf := 1

		for i := l; i < len(nums); i++ {
			if buf < buf*nums[i] {
				buf = buf * nums[i]
			} else {
				if rtn < buf {
					rtn = buf
				}
				break
			}
		}
		l++
	}

	return rtn

}

func main() {

	ds := []int{2, 3, -2, 4}

	fmt.Print(maxProductSubArray(ds))

}
