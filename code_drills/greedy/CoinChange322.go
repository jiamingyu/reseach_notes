package main

import (
	"fmt"
	"sort"
)

func coinChange(coins []int, amount int) int {
	sort.Sort(sort.Reverse(sort.IntSlice(coins)))
	countTrack := make([]int, len(coins))
	return makeCountMatchOnlyOptism(countTrack, 0, coins, amount)

}

func makeCountMatchOnlyOptism(countTrackOrig []int, count int, coins []int, amount int) int {
	//block slice sharing
	countTrack := make([]int, len(countTrackOrig))
	copy(countTrack, countTrackOrig)

	fmt.Println(amount)
	if amount == 0 {
		return count
	}

	if len(coins) == 0 {
		return -1
	}

	ctLimit := amount / coins[0]
	considered := -1

	if !(ctLimit > 0) {
		return makeCountMatchOnlyOptism(countTrack, count, coins[1:], amount)
	}

	for ct := ctLimit; ct > 0; ct-- {
		cur := len(countTrack) - len(coins)
		countTrack[cur] = ct

		considered = makeCountMatchOnlyOptism(countTrack, count+ct, coins[1:], amount%coins[0])

		if len(countTrack) == len(coins) && considered != -1 { // big value coins has preferences to win
			fmt.Println("result track at top level ... ... ...", considered)
			return considered
		}

		unconsidered := makeCountMatchOnlyOptism(countTrack, count, coins[1:], amount)

		fmt.Println("considered vs unconsidered: ", considered, unconsidered)

		switch {
		case considered > 0 && unconsidered > 0 && considered < unconsidered:
			return considered
		case considered > 0 && unconsidered > 0 && considered >= unconsidered:
			{
				return unconsidered
			}
		case considered > 0 && unconsidered == -1:
			{
				fmt.Println("ooooop", countTrack)
				return considered

			}
		case considered == -1 && unconsidered > 0:
			return unconsidered
		default:
			return -1

		}

	}

	fmt.Println("tracking why coming here")
	return -1
}

func main() {
	coins := []int{2, 1, 5}
	amount := 11

	fmt.Println("total count, if not -1,  is :  ", coinChange(coins, amount))
}
