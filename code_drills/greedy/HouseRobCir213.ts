//https://leetcode.wang/leetcode-213-House-RobberII.html

// When circle, ref first robbed y/n to decide if consider last to be robbed

function robcirRecur(nums: number[] , startIdx : number ): number {
    // if (nums.length == 1)
    //     return nums[0]

    // if (nums.length==2)
    //     return Math.max(nums[0], nums[1])

    if (startIdx == -1) {
        if (nums.length == 1)
            return nums[0]
    
        if (nums.length==2)
            return Math.max(nums[0], nums[1])

        let maxV = Math.max(
            robcirRecur(nums.slice(2), 0) + nums[0], 
            robcirRecur(nums.slice(1), 1) 
        )
        console.log(`top tier voted max: ${maxV}`)
        return maxV
    } else {
        if (nums.length == 1) {
            if (startIdx == 0){
                return 0 
                // You can not rob last since first has been robbed
            } else {
                return nums[0]
            }
        }
    
        if (nums.length==2){
            if (startIdx == 0){
                // You can not rob last since first has been robbed
                return nums[0]
            } else {
                return Math.max(nums[0], nums[1])
            }
        }


        let maxV = Math.max(
            robcirRecur(nums.slice(2), startIdx) + nums[0], 
            robcirRecur(nums.slice(1), startIdx) 
        )
        console.log(`${nums} voted max: ${maxV}`)
        return maxV
    }


}


// const numsrobcir = [2,3,2]
const numsrobcir = [1,2,3,1]

console.log(robcirRecur(numsrobcir, -1))
