package main

import (
	"fmt"
)

func removeKdigits(s string, k int) string {
	if k == len(s) {
		return "0"
	}

	ds := make([]int, len(s))
	for idx, ss := range s {
		v := int(ss) - int('0')
		ds[idx] = v
	}

	rmIdxes := make([]int, 0)
	k2 := k + 1
	lIdx := 0

	for k2 > 1 {
		minIdx := lIdx

		for idx := lIdx; idx < lIdx+k2; idx++ {
			if ds[minIdx] > ds[idx] {
				minIdx = idx
			}
		}

		if lIdx == minIdx { //leftmost is the min, it is (k+1)'s one, therefore not consider in next round
			lIdx = minIdx + 1

		} else {
			for idx := lIdx; idx < minIdx; idx++ {
				rmIdxes = append(rmIdxes, idx)
				k2--
			}

			lIdx = minIdx
		}

	}

	rtn := ""
	bufIdx := 0
	for _, rmIdx := range rmIdxes {
		buf := s[bufIdx:rmIdx]

		rtn = rtn + buf
		bufIdx = rmIdx + 1
	}
	rtn = rtn + s[bufIdx:]

	// 0 head check
	keepAfterIdx := -1
	noneZeroShow := false
	for idx, v := range rtn {
		if v == '0' && !noneZeroShow {
			keepAfterIdx = idx
		} else {
			noneZeroShow = true
		}
	}

	return rtn[keepAfterIdx+1:]
}

func main() {
	s := "10"
	k := 2
	// s := "1432219";k := 3
	// s := "10200";k := 1

	fmt.Println(removeKdigits(s, k))
}
