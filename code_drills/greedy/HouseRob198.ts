// https://leetcode.com/problems/house-robber/description/

function rob(nums: number[]): number {
    if (nums.length == 1)
        return nums[0]

    if (nums.length==2)
        return Math.max(nums[0], nums[1])


    let maxV = Math.max(
        rob(nums.slice(2)) + nums[0], 
        rob(nums.slice(1)) 
    )

    console.log(`${nums} voted max: ${maxV}`)


    return maxV
    
};

// const numsrob = [1,2,3,1]
const numsrob = [2,7,9,3,1]

console.log(rob(numsrob))
