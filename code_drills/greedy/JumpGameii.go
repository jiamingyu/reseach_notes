package main

import (
	"fmt"
)

/*
*
https://leetcode.com/problems/jump-game-ii/description/

Note:

It doesn't matter which idx under radar reached rightMost. Next step just will based on the rightmost idx
*/
func jump(nums []int) int {
	//starting as 1 because it will first jump to any idx under nums[0] radar
	jumpCt := 1
	curRadarLimit := nums[0]
	rightMostReachedByRadarIdxRecon := nums[0]

	for idx, dlta := range nums {

		if idx+dlta > rightMostReachedByRadarIdxRecon {
			rightMostReachedByRadarIdxRecon = idx + dlta
		}

		if rightMostReachedByRadarIdxRecon >= len(nums)-1 {
			fmt.Println("idx ", idx, " reached to last step declaring done")
			return jumpCt + 1 // the last recon now should be impl & counted
		}

		if idx == curRadarLimit {
			fmt.Println("current idx ", idx, "'s radar range will move on to ", rightMostReachedByRadarIdxRecon, "with one step increment ")
			jumpCt += 1
			curRadarLimit = rightMostReachedByRadarIdxRecon
		}

	}

	return jumpCt
}

func main() {
	// ds := []int{2, 3, 1, 1, 4}
	// ds := []int{2, 3, 0, 1, 4}
	ds := []int{2, 4, 1, 1, 10, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}

	fmt.Println(jump(ds))
}
