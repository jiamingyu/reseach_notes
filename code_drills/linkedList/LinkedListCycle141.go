package main

import (
	"encoding/json"
	"fmt"
)

func main() {
	var cur *ListNode
	var hd *ListNode

	cycleEntrIdx := 1
	var cycleEntr *ListNode

	ds := []int{1, 2, 3, 4}
	// ds := []int{1, 2, 3}
	// ds := []int{1, 2}
	// ds := []int{1}

	for idx, v := range ds {
		tmpV := &ListNode{V: v}
		if cycleEntrIdx == idx {
			cycleEntr = tmpV
		}

		if idx == 0 {
			hd = tmpV
			cur = hd
		} else {
			cur.Next = tmpV
			cur = cur.Next
		}

		// Enable cycle
		if cycleEntr != nil {
			cur.Next = cycleEntr
		}

	}

	if hasCyclefastSlow(hd) {
		fmt.Println("detected cycle")
	} else {
		v, e := json.Marshal(*hd)
		if e == nil {
			fmt.Println(string(v))
		} else {
			fmt.Println(e)
		}
	}

}

func hasCyclefastSlow(head *ListNode) bool {
	slow := head
	fast := head

	for slow.Next != nil && fast.Next != nil && fast.Next.Next != nil {

		slow = slow.Next
		fast = fast.Next.Next
		fmt.Println("slow & fast:  ", slow.V, fast.V)

		if slow == fast {
			return true
		}

	}

	return false

}

/** End of recursive solution */

type ListNode struct {
	V    int
	Next *ListNode
}
