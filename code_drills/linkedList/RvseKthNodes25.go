/*
* This is for 25: https://leetcode.com/problems/reverse-nodes-in-k-group/

But when reverse size = 2, it also resolve 24
https://leetcode.com/problems/swap-nodes-in-pairs/description/
 */
 package main

 import (
	 "fmt"
 )
 
 func main() {
	 headArray := []int{1, 2, 3, 4, 5, 6, 7}
	 k := 3
 
	 var head *ListNode
	 var cur *ListNode
	 for i, v := range headArray {
		 if i == 0 {
			 head = &ListNode{v: v, next: new(ListNode)}
			 cur = head.next
		 } else {
			 cur.v = v
			 cur.next = new(ListNode)
			 cur = cur.next
		 }
	 }
 
	 //// TBD: study why original passin head on upper stack still point to old
	 // head = reverseStartXEnd(head, k)
	 head = reverseKGroup(head, k)
 
	 // Validate
	 head.printAll()
 
 }
 
 /**
 *		pointer parameter passing in is passing address value copy, meaning "finding pointed value through copy of address value"
 */
 func reverseStartXEnd(head *ListNode, reverseSize int) *ListNode {
	 if head.next == nil {
		 return head
	 }
 
	 nh := head
	 w := nh.next
	 wnext := w.next
 
	 for i := 1; i < reverseSize; i++ { // start w. 1 since above already executed once
		 fmt.Printf("new head is : %d \n", nh.v)
		 w.next = nh
		 nh = w // round 1, head still to original head
 
		 // prepare 4 next round
		 w = wnext
		 wnext = wnext.next //check next
	 }
 
	 head.next = w //connect head.next to last w (its index isi2x excluded)
	 head = nh
 
	 return nh
 }
 
 func reverseKGroup(head *ListNode, k int) *ListNode {
	 if k <= 1 {
		 return head
	 }
	 f := head
	 kd := k
	 for kd > 1 && f != nil && f.next != nil {
		 fmt.Printf("f value .....:  %d \n", f.v)
		 f = f.next
 
		 kd -= 1
	 }
 
	 head.printAll()
 
	 if f.next == nil {
		 fmt.Println("  blllllll")
	 }
 
	 if f.next != nil {
		 f.next = reverseKGroup(f.next, k)
		 return reverseStartXEnd(head, k)
	 } else {
		 f.next = reverseKGroup(f.next, k-kd)
		 return reverseStartXEnd(head, k-kd)
	 }
 }
 
 type ListNode struct {
	 v    int
	 next *ListNode //nil
 }
 
 func (ln ListNode) printAll() {
	 fmt.Printf("head value is %d\n", ln.v)
	 var pt *ListNode = &ln
 
	 for pt.next != nil {
		 fmt.Printf("cursor value is %d \n", pt.v)
		 pt = pt.next
	 }
 
 }
 