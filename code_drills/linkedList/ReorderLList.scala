import org.scalatest.flatspec.AnyFlatSpec

import scala.annotation.tailrec
import scala.collection.mutable
import scala.collection.mutable.{ArrayBuffer, ListBuffer}

class ReorderLList extends  AnyFlatSpec{
  
  case class ListNode(v: Int, var next: ListNode) {
    override def toString: String = s"value is $v"

  }

  @tailrec
  final def tailRecList(cur: ListNode, stk: mutable.Stack[ListNode]) : Unit = {
    val popped = stk.pop()
    val buf =  if (stk.isEmpty) null else cur.next // detect last popped

      cur.next = popped
    popped.next = buf

    if (buf == null)
      return
    else
      tailRecList(popped.next, stk)
  }

  def reorderList(d: List[Int]): Unit = {

    var head = makeList(d)
    //    var head = makeList(List(4,2,5,1,3))

    var fast = head
    var slow = head

    while (fast.next != null) {
      fast = fast.next
      if (fast.next != null) { //even count scenario. odd count just skip double move for last
        fast = fast.next
      }
      slow = slow.next
    }
    // now slow should be: A. even count, @first of 2nd half. B. Odd count, mid of collection

    val stk = mutable.Stack[ListNode]()

    while (slow != null) {
      stk.push(slow)
      slow = slow.next
    }

    val cur = head
    tailRecList(cur, stk)

    while (head != null) {
      println(head.v)
      head = head.next
    }

  }

  "143 reorder both ends twd intermittent List(4,2,1,3) " should "bla" in {
    val d = List(4,2,1,3)
    reorderList(d)
  }

  "143 reorder both ends twd intermittent List(4,2,5,1,3) " should "bla" in {
    val d = List(4, 2, 5, 1, 3)
    reorderList(d)

  }

  def makeList(value: List[Int]): ListNode = {
    var rtn : ListNode = null
    var itr : ListNode = null

    var ct = 0
    for(v <- value ) yield {

      if (ct == 0) {
        rtn = ListNode(v, null)
        itr = rtn
      } else {

        itr.next = ListNode(v, null)
        itr = itr.next
      }
      ct+=1
    }
    rtn
  }


}