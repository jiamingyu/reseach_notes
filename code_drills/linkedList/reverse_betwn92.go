package main

import (
	"encoding/json"
	"fmt"
)

func main() {
	var tl *LinkListNode
	var hd *LinkListNode

	ds := []int{1, 2, 3, 4}
	// ds := []int{1, 2, 3}
	// ds := []int{1, 2}
	// ds := []int{1}

	for idx, v := range ds {
		if idx == 0 {
			hd = &LinkListNode{V: v}
			tl = hd
		} else {
			tl.Next = &LinkListNode{V: v}
			tl = tl.Next
		}

	}

	headNode := reverseBetween(hd, 1, 2)

	v, e := json.Marshal(*headNode)
	if e == nil {
		fmt.Println(string(v))
	} else {
		fmt.Println(e)
	}

}

func reverseBetween(head *LinkListNode, left int, right int) *LinkListNode {
	//assert right < len()
	cur := head
	// var prev *LinkListNode
	for ct := 0; ct < right; ct++ {
		if ct+1 >= left {
			p := cur.Next
			if p.Next == nil {
				break
			}

			q := p.Next

			p.Next = q.Next
			q.Next = cur.Next //p
			cur.Next = q

			cur = cur.Next // since one swapped to cur.Next, just skip 1
		}
		cur = cur.Next
	}

	return head

}

/** End of recursive solution */

type LinkListNode struct {
	V    int
	Next *LinkListNode
}
