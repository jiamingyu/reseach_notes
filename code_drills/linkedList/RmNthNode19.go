package main

import (
	"fmt"
)


func main() {
	ns := []int{1, 2, 3, 4, 5}
	idxRvs := 2

	prehead := ListNode{v: -100, next: new(ListNode)}

	var pt *ListNode = &prehead
	for _, v := range ns {
		pt = pt.next // at this time pt.next must NOT nil(u can understand nil as some static value)

		pt.next = new(ListNode) // same above
		pt.v = v
	}

	removeNthFromEnd(&prehead, idxRvs)

	prehead.next.printAll()

}

type ListNode struct {
	v    int
	next *ListNode //nil
}

func (ln ListNode) printAll() {
	fmt.Printf("head value is %d\n", ln.v)
	var pt *ListNode = &ln

	for pt.next != nil {
		fmt.Printf("cursor value is %d \n", pt.v)
		pt = pt.next
	}

}

func removeNthFromEnd(head *ListNode, nth int) *ListNode {
	var f *ListNode = head
	var s *ListNode = head //target dest's parent
	ct := 0                //count
	for ct < nth+1 {
		f = f.next
		ct += 1
	}

	for f.next != nil { //since above ct is $nth+1 value,making s pointing to parent
		f = f.next
		s = s.next
	}
	fmt.Printf("Verify slow pointing to target pos's parent: %d  \n", s.v)
	s.next = s.next.next
	return head

}
