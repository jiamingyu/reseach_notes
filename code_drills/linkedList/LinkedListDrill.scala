package net.jyu.scalademo

import org.scalatest.flatspec.AnyFlatSpec

import scala.annotation.tailrec
import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer



class LinkedListDrill extends AnyFlatSpec{

  def cloneNode(n: RandomListNode[Int]): RandomListNode[Int] = {
    new RandomListNode[Int](n.v, null)

  }

  "138 " should "Input: head = [[7,null],[13,0],[11,4],[10,2],[1,0]]\n" +
    "Output: [[7,null],[13,0],[11,4],[10,2],[1,0]]" in {
    val rdm : List[(Int, Option[Int])]  = List((7,None),(13,Some(0)),(11,Some(4)),(10,Some(2)),(1,Some(0)))

    val head = makeRandomLinkedList(rdm)

    copyRandomList(head)

  }

  def makeRandomLinkedList(value: List[(Int, Option[Int])]): RandomListNode[Int] = {
    var rtn: RandomListNode[Int] = null
    var itr: RandomListNode[Int] = null
    val buffer = new ArrayBuffer[RandomListNode[Int]](value.length)

    var ct = 0
    for (v <- value) yield {

      if (ct == 0) {
        rtn = RandomListNode(v._1, null)
        itr = rtn
      } else {
        itr.next = RandomListNode(v._1, null)
        itr = itr.next
      }
      buffer.addOne(itr)
      ct += 1
    }


    val rdmItrs = value.map{case (n, r)=>r}
    var tmp = rtn
    for(rdmOpt <- rdmItrs) yield {
      rdmOpt match {
        case None => tmp = tmp.next
        case sr  => {
          tmp.random = Some(buffer(sr.get))
          tmp = tmp.next
        }
      }
    }
    rtn
  }

  def copyRandomList(head: RandomListNode[Int]): RandomListNode[Int] = {

    val old2new = mutable.Map[RandomListNode[Int], RandomListNode[Int]]()
//    val clonedBuffer = new ArrayBuffer[RandomListNode[Int]](rdm.length)

    val rdmIdxs = new ArrayBuffer[Option[Int]]()


    var itr = head
    var prior = head
    val stk = mutable.Stack[RandomListNode[Int]]()

    while (itr.next != null) {
      stk.push(itr)
      val rdmIdx = itr.random match {
        case None => None
        case Some(n) => n
      }
//      rdmIdxs.addOne(itr.random)
      itr = itr.next
    }

    var oldc: RandomListNode[Int] = null
    var copiedNode: RandomListNode[Int] = null
    while (!stk.isEmpty) {
      val originalNode = stk.pop()
      copiedNode = cloneNode(originalNode)

      if (oldc != null)
        copiedNode.next = oldc

      old2new.put(originalNode, copiedNode)

      oldc = copiedNode
    }
    val newHead = copiedNode

    var itr2 : RandomListNode[Int] = head
    var itr2New = newHead
    while (itr2.next != null)
    {

      val newRdm = itr2.random match {
        case Some(old) => {old2new(itr).random = Some(old2new(old))}
        case None =>
      }

      itr2 = itr2.next
    }

    newHead
  }

  "86 " should "" in {
    val d = List(1,4,3,2,5,2)

    //incremenal tests pass
    //    val d = List(1,4,3,2)
    //    val d = List(1,4,2,3)
    //    val d = List(1,4,3)
    //    val d = List(1,3,2)
    var h =partition(makeLinkedList(d), 3 )

    while (h != null) {
      println(h.v)
      h = h.next
    }
  }

  def partition(head: ListNode[Int], target: Int): ListNode[Int] = {
    val ahead = ListNode(-100, head)

    var targetP = ahead
    // anchor target parent
    while(targetP.next.v != target)
      targetP = targetP.next

    var curP = ahead
    var reachedTarget = false
    while(curP != null && curP.next != null) {
      (curP.next.v <= target, reachedTarget) match {
        case (true, false) => // $cur (L) <= $target (R)
        case (false, true) => // $target (L) < $cur (R)
        case (true, true) => // $cur (R) <= $target (L)
          val cur = curP.next
          val target = targetP.next

          //move cur pointed to the left side of  target
          curP.next = cur.next
          targetP.next = cur
          cur.next = target

        // $cur(L) > $taget(R), cur node move to right next of target
        // cur is NOT target neighbour
        case (false, false) if curP.next != targetP =>
          val cur = curP.next
          val futureCurPNext = cur.next //no null since target still on its right side
          val target = targetP.next

          cur.next = target.next
          target.next = cur

          //cur parent heal hole
          curP.next = futureCurPNext

        // $cur(L) > $taget(R), cur node move to right next of target
        // cur & target neighbour, meaning $cur is $targetP
        case (false, false) if curP.next == targetP =>
          val cur = curP.next
          val futureCurPNext = curP.next.next //TODO: null check
          val target = targetP.next

          cur.next = target.next
          target.next = cur

          curP.next = futureCurPNext
          // Now, since cur is targetP, targetP must be reset
          targetP = curP
      }

      if(curP.v == target || curP.next != null && curP.next.v != null && curP.next.v == target) {
        reachedTarget = true
      }
      curP = curP.next
    }
    ahead.next
  }

  "leet 82 keep dup distinct val [1,2,3,3,4,4,5]" should "" in {
    val d = List(1,2,3,3,4,4,5)
    val head = makeLinkedList(d)
    val ahead : ListNode[Int] = new ListNode(-100, head)

    detectDuplicatesWKeepDistinceFlag(ahead)

    var h = ahead.next
    while (h != null) {
      println(h.v)
      h = h.next
    }

  }

  "leet 83 remove dup in  val [1,2,3,3,4,4,5]" should "" in {
    val d = List(1, 2, 3, 3, 4, 4, 5)
    val head = makeLinkedList(d)
    val ahead: ListNode[Int] = new ListNode(-100, head)

    detectDuplicatesWKeepDistinceFlag(ahead, false)

    var h = ahead.next
    while (h != null) {
      println(h.v)
      h = h.next
    }

  }

  def detectDuplicatesWKeepDistinceFlag[T](head: ListNode[T], keepDistinct: Boolean = true): ListNode[T] = {
    var lparent = head // need to place to parent of l
    var rparent = if (lparent.next != null ) lparent.next  else null

    while(rparent != null && rparent.next != null) {
      while (rparent.next != null && lparent.next.v == rparent.next.v) {
        rparent = rparent.next
      }

      if (lparent.next.next != rparent.next) {
        if (keepDistinct && rparent.next != null) { //?? r ==null
          lparent.next.next = rparent.next

        } else {
          lparent.next = rparent.next
        }
      } else {
        lparent = lparent.next
      }
      rparent = if (lparent.next != null ) lparent.next  else null
    }
    head.next
  }

  "swapNodeNext (1,2,3,4) of 1->(2), 3->(4)" should "should be (1,4,3,2)" in {
    val hv = makeLinkedList( List(1,2,3,4) )
    swapNodeNextPointed(hv, hv.next.next) // swap 2 & 4

    var h = hv
    while(h!= null) {
      println(h.v)
      h = h.next
    }
  }

  "Now for 24, swap right/left nbr, swapNodeNext (1,2,3,4) of ->1, 1->" should "should be (2,1,3,4)" in {
    val head = makeLinkedList(List(1, 2, 3, 4))

    var h = swapPairs(head)
    while (h != null) {
      println(h.v)
      h = h.next
    }
  }

  def swapPairs(head: ListNode[Int]): ListNode[Int] = {
    val pointing2Head : ListNode[Int] = ListNode(-100, head) //must create an extra listNode pointing to head

    var cur = pointing2Head
    while (cur != null) {

      swapNbrNodeLeftPointedNext(cur, cur.next)
      cur = if (cur.next != null && cur.next.next != null && cur.next.next.next != null) //cur.next.next.next: right node's next pointed
        cur.next.next
      else null
    }
    return pointing2Head.next

  }

  def swapNbrNodeLeftPointedNext[T](a: ListNode[T], b: ListNode[T]): Unit = {
    val nextBufA = a.next
    val nextBufB = b.next
    val nextBufBnext = nextBufB.next

    a.next = nextBufB
    nextBufB.next = nextBufA //!!: This is the delta with scenario that A/B not next to each other

    b.next = nextBufA
    nextBufA.next = nextBufBnext //Pay attention !!
  }

  def swapNodeNextPointed(a: ListNode[Int], b: ListNode[Int]): Unit = {
    val nextBufA = a.next
    val nextBufB = b.next
    val nextBufBnext = nextBufB.next

    a.next = nextBufB
    nextBufB.next = nextBufA.next

    b.next = nextBufA
    nextBufA.next = nextBufBnext //Pay attention !!
  }

  "leet 92 reverse 1,2,3,4" should "4,3,2,1" in {
    val d = makeLinkedList( List(1,2,3,4) )
    var r = reverseOrderWStack(d)
    while(r != null) {
      println(r.v)
      r = r.next
    }
  }

  def reverseOrderWStack[T](head: ListNode[T]) = {
    var cur = head
    val stk = mutable.Stack[ListNode[T]]()

    while (cur != null) {
      stk.push(cur)
      cur = cur.next
    }

    val rtn : ListNode[T] = if (stk.nonEmpty) stk.pop() else null
    var cur2 = rtn

    while(stk.nonEmpty) {
      cur2.next = stk.pop()
      cur2 = cur2.next
      cur2.next = null

    }
    rtn
  }

  def reverseOrderOnlyByNextPointer[T](head: ListNode[T]) : ListNode[T] = {
    var rtn = head
    var futureRtn : ListNode[T] = rtn.next

    while(rtn != null && futureRtn != null) {
      val lastRtn = rtn

      rtn = futureRtn
      futureRtn = rtn.next

      rtn.next = lastRtn // original head, its next.next == itsself
      if (lastRtn == head)
        lastRtn.next = null

    }
    rtn
  }

  " leet 92 reverse 1,2,3,4 purly by next pointer switch" should "4,3,2,1" in {
    val d = makeLinkedList(List(1, 2, 3, 4))
    var r = reverseOrderOnlyByNextPointer(d)
    while (r != null) {
      println(r.v)
      r = r.next
    }
  }


  @tailrec
  final def tailRecList[T](cur: ListNode[T], stk: mutable.Stack[ListNode[T]]): Unit = {
    val popped = stk.pop()
    val buf = if (stk.isEmpty) null else cur.next // detect last popped

    cur.next = popped
    popped.next = buf

    if (buf == null)
      return
    else
      tailRecList(popped.next, stk)
  }

  def makeLinkedList(value: List[Int]): ListNode[Int] = {
    var rtn: ListNode[Int] = null
    var itr: ListNode[Int] = null

    var ct = 0
    for (v <- value) yield {

      if (ct == 0) {
        rtn = ListNode(v, null)
        itr = rtn
      } else {

        itr.next = ListNode(v, null)
        itr = itr.next
      }
      ct += 1
    }
    rtn
  }


  case class ListNode[T](v: T , var next: ListNode[T]) {
    override def toString: String = s"value is $v"
  }

  case class RandomListNode[T](v: T, var next: RandomListNode[T], var random: Option[RandomListNode[T]]=None) {
    override def toString: String = s"value is $v"
  }
}
