package main

import (
	"encoding/json"
	"fmt"
)

func main() {
	var tl *LinkedList
	var hd *LinkedList
	ds := []int{1, 2, 3}
	// ds := []int{1, 2}
	// ds := []int{1}

	for idx, v := range ds {
		if idx == 0 {
			hd = &LinkedList{V: v}
			tl = hd
		} else {
			tl.Next = &LinkedList{V: v}
			tl = tl.Next
		}

	}

	// Different approaches
	// headNode := reverseLinkedListRecur(hd, tl)
	headNode := reverseLinkedListInterative(hd)

	v, e := json.Marshal(*headNode)
	if e == nil {
		fmt.Println(string(v))
	} else {
		fmt.Println(e)
	}

}

func reverseLinkedListInterative(hd *LinkedList) *LinkedList {
	dummy := &LinkedList{V: -100, Next: hd}
	p := hd

	for p != nil && p.Next != nil {
		q := p.Next
		p.Next = q.Next
		q.Next = dummy.Next
		dummy.Next = q

		fmt.Println(*p)
	}

	return dummy.Next
}

/** Recursive solution   */
func reverseLinkedListRecur(head *LinkedList, tail *LinkedList) *LinkedList {

	h, _ := doReverse(head, tail)
	return h

}

func doReverse(head *LinkedList, tail *LinkedList) (*LinkedList, *LinkedList) {
	if head == tail {
		return head, tail
	}
	if head.Next == nil {
		return head, tail
	}

	fmt.Println(head.V, tail.V)

	head2 := head.Next
	head.Next = nil

	h3, t3 := doReverse(head2, tail)
	t3.Next = head
	return h3, head

}

/** End of recursive solution */

type LinkedList struct {
	V    int
	Next *LinkedList
}
