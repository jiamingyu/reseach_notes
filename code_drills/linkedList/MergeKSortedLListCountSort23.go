package main

import (
	"fmt"
)

func main() {
	nss := [][]int{{1, 4, 5}, {1, 3, 4}, {2, 6}}
	lns := make([]*ListNode, 3)

	// Make []*ListNode
	for r, ns := range nss {

		lns[r] = &ListNode{v: -100, next: new(ListNode)}
		cur := lns[r]

		for idx, v := range ns {
			cur.v = v
			// fmt.Println(*cur)
			if idx < len(ns)-1 { // last idx skip assign new
				cur = cur.next
				cur.v = -200 //default is 0, reset
				cur.next = new(ListNode)
			}

		}
		cur.next = nil //cut tail

	}

	head := mergeKListsByCountSort(lns)

	// Validate
	head.printAll()

}

func mergeKListsByCountSort(lns []*ListNode) *ListNode {
	var head *ListNode

	countIdx := make([][]*ListNode, 10) // assert input only 0-9
	for i := 0; i < 10; i++ {
		countIdx[i] = make([]*ListNode, 3) // assert lower than 3 duplicates
	}

	for _, lnr := range lns {
		c := lnr
		for c.next != nil {
			pts := countIdx[c.v]
			for i := 0; i < 3; i++ {
				if pts[i] == nil {

					pts[i] = c
					break
				}
			}
			// fmt.Println(pts)
			c = c.next
		}
	}

	// counting sort on []countIdx
	var curhead *ListNode
	for _, r := range countIdx {
		for _, ptr := range r {

			if head == nil {
				head = ptr
				curhead = head
			} else {
				if ptr != nil { //Ptr can be nil
					// if curhead != nil && ptr != nil { //TODO
					curhead.next = ptr
					curhead = curhead.next
				}
			}
		}
	}

	return head
}

type ListNode struct {
	v    int
	next *ListNode //nil
}

func (ln ListNode) printAll() {
	fmt.Printf("head value is %d\n", ln.v)
	var pt *ListNode = &ln

	for pt.next != nil {
		fmt.Printf("cursor value is %d \n", pt.v)
		pt = pt.next
	}

}
