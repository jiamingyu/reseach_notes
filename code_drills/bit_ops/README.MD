
# 136, 137

https://leetcode.com/problems/single-number/

https://leetcode.com/problems/single-number-ii/



# 201
Post snipped here:

Analysis: 

A. start to end number is incremental, meaning other than leftmost, all other right position must have 0/1 switch showing up, leading to & result as 0

B. If left(small) bit representation is shorter than right, bit & must be 0

B. Only when both are 1 and identical in bitwise length, the bit & will be 1<< ${bitwise length}

```

        @Test
        public void test201(){
            int left = 1;
            int right = 2147483647;

            int lIdx = findLeftMostBitOneIdx(left);
            int rIdx = findLeftMostBitOneIdx(right);

            if (lIdx == rIdx) {
                System.out.println(1 << lIdx);
            } else {
                System.out.println(0);
            }
        }

        public int findLeftMostBitOneIdx(int d) {
            int count = 0;

            while(d > 1) {
                d = d >> 1;
                count++;
            }
            return count;

        }

```