package main

// ahttps://leetcode.com/problems/longest-increasing-subsequence/description/
import (
	"fmt"
)

func main() {

	// nums := []int{10, 9, 2, 5, 3, 7, 101, 18} //4
	nums := []int{7, 7, 7, 7, 7} //4

	// nums := []int{0, 1, 0, 3, 2, 3} //3
	fmt.Println(lengthOfLIS(nums))

}

func lengthOfLIS(nums []int) int {
	idx2PrevShorterIdx := make(map[int]int)
	for idx := len(nums) - 1; idx > 0; idx-- { // first ele [0] should skip
		idx2PrevShorterIdx[idx] = idx
		for cur := idx; cur >= 0; cur-- {
			if nums[cur] < nums[idx] {
				idx2PrevShorterIdx[idx] = cur
				break
			}
		}
		// fmt.Println(idx2PrevShorterIdx[idx], idx)
	}

	rtn := 1
	cum := make(map[int]int)
	for idx, _ := range nums {
		if idx == idx2PrevShorterIdx[idx] { // no smaller on left side
			cum[idx] = 1
			continue
		}

		cum[idx] = cum[idx2PrevShorterIdx[idx]] + 1
		// fmt.Println(idx2PrevShorterIdx[idx], cum[idx], rtn)
		if cum[idx] > rtn {
			rtn = cum[idx]
		}
	}

	return rtn
}
