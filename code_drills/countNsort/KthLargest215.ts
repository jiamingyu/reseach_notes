// https://leetcode.com/problems/kth-largest-element-in-an-array/
function findKthLargest(nums: number[], k: number): number {

    let bufAsc : number[] = new Array<number>(k-1) as number[]
    // console.log(bufAsc.filter(x=>x != null).length)

    for(const v of nums){
        giveNthMax(v, bufAsc)
    }

    function giveNthMax(seed: number, abuf: number[]): number {
        let size = abuf.filter(x=>x!=null).length
        // console.log(`size: ${size}; seed ${seed}; ${abuf}`)
        let isFull = abuf.filter(x=>x==null).length == 0

        if (abuf.filter(x=>x!=null).length==0)
            {abuf[0] = seed; return abuf[0]}
        if (abuf.filter(x=>x!=null).length==1){
            if (seed > abuf[0]){
                {abuf[1] = seed; return abuf[1]}
            } else {
                abuf[1] = abuf[0]
                abuf[0] = seed
                return abuf[1]
            }
        }

        if (seed < abuf[0]){
            if(isFull) {
                return abuf[size-1]
            } else {
                for(let i=size;i >= 1;i--) {
                    abuf[i] = abuf[i-1]
                }
                return abuf[size]
            }

        } else if (seed > abuf[size-1]){
            if (isFull) {
                abuf[size-1] = seed
                return abuf[size-1]
            } else {
                abuf[size+1] = seed
                return abuf[size]
            }

        } else {


            // binary fit
            let cur = Math.floor((size-1)/2)

            // /** 
            let dbg = 0
            while (!(abuf[cur] < seed && abuf[cur+1] > seed) && dbg < 2){
                console.log(`cur:  ${cur}, ${abuf}`)
                if(seed <= abuf[cur+1]) {
                    cur = cur/2
                } else {
                    cur = cur + (size - cur)/2
                }
    
                dbg += 1
            }

            if (isFull) {
                for(let i=size-1;i >= cur;i--) {
                    abuf[i] = abuf[i-1]
                }
                abuf[cur] = seed
                return abuf[size-1]
            } else {
                for(let i=size;i >= cur;i--) {
                    abuf[i] = abuf[i-1]
                }
                abuf[cur] = seed
                return abuf[size]
            }

        }
    } 
    return bufAsc[0] 
};

const nums215 = [3,2,1,5,6,4];
const k = 2
console.log(findKthLargest(nums215, 2))
