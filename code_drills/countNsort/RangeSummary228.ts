// A. Binary approach for O(ln(N)), with recur call
// B. For scenario that splitting points is continuous counting, trying to expand both side and make it a separate range record


function summaryRanges(nums: number[]): string[] {
    var rtn = [] as string[]

    summaryRangesBinary(rtn, nums)
    return rtn
};

function summaryRangesBinary(rtn: string[], nums: number[]) {
    if (nums.length ==1)
        return

    if (nums.length-1 == nums[nums.length-1] - nums[0]) {
        const x = `${nums[0]} until ${nums[nums.length-1]} `
        rtn.push(`${nums[0]} until ${nums[nums.length-1]} `)
        return
    }

    let curR = Math.floor((nums.length+1)/2)
    while(curR < nums.length-1 && nums[curR] + 1 == nums[curR+1]) {
        curR += 1
    }
    
    // console.log(curR)
    // console.log(nums)
    
    let curL = Math.floor((nums.length)/2)
    while(curL > 0 && nums[curL] - 1 == nums[curL-1]) {
        curL -= 1
    }  

    if (curL + 1 < curR)
        rtn.push(`${nums[curL]} until ${nums[curR]} `)

    if (nums.length > 2)
        summaryRangesBinary(rtn, nums.slice(0, curL)) // ?? recur

    if (curR < nums.length) {
        summaryRangesBinary(rtn, nums.slice(curR+1))
    }
}

const nums228 = [0,2,3,4,6,8,9] as number[]
// const nums228 = [0,1,2,4,5,7] as number[]
const rtnall = summaryRanges(nums228)
console.log( rtnall)
