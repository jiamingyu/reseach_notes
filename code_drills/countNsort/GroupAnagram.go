// You can edit this code!
// Click here and start typing.
package main

import (
	"fmt"
	"maps"
	"slices"
	"strings"
)

func main() {
	var strs = []string{"eat", "tea", "tan", "ate", "nat", "bat"}

	fmt.Println(groupAnagrams(strs))
}

func groupAnagrams(strs []string) [][]string {

	var mp = make(map[string][]string)
	fmt.Println(mp)

	for idx, s := range strs {
		var pos = [26]int{}
		fmt.Printf("At %d value is %s \n", idx, s)
		for _, c := range s {
			fmt.Println(c - 'a')
			pos[c-'a'] += 1

		}
		//fmt.Println(strings.Join(fmt.Sprint(pos), "#"))

		//fmt.Println(strings.Trim(strings.Join(strings.Fields(fmt.Sprint(pos)), "#"), "[]"))
		klead := strings.Trim(strings.Join(strings.Fields(fmt.Sprint(pos)), "#"), "[]")

		mp[klead] = append(mp[klead], s)

		/**
		k, v := mp[klead]
		if !v {
			mp[klead] = append(mp[klead], s)

		} else {

			//fmt.Println(v)
			//fmt.Println(k)
		}
		*/

	}

	return slices.Collect(maps.Values(mp))

}
