function getHint(secret: string, guess: string): string {
    let rtn = ""

    let sec = parseInt(secret)
    let gue = parseInt(guess)


    let unmatchedSecrets = new Array<number>(9) // [digit] = count
    let unmatchedGuesses = new Array<number>(9)

    unmatchedSecrets.fill(0)
    unmatchedGuesses.fill(0)

    let bull = 0;
    let cow = 0;

    while(sec > 0) {
        const s = sec % 10
        const g = gue % 10

        // console.log(`${s} vs ${g}`)
        if (s==g) {
            bull += 1
        } else {
            unmatchedGuesses[g] += 1
            unmatchedSecrets[s] += 1
        }

        
        
        sec = Math.floor(sec / 10)
        gue = Math.floor(gue / 10)
    }

    for(const idx in unmatchedGuesses) {
        cow += Math.min(unmatchedGuesses[+idx], unmatchedSecrets[+idx])
    }


    rtn = `${bull}A${cow}B`
    return rtn
};

// const secret = "1807"; const guess = "7810";
const secret = "1123"; const guess = "0111"
console.log(getHint(secret, guess))
