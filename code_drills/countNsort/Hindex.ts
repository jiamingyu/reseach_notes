

function hIndexDiffOutNumber(citations: number[]): number {

    const paperCountIndexedByCitations : number[]  = [...new Array(citations.length + 1 )].map(v=>0); //2 XTRA spot

    let i =0;
    for(; i< citations.length;i++) {
        if (citations[i] < citations.length) {
            paperCountIndexedByCitations[citations[i]] +=1
        } else {
            paperCountIndexedByCitations[citations.length] += 1
        }
    }


    i=0
    let baseCount = paperCountIndexedByCitations[citations.length]
    if ( baseCount == citations.length) // one & only one exceptional hight
        return citations.length
    
    let sum = baseCount

    for(; i< citations.length;i++) {
        sum += paperCountIndexedByCitations[i]
        if (sum >= citations.length) {
            return i
        }


    }

    return -1;
};



let citations = [3,0,6,1,5]
// let citations = [13,20,16,11,15]

console.log(`hindex is: ${hIndexDiffOutNumber(citations)}`)
