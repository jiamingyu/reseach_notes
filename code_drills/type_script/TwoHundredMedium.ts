
    /**
     * https://leetcode.com/problems/game-of-life/
     * https://leetcode.wang/leetcode-289-Game-of-Life.html
     */



/**
 Do not return anything, modify board in-place instead.
 */
 const DEAD = 0; const ALIVE = 1;
 const TO_DEAD = 3; const TO_ALIVE = 5;
 
  const DEAD2DEAD = DEAD + TO_DEAD;//3
  const DEAD2ALIVE = DEAD + TO_ALIVE;//5
  const ALIVE2DEAD = ALIVE + TO_DEAD;//4
  const ALIVE2ALIVE = ALIVE + TO_ALIVE;//6
 
 
 
  let m = [[0,1,0],[0,0,1],[1,1,1],[0,0,0]];
   gameOfLife(m);
  
  function gameOfLife(board: number[][]): void {
      for(let r = 0; r < board.length; r++) {
          for (let c = 0 ; c < board[r].length; c++) {
              board[r][c] = calibNewStatus(board, r, c);
          }
      }
  
      console.log(m)
      
      interpretGameOfLife(board);
  
      console.log("interpretting:")
  
      console.log(m)
  
  };
  
  function calibNewStatus(board: number[][],r: number,c: number): number {
      let status = board[r][c];
  
      const aliveNbrs = countAliveNbrs(board,r,c);
  
     //  console.log(`r: ${r}, c: ${c}, alive_count: ${aliveNbrs}`);
  
      /**
       * # Starting w. alive
       * Any live cell with fewer than two live neighbors dies, as if caused by under-population.
       * Any live cell with two or three live neighbors lives on to the next generation.
       * Any live cell with more than three live neighbors dies, as if by over-population.
       * 
       * # Starting w. dead
       *  Any dead cell with exactly three live neighbors becomes a live cell, as if by reproduction.
       * 
         
       */
          if ( isInitialAlive(status) ) {
              switch(aliveNbrs){ //TODO
                  case 0: case 1 : {status = updateInitOnly(status, TO_DEAD); break};
                  case 2: case 3 : {status = updateInitOnly(status, TO_ALIVE); break;}
                  default :{console.log(`default status ${status}`);status = updateInitOnly(status, TO_DEAD);} //more than 3
              }
             //  console.log(`${r}, ${c} w. initial alive status: ${status}`);
          } else {
              console.assert(!isInitialAlive(status));
  
              switch(aliveNbrs) {
                  case 3 : {status = updateInitOnly(status, TO_ALIVE); break;}
                  default: status = updateInitOnly(status, TO_DEAD)
  
              }
             //  console.log(`${r}, ${c} w. initial DEAD status: ${status}`);
  
          }
  
  
  
      return status;
  
  }
  
  function isInitialAlive(status: number) : boolean {
      switch(status) {
          case DEAD  :
          case DEAD2ALIVE : 
          case DEAD2DEAD : return false;
  
          case ALIVE  : return true;
          case ALIVE2ALIVE: return true;
          case ALIVE2DEAD : return true;
  
          default: {console.log(status);throw new Error("Status code is not in scope");}
      }
  
  }
  
  
  
  function interpretGameOfLife(board: number[][]) {
          for(let r = 0; r < board.length; r++) {
              for (let c = 0 ; c < board[r].length; c++) {
                 //  console.log(`evaluate ${r}, ${c} w. value ${board[r][c]}`)
              switch (board[r][c]) {
                  case DEAD2DEAD:
                  case ALIVE2DEAD: {board[r][c] = DEAD;break;}
                  case DEAD2ALIVE: case ALIVE2ALIVE: {board[r][c] = ALIVE; break}
              }
             //  console.log(`post - evaluate ${r}, ${c} w. value ${board[r][c]}`)
          }
      }
  
  }
  
  
  function countAliveNbrs(board: number[][],r: number,c: number) : number {
      const upper = r > 0 ? r-1 : r;
      const left = c > 0 ? c-1 : c;
      const lower = r + 1 >= board.length? r : r+1;
      const right = c + 1 >= board[r].length ? c: c+1;
  
      let aliveNbrCt = 0;
  
      for(let r1 = upper; r1 < lower+1;r1++){
          for(let c1=left; c1 < right + 1; c1++) {
              // console.log(`${lower}: kkkkkk *****: ${r1}, ${c1}`);
  
              //TODO: log r,c
             //  console.log(`${r}, ${c}, *****: ${r1}, ${c1} ***** ${board[r1][c1]}`);
              if (r1==r && c1==c)
                  continue;
              
              if(board[r1][c1]==1 || board[r1][c1]== 1 + TO_ALIVE || board[r1][c1]== 1 + TO_DEAD) {
                  aliveNbrCt += 1;
                 //  console.log(`${r} & ${c} alive count increase by nbr: ${r1}, ${c1}`)
  
              }
          }
      }
      return aliveNbrCt;
  }
  
  function updateInitOnly(status: number, delta: number): number {
      if (status > ALIVE)
          return status;
  
      return status + delta;
      
      
  }


/**
    https://leetcode.wang/leetcode-287-Find-the-Duplicate-Number.html
     
    Different from java version impl, here I use [0] as swap spot
    I like this implementation
 * @param nums 
 * @returns 
 */

    function findDuplicate(nums: number[]): number {
        let d = [ -1, ...nums];
    
        let idx = 1;
    
        while(idx < d.length) {
            
            // idx reaches next first index with value not equals index && UNIQUE
            while(d[idx] == idx){
                idx += 1;
            }
            d[0] = d[idx];
            d[idx] = -1;
    
            while (d[0] != -1) {
                if (d[0] === d[d[0]]){
                    console.log(`returning value ${d[0]}`)
                    return d[0]
                }
    
                console.log(`${d[0]} :: ${d[d[0]]}`)
                let buf = d[d[0]];
                d[d[0]] = d[0];
                d[0] = buf;
            }
        }
    
        return -1;
    };
    
    // const d = [1,3,4,2,2];
    const d = [3,1,3,4,2];
    console.log(findDuplicate(d));
    

/**
 * https://leetcode.wang/leetcode-274-H-Index.html
 * Solution A: Not differentiate maxCount vs GT maxCount
 * 
 * $paperCountIndexedByCitations to index citation count, from 0 upto paper counts. 
 * Citation of the paper, if more than paper count, considered +1 for all current papers (Do we care citation count GT paper count? Not really)
 * @param citations 
 * @returns 
 */
function hIndex(citations: number[]): number {

    const paperCountIndexedByCitations : number[]  = [...new Array(citations.length + 1)].map(v=>0); // because 0 count publish considered

    for(let i =0; i< citations.length;i++) {

        const cits = citations[i];
        let citsOpt = cits;
        // citation of the paper, if more than paper count, considered +1 for all current papers
        if (cits > paperCountIndexedByCitations.length){
            console.log(`citatiion GT paper counts : ${cits}`)
            citsOpt = paperCountIndexedByCitations.length
            // console.log(`${paperCountIndexedByCitations[5]} : ${citsOpt}: After adding 1: ${paperCountIndexedByCitations[paperCountIndexedByCitations.length-1]}`)
        }
        for(let j=0; j <citsOpt+1; j++) { // All count index below get +1 credit
                paperCountIndexedByCitations[j] += 1
                // console.log(` ${j} :  ${paperCountIndexedByCitations[j]}`)
        }
        
    }

    if (paperCountIndexedByCitations[paperCountIndexedByCitations.length-1] >= citations.length) { // All paper has citations GT papaer count
        return Number.MAX_VALUE // ??
    }

    for(let k = paperCountIndexedByCitations.length-2; k >=0 ;k--) { //TODO: find better way
        console.log(`${k},`)
        if(paperCountIndexedByCitations[k] == k)
            return k;
    }


    return -1;
};



// let citations = [3,0,6,1,5]
let citations = [13,20,16,11,15]

console.log(hIndex(citations))


/**
 * 
 * SECOND SOLUTION ON THIS:
 * 
 * Difference from version above: Citation count GT paper count is placed as last count number in the n+2 array. 
 * $paperCountIndexedByCitations to index citation count, from 0 upto paper counts (overnumber count reset). 
 * @param citations 
 * @returns 
 */
function hIndexDiffOutNumber(citations: number[]): number {
    // Consider 0 count publish considered, & list citing count outnumber paper count separately

    const paperCountIndexedByCitations : number[]  = [...new Array(citations.length + 1 + 1)].map(v=>0); 

    for(let i =0; i< citations.length;i++) {

        const cits = citations[i];
        let citsOpt = cits
        // citation of the paper, if more than paper count, considered +1 for on last element of paperCountIndexedByCitations 
        if (cits > citations.length ){
            citsOpt = citations.length + 1;
        } 
            
        for(let j=0; j <= citsOpt; j++) { // All count index below get +1 credit
                paperCountIndexedByCitations[j] += 1
        }
        
    }

    console.log(paperCountIndexedByCitations)

    // Scenario: all paper's citation counts outnumber paper count
    if (paperCountIndexedByCitations[paperCountIndexedByCitations.length-1] == citations.length) { 
        return Number.MAX_VALUE 
    }

    for(let k = citations.length; k >=0 ;k--) { //TODO: find better way
        console.log(`${k},`)
        if(paperCountIndexedByCitations[k] == k)
            return k;
    }


    return -1;
};



// let citations = [3,0,6,1,5]

let citations = [13,20,16,11,15]

console.log(hIndexDiffOutNumber(citations))


    /**
     * https://leetcode.wang/leetcode-279-Perfect-Squares.html
     *
     * 
     *
     */

    //TODO: Looks like even container obj. still does not achieve pass by reference & keep map attribute values.
    class MapContainer {
        n2minCounts: Map<number, number> = new Map()
    }
    
    function numSquaresRecur(n: number, mc: MapContainer): number {
    
        let minCount = Number.MAX_VALUE;
    
        let uplimitV = Math.ceil(n / 2);
        while(uplimitV > 0) {
            if (uplimitV * uplimitV <= n)
            break;
            uplimitV-=1
        }
        
        for(let v = uplimitV;v>0;v--) {
    
            if (n==v*v){ // stack buttom, meaningful
                minCount = 1;
            }
            
            console.log(`${n} ::<<>>:: ${v} :: ${mc.n2minCounts.get(n-v*v)}`)
    
    
            if (mc.n2minCounts.get(n-v*v) != undefined){
                console.log(`================ ${n-v*v}::${mc.n2minCounts.get(n-v*v)}`)
            }
    
    
            const buf = mc.n2minCounts.get(n-v*v) == undefined ? numSquaresRecur(n-v*v, mc) + 1 : mc.n2minCounts.get(n-v*v)
            if (buf != Number.MAX_VALUE) {
                if (minCount > buf)
                    console.log(`${minCount}, ${buf}`)
    
                minCount = Math.min(minCount, buf)
            }
    
        }
    
        // console.log(`pre: ${mc.n2minCounts}`)
        mc.n2minCounts.set(n, minCount)
        // console.log(mc.n2minCounts)
    
    
        return minCount;
    }
    
    function numSquares(n: number): number {
        const mc = new MapContainer();
    
        return numSquaresRecur(n, mc)
    
    };
    
    
    
    console.log(numSquares(12))
    
    /**
     * https://leetcode.com/problems/single-number-iii/description/
     * 
     * https://leetcode.wang/leetcode-260-Single-NumberIII.html
     * 
     * 
     * 
     * @param numsIn 
     * @returns 
     */
    function findNoneNegNumbersExistAsSpecified(numsIn: number[], exactCount: number): number[] { 
        //Notice: This solution only restricted to entries GT -1
        const lmt = numsIn.reduceRight((l, r)=> Math.max(l,r), Number.MIN_SAFE_INTEGER )
        const valueIndexedWCount : number[] = new Array(lmt+1);
        valueIndexedWCount.fill(0)
    
        for(const v of numsIn) { // value used to find related index in arrayWMaxInputV
            valueIndexedWCount[v] += 1
        }
    
        const demoFixedArray : number[] = [... new Array(2)]; // spread
        let r2idx : number = 0
        const demoAppendArray = []
    
        for(var x in valueIndexedWCount){
            // console.log(`${x}, ${arrayWMaxInputV[x]}`)
            if (valueIndexedWCount[x] == exactCount){
                console.log(`value ${x} has dup records`); 
                demoFixedArray[r2idx++] = Number(x);
                demoAppendArray.push(x)
            }
        }
    
        
        console.log(`demo append result as string collect : ${demoAppendArray}`)
    
    
        return demoFixedArray
    }
    
    const nums = [1,2,1,3,2,5]
    console.log(`exact 2 entries number: ${findNoneNegNumbersExistAsSpecified(nums, 2)}`)
    console.log(`exact 1 entries number:   ${findNoneNegNumbersExistAsSpecified(nums, 1)}`)
    
    /**
     * https://leetcode.com/problems/ugly-number-ii/
     * 
     * Note: This is different from https://leetcode.com/problems/ugly-number/
     * To check if is ugly number, just need to moduling the 3 numbers (easy).
     */
    class UglyNumber {
        num: number
        itr : number = 0
    
        constructor(n: number) {
            this.num = n;
        }
    
        reconVal(d: number[]): number {
            return this.num * (d[this.itr])
        }
    
        increase(d: number[]): number {
            return this.num * (d[this.itr++])
        }
    }
    
    function nthUglyNumber(n: number): number {
        let r = -1;
    
        let ds = [ ... Array(n)]
        ds[0] = 1
    
        const uns = [new UglyNumber(2), new UglyNumber(3), new UglyNumber(5)]
        
        let dsCur = 1; 
        while(dsCur < n) {
            let nextMinV : UglyNumber = undefined
    
            for(const un of uns ) {
    
                if(un.reconVal(ds) <= ds[dsCur-1]){
                    un.increase(ds) // stip but move ahead. Otherwise it will be missed from now on.
                    continue
                }
    
                if ( nextMinV===undefined || nextMinV.reconVal(ds) > un.reconVal(ds) ){
                    nextMinV = un
                }
            }
            ds[dsCur++] = nextMinV.increase(ds)
        }
    
        // console.log(ds)
        return ds[n-1];
        
    };
    
    console.log(nthUglyNumber(10))
    console.log(nthUglyNumber(1))
    
/**
 * https://leetcode.com/problems/different-ways-to-add-parentheses/description/
 * https://leetcode.wang/leetcode-241-Different-Ways-to-Add-Parentheses.html
 * 
 * 
 */


function diffWaysToCompute(expression: string): number[] {
    let r : number[] = [];
    
    return r;
};

type OpType = (arg2: number, arg1: number)=>number 


const op2arithOp = new Map<string, OpType>(
        [
            ["+", (arg2: number, arg1: number)=> {console.log(`${arg2} + ${arg1}` ); return arg1 + arg2} ],
            ["-", (arg1: number, arg2: number)=> {console.log(`${arg1} - ${arg2}` ); return arg1 - arg2;} ],
            ["*", (arg1: number, arg2: number)=> {console.log(`${arg1} * ${arg2}` ); return arg1 * arg2;} ],
            ["/", (arg1: number, arg2: number)=> {console.log(`${arg1} / ${arg2}` ); return arg1 / arg2;} ],
        ]
        );


class NumberNOpOption {
    constructor(public num: number, public op? : string){}
    
}


function drillDyna(ds: NumberNOpOption[], cur: number, stackLevel: number) : number[]{
    console.log(`===  ${stackLevel}  ======`)
    if (cur === ds.length-1) {
    console.log(`||||  ${stackLevel} ///  last stack  ///`)
        return [ds[cur].num]
    }

    let r = new Array<number>()
     
    if(op2arithOp.get(ds[cur].op) == undefined) 
        throw new Error(`data issue, ${ds[cur].op} of ${cur} index data need verifying `);
    


    const foldWRightNum = op2arithOp.get(ds[cur].op)(ds[cur].num,ds[cur+1].num)
    if (cur+1 < ds.length-1) { //last second index as buttomline
        let rightCompounds = drillDyna(ds, cur+1, stackLevel+1)
        for(const v of rightCompounds) {
                const element = op2arithOp.get(ds[cur].op)(ds[cur].num,v)     //
                r.push(element)
        }

        let param2ndB = drillDyna(ds, cur+2, stackLevel+1)
        for(const v of param2ndB) {
                r.push(op2arithOp.get(ds[cur+1].op)(foldWRightNum, v))      //
        }
    } else
        r.push( foldWRightNum )

    console.log(`||||  ${stackLevel} //////`)

    return r

}

const ds = [ new NumberNOpOption(8,"+"), new NumberNOpOption(2,"*"), new NumberNOpOption(6,"-"), new NumberNOpOption(4)]
const xxx =drillDyna(ds, 0, 0)
console.log(xxx)