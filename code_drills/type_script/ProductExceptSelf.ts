
/**
 * https://leetcode.com/problems/product-of-array-except-self/description/
 * 
 * 
 * @param nums 
 * @returns 
 */
function productExceptSelf(nums: number[]): number[] {
    const rtn : number[] = []
    
    const lprod = []
    const rprod = []


    for(const idxStr in nums) {
        const idx : number = +idxStr

        if (idx==0){
            lprod[idx] = nums[0]
            rprod[nums.length -1] = nums[nums.length -1]
        } else {
            lprod[idx] = lprod[idx-1] * nums[idx]
            rprod[nums.length -1-idx] = rprod[nums.length -1-idx + 1] * nums[nums.length -1-idx]
        }
        // console.log(lprod[idx])

        // console.log(rprod[nums.length -1-idx])

        // console.log(`************`)

    }

    rtn[0] = rprod[1]
    rtn[nums.length-1] = lprod[nums.length-1-1]

    for(const idxStr in nums) {
        const idx : number = +idxStr
        const l = idx - 1
        const r = idx + 1

        if (idx > 0 && idx < nums.length-1) {
            rtn[idx] = lprod[idx-1] * rprod[idx+1]
        }
    }



    return rtn
};

// const nums = [1,2,3,4]
const nums = [-1,1,0,-3,3]

const r = productExceptSelf(nums)
console.log(r)
