// https://leetcode.wang/leetcode-304-Range-Sum-Query-2D-Immutable.html
// https://leetcode.com/problems/range-sum-query-2d-immutable/

// Built on ts v5.3.2

class NumMatrixByRowSum {
    matrixSumPerRow : number[][]=  []; 

    constructor(matrix: number[][]) {

        for (let ridx =0; ridx<matrix.length; ridx++) {
            const rowSum : number[] = [];
            var sum = 0;
            for(let cidx = 0 ; cidx < matrix[ridx].length; cidx++) {
                rowSum.push(sum+=matrix[ridx][cidx]);
            }
            this.matrixSumPerRow.push(rowSum);
        }
    }



    // sumRegion(row1: number, col1: number, row2: number, col2: number): number {
        //        0.            1             2               3
    sumRegion(...rc: number[]): number { // rest parameter,  because they capture the rest of the parameters

        let sum = 0;
        for(let r = rc[0]; r< (rc[2] < this.matrixSumPerRow.length ? rc[2] +1 : this.matrixSumPerRow.length); r++) {
            // console.log(`${r}:${rc[3]}:${this.matrixSumPerRow[r][rc[3]]} :: ${this.matrixSumPerRow[r][rc[1]]} : ${rc[1]}`);
            sum += this.matrixSumPerRow[r][rc[3]] - this.matrixSumPerRow[r][rc[1]-1]; //TODO: boardline
            // console.log(`row cum sum: ${sum}`);
        }
        return sum;
    }
}

const matrix = [[3, 0, 1, 4, 2], [5, 6, 3, 2, 1], [1, 2, 0, 1, 5], [4, 1, 0, 1, 7], [1, 0, 3, 0, 5]];
const test1 = new NumMatrixByRowSum(matrix);
// console.log(test1.matrixSumPerRow);

const sumRegion1 = [2, 1, 3, 3];
console.log(test1.sumRegion(...sumRegion1));//spread operator 
