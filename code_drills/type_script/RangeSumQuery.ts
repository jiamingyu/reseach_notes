// https://leetcode.wang/leetcode-307-Range-Sum-Query-Mutable.html

// https://leetcode.com/problems/range-sum-query-mutable/editorial/

// Built on ts v5.3.2

class NumArray {
    cubSums = new Array<number>();
    cubsize = 1;
    _nums: number[]|undefined;

    constructor(nums: number[]) {
        this._nums = nums;
        this.cubsize = Math.round( Math.sqrt(nums.length) );

        for(let i = 0; i * this.cubsize < nums.length; i++ ) {
            let localSum = 0;
            for(let j = i * this.cubsize; j < i * this.cubsize + this.cubsize && j < nums.length; j++) {
                localSum += nums[j];
            }
            this.cubSums.push(localSum);
        }
    }

    update(index: number, val: number) {
        if (this._nums === undefined) {
            return;
        }
        
        let impactedCubIdx = Math.floor(index / this.cubsize);
        this.cubSums[impactedCubIdx] -= this._nums[index];
        
        //
        this.cubSums[impactedCubIdx] += val;
        this._nums[index] = val;
    }

    sumRange(left: number, right: number): number  {
        if ( left < 0) throw new Error(`left idx ${left} out of range`);
        if (right > nums.length-1) throw new Error(`right idx ${right} GT range`);

        var sum = 0;

        //TODO: left / right index
        const leftcubIdx = Math.floor(left / this.cubsize);
        const rtcubIdx = Math.floor(right / this.cubsize);

        for(let sumIdx = leftcubIdx; sumIdx <= rtcubIdx; sumIdx++){
            sum += this.cubSums[sumIdx];
        }

        //
        if (this._nums !== undefined) {
            for(let lftcur = leftcubIdx; lftcur < left; lftcur++) {
            // console.log(`debug: ${this._nums !== undefined ?? this._nums[lftcur]}`);
                sum -= this._nums[lftcur];
            }

            let rtlimit = rtcubIdx + this.cubsize > this._nums.length ? this._nums.length-1 : rtcubIdx + this.cubsize
            for (let rtcur = rtlimit; rtcur > rtcubIdx +1;rtcur--) {
                console.log(`debug: ${rtcur}`);
                sum -= this._nums[rtcur];
            }

        }
        return sum;
    }
}

let nums = [11, 2, 33, 45, 97];
let na = new NumArray(nums);

console.log(na.cubSums);

// console.log(`sqrt result: ${Math.round( Math.sqrt(nums.length) )}`);

console.log(na.sumRange(1,2));

na.update(1, 22)

console.log(na.sumRange(1,2));
