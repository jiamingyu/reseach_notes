import { BNode } from "./BTreeModule";

// https://leetcode.com/problems/count-complete-tree-nodes/description/
// 
// The definition of api input as root node generated on right side is not in align with
// sample given as array of data
// 
// Solution here try to stick with left side description 

// Full nodes Meaning not counting leaf. Therefore it comes as how many of 2nd from lowest is full node
function countNodesCompleteTree(d: number[]): number {
// function countNodesCompleteTree(root: BNode<number> | null): number {
    let count = 0
    let cur = 0
    let tier = 1

    while(cur + tier*2 < d.length) { // detect first b4 update $tier value
        tier = tier * 2
        cur = cur + tier
    }
    //
    const lastTierLeafCounts = d.length-1 - cur
    const secondLastTierFullNodes = Math.floor(lastTierLeafCounts / 2)

    // console.log(`${cur}, ${tier}, ${lastTierLeafCounts} , ${secondLastTierFullNodes}`)

    return (cur+1) - tier + secondLastTierFullNodes //cur+1 as counted node so far, 
};

const d = [1,2,3,4,5,6]

console.log( `nodes qualified : ${countNodesCompleteTree(d)}` )
