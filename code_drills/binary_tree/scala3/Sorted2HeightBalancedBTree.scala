import scala.collection.mutable.ListBuffer

// object SortedLListBST {

case class TreeNode(var vOpt: Option[Int] = None, var left: Option[TreeNode] = None, var right: Option[TreeNode] = None)
case class ListNode[T](val v: T, var next: Option[ListNode[T]] = None)

def sortedListToBST(head: ListNode[Int]): Option[TreeNode] = {
	None

}


// val vs = List(-10,-3,0,5,9)
/**
 * 
 *  Notice: 3 vs 10, 9 vs 5, could be flexible depends on it asks for "complete" or "height balanced" BTree
TreeNode(Some(0),
	Some(TreeNode(Some(-3),Some(TreeNode(Some(-10),None,None)),None)),
	Some(TreeNode(Some(9),Some(TreeNode(Some(5),None,None)),None)))
 * )
 * 
 */


val vs = List(-10,-3,5,9)
/**
 * 
 * TreeNode(Some(0),
 * 		Some(TreeNode(Some(-3),
 * 				Some(TreeNode(Some(-10),None,None)),	None)),
 * 		Some(TreeNode(Some(9),
 * 				Some(TreeNode(Some(5),None,None)),		None))
 * )
 * 
 * /


val head : ListNode[Int]= ListNode(-1000)
var cur = head


for(v <- vs) yield {
	cur.next = Some(ListNode(v))
	println(cur.v)

	cur = cur.next.get
}
	println(cur.v) // last v

// point to real data node instead of pseudo head listNode
var s = head.next.get
var f = head.next.get

var curOpt : Option[TreeNode] = None
var left : Option[TreeNode] = None
val lb = ListBuffer[Int]()
val halflb = ListBuffer[Int]()

var keepLoop = true
while(keepLoop) {

	f.next match {
		case None => { // last listNode , total even
			println("ODD");
			lb += f.v; keepLoop = false
		}
		case Some(v) => {
			println("===========")
			println(s"halflb: ${s.v}")

			halflb += s.v
			s = s.next.get


			println(s"lb1: ${f.v}")
			lb += f.v
			
			
			f = f.next.get
			println(s"lb2: ${f.v}")

			lb += f.v

			f.next match {
				case None => println("EVEN "); keepLoop = false
				case Some(lnode) => f = lnode // f move forward & ready to be read to full_lists
			}

		}
	}
}

println(halflb)
println(lb)


val ct = lb.length

def buildBTree(l: List[Int]): TreeNode = {

	l.length match {
		case i if i > 3 && i%2==0 => {
			println( s"${l.slice(0, l.length/2)}") 
			println( "xxxxxxyyyyyzzzzzzzzz")
			println(s"${l.slice(l.length/2, l.length)}")
			val ln : TreeNode = buildBTree(l.slice(0, l.length/2))
			val rn : TreeNode = buildBTree(l.slice(l.length/2 + 1, l.length))
			TreeNode(Some(l(l.length/2)), Some(ln),Some(rn))

		}

		case i if i > 3 && i%2==1 => {
			println( s"${l.slice(0, l.length/2)}") 
			println( s"${l(l.length/2)}:aaaaa--------bbb------cccccccc")
			println(s"${l.slice(l.length/2+1, l.length)}")
			val ln : TreeNode = buildBTree(l.slice(0, l.length/2))
			val rn : TreeNode = buildBTree(l.slice(l.length/2 + 1, l.length))
			TreeNode(Some(l(l.length/2)), Some(ln),Some(rn))

		}

		// case i if i > 3 && i%2==1 => {
		// 	val l : TreeNode = buildBTree(l.slice(0, l.length/2))
		// 	val r : TreeNode = buildBTree(l.slice(l.length/2 + 1, l.length))
		// 	TreeNode(Some(l(l/2)), Some(ln),Some(rn))
		// }
		case i if i==3 => {println("three ... ... ... "); TreeNode(Some(l(1)), Some(TreeNode(Some(l(0)))),Some(TreeNode(Some(l(2))))  )}
		case i if i == 2 => {TreeNode(Some(  l(1)), Some(TreeNode(Some(l(0))))  )}
		case i if i== 1=> {println(s"rrrrrn: l(0)");TreeNode(Some(l(0)))}
	
		case i: Int => println(s"ok, $i"); TreeNode()
	}


}

val ss = buildBTree(lb.toList)
println(ss)
