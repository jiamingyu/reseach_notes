
case class BtNode(var vOpt: Option[Int] = None, var left: Option[BtNode] = None, var right: Option[BtNode] = None)

// case class Leaf(var leafV: Int) extends BtNode(Some(leafV), None, None)

// case class Node(var leafV: Int, var lc: BtNode = null, var rc: BtNode = null) extends BtNode(if(leafV == -100)Some(leafV) else None, Some(lc), Some(rc))


// extension(x: BtNode)
// 	def isLeaf : Boolean = {
// 		x match {
// 			case Leaf(v1) => true
// 			case Node(a,b,c) => false
// 		}
// 	}

// val l = Leaf(12)
// println( l.isLeaf )

// enum EnumBTNode:
// 	case LeafNode, ParentNode	
		
import scala.collection.mutable.Queue

def convertSorted2BTree(ds: List[Int]): BtNode = {
	val root = BtNode(Some(ds.head))

	val q = Queue[BtNode]()

	val rot = BtNode(Some(ds.head))
	q.enqueue(rot)

	buildBTreeBFS(ds.tail, q)

	rot
}

def buildBTreeBFS(ds: List[Int], q: Queue[BtNode]) : Unit = {

	q.isEmpty match {
		case true => {}
		case _ => {
			val ndv = q.dequeue

			ds.length match {
				case 0 => {}
				case 1 => {
					ndv.left = Some(BtNode(Some(ds.head)));
				}
				case 2 => {
					ndv.left = Some(BtNode(Some(ds.head)));
					ndv.right = Some(BtNode(Some(ds.tail.head)));
					// q.enqueue(ndv.left)
					// q.enqueue(ndv.right)

				}
				case _ => {

					ndv.left = Some(BtNode(Some(ds.head)));
					ndv.right = Some(BtNode(Some(ds.tail.head)));
					q.enqueue(ndv.left.get)
					q.enqueue(ndv.right.get)

					buildBTreeBFS(ds.tail.tail, q)

				}



			}
			


			
		}
	}


// (ds.length, cur.left, cur.right) match {
// 				case (0, lft, rt) =>  println("find exit ... ...");root

// 			case (s, None, None) => println("left nonenonenone");cur.left= Some(BtNode(Some(ds.head))); buildBTreeBFS(ds.tail, root, cur, q)
		
// 			case (s, Some(lft), None) => println("rtnonenonenone");cur.right= Some(BtNode(Some(ds.head))); buildBTreeBFS(ds.tail, root, cur, q)
// 			case (s, Some(lft), Some(rt)) => println("new cur");buildBTreeBFS(ds.tail, root, cur.left.get, q)

// 			case _ => {println("default"); println(cur.left);println(cur.right);BtNode(Some(100))}



	// ds.length match {
	// 	case 0 => root
	// 	case v if v > 0 && cur.left == None && cur.right == None => println("left nonenonenone");cur.left= Some(Node(ds.head)); buildBTreeBFS(ds.tail, root, cur, q)
	// 	case v if v > 0 && cur.left != None && cur.right==None => cur.right= Some(Node(ds.head)); buildBTreeBFS(ds.tail, root, cur, q)
	// 	case _ => {println(cur.left);println(cur.right);Node(100)}

	// }

}

val r = convertSorted2BTree(List(4,2,6))
println(s"return value:   $r")




