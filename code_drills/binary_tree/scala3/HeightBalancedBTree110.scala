

case class TreeNode(val v: Int, var left: Option[TreeNode] = None, var right: Option[TreeNode] = None)


def calibBalanced(nd: TreeNode): Either[Boolean, Int] = {
	println(s" >>> calib for ${nd}")

	(nd.left, nd.right) match {
		// Leaf
		case (None, None) => {
			println(s"${nd.v} is leaf")
			Right(1)
		}

		// Either side is None
		case (Some(nd), None) => {
			println(s"left child only: ${nd}")

			calibBalanced(nd) match {
				case Right(ct: Int) if ct == 1 => {
					println(s"calib ${ct} + 1")
					Right(ct + 1)
				}
				case _ => Left(false) 
			}
		}
		case (None, Some(nd)) => {
			println(s"right child only: ${nd}")
			calibBalanced(nd) match {
				case Right(ct: Int) if ct == 1 => Right(ct + 1)
				case _ => Left(false) 
			}
		}

		// Full Tree Node
		case (Some(l), Some(r)) => {
			println(s"Full Tree Node: ${l}, ${r}")
			(calibBalanced(l), calibBalanced(r)) match {

				// <<demo_bug_1 test>>

/** Bug: Any side if already not height balanced, should be treated as false;
 * 
 * [uncomment below 3 lines to prove by running <<demo_bug_1 test>>]
				case (a, b) if (a == Left(false) &&  b== Left(false)) => Left(false)

				case (Right(a), Left(false))=>Right(a+1)
				
				case (Left(false), Right(a)) => Right(a+1)
				*/

				//comment out below case match 
				case (a, b) if (a == Left(false) ||  b== Left(false)) => {
					println(s"<flag False as of valid> Back to stack Full Tree Node: \n\t check result is $a for ${l}, \n\t check result is  $b for  ${r}")
					Left(false)
				}
				// End of  <<demo_bug_1 test>>, comment 

				case (Right(a), Right(b)) if Math.abs(a-b) > 1 => {
					println("invalid height for ${nd.v} as root ")
					Left(false)
				}
				case (Right(a), Right(b))  => {
					println(s"see balanced height ${a}... ${b} ...for ${nd.v} as root")

					Right(Math.max(a,b) + 1)

				} 
				case _ => {println("uncovered scenario, please debug if you see this !!"); Left(false)}


			}
		}

	}

}

def isBalanced(root: TreeNode): Boolean = {
	calibBalanced(root) match {
		case Left(false) => false
		case _ => true

	}

}

//<<demo_bug_1 test>>  Test case to prove related comments
val root = TreeNode(3,
	Some(TreeNode(9, Some(TreeNode(5, Some(TreeNode(2, None, None)), None)) , None)),
	Some(TreeNode(20, Some(TreeNode(15, None, None)), Some(TreeNode(7, None, None))))
)

// [3,9,20,null,null,15,7]: true

// val root = TreeNode(3,
// 	Some(TreeNode(9, None , None)),
// 	Some(TreeNode(20, Some(TreeNode(15, None, None)), Some(TreeNode(7, None, None))))
// )

//[1,2,2,3,3,null,null,4,4], false
// val root = TreeNode(1,
// 	Some(TreeNode(2, 
// 			Some(TreeNode(3, Some(TreeNode(4, None, None)), Some(TreeNode(4, None, None)) )),
// 			Some(TreeNode(3, None, None)) 
// 		)),
// 	Some(TreeNode(6, None, None  ))
// )

// val root = TreeNode(100, None , None) //true


// val root = TreeNode(3,
// 	None,
// 	Some(TreeNode(20, Some(TreeNode(15, None, None)), None  ))
// ) // false

isBalanced(root)
