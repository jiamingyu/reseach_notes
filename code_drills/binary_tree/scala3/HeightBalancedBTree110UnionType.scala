
type ChildLevelCountOrNotBalancedBT = Int | Boolean

case class TreeNode(val v: Int, var left: Option[TreeNode] = None, var right: Option[TreeNode] = None)

type NodeOrResult = ChildLevelCountOrNotBalancedBT | TreeNode


def calibBalanced(nd: TreeNode): ChildLevelCountOrNotBalancedBT = {
	println(s" >>> calib for ${nd}")

	(nd.left, nd.right) match {
		// Leaf
		case (None, None) => {
			println(s"${nd.v} is leaf")
			1
		}

		// Either side is None
		case (Some(nd2), None) => {
			println(s"left child only: ${nd2}")

			calibBalanced(nd2) match {
				case ct: Int if ct ==1  => { // Must check value as 1, since > 1 will disqualify to false directly
					println(s"${nd.v} calib ${ct} + 1")
					ct + 1
				}
				case _ => false 
			}
		}
		case (None, Some(nd)) => {
			println(s"right child only: ${nd}")
			calibBalanced(nd) match {
				case ct: Int if ct == 1 => ct + 1
				case _ => false
			}
		}

		// Full Tree Node
		case (Some(l), Some(r)) => {
			println(s"Full Tree Node: ${l}, ${r}")
			(calibBalanced(l), calibBalanced(r)) match {

				// <<demo_bug_1 test>>

/** Bug: Any side if already not height balanced, should be treated as false;
 * 
 * [uncomment below 3 lines to prove by running <<demo_bug_1 test>>]
				case (a, b) if (a == Left(false) &&  b== Left(false)) => Left(false)

				case (Right(a), Left(false))=>Right(a+1)
				
				case (Left(false), Right(a)) => Right(a+1)
				*/

				//comment out below case match 
				case (false, false) => {
					println(s"<flag False as of valid> Back to stack Full Tree Node: \n\t check result is false for ${l}, \n\t check result is false for  ${r}")
					false
				}
				// End of  <<demo_bug_1 test>>, comment 

				case (a: Int, b: Int) if Math.abs(a-b) > 1 => {
					println("invalid height for ${nd.v} as root ")
					false
				}
				case (a: Int, b: Int)  => {
					println(s"see balanced height ${a}... ${b} ...for ${nd.v} as root")

					Math.max(a,b) + 1

				} 
				case _ => {println("uncovered scenario, please debug if you see this !!"); false}


			}
		}

	}
}

def isBalanced(root: TreeNode): Boolean = {
	calibBalanced(root) match {
		case b: Boolean if b == false => false
		case l: Int => true
		case _ => println("Not identified"); false

	}
}

//<<demo_bug_1 test>>  Test case to prove related comments
val root = TreeNode(3,
	Some(TreeNode(9, Some(TreeNode(5, Some(TreeNode(2, None, None)), None)) , None)),
	Some(TreeNode(20, Some(TreeNode(15, None, None)), Some(TreeNode(7, None, None))))
)

// [3,9,20,null,null,15,7]: true
// val root = TreeNode(3,
// 	Some(TreeNode(9, None , None)),
// 	Some(TreeNode(20, Some(TreeNode(15, None, None)), Some(TreeNode(7, None, None))))
// )


// [1,2,2,3,3,null,null,4,4], false
// val root = TreeNode(1,
// 	Some(TreeNode(2, 
// 			Some(TreeNode(3, Some(TreeNode(4, None, None)), Some(TreeNode(4, None, None)) )),
// 			Some(TreeNode(3, None, None)) 
// 		)),
// 	Some(TreeNode(6, None, None  ))
// )

// val root = TreeNode(3,
// 	None,
// 	Some(TreeNode(20, Some(TreeNode(15, None, None)), None  ))
// ) // false

// val root = TreeNode(100, None , None) //true

isBalanced(root)

