import { BNode, convert2BTreeBFS } from "./BTreeModule";

function rightSideView(root: BNode<number> | null): number[] {
    let rtn  = [] as number[]
    let levelCur = 0
    
    recurRtTraverse(root, rtn, levelCur)





    return rtn
};

function recurRtTraverse(nd: BNode<number>, artn: number[], levelCur: number) {
    // console.log(artn)

    if (levelCur == artn.length) {
        artn.push(nd.value)
        levelCur += 1
    }


    if (nd.right != null && nd.right.value != null) {
        recurRtTraverse(nd.right, artn, levelCur)
    }
    if (nd.left != null && nd.left.value != null) {
        recurRtTraverse(nd.left, artn, levelCur)
    } 
}

// const root = [1,2,3,null,5,null,4]
// const root = [1,null,3]
const root = [] as number[]

const r = convert2BTreeBFS(root)
const ss = rightSideView(r)
console.log(ss)
