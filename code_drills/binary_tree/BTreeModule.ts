export type BNode<T> = {value: T, left: BNode<T>, right: BNode<T>}


export const displayTree = (root: BNode<number>) =>  {
    console.log(`now starting present ${root}`)

    const q = new Array<BNode<number>>()
    q.push(root)
    q.push({value: Number.MAX_VALUE} as BNode<number>)

    let itr : BNode<number> = undefined
    let s : String = ''

    const safepush = (q: Array<BNode<number>>, node: BNode<number>) => {
        if (node != undefined) {
            q.push(node)
        } else {
            q.push( {value: Number.MIN_VALUE} as BNode<number> )
        }
    }

    while(true) {
        itr = q.shift()

        if (itr.value == Number.MAX_VALUE) {
            console.log(`Tree level value : ${s}`)

            s = ''
            q.push({value: Number.MAX_VALUE} as BNode<number>)
            continue
        }

        safepush(q, itr.left)
        safepush(q, itr.right)

        // prepare string in line
        if (itr.value == Number.MIN_VALUE) {
            s = s.concat(`||empty||`)
            continue
        }

        s = s.concat(`|| ${itr.value} ||`)
        
        if (q.filter(x => x.value != Number.MIN_VALUE && x.value != Number.MAX_VALUE).length == 0) {
            console.log(`Tree level value : ${s}`)
            break;
        }
    }




}

export const convert2BTreeBFS = (inNodes : number[]) => {
    const BNodeQ = new Array<BNode<number>>()
    const root : BNode<number> = {value: inNodes[0], left: undefined, right: undefined}
    BNodeQ.push(root)

    let nodeItr = BNodeQ.shift()
    let idx = 1

    
    while(idx < inNodes.length ) {

        nodeItr.left = {value: inNodes[idx++], left: undefined, right: undefined}
        BNodeQ.push(nodeItr.left)

        nodeItr.right = {value: inNodes[idx++], left: undefined, right: undefined}
        BNodeQ.push(nodeItr.right)

        // console.log(`left value :   ${nodeItr.left.value}`)
        // console.log(`right value :   ${nodeItr.right.value}`)

        nodeItr = BNodeQ.shift()
        while(nodeItr.value == null)
            nodeItr = BNodeQ.shift()

        // console.log(`now pointing to ${nodeItr.value}`)
    }



    return root
}


// const inData = [3,null,1,6,2,0,8,null,null,7,4]
// const inData = [3,5,1,6,2,0,8,18,17,7,4]
// const inData = [3,5,1,6,2,0,8,null,null,7,4]

// const r = convert2BTreeBFS(inData)
// displayTree(r)
