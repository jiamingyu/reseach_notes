/**
 * Restriction: All number values positive
 * @param target 
 * @param nums 
 * @returns 
 */
function minSubArrayLenBinary(target: number, nums: number[]): number {

    let cum = 0
    const sumCum = nums.map(x=> cum=cum+x) //This break principle, but in TS any better way?
    // Map to [value of last sum] then zip

    let sumSize = nums.length
    let rtn = 0
 
    while(sumSize >= 1) {
        let qualifiedCt = 0
        let subSum : number[] = Array<number>(nums.length).fill(-1)

        for(let i=0; i <= nums.length - sumSize; i++) {
            if (i==0 ) {
                subSum[i] = sumCum[sumSize-1]
            } else {
                subSum[i] = sumCum[i+sumSize-1] - sumCum[i-1]
            }

            if (subSum[i] < target)
                continue

            qualifiedCt+=1
            // console.log(`index ${i} with sumSize ${sumSize} has qValue: ${subSum[i]}`)
        }

        if (rtn==0 && qualifiedCt == 0) {
            // sumSize starts at full length but still nothing qualify
            return 0
        }

        if (rtn != 0 && qualifiedCt == 0){
            // last round already captured possible count
            // this is reduce rounds and found nothing qualified, return last rounds

            console.log(`last round check should be good although this round too short`)
            return rtn
        }

        // Now qualifiedCt > 0
        // could be oversized, needs to try another round of downsize
        rtn = sumSize
        if (sumSize == 1)
            return rtn

        sumSize = Math.ceil(sumSize/2)
        // console.log(`AAAA: ${rtn}, ${sumSize} `)
    }

    return rtn
}

// const targetb = 7
// const nums209b = [2,3,1,2,4,3]

const targetb = 4
const nums209b = [1,4,4]

// const targetb = 11 
// const nums209b = [1,1,1,1,1,1,1,1]

console.log("Min length of subarray to be EQ or GT a number is: ")
console.log(minSubArrayLenBinary(targetb, nums209b))

// console.log(Array<number>(3).fill(-9))
