/**
 * Restriction: All number value positive
 * @param target 
 * @param nums 
 * @returns 
 */
function minSubArrayLen(target: number, nums: number[]): number {

    const asserts = nums.filter(x => x >= target)
    if (asserts.length > 0) {
        // return asserts.reduce((ac, v) => Math.max(ac, v))
        return 1
    }

    let subSize = 1
    let subSum : number[] = [ ... nums]

    while(subSize < nums.length) {
        let sizeSpecificMax = 0
        for(let i=0; i < nums.length - subSize; i++) {
            subSum[i] = subSum[i] + nums[i+subSize]
            if (subSum[i] < target)
                continue
            sizeSpecificMax = Math.max(subSum[i], sizeSpecificMax)
            if (sizeSpecificMax == subSum[i]) {
                console.log(`update to ${i} to ${i+subSize}, with max of ${sizeSpecificMax}`)
            }
        }
        if (sizeSpecificMax > 0)
            return subSize

        subSum[subSum.length-subSize] = -1 //mark invalid entry
        subSize += 1
    }
    return 0
}

// const target = 7
// const nums209 = [2,3,1,2,4,3]

// const target = 4
// const nums209 = [1,4,4]

const target = 11 
const nums209 = [1,1,1,1,1,1,1,1]

console.log(minSubArrayLen(target, nums209))
