/**
 * https://leetcode.wang/leetCode-126-Word-LadderII.html
 * 
 * https://leetcode.wang/leetCode-127-Word-Ladder.html 
 * 
 */
// TODO: const buildLadderBFS // using queue<List<numbers>>

const buildLadderDFS : (ladderStr: string, wordStart: string, wordEnd: string, wordSet: Set<string>)=> void = 
        (ladderStr: string, wordStart: string, wordEnd: string, wordSet: Set<string>) => {
            // wordSet.forEach(x=> console.log(x))
            
            if (computePositionedDelta(wordStart, wordEnd)==1) {
                const r = ladderStr.concat(`, ${wordEnd}`)
                console.log(`FULL_PATH: ${r}`)

            }

            wordSet.forEach ( w => {
                // console.log(`looking for delta: ${w} `)
                if (computePositionedDelta(wordStart, w)==1) {
                    // console.log(`find 1 char delta: ${w} `)
                    wordSet.delete(w)
                    buildLadderDFS(wordStart.concat(`, ${w}`), w, wordEnd, wordSet)
                }
            } )


        }

const computePositionedDelta : (word: string, beginWord: string)=>number = 
(word: string, word2: string) => {
    const cs  = word.split('')
    if (word.length != word2.length)
        return -1

    let count = 0
    for(const idx in cs) {
        if (cs[idx] != word2.charAt(Number(idx))) {
            count +=1
        }

    }
    return count
    
}

/** 
// Test computePositionedDelta
const r = new Set()
for(const word of wordList) {
    if ( (computePositionedDelta(word, beginWord) == 1)) {
        r.add(word)
    }
}

r.forEach(x => console.log(x))
// console.log(`${r}`)
*/

const beginWord = "hit"
const endWord = "cog"
const wordList = ["hot","dot","dog","lot","log"]
const wordSet : Set<string> = new Set(wordList)

buildLadderDFS('', beginWord, endWord, wordSet)



/** OPTIMIZED SOLUTION TO FIND SHORTEST PATH 
 * 
 * K for next matched char count, d for final path nodes count
 * 
 * one way : K ^ d
 * both ends try: K^(d/2) + K^(d/2)
 * 
 * 
 */

function buildShortestLadder(beginWord: string, endWord: string, wordSet: Set<string>) : List<String>  {
    const headPaths : Array<List<string>>  = [] as Array<List<string>>
    const tailPaths : Array<List<string>>  = [] as Array<List<string>>

    headPaths.push([beginWord])
    tailPaths.push([endWord])

    doBuildLadder(beginWord, endWord, wordSet, headPaths, tailPaths)
    
    
}

console.log(`===== Now start showing shortest path impl by exploring both ends =====`)
const wordSet2 : Set<string> = new Set(["hot","dot","dog","lot","log"])
r : List<String> = buildShortestLadder(beginWord, endWord, wordSet2)


function doBuildLadder(beginWord: string, endWord: string, wordSet: Set<string>, headPaths: List<string>[], tailPaths: List<string>[]) {
    
    for (const headPath of headPaths) {
        for(const tailPath of tailPaths) {

            const headPaths2 : Array<List<string>>  = [] as Array<List<string>>
            const tailPaths2 : Array<List<string>>  = [] as Array<List<string>>

            const pathSet = new Set([...headPath].concat(tailPath))
            if (pathSet.size == wordSet.size + 2) {
                console.log(`NO path possible this way`)
                continue
            }

            let headW = [ ... headPath][headPath.length-1]
            let tailW = [ ... tailPath][tailPath.length-1]


            if (headW != undefined && headW === tailW) {
                        console.log(` ${[...headPath].join(',')}  ... :${headW}: ... ${[...tailPath].join(',')}`)
                        return
            }

            // let visitedHead : Set<string>= new Set<string>() // bug: visited can not be shared for diff tracked collection
            let visited = [ ... headPath].concat([ ... tailPath])
            let identifiedNew = false
            let headBufLocal : string[] = []
            let tailBufLocal: string[]  = []

            wordSet.forEach(w=>{
                if (visited.filter(x=>x===w).length ==0 && computePositionedDelta(w, headW)==1){
                    headBufLocal.push(w)
                }
            })

            wordSet.forEach(w=>{
                if (visited.filter(x=>x===w).length ==0  && computePositionedDelta(w, tailW) == 1) {
                    tailBufLocal.push(w)
                }
            })

            let found : boolean = false
            headBufLocal.forEach( s => {
                tailBufLocal.forEach ( e => {
                    if (s === e) {
                        found = true
                        console.log(`full path:    ${headPath} || ${s}  || ${tailPath}`)
                    }

                })}
            )

            if (!found) {
                console.log(`Not found, preparing for next stack craweling ...`)
                headBufLocal.forEach( s => {
                    console.log(`s val: ${s}`)
                    const wsBufHead2 = [...headPath]
                    wsBufHead2.push(s)

                    headPaths2.push(wsBufHead2)

                })
                tailBufLocal.forEach ( e => {
                    console.log(`e val: ${e}`)
                    const wsBufTail2 = [...tailPath]
                    wsBufTail2.push(e)

                    tailPaths2.push(wsBufTail2)
                })

                doBuildLadder(beginWord, endWord, wordSet, headPaths2, tailPaths2)
            }
        }
    }

}

