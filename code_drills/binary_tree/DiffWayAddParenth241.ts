/**
https://leetcode.wang/leetcode-241-Different-Ways-to-Add-Parentheses.html
 * 
 */

const ops = ['+','-','*','/'] 
const opMap = new Map<string, (l: number, r: number)=>number> ([
    ["+", (l: number, r: number)=>l+r],
    ["-", (l: number,r: number)=>l-r],
    ["/", (l: number,r: number)=>l/r],
    ["*", (l: number,r: number)=>l*r],
])

function diffWaysToCompute(priorNum: number, priorOp: string, s: string, str2nums: Map<string, Array<number>>): Array<number> {
    if (s.length==0)
        return []

    console.log(str2nums)

    if (str2nums.get(s) != undefined)
        return str2nums.get(s)


    // console.log(`${priorNum}, ${priorOp}, ${s}`)

    const rc = Array.from(s)

    let ci = 0
    let digitsBuf  = ''
    let rtn : number[] = [] 
    let keepCrawl = true
    

    while(keepCrawl && ci < s.length) {
        if (ops.filter(o=> o == rc[ci]).length > 0){

            keepCrawl = false
            const digit = +digitsBuf
            
            
            //scenario one: left happen first
            const calibLft = opMap.get(priorOp).apply(this, [priorNum, digit])
            // console.log(`${priorNum}, ${lft}:calibLft: ${calibLft}`)

            const sc1 = diffWaysToCompute(calibLft, rc[ci], s.substring(ci+1), str2nums)
            // console.log(`sc1 : ${sc1}`)
            rtn = rtn.concat(Array.from(sc1))
            // console.log(`rtn after sc1 : ${rtn}`)
            // sc2
            // 

            ////scenario two: right happen first
            const calibRightNums = diffWaysToCompute(digit, rc[ci], s.substring(ci+1),str2nums)
            // console.log(`, calibRight: ${calibRightNums}`)
            for(const n of calibRightNums){
                const sc2 = opMap.get(priorOp).apply(this, [priorNum, n]) 
                // console.log(`priorNum:${priorNum}, priorOp: ${priorOp}, n: ${n}, sc2 : ${sc2}`)
                rtn = rtn.concat(sc2)
            }
            
            //reset buf
            digitsBuf=''
        }

        digitsBuf = digitsBuf.concat(rc[ci])
        ci++
    }
    // handle last one as digits
    if (rtn.length==0){

        //
        const lastCalib = opMap.get(priorOp).apply(this, [priorNum, +digitsBuf])
        // console.log(`lllllength 0: ${lastCalib}`)


        rtn.push(lastCalib)
    }

    console.log(`${rtn}:::${s}`)

    str2nums.set(s, rtn)
    return rtn
};


const s =  "2-1-1"
console.log(diffWaysToCompute(0, '+', s, new Map<string, Array<number>>()))
//TODO: use Set(Array), Map<string_of_digs, [nums]>