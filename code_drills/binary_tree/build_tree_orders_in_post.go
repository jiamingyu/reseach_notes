package main

import (
	"encoding/json"
	"fmt"
)

/*
*
https://leetcode.com/problems/construct-binary-tree-from-inorder-and-postorder-traversal/description/

Input: inorder = [9,3,15,20,7], postorder = [9,15,7,20,3]
Output: [3,9,20,null,null,15,7]
*/
func buildTree(postorder []int, inorder []int) *TreeNode {
	inOrderv2Idx = make(map[int]int, len(inorder))
	inorderIdxVisited = make(map[int]bool, len(inorder))

	for idx, v := range inorder {
		inOrderv2Idx[v] = idx
	}
	postorderIdxCur = len(postorder) - 1

	rootNode := &TreeNode{V: postorder[postorderIdxCur], parentInorderIdxAsRightChld: -1}
	activeNode = rootNode
	// mark visited
	inorderIdx := inOrderv2Idx[rootNode.V]
	inorderIdxVisited[inorderIdx] = true
	doBuild(postorder, inorder)

	return rootNode
}

func doBuild(postorder []int, inorder []int) {
	postorderIdxCur -= 1
	if postorderIdxCur == -1 {
		return
	}

	stackLocalNode := activeNode
	// fmt.Println(stackLocalNode)
	currentNodeInorderIdx := inOrderv2Idx[stackLocalNode.V]

	// check if fall into right child
	if inOrderv2Idx[postorder[postorderIdxCur]] > currentNodeInorderIdx { // expand right
		stackLocalNode.Right = &TreeNode{V: postorder[postorderIdxCur], parentInorderIdxAsRightChld: inOrderv2Idx[stackLocalNode.V]}

		// mark visited
		inorderIdx := inOrderv2Idx[postorder[postorderIdxCur]]
		inorderIdxVisited[inorderIdx] = true

		activeNode = stackLocalNode.Right
		granuRightChildBuild(postorder, inorder, stackLocalNode) // left child will stack back here
		//Attention: preorderIdxCur mostly incremented after above
	}
	// After stack call above, preorderIdxCur mostly moved and need to recheck
	if postorderIdxCur == -1 { // TODO: optimize
		return
	}

	// Now Lets work on post left child crawl

	// next preorderIdxCur falls between left child & stackLocalNode
	/**Let talk about scenario: btwn left child & current node :
	Based on rule of pre-order traverse, this will never happen. traverse left over in inorder, then next inorder data must be the first right child

	*/

	// Only handle local when current is root of left expanding (var as leftCrawlRoot)
	if stackLocalNode.is1stLeftChild(postorder) {
		//post order: kick on the left child, but it is reverse-POSTOrder, meaning active node should stay on parent
		stackLocalNode.Left = &TreeNode{V: postorder[postorderIdxCur], parentInorderIdxAsRightChld: -1}
		activeNode = stackLocalNode.Left

		// mark visited
		inorderIdx := inOrderv2Idx[postorder[postorderIdxCur]]
		inorderIdxVisited[inorderIdx] = true

		granuRightChildBuild(postorder, inorder, stackLocalNode)

	} else {

		// just stack up
	}

}

func (nd *TreeNode) is1stLeftChild(postorder []int) bool {

	// fmt.Println("nd >>>>>>>  ", nd.V)
	for i := inOrderv2Idx[nd.V] - 1; i > inOrderv2Idx[postorder[postorderIdxCur]]; i-- {
		// fmt.Println("decrese crawl idx inorder: ", i, inorderIdxVisited[i])
		if inorderIdxVisited[i] {
			return false
		}
	}
	return true
}

func granuRightChildBuild(postorder []int, inorder []int, localNode *TreeNode) {
	doBuild(postorder, inorder)
	activeNode = localNode
}

func main() {
	/**
		3
	9		20
		  15   7
		96 88
	*/
	postorder := []int{9, 96, 88, 15, 7, 20, 3}
	inorder := []int{9, 3, 96, 15, 88, 20, 7}

	rootTrNode := buildTree(postorder, inorder)
	v, e := json.Marshal(rootTrNode)
	if e != nil {
		fmt.Println(e)
	} else {
		fmt.Println(string(v))

	}
}

type TreeNode struct {
	Left                        *TreeNode
	Right                       *TreeNode
	parentInorderIdxAsRightChld int // -1 means it is not any other's left child
	V                           int
}

var inOrderv2Idx map[int]int
var inorderIdxVisited map[int]bool

var postorderIdxCur = -1
var activeNode *TreeNode
