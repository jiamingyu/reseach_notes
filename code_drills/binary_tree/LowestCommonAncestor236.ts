// https://leetcode.com/problems/lowest-common-ancestor-of-a-binary-tree/

// import * as BTreeDrill from "./BTreeDrill"
import { BNode, convert2BTreeBFS, displayTree }  from "./BTreeModule"



const lowestCommonAncestor : (root: BNode<number> | null, p:number, q: number )=> number = (root: BNode<number> | null, p:number, q: number ) =>  {

    return asChildren(root, p, q)[0]
}

/**
 * 
 * @param root 
 * @param p 
 * @param q 
 * @returns [answer, p, q]: must be 3 W. first as result. size can only be empty, 1 or 3, 
 */
const asChildren :  (root: BNode<number>, p:number, q: number ) => Array<number>  =
 (root: BNode<number> , p:number, q: number ) => {
    console.log(`root value:  ${root == undefined ? 'undefined_v' : root.value}`)
    if (root == undefined || root.value == null) {
        console.log(`return empty col`)
        return []
    }

    //one side has 3 elements: directly return venilla case
    const lr = asChildren(root.left, p, q)
    if(lr.length == 3)
        return lr
    const rr = asChildren(root.right, p, q)
    if (rr.length == 3)
        return rr

    // 1 found on each side
    if (lr.length == 1 && rr.length == 1)
        // 3
        return [root.value, lr[0], rr[0]]


    console.log(`  ${root.value} right V: ${rr}  `)

    if (root.value == p || root.value == q) {
        if (lr.length == 1 || rr.length == 1) {// root & one of L/R
            // 3

            return [root.value, p, q]
        }
        else {
            return [root.value]
        }
    }

    if (lr.length==1 && rr.length==0)
        return lr

    if (rr.length==1 && lr.length==0){

        return rr

    }

    return []
}

// const inData = [3,null,1,6,2,0,8,null,null,7,4]
// const inData = [3,5,1,6,2,0,8,18,17,7,4]
const inData = [3,5,1,6,2,0,8,null,null,7,4]

const r = convert2BTreeBFS(inData)
displayTree(r)

// console.log( lowestCommonAncestor(r, 5,1) )
console.log( lowestCommonAncestor(r, 5,4) )


const lowestCommonAncestorFromBFSArray :  (nodes: Array<number>, p:number, q: number ) => number = 
    (nodes: Array<number>, p:number, q: number ) => {
        const c2p = new Map<number, number>()

        c2p.forEach(( v ,k) => console.log(`key: ${k} => ${v}`))

        nodes.forEach ( (v, idx) =>{
            if (v==5) {
                console.log(`xxx: ${idx}, ${nodes[idx * 2 + 1]}, ${nodes[idx * 2 + 2]}`)
            }

            if (idx * 2 + 1 < nodes.length && nodes[idx * 2 + 1] != null)
                c2p.set(nodes[idx * 2 + 1], v)

            if (idx * 2 + 2 < nodes.length && nodes[idx * 2 + 2] != null)
                c2p.set(nodes[idx * 2 + 2], v)
                
        })

        c2p.forEach(( v ,k) => console.log(`key: ${k} => ${v}`))

        const path : Set<number> = new Set()
        let pbuf = p
        let qbuf = q
        while(c2p.get(pbuf) != undefined) {
            path.add(pbuf)
            pbuf = c2p.get(pbuf)
        }

        while(c2p.get(qbuf) != undefined && !path.has(qbuf)) {
            path.add(qbuf)
            qbuf =c2p.get(qbuf)
        }

        

        
        return qbuf

    }


console.log(lowestCommonAncestorFromBFSArray(inData, 5,1))
console.log(lowestCommonAncestorFromBFSArray(inData, 5,4))
