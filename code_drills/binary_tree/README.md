

Scala3 imp is under [scala3]|(./scala3)

# Binary Tree

- Any BT, BST question first is to use recur/stack approach

- Differentiate B-tree vs B search tree (BST)

- Arrays reprented W. BFS can be used to construct tree, either using queue, or with [N, N*2+1 as left, N*2+2 as right]

- DFS & stack are same solution with different approaches

- Couple of concepts

```
Perfect Binary Tree: All internal nodes have two children and all leaves are at same level.

Complete Binary Tree: All levels are completely filled except possibly the last level and the last level has all keys as left as possible. (It must be a height-balanced binary tree)

Full Binary Tree: A Binary Tree every node has 0 or 2 children


A height-balanced binary tree: A binary tree in which the depth of the two subtrees of every node never differs by more than one.



```


# Questions pattern category (B search Tree as default)


##  Perfect tree, complete tree


- Complete TreeNode count : 222:

https://leetcode.com/problems/count-complete-tree-nodes/description/

[FullNodeCount222.ts](./FullNodeCount222.ts)


- convert sorted array to height-balanced BST (both side tree height delta is 0 or 1) (108) + source is LinkedList sorted (109)

Here reserve the discussion on complete b-tree vs height balanced b-tree

[Sorted2HeightBalancedBTree.scala](./scala3/Sorted2HeightBalancedBTree.scala)

- convert sorted array to BST with all variances (95, 96)

- 110 balanced tree is also regarding height balanced tree

[HeightBalancedBTree110.scala](./scala3/HeightBalancedBTree110.scala)

Two info needs to pass through stack (Only 1 of them but different in each node): A. Max sub level is still a Height-balanced node. B. Boolean indicating if Height-balanced. Scala Either[A, B] Type fits this need greately!! And it helps achieved Ln(N) in BigO

This brings me to question on marking it as "easy level" is a very tricky topic. By reading Gk4Gk solution, I believe the author is wrong is marking the solution 's Big O when getHeight() & isHeightBalanced() both called separately in recursive way.
 
Compare [HeightBalancedBTree110UnionType.scala](./scala3/HeightBalancedBTree110UnionType.scala): What about union type of scala3 instead of Either?

Regarding `union type` , which in typescript also used. However, the type pattern match in typescript (I would say there isn't such feature really in all None-typestatic proramming language ) can not compete with that of Scala.

## Order Traversing: In, Pre, post Order (94, 144, 145, 199*)

- Taking [199]|(./BTreeRightView199.ts as first to impl for post order considering this is right child first corner case

- 144 preorder: 

- 145 Postorder

- 156: Another form of post order

https://leetcode.com/problems/binary-tree-upside-down/

https://www.geeksforgeeks.org/flip-binary-tree/

## BTree construction from 2 types of traverse collections

### [105](https://leetcode.com/problems/construct-binary-tree-from-preorder-and-inorder-traversal/description/)

[build_tree_in_pre.go](./build_tree_in_pre.go)

This is the most hardship mis-categorized topic in leetcode. I read and practices couple of approaches on this before get verified solution

- What most post miss is not fully understanding traverse rules, esp for pre-order

- B4 work on this, pre-order tree traveral topic, including Morrise algorithm, should be studied first.


#### Solution description:

- Each left child re-cur build create new stack, with local ref to stack specific node

- Right child create is tricky: A. For upcoming value on right side of current node, leave it to parent (throw up to upper stack).
B. Parent handling upcoming value on left side but GT left child, create left child 's right child ???? what is there has been left_child's right child existed?




### [106]  https://leetcode.com/problems/construct-binary-tree-from-inorder-and-postorder-traversal/description/




Opean for discussion: One of the type must be In-order, with the other either be pre or post order

## BST validate (98), correct(99): 2 nodes order misplaced

98 validate: https://leetcode.com/problems/validate-binary-search-tree/

99 correction : https://leetcode.com/problems/recover-binary-search-tree/


# Other misc quiestions using BTree for solution

- Lowest common ancestor: 236  (https://leetcode.com/problems/lowest-common-ancestor-of-a-binary-tree/)



- 116

https://leetcode.com/problems/populating-next-right-pointers-in-each-node/


- 120 : Spcific DFS dealing with tri-angle

https://leetcode.com/problems/triangle/


- 128, high level: iterate array and build binary search tree, then read in order and check if 2 numbers consecutive.

 https://leetcode.com/problems/longest-consecutive-sequence/



-129

## N-th min number treenode (230)
 
A. in-order traverse

B. Binary opt-out solution


- BT max sum path (124), I would rather think this as Graph topic


- https://leetcode.wang/leetcode-226-Invert-Binary-Tree.html

Skip impl for now considering its simplicity. Stack, Q and heap recur(or you can say still stack) are all 3 way that can do that.

- House robbery

- 213
- 198

- minimum-size-subarray-sum 209

[MinSubarraySum209.ts](./MinSubarraySum209.ts)

[MinSubarraySum209binary.ts](./MinSubarraySum209binary.ts)

- TODO: https://leetcode.com/problems/two-sum-iv-input-is-a-bst/description/

This is a comb of B-Tree with those [NSumDrill](../numbers/NSumDrill.scala)



- 241 (variance to add parenthesis) : [DiffWayAddParenth241.ts](./DiffWayAddParenth241.ts)


