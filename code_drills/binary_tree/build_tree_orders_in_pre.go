package main

import (
	"encoding/json"
	"fmt"
)

/*
*

Input: preorder = [3,9,20,15,7], inorder = [9,3,15,20,7]
Output: [3,9,20,null,null,15,7]
Example 2:

Input: preorder = [-1], inorder = [-1]
Output: [-1]

  - Definition for a binary tree node.
  - type TreeNode struct {
  - Val int
  - Left *TreeNode
  - Right *TreeNode
  - }
*/
func buildTree(preorder []int, inorder []int) *TreeNode {
	inOrderv2Idx = make(map[int]int, len(inorder))
	inorderIdxVisited = make(map[int]bool, len(inorder))
	for idx, v := range inorder {
		inOrderv2Idx[v] = idx
	}

	treeRootNode := &TreeNode{V: preorder[0]}
	inorderIdxVisited[0] = true
	activeNode = treeRootNode

	doBuild(preorder, inorder)
	return treeRootNode
}

func doBuild(preorder []int, inorder []int) {
	preorderIdxCur += 1
	if preorderIdxCur == len(preorder) {
		return
	}

	stackLocalNode := activeNode
	currentNodeInorderIdx := inOrderv2Idx[stackLocalNode.V]

	// check if fall into left child
	if inOrderv2Idx[preorder[preorderIdxCur]] < currentNodeInorderIdx { // expand left
		inorderIdxVisited[preorderIdxCur] = true
		stackLocalNode.Left = &TreeNode{V: preorder[preorderIdxCur]}
		activeNode = stackLocalNode.Left
		granuLeftChildBuild(preorder, inorder, stackLocalNode) // left child will stack back here
	}
	//Attention: preorderIdxCur mostly incremented after above after stack call
	if preorderIdxCur == len(preorder) { // TODO: optimize
		return
	}

	if stackLocalNode.is1stRightChild(preorder) {
		stackLocalNode.Right = &TreeNode{V: preorder[preorderIdxCur]}
		inorderIdxVisited[preorderIdxCur] = true
		activeNode = stackLocalNode.Right

		granuLeftChildBuild(preorder, inorder, stackLocalNode)

	}
	// just stack up
}

func granuLeftChildBuild(preorder []int, inorder []int, localNode *TreeNode) {
	doBuild(preorder, inorder)
	activeNode = localNode
}

func main() {
	/**
		3
	9		20
		  15   7
		96 88
	*/
	preorder := []int{3, 9, 20, 15, 96, 88, 7}
	inorder := []int{9, 3, 96, 15, 88, 20, 7}
	//               0  1  2   3   4   5   6

	/**
		3
	9		20
		  15   7
	*/
	// preorder := []int{3, 9, 20, 15, 7}
	// inorder := []int{9, 3, 15, 20, 7}

	rootTrNode := buildTree(preorder, inorder)
	v, e := json.Marshal(rootTrNode)
	if e != nil {
		fmt.Println(e)
	} else {
		fmt.Println(string(v))

	}
	fmt.Println(activeNode)
}

type TreeNode struct {
	Left  *TreeNode
	Right *TreeNode
	V     int
}

func (nd *TreeNode) is1stRightChild(preorder []int) bool {

	if inOrderv2Idx[nd.V] > inOrderv2Idx[preorder[preorderIdxCur]] { // extra verify
		return false
	}

	/** Inorder rule: Regardless how far rightward from certain left child,
	on the in-order data, the far end right child should still be somewhere on left of the parent of the left child (visited).
	Therefore, if cur pointed inorder data separated by visited node/s from current node, that means the cur proposed node should NOT be added as current Node's right child
	*/
	for i := inOrderv2Idx[nd.V] + 1; i < inOrderv2Idx[preorder[preorderIdxCur]]; i++ {
		if inorderIdxVisited[i] {
			return false
		}
	}
	return true
}

var inOrderv2Idx map[int]int
var inorderIdxVisited map[int]bool

var preorderIdxCur = 0
var activeNode *TreeNode
