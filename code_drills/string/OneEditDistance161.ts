
function isOneEditDistance(s1: string, s2: string): boolean {
    let rtn = true

    switch ( s1.length - s2.length ) {
        case 0 : {
            let gap = 0
            for(let i = 0; i < s1.length;i++) {
                if (s1.charAt(i) != s2.charAt(i)){
                    gap += 1
                    if (gap > 1)
                        return false
                }
            }
            break;

        }
        case (s1.length - s2.length)*Math.abs((s1.length - s2.length))/(s1.length - s2.length) : { // s1 longer

            let gap =0
            for(let i = 0; i < s1.length;i++) {
                if (gap==0 && s1.charAt(i) != s2.charAt(i)){
                    gap += 1
                    
                } 
                if (gap != 0 && s1.charAt(i) != s2.charAt(i-1)) {
                    return false
                }
            }

            break
        }
        case (-1)*(s1.length - s2.length)*Math.abs((s1.length - s2.length))/(s1.length - s2.length): {
            let gap =0
            console.log(`s1 is shorter than s2`)
            for(let i = 0; i < s1.length-1;i++) {
                if (gap==0 && s1.charAt(i) != s2.charAt(i)){
                    gap += 1
                    
                } 
                if (gap != 0 && s1.charAt(i) != s2.charAt(i+1)) {
                    return false
                }
            }

            break;
        }
        default: return false // length delta more than one
    }
    return rtn
}

// const s161 = "1203"
// const t = "1213"

// const s161 = "cab"; const t = "ad";

const s161 = "ab"; const t = "acb"

console.log(isOneEditDistance(s161, t))



