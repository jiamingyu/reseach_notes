package main

import (
	"fmt"
	"strings"
)

func main() {

	strs := []string{"eat", "tea", "tan", "ate", "nat", "bat"}

	fmt.Println(groupAnagrams(strs))

}

func groupAnagrams(strs []string) [][]string {
	if len(strs) == 0 {
		return [][]string{{}}
	}
	// one empty string
	if len(strs) == 1 {
		return [][]string{{strs[0]}}
	}

	m := make(map[string][]string)
	for _, s := range strs {
		k := makeIdxCountStr(s)
		vs, ok := m[k]
		if ok {
			m[k] = append(vs, s)
		} else {
			m[k] = []string{s}
		}

	}
	r := make([][]string, 0)
	for _, vs := range m {
		r = append(r, vs)
	}

	return r

}

func makeIdxCountStr(s string) string {
	runeidxCount := make([]int, 26)

	for _, r := range s {
		d := r - 'a'
		// fmt.Println(d)
		runeidxCount[d] += 1
	}

	p := strings.Trim(strings.Join(strings.Fields(fmt.Sprint(runeidxCount)), ":"), "[]")
	return p
}
