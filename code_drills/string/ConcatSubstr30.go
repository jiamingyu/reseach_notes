package main

import "fmt"

var primeNumberIncremental = PrimeNumberIncremental()

func main() {
	//    01234567890123456789   should be 0,9
	// s := "barfoothefoobarmanaz"
	// words := []string{"foo", "bar"}

	//    012345678901234567890123   should be no concatenated substring
	// s := "wordgoodgoodgoodbestword"
	// words := []string{"word", "good", "best", "word"}

	//    012345678901234567890123   should be Output: [6,9,12]
	s := "barfoofoobarthefoobarman"
	words := []string{"bar", "foo", "the"}

	fmt.Printf("index w. full string match is :  %d \n", FindSubstringCombStartingIdxes(s, words))
	//FindSubstringCombStartingIdxes("foobarbarfoobarfuck", []string{"foo", "bar"})

}

// package function to gen prime number
func PrimeNumberIncremental() func() int { //func() int, is return type
	d := []int{2, 3, 5, 7, 11, 13, 17, 19}
	var ct int

	return func() int {
		r := d[ct]
		ct += 1
		return r
	}
}

/*
*
O(s.size * Ln(totoal_char_in_words))
*/
func FindSubstringCombStartingIdxes(s string, words []string) []int {
	primeCalibPerPos := make([]int, len(s))

	// A. None pointer, which has to init here
	// var root Root = Root{nodes: make([]SingleCharTriNode, 26)}
	// root.ReadWords(words)

	// B. Pointer
	root2 := new(Root)
	root2.ReadWords(words)

	scanDistance := len(words[0]) * len(words)
	//Each legitimate position will be scanned extending to word's total length
	for idx := 0; idx < len(s)-scanDistance; idx++ {
		primeCalibPerPos[idx] = root2.CalibPrime(s[idx:idx+scanDistance], words)
	}

	fmt.Println(primeCalibPerPos)

	r := make([]int, 0)
	for idx, v := range primeCalibPerPos {
		if v == root2.primeNumberMultiply {
			r = append(r, idx)
		}

	}

	return r
}

type Root struct {
	inited              bool
	nodes               []SingleCharTriNode
	primeNumberMultiply int
	totalWordSize       int
}

type LoadCalib interface {
	ReadWords(words []string)
	CalibPrime(s string) int
}

type SingleCharTriNode struct {
	v        rune
	children []SingleCharTriNode
	primeN   int //assigned by calling package function
}

type SingleCharTriNodeOps interface {
	load(w string) int
	checkAssignedPrimeNumber(subStr string) int
}

func (r *Root) CalibPrime(s string, words []string) int {
	unitSize := len(words[0])
	multiLead := 1

	for i := 0; i < len(s)/unitSize; i++ {
		wordLead := s[i*unitSize : (i+1)*unitSize]
		headCharNode := r.nodes[rune(wordLead[0])-'a']
		primeCheckResult := headCharNode.checkAssignedPrimeNumber(wordLead[1:])
		multiLead *= primeCheckResult
	}
	return multiLead
}

func (r *Root) ReadWords(words []string) {
	if r.nodes == nil {
		r.inited = true
		r.nodes = make([]SingleCharTriNode, 26)
	}

	for i := 0; i < 26; i++ {
		r.nodes[i].v = rune('a' + i)
	}

	// load each words to under tri, assign prime on finishing each words
	primeNumberMultiply := 1
	size := 0
	for _, w := range words {
		size = size + len(w)
		primeNumLead := r.nodes[rune(w[0])-'a'].load(w[1:])
		if primeNumLead != 0 {
			primeNumberMultiply = primeNumberMultiply * primeNumLead
		}
	}
	r.totalWordSize = size

	if primeNumberMultiply != 1 {
		r.primeNumberMultiply = primeNumberMultiply
	}

}

/*
*
@return prime number assigned
*/
func (sn *SingleCharTriNode) load(w string) int {
	if len(w) == 1 {
		sn.primeN = primeNumberIncremental()
		return sn.primeN
	}

	if sn.children == nil {
		sn.children = make([]SingleCharTriNode, 26)
	}
	child := &sn.children[rune(w[0])-'a'] //Attn: Without '&', it return copy of child obj instead of reference value copy,
	child.v = rune(w[0])
	return child.load(w[1:])

}

func (sn *SingleCharTriNode) checkAssignedPrimeNumber(subStr string) int {
	r := 1
	switch {
	case len(subStr) == 1 && sn.primeN > 1:
		//TODO: test case:  sn has no primeN assign, throw Error + handle it on main stack
		r = sn.primeN
	case len(subStr) == 0:
		// TODO: test case: to make sure throw error

	// Now only    len(subStr) > 1
	case sn.children == nil:
	case sn.children[rune(subStr[0])-'a'].v == 0:
	default:
		{
			c := sn.children[rune(subStr[0])-'a']
			r = c.checkAssignedPrimeNumber(subStr[1:])
		}
	}
	return r
}
