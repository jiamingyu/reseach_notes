def length_of_longest_substring_two_distinct( s: str) -> int:
    c2_lastidx = {}
    maxl = 0
    
    for i in range(0, len(s)):
        if s[i] in c2_lastidx or len(c2_lastidx.keys()) < 2:
            c2_lastidx[s[i]] = i
            # print(f"update: ${s[i]}, ${i}")
            continue
        #new c
        
        toremove = min(c2_lastidx.values())
        buf = i - toremove
        maxl = buf if buf > maxl else maxl
        
        for k,v in c2_lastidx.items():
            if v == toremove:
                # print(f"to remove : ${v}, ${k} ")
                del(c2_lastidx[k])
                break
        # print(f"maxl: {maxl}, TO_ADD:  {s[i]}, {i}")
        c2_lastidx[s[i]] = i
        
    return maxl + 1
            
# s = "eceba"
s = "aaa"

print(length_of_longest_substring_two_distinct(s))
