package net.jyu.scalademo

import org.scalatest.flatspec.AnyFlatSpec

import scala.collection.mutable
import scala.collection.mutable.{ArrayBuffer, ListBuffer}

class PalindromSpecs extends  AnyFlatSpec{
  "5 longest palindrome" should "" in {
    val s = "cacbcac"
//    val s = "cccbccc"
    println(longestPalindrome3(s))

  }

  def calib(delta: Int, idx: Int, cs: Array[Char]): Int = {
    delta match {
      case d: Int if (idx-delta < 0 || idx+delta >= cs.length) => d-1
      case d: Int if (cs(idx-delta) != cs(idx+delta)) =>{
        d-1
      }
      case _ => {
        calib(delta+1, idx, cs)
      }
    }
  }

  def hardCalibMaxPalindrPerIdx(idx: Int, size: Int, malchArray: ArrayBuffer[Int], malchArray2: ArrayBuffer[Int]) = {
    var incre = if (malchArray2(idx) >0) malchArray2(idx) else 1
    while(incre <= size) {
      if (malchArray(idx - incre)> 0)
        malchArray2(idx + incre) = malchArray(idx - incre)
      incre += 1
    }
  }



  "131 prep longest palindrome" should "build matrix indicating all possible palindr " in {
    val s = "cacbcac"
    //    val s = "cccbccc"

    val m=buildFullPalidromMatrix(s)
    m.foreach(println(_))
  }

  "131 integration   " should "    " in {
    val s = "cacbcac"
    //    val s = "cccbccc"

    val m = buildFullPalidromMatrix(s)

    val r = calSegmentsFromMatrix(m)

    r.zipWithIndex.foreach{ case (c, idx) => println(s"comb ${idx}: ${c}") }
  }

  "132   min cuts for partition palindrom " should "  give idx of cut  " in {
    val s = "cacbcac"
    //    val s = "cccbccc"

    val m = buildFullPalidromMatrix(s)

    val r = calSegmentsFromMatrix(m)

//    r.zipWithIndex.foreach { case (c, idx) => println(s"comb ${idx}: ${c}") }
    val r2 = r.foldLeft(r(0)){(a,b) => if (a.length > b.length) b else a}
    println(r2)
  }

  /**
   * This is upstream function of `calSegmentsFromMatrix``
   *
   * @param s
   * @return
   */
  def buildFullPalidromMatrix(s: String): List[List[Boolean]] = {

    val malchArray = ArrayBuffer.fill(s.length * 2 - 1)(0)
    val malchArray2 = ArrayBuffer.fill(s.length * 2 - 1)(0)

    val cs = s.toCharArray.zipWithIndex.map { case (c, i) => if (i != s.length - 1) List(c, '#') else List(c) }.flatten

    calibPalindrMaxLength(cs, malchArray, malchArray2)

    val rmTokens = malchArray.zipWithIndex.filter { case (v, i) => i % 2 == 0 }

    val idxWPaliDt = rmTokens.map { case (v, i) => (v, i / 2) } //.map{case (l, idx) => (idx, idx - l/2, idx + l/2) }

    val startEndMatri = ArrayBuffer.fill(s.length)(ArrayBuffer.fill(s.length)(false))

    idxWPaliDt.foreach { case (l, idx) =>
      startEndMatri(idx)(idx) = true
      var dt = l / 2
      while (dt > 0) {
        startEndMatri(idx - dt)(idx + dt) = true
        dt -= 1
      }
    }

    startEndMatri.map(_.toList).toList
  }


  /**
   * malchArray can be used for leet 131
   *
   * @param cs
   * @param malchArray
   * @param malchArray2
   * @return
   */
  def calibPalindrMaxLength(cs: Array[Char], malchArray: ArrayBuffer[Int], malchArray2: ArrayBuffer[Int]): (Int, Int) = {
    var maxIdx = 0
    for((c,idx) <- cs.zipWithIndex) {
      idx match {
        case i: Int if i == malchArray.length || i==0 => ""
        case _ => {
          val size = calib(0, idx, cs)
          size match {
            case 0 =>
            case s: Int if (cs(idx) == '#') => {malchArray(idx) = s; hardCalibMaxPalindrPerIdx(idx, s, malchArray, malchArray2)}
            case s:Int if (cs(idx) != '#' && s > 1) => {
              malchArray(idx) = s+1 ; hardCalibMaxPalindrPerIdx(idx, s, malchArray, malchArray2)
            }

            case s: Int  if (cs(idx) == '#' && s == 1) => {malchArray(idx) = s}
            case _: Int =>
          }
        }
      }
      maxIdx = if (malchArray(maxIdx) >= malchArray(idx)) maxIdx else idx
    }


    (maxIdx, malchArray(maxIdx))
  }

  def longestPalindrome3(s: String): String = {
    val malchArray = ArrayBuffer.fill(s.length * 2 - 1)(0)
    val malchArray2 = ArrayBuffer.fill(s.length * 2 - 1)(0)

    val cs = s.toCharArray.zipWithIndex.map { case (c, i) => if (i != s.length - 1) List(c, '#') else List(c) }.flatten

    val doubleIdxWLength = calibPalindrMaxLength(cs, malchArray, malchArray2)
    val idx1 = (doubleIdxWLength._1)/2

    (idx1, doubleIdxWLength._2) match {
      case (idx, l) if l %2 ==0 => {
        s.substring(idx - l/2, idx+1+l/2)
      }
      case (idx, l) if l %2 !=0 => {
        s.substring(idx - l/2, idx+l/2+1)
      }
      case _ => ""
    }

  }


  " calSegmentsFromMatrix  " should "bla " in {
    val m = List(
      List(true, false,  false, false, true),
      List(false, false, false, true, false),
      List(false, false, false, false, false),
      List(false, false, false, false, true),
      List(false, false, false, false, false)
    )
    val r = calSegmentsFromMatrix(m)
    r.foreach(println(_))

  }

  def recurCal(startIdx: Int, matrix: List[List[Boolean]]): List[List[String]] = {
    if (startIdx == matrix.length-1)
      return List(List(s"$startIdx"))

    val lbl : ListBuffer[List[String]] = ListBuffer[List[String]]()

    val y : List[List[String]] = recurCal(startIdx+1, matrix)
    for(sy <- y) yield {
      val lb = ListBuffer[String]()
      lb.addOne(startIdx.toString)
      lb.appendAll(sy)
      lbl.addOne(lb.toList)
    }

    for((c, cidx) <- matrix(startIdx).zipWithIndex if (cidx > startIdx &&  cidx+1 < matrix.length && c) ) {
      val lb = ListBuffer[String]()
      val x : List[List[String]] = recurCal(cidx+1, matrix)
      for(sx <- x) yield {
        lb.addOne(s"$startIdx-$cidx")
        lb.addAll(sx)
        lbl.addOne(lb.toList)
      }
    }

    lbl.toList
  }

  def calSegmentsFromMatrix(matrix : List[List[Boolean]]): List[List[String]] = {
    recurCal(0, matrix)
  }

}
