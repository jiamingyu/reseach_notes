  "I the sky is blue" should "blue is sky the" in {
    val cs = "the sky is blue".toCharArray()
    reverseStrInplaceByStartXcludeEndIdx(cs, 0, cs.length)
    reverseCharsInWord(cs)
    println( cs.mkString )

  }
  
  " II Given an input string, reverse the string word by word, [\"t\",\"h\",\"e\",\" \",\"s\",\"k\",\"y\",\" \",\"i\",\"s\",\" \",\"b\",\"l\",\"u\",\"e\"] " should
    "[\"b\",\"l\",\"u\",\"e\",\" \",\"i\",\"s\",\" \",\"s\",\"k\",\"y\",\" \",\"t\",\"h\",\"e\"]" in {

    val cs = "the sky is blue".toCharArray
    reverseStrInplaceByStartXcludeEndIdx(cs, 0, cs.length)

    reverseCharsInWord(cs)
    println( cs.mkString )
  }


  "III Let's take LeetCode contest" should "s'teL ekat edoCteeL tsetnoc" in {
    val s = "Let's take LeetCode contest"
    val cs : Array[Char] = s.toCharArray();

    reverseCharsInWord(cs)
    println(cs.mkString)
  }

  def reverseCharsInWord(cs: Array[Char]): Unit =  {
    val spaceIdxes = ArrayBuffer[Int]()
    cs.zipWithIndex.foreach {
      case (c, idx) if c == ' ' => {
        spaceIdxes.addOne(idx)
      }
      case _ => {}
    }

    spaceIdxes.toList.zipWithIndex.foldLeft(-1)((l, rTupleIdx) => {
      reverseStrInplaceByStartXcludeEndIdx(cs, l + 1, rTupleIdx._1)
      if (rTupleIdx._2 == spaceIdxes.length - 1) // dealing with last words, which end nof with a space but return
        reverseStrInplaceByStartXcludeEndIdx(cs, rTupleIdx._1 + 1, cs.length);
      rTupleIdx._1
    })
  }

  def reverseStrInplaceByStartXcludeEndIdx(cs: Array[Char], startIdx: Int, endIdx: Int): Unit = {
    var l = startIdx;
    var r = endIdx - 1

    while (l < r) {
      val buf: Char = cs(l)
      cs(l) = cs(r)
      cs(r) = buf
      l += 1
      r -= 1
    }

  }
