

import org.scalatest.flatspec.AnyFlatSpec

import scala.collection.mutable.{ArrayBuffer, ListBuffer}

class CollectionDrill extends  AnyFlatSpec{
  case class VersionRec(v: String)  extends Ordered[VersionRec] {
    override def compare(that: VersionRec): Int = {
      val split1 = v.split('.')
      val split2 = that.v.split('.')

      var count = 0
      while(split1.length > count && split2.length > count) {
        (Integer.parseInt(split1(count)) - Integer.parseInt(split2(count)) ) match {
          case x: Int if x==0 => {
          }
          case x => return x/Math.abs(x)
        }
        count+= 1
      }
      // reach here means shorter one has earlier/small version, but still check in case somewhere "01" vs "001"
      if (split1.length != split2.length) (split1.length - split2.length)/math.abs(split1.length - split2.length) else 0

    }
  }

//  implicit def versionOrdering[VersionRec]

  "165 version1 = \"0.1\", version2 = \"1.1\"" should " -1 " in {
    val version1 = "0.1"; val version2 = "1.1";
    println(compareVersion(version1, version2))

  }

  "165 version1 = \"1.0.1\", version2 = \"1\"" should "1" in {
    val version1 = "1.0.1"; val version2 = "1"
    println(compareVersion(version1, version2))

  }

  "165 version1 = \"7.5.2.4\", version2 = \"7.5.3\"" should " -1  " in {
    val version1 = "7.5.2.4"; val version2 = "7.5.3";
    println(compareVersion(version1, version2))
  }

  "165 version1 = \"1.01\", version2 = \"1.001\"" should " 0  " in {
    // I disagree what the 165 case defined.
    val version1 = "1.01";
    val version2 = "1.001"
    println(compareVersion(version1, version2))
  }

  "165 version1 = \"1.0\", version2 = \"1.00\"" should " 0  " in {
    val version1 = "1.0";
    val version2 = "1.00"
    println(compareVersion(version1, version2))
  }

  def compareVersion(version1: String, version2: String): Int = {
    val v1 = VersionRec(version1)
    val v2 = VersionRec(version2)
    v1.compare(v2)
  }
}
