import { MyStack } from "./StackNQ"; 
/**
 * https://leetcode.com/problems/trapping-rain-water/
 * 
 * 
 * @param height 
 * @returns 
 */
function trap(height: number[]): number {
    const trapStack : MyStack<number>  = new MyStack<number>();
    let trapped = 0

    let idxCur = 1

    let lftIdx = 0
    trapStack.push(lftIdx)

    function inspectTrapStack(minH: number) {
        while(!trapStack.isEmpty()) {
            
            const singleDelta = minH - height[trapStack.pop()]
            trapped += singleDelta > 0 ? singleDelta : 0
        }
    }

    while(idxCur <= height.length) {
        // Corner case: last idx with zero height, 
        if ( idxCur== height.length-1  ) {
            let rt = height[idxCur]

            while(!trapStack.isEmpty() && rt <= height[trapStack.peek()]) {
                rt = height[trapStack.pop()]
            }
            //now rt taller than peek()
            const minH = Math.min(rt, height[lftIdx])

            inspectTrapStack(minH)
            break
        }

        if (height[lftIdx] > height[idxCur] && idxCur < height.length) {
            trapStack.push(idxCur)
            idxCur++
            continue
        }

        // CORNER CASE:  Either ends height zero, let it go
        if ((lftIdx==0 && height[lftIdx] == 0) 
        // || (idxCur== height.length+1 && height[idxCur] == 0) 
        ) {
            lftIdx = idxCur
            trapStack.emptyAll()
            trapStack.push(idxCur)

            idxCur++
            continue
        }



        //calib trapped, left should be lower end already unless last idx
        const minH = idxCur == height.length ? Math.min(height[height.length-1], height[lftIdx] ) : height[lftIdx]
        inspectTrapStack(minH)
        
        lftIdx = idxCur
        trapStack.push(idxCur) //current idx will be 1st of new, last one single will be there on ending
        idxCur++
    }
    

    return trapped
    
};


const height = [4,2,0,3,2,5]
console.log(`trapped water  :   ${trap(height)}`  )  //9

// const height = [0,1,0,2,1,0,1,3,2,1,2,1]
// console.log(`trapped water  :   ${trap(height)}`  )  //6
