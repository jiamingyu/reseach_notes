type QT = number | string

export class MyStack <T extends QT> {
    ds : T[] = [] as T[]

    peek() : T  {
        console.log( `${this.ds[this.ds.length-1]},${this.isEmpty()}, llllength: ${this.ds.length}`)
        return this.isEmpty() ? undefined : this.ds[this.ds.length-1]
    }

    addAll(ds: T[]) : boolean {

        for( const d of ds ) {
            this.push(d)
        }

        return true

    }

    push(e: T): T  {
        return this.ds[this.ds.length] = e
    }

    pop() : T {
        if (!this.isEmpty()){
            const e = this.ds[this.ds.length-1]
            // this.ds[this.ds.length-1] = undefined  //bug
            this.ds.splice(this.ds.length-1)
            return e

        } else {
            return undefined
        }
    }
    
    isEmpty() {
        return this.ds.length == 0
    }
} 

export class MyQ <T extends QT> {
    ds : T[] = [] as T[]

    addAll(ds: T[]) : boolean {

        for( const idx in ds ) {
            this.add(ds[ds.length-1-(+idx)])
        }

        return true

    }

    private fillAHead() : boolean {
        // validate 0 idx empty

        let buf: T = undefined
        for(const idx in this.ds) {
            this.ds[idx] = this.ds[+idx+1]
            this.ds[+idx+1] = undefined
        }

        return false

    }

    add(e: T): T  {
        return this.ds[this.ds.length] = e
    }

    poll() : T {
        if (this.isEmpty()) {
            return undefined
        }

        const r = this.ds[0]
        this.fillAHead()
        return r
    }
    
    isEmpty() {
        return this.ds.length == 0
    }
} 


/**
 * Quick test
 */
// const t = new MyStack()

// console.log( t.push(10) )
// console.log( t.pop() )
// console.log(`is stack empty ? : ${t.isEmpty()}`)


// const q = new MyQ()
// console.log( q.add(3))
// console.log( q.add("L"))

// console.log( q.poll() )
// console.log(`is stack empty ? : ${q.isEmpty()}`)
