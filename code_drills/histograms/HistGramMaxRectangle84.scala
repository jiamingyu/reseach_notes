  
  /**
  * scala3 compiler
  */
  def detectLeftExpandMargins(heightsCust: Array[Int]): Array[Int] = {
    var ascIdxStack = List[Int]() // scala deprecate Stack, instead use list
    ascIdxStack = -1 +: ascIdxStack

    // Array to track left idx where rectangle W. current height can extend until(including)
    val leftIdxWCurrentIdxHeight = Array.fill(heightsCust.length)(100)

    heightsCust.zipWithIndex.foreach { (h, i) => //scala3 no need `case (h,i) =>`

      ascIdxStack.head match {
        case -1 => ascIdxStack = i +: ascIdxStack
        case sidx if heightsCust(sidx) < h => { // current bar taller than last of stack, add idx position to stack
          ascIdxStack = i +: ascIdxStack
        }
        case _ => { // current bar lower | eq the last of the none-empty stack
          ascIdxStack = stackFillSideParam(ascIdxStack, leftIdxWCurrentIdxHeight, h, heightsCust)
          ascIdxStack = i +: ascIdxStack // still need to add to stack
        }
      }
      //TODO: ascIdxStack = i +: ascIdxStack , consolidate, But really all 3 need to add ???

    }

    //Now check what is in the stack after crawling input data
    stackFillSideParam(ascIdxStack, leftIdxWCurrentIdxHeight, -100, heightsCust)

    leftIdxWCurrentIdxHeight
  }


  def stackFillSideParam(ascIdxStck: List[Int], leftIdxWCurrentIdxHeight: Array[Int], h: Int, heightsCust: Array[Int]): List[Int] = {

    var askIdxStackBuf = ascIdxStck //need a buffer s to track s2.tail update

    while (askIdxStackBuf.head != -1 && heightsCust(askIdxStackBuf.head) >= h) { // as long as stack's last tracked bar not lower than current bar
      //Attention: for those to be removed from stack, its expansion width must based on its distance from its direct stack elemet
      leftIdxWCurrentIdxHeight(askIdxStackBuf.head) = askIdxStackBuf.head - askIdxStackBuf.tail.head //TODO: move this out of while & only one delta calculation
      askIdxStackBuf = askIdxStackBuf.tail
    }
    askIdxStackBuf
  }


  def largestRectangleArea(heights: Array[Int]): Int = {

    val leftexpands = detectLeftExpandMargins(heights)
    // right expands first reverse, then reverse again after function call
    val rightexpands = detectLeftExpandMargins(heights.reverseIterator.toArray).reverseIterator

    // TODO: research on why below call will leads to return value empty
//    leftexpands.zip(rightexpands).foreach(println)

    leftexpands.zip(rightexpands).zip(heights).map { (tp: ((Int, Int), Int)) => (tp._1._1 + tp._1._2 - 1) * tp._2 }.foldLeft(0)((a, b) => Math.max(a, b))
  }



/** Test case:
Input: heights = [2,1,5,6,2,3]
Output: 10
    */
// val hs = Array(2,1,5,6,2,3)

/**
 * Input: heights = [2,4]
Output: 4
*/
// val hs = Array(2,4)

// This test case is added by me, to verify scenario of 2 equal height bars separated
// Output : 12
val hs = Array(2,3,5,4,3)


println("max area is: ")
println(largestRectangleArea(hs))
