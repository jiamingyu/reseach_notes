import {MyStack, MyQ} from './StackNQ'


function largestRectangleArea(heights: number[]): number {

    let rtn = 0
    

    const idxStack = new MyStack<number>()
    
    let idx = -1
    while (++idx <= heights.length) {
        
        if (idxStack.isEmpty() || heights[idx] >= heights[idxStack.peek()]) {
            idxStack.push( idx )
            continue
        }

        const topIdx = idxStack.pop() //OK right after empty check
        let buf = heights[topIdx]

        let debug = 0 //
        console.log(`>>> ${!idxStack.isEmpty()} && ${heights[idxStack.peek()]} > ${heights[idx]}`)
        while(!idxStack.isEmpty() && heights[idxStack.peek()] > heights[idx] && debug < 10){

            debug += 1

            const popidx = idxStack.pop()
            console.log(`pppop: ${popidx}`)
            const lead = (topIdx - popidx + 1) * heights[popidx]
            console.log(`${topIdx} - ${popidx} * ${heights[popidx]}`) //
            buf = buf > lead ? buf : lead

        }

        rtn = rtn >= buf ? rtn : buf

        //
        idxStack.push( idx )

    }

    return rtn
};

// const heights = [2,1,5,6,2,3]
// const heights = [2,1,5,6,2,3, 35]
//const heights = [0, 2,1,5,6,2,3]
const heights = [2,1,5,6,2,3,1]


console.log( largestRectangleArea(heights) )