import scala.collection.mutable.ListBuffer

def trap(height: Array[Int]): Int = {


	def calibVByIdx(startIdx: Int, endIdx: Int): Int = {

		val top = Math.min(height(startIdx), height(endIdx))

		var r = 0
		for(x <- startIdx+1 until endIdx){
			r += top - height(x)
		}
		r
	}


	var sum = 0

	val lb = ListBuffer[Int]() //main stack
	val rlb = ListBuffer[Int]() // sub-stack to track all bars b4 next higher than lb stack head bar

	for(hi <- height.zipWithIndex) yield {
		hi match {
			case (_, 0) => hi._2 +=: lb
			case (v, i) if v > height(lb.head) => {
				val removedLeft = lb.remove(0)
				val vol = calibVByIdx(removedLeft, i)
				sum += vol

				rlb.clear()
				i +=: lb
			}
			case (v, i) => {
					i +=:rlb
			}
		}
	}


	// Now deal with the sub-stack 
	// ?? At this point, could lb be empty? I believe not
	// if(!lb.isEmpty) {
		rlb += lb.head 
	// }


	val stk = ListBuffer[Int]()

	// For scenario that rlb is empty, then below just skipped
	for(ridx <- rlb) {
		stk.length match {
			case 0 => ridx +=: stk
			case n if height(stk.head) <= height(ridx) =>  {

				sum += calibVByIdx(ridx, stk.head)


				stk.remove(0)				
				ridx +=: stk
			}
		case n => {}
		}
		
	}

	sum
}

/**
Input: height = [0,1,0,2,1,0,1,3,2,1,2,1]
Output: 6
*/

val height = Array(0,1,0,2,1,0,1,3,2,1,2,1)
println(trap(height))