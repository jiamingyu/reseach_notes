import { MyQ, MyStack } from "./StackNQ"; 



function maxDam(height: number[]): number {
    let maxVol = 0
    let l = 0
    let r = height.length-1

    while(l < r) {
        //
        const v = Math.min(height[l], height[r]) * (r-l)
        maxVol = v> maxVol ? v : maxVol
        console.log(maxVol)


        //
        if (height[l] < height[r]) {
            l++
        } else {
            r--
        }

    }
    return maxVol
}

function maxDam2(height: number[]): number {
    let maxVol = 0

    let idx=0
    while(idx < height.length-1) {
        let ridx = idx+1

        while(ridx < height.length) {
            const v = Math.min(height[ridx], height[idx]) * (ridx - idx)
            maxVol = maxVol < v ? v: maxVol
            console.log(maxVol)

            ridx++
        }


        idx++
    }
    return maxVol
}

const height = [1,8,6,2,5,4,8,3,7] // 49
//              0 1 2 3 4 5 6 7 8
// console.log(maxDam2(height))
console.log(maxDam(height))
