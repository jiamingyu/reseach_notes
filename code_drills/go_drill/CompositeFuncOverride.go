package main

import "fmt"

/**
This drill proves Go use composite + function passing down to its composite to achieve function override
 */

type WidthHeight struct {
	width  float64
	height float64
}

func (this *WidthHeight) SetWidth(w float64) {
	this.width = w
}
func (this *WidthHeight) SetHeight(h float64) {
	this.height = h
}
func (this *WidthHeight) GetWidth() float64 {
	fmt.Println("WidthHeight  being called GetWidth")
	return this.width
}
func (this *WidthHeight) GetHeight() float64 {
	fmt.Println("in WidthHeight.GetHeight")
	return this.height
}

// interface
type Shape interface {
	Area() float64
	GetWidth() float64
	GetHeight() float64
	SetWidth(float64)
	SetHeight(float64)
}

// Composite
type Rectangle struct {
	WidthHeight 
	//Attn: Only declare fields type, but not var field name 
	// ( The only way to override work using embedding. )
	// ref: https://go.dev/doc/effective_go#embedding
}

func (this *Rectangle) Area() float64 {
	return this.GetWidth() * this.GetHeight() / 2
}

// override
func (this *Rectangle) GetHeight() float64 {
	fmt.Println("in Rectangle.GetHeight, call its composite WidthHeight.GetHeight()")
	// Com
	return this.WidthHeight.GetHeight()
}

/**
Why GetWidth call its only composite child with same method api

If a struct embeds another struct, and both structs have a method with the same name, the method in the outer struct will "override" the method in the embedded struct.


*/

func main() {
	var r Rectangle
	var i Shape = &r
	i.SetWidth(4)
	i.SetHeight(6)

	fmt.Println(i)
	fmt.Println("width: ", i.GetWidth())
	fmt.Println("height: ", i.GetHeight())
	fmt.Println("area: ", i.Area())

}
