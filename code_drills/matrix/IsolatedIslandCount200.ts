// https://leetcode.com/problems/number-of-islands/description/

function numIslands(grid: string[][]): number {
    let count = 0
    let visited : boolean[][] = grid.map(cs => cs.map(c=>false))

    let rct =-1 
    while(++rct < grid.length) {
        let cct=-1
        while(++cct < grid[0].length) {
            if (!visited[rct][cct] && grid[rct][cct] == "1") {
                    // Only place to add isolated island count
                count+=1
                crawlNbrHoriVert(rct, cct)
        
            } else {  //visited[rct][cct] ||  grid[rct][cct] == "0"
                visited[rct][cct] = true // yes, we might double write visited for those grid=0 to true
            }
        }
    }
    return count

    function crawlNbrHoriVert(rct1:number, cct1: number) {
        let nbrs : number[][] = []

        if (cct1 -1 > 0)
            nbrs.push([rct1, cct1-1])
        if (cct1 + 1 < grid[0].length)
            nbrs.push([rct1, cct1+1])
        if (rct -1 > 0)
            nbrs.push([rct1-1, cct1])    
        if (rct1 + 1 < grid.length)
            nbrs.push([rct1+1, cct1])    

        for (const nbr of nbrs){
            if (!visited[nbr[0]][nbr[1]]) {
                visited[nbr[0]][nbr[1]] = true
                if (grid[nbr[0]][nbr[1]] == "1") {
                    crawlNbrHoriVert(nbr[0],  nbr[1])
                }
            }
        }
        
    }
};

const grid = [
    ["1","1","0","0","0"],
    ["1","1","0","0","0"],
    ["0","0","1","0","0"],
    ["0","0","0","1","1"]
  ]

console.log( numIslands(grid) )
