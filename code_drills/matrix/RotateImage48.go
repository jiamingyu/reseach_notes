/**
   https://leetcode.com/problems/rotate-image/description/

   Feature:
	1. To guarantee clockwise,p[n+1] must be p[n]'s static column number as its row. & *p[n+1] value to replace p[n]
	2. Like c++, pointer (or value's pointer address), if demo used correctly, can be very memory efficient
 */

 package main

 import (
	 "fmt"
 )
 
 func main() {
	 // var strs = [][]int{{1, 2, 3},{4, 5, 6},{7, 8, 9}}
 
	 var strs = [][]int{{5, 1, 9, 11}, {2, 4, 8, 10}, {13, 3, 6, 7}, {15, 14, 12, 16}}
 
	 fmt.Printf("Clockwise rotation final result is: %d", rotate(strs))
 }
 
 func rotate(matrix [][]int) [][]int {
	 for ridx := 0; ridx < len(matrix)/2; ridx++ {
		 var l = len(matrix) - 1
		 for cidx := ridx; cidx < l-ridx; cidx++ {
 
			 //@Here, we use pointer to directly work on matrix's int memory type, avoiding extra space lavish
			 var p1 *int = &matrix[ridx][cidx]
			 var p2 *int = &matrix[cidx][l-ridx]
			 var p3 *int = &matrix[l-ridx][l-cidx]
			 var p4 *int = &matrix[l-cidx][ridx]
 
			 var tmp = *p4
 
			 *p4 = *p3
			 *p3 = *p2
			 *p2 = *p1
			 *p1 = tmp
 
			 fmt.Println(*p1)
			 fmt.Println(*p2)
			 fmt.Println(*p3)
			 fmt.Println(*p4)
			 fmt.Println("== == == ==")
 
		 }
		 fmt.Println("++ ++ ++ ++")
 
	 }
 
	 return matrix
 }
 