import org.scalatest.flatspec.AnyFlatSpec

import scala.collection.mutable.ListBuffer
import scala.collection.mutable.ArrayBuffer

class MyTest extends AnyFlatSpec{

  "241 " should "diff way to add parenthesis to compute " in {

//    val d = "2-1*3"
    val d = "2-10*3/2"
    val x = diffWaysToCompute(d)
    println(x)
  }

  /**
   * TODO: return type is not needed, but Scala has problem in return in middle for Unit
   * 
   * @param op2Apply
   * @param sIdx
   * @param eIdx
   * @param ascRangeMatrix
   * @param digits
   * @param ops
   * @return
   */
  def calib(op2Apply : Map[Char, (Int, Int)=>Option[Int]] )(sIdx: Int, eIdx: Int, ascRangeMatrix: ArrayBuffer[ArrayBuffer[List[Int]]], digits: Array[Int], ops: Array[Char]) : Boolean = {
    eIdx - sIdx match {
      case 0 => {true}
      case 1 => {
        val opC = ops(sIdx)
        val tmp = op2Apply.get(opC).get.apply(ascRangeMatrix(sIdx)(sIdx)(0), ascRangeMatrix(eIdx)(eIdx)(0))
        ascRangeMatrix(sIdx)(eIdx) = List(tmp.get)
        true
      }
      case x if x > 1 => {
        val buffer : ListBuffer[Int] = ListBuffer[Int]()
        for (ct <- sIdx  until eIdx ) {
          if (ascRangeMatrix(sIdx)(ct).isEmpty) {
            val rx = calib(op2Apply)(sIdx, ct, ascRangeMatrix, digits, ops)
          }
          if (ct<eIdx && ascRangeMatrix(ct+1)(eIdx).isEmpty) {
            val ry = calib(op2Apply)(ct+1, eIdx, ascRangeMatrix, digits, ops)
          }

          for(sn <- ascRangeMatrix(sIdx)(ct)) {
            val adjLocalLeftIdx = if (ct < eIdx) ct+1 else eIdx

            ascRangeMatrix(adjLocalLeftIdx)(eIdx).foreach{en=>
              val tmp = op2Apply.get(ops(ct)).get.apply(sn, en) //?? ops(sIdx)
              tmp.foreach(buffer.addOne(_))
            }
          }
        }
        ascRangeMatrix(sIdx)(eIdx) = buffer.toList  //Populate Matrix
        true
      }
      case _ => {
        false
      }
    }
  }

  def diffWaysToCompute(str: String): List[Int] = {
    // parse to digit[] & op[]
    val digits : Array[Int] = str.split("[+*-/]").map(Integer.parseInt(_))

    val ops: Array[Char] = str.filter(c=> c=='+'||c=='/'||c=='*'||c=='-').toCharArray

    val addOp : (Int, Int)=>Option[Int] = (a: Int, b: Int) => Some(a + b)
    val reduceOp : (Int, Int)=>Option[Int] = (a: Int, b: Int) => Some(a - b)
    val timesOp : (Int, Int)=>Option[Int] = (a: Int, b: Int) => Some(a * b)
    val divOp : (Int, Int)=>Option[Int] = (a: Int, b: Int) => if (b == 0) None else Some(a/b)

    implicit val op2Apply : Map[Char, (Int, Int)=>Option[Int]] = Map(
      '+'-> addOp,
      '-' -> reduceOp,
      '*' -> timesOp,
      '/' -> divOp
    )

    val ascRangeMatrix : ArrayBuffer[ArrayBuffer[List[Int]]]= ArrayBuffer.fill(digits.length, digits.length)(List())

    digits.zipWithIndex.foreach {case (v: Int, idx: Int)=> ascRangeMatrix(idx)(idx) = List(v)}

    if (ascRangeMatrix(0)(digits.length-1).isEmpty)
      calib(op2Apply)(0, digits.length-1, ascRangeMatrix, digits, ops)

    ascRangeMatrix(0)(digits.length-1)
  }

}
