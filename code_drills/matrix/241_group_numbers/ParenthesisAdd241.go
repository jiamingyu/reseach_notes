package main

import (
	"fmt"
	"strconv"
)

func main() {
	// s := "2-1-1"

	s := "2*3-4*5"
	// s := "220+3*405"
	ops := make([]rune, 0)
	digitStartIndex := -1
	nbs := make([]int, 0)

	for i, ss := range s {
		switch ss {
		case 43, 45, 42, 47:
			{
				ops = append(ops, ss)

				num, err := strconv.Atoi(s[digitStartIndex:i])
				if err == nil {
					nbs = append(nbs, num)
				}

				digitStartIndex = -1
			}
		case 48:
		case 49, 50, 51, 52, 53, 54, 55, 56, 57:
			{
				if digitStartIndex == -1 {
					digitStartIndex = i
				}

				if i == len(s)-1 {
					num, err := strconv.Atoi(s[digitStartIndex : i+1])
					if err == nil {
						nbs = append(nbs, num)
					}
				}
			}

		default:

		}

	}

	mtx := make([][][]int, len(nbs))
	for rc := range nbs {
		mtx[rc] = make([][]int, len(nbs))
		mtx[rc][rc] = []int{nbs[rc]}
	}
	for dlt := 1; dlt < len(nbs); dlt++ {
		for startIdx := 0; startIdx+dlt < len(nbs); startIdx++ {
			// fmt.Println(startIdx, startIdx+dlt) //
			if len(mtx[startIdx]) == 0 {
				mtx[startIdx] = make([][]int, len(nbs))
			}

			buf := make([]int, 0)
			for cut := startIdx; cut < startIdx+dlt; cut++ {
				// fmt.Println("x:", startIdx, "to", cut, string(ops[cut]), cut+1, "to", startIdx+dlt) //

				for _, a := range mtx[startIdx][cut] {
					for _, b := range mtx[cut+1][startIdx+dlt] {
						buf = append(buf, calib(ops[cut], a, b))

						// fmt.Println("rr", a)
						// fmt.Println("cc", b)
					}
				}
			}
			// fmt.Println("buf:", buf)
			mtx[startIdx][startIdx+dlt] = buf
			// fmt.Println("aaa:aaa:", mtx)

		}
	}
	fmt.Println(mtx[0][len(nbs)-1])

}

func calib(m rune, a int, b int) int {
	switch m {
	case 43:
		return a + b
	case 45:
		return a - b
	case 42:
		return a * b
	case 47:
		return a / b
	default:
		{
			return -1000
		}

	}
}
