    /**
     * https://leetcode.wang/leetcode-306-Additive-Number.html
     *
     * https://leetcode.com/problems/additive-number/
     * 
     * Ref java solutions in WangeThreeHundreds.java
     */

    function diffWaysToCompute(expression: string): number[] {
        let r : number[] = [];
        
        return r;
    };
    
    type OpType = (arg2: number, arg1: number)=>number 
    
    
    const op2arithOp = new Map<string, OpType>(
            [
                ["+", (arg2: number, arg1: number)=> {console.log(`${arg2} + ${arg1}` ); return arg1 + arg2} ],
                ["-", (arg1: number, arg2: number)=> {console.log(`${arg1} - ${arg2}` ); return arg1 - arg2;} ],
                ["*", (arg1: number, arg2: number)=> {console.log(`${arg1} * ${arg2}` ); return arg1 * arg2;} ],
                ["/", (arg1: number, arg2: number)=> {console.log(`${arg1} / ${arg2}` ); return arg1 / arg2;} ],
            ]
            );
    
    
    class NumberNOpOption {
        constructor(public num: number, public op? : string){}
        
    }
    
    let calibBuf : Array<number>[][] = undefined
    
    function diffNumbersCombCalib(curL: number, curR: number, stackLevelDebug: number) : number[]{
    
        if (calibBuf[curL][curR] != undefined ) {
            return;
        }
    
        if (curL == curR){
            return;
        }
    
        let dd : number[] = []
    
        for(let cut = curL; cut < curR; cut+=1) {
            if (calibBuf[curL][cut] == undefined) {
                diffNumbersCombCalib(curL, cut, stackLevelDebug + 1)
            }
            
            if (calibBuf[cut + 1][curR] == undefined) {
                diffNumbersCombCalib(cut+1, curR, stackLevelDebug + 1)
            }
    
            for(const l of calibBuf[curL][cut]) {
                for(let r of calibBuf[cut + 1][curR]) {
                    dd.push(op2arithOp.get(ds[cut].op)(l, r))
                }
                
            }
        }
    
        //TODO: Eliminate dd W. calibBuf
        calibBuf[curL][curR]=dd
    }
    
    /** 
     * // Good for interview bug fixing
     * 
    function diffNumbersCombCalibBak(curL: number, curR: number, stackLevelDebug: number) : number[]{
        let r = new Array<number>()
    
        if (curL == curR)
            return;
    
        if (calibBuf[curL][curR] !== undefined ) {
            console.log(`${curL},  ${curR}`)
            return;
        }
    
        for(let cut = curL + 1; cut < curR; cut+=1) {
            console.log("aaaaa")
            console.log(`${curL}, ${cut}`)
            if (calibBuf[curL][cut] == undefined) {
                calibBuf[curL][cut] = diffNumbersCombCalib(curL, cut, stackLevelDebug+1)
            }
    
            let lSub = calibBuf[curL][cut]
            console.log("yyyyyyyy")
            console.log(`${curL+1}, ${curR}, ${lSub}`)
            if (calibBuf[cut+1][curR] !== undefined) {
                calibBuf[cut+1][curR] = diffNumbersCombCalib(cut+1, curR, stackLevelDebug+1)
            }
            let rSub = calibBuf[cut+1][curR]
    
            let bs : number[] = [];
            for( const lv of lSub ) {
                    for ( const rv of rSub) {
                        let x = op2arithOp.get(ds[cut].op)(lv, rv)
                        console.log(`AAAAAny x: ${x}`)
                        bs.push(x);
                    }
            }
            console.log(`${curL}, ${curR}, ${bs}`)
            calibBuf[curL][curR] = bs
    
        }
        
        return r
    
    }
    */
    
    
    
    function convertStr2NumberNOpOptions(s: String) : NumberNOpOption[] {
    
        let r : NumberNOpOption[]= [];
        // should start w. Num
        let numStr = []
        let opChar = undefined
        for(const idx in s) {
            // console.log(`${s[idx]}::${idx}`)
            if(s[idx] >= '0' && s[idx] <= '9' )
                numStr.push(s[idx])
            else {
                // 
                r.push(new NumberNOpOption(Number(numStr.join('')), s[idx]))
                numStr = []
            }
        }
        //last number
        r.push( new NumberNOpOption(Number(numStr.join(''))) )
    
        return r
    }
    
    // const ds = convertStr2NumberNOpOptions("8+2*6-4")
    // const ds = convertStr2NumberNOpOptions("180+25*6-34")
    const ds = convertStr2NumberNOpOptions("2*3-4*5") // Issue: seems missing -10, & another 10
    
    calibBuf = [... Array<number>(ds.length)].map(e => [... Array(ds.length)] )
    
    for(const r in calibBuf) {
        for(const c in calibBuf[r]) {
            if (r ===c )
                calibBuf[r][c] = [ds[r].num]
        }
    }
    
    
    // mock data for debugging diffNumbersCombCalib
    // const ds = [ new NumberNOpOption(8,"+"), new NumberNOpOption(2,"*"), new NumberNOpOption(6,"-"), new NumberNOpOption(4)]
    diffNumbersCombCalib(0, ds.length-1, 0)
    
    console.log(`Overall possible calibration results: ${calibBuf[0][ds.length-1]}`)
    
    