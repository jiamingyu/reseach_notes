import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.function.BiFunction;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Leet241Drill {

    /**
     * https://leetcode.wang/leetcode-241-Different-Ways-to-Add-Parentheses.html
     * Compare with pure parenthses combination
     */


    static Map<Character, BiFunction<Integer, Integer, Integer>> op2ImplStatic = new HashMap<>();
    static List<Integer>[][] subResultStatic;

    static {
        op2ImplStatic.put('+', (a, b) -> a + b);
        op2ImplStatic.put('-', (a, b) -> a - b);
        op2ImplStatic.put('*', (a, b) -> a * b);
        op2ImplStatic.put('/', (a, b) -> a / b);


    }

    @Test
    public void testcomputeWCombVariance() {

//        diffWaysToCompute("8+2");
//        computeWCombVariance("8+2*6-4");
//        diffWaysToCompute("8+2*6");

        computeWCombVariance("2*3-4*5");

        System.out.println(subResultStatic[0][subResultStatic.length-1]);

    }

    public void computeWCombVariance(String s) {


        /**
         *         Good for interview : why below will not work
         */
//        for(List[] rs : subResultStatic) {
//            for(List rc : rs) {
//                rc = new ArrayList();
//            }
//        }


        List<NumbOpComb> combs = convert2NumOpCombs(s);
        subResultStatic = new List[combs.size()][combs.size()];

        // init & fill self position's value as origin
        for (int r = 0; r < combs.size(); r++)
            for (int c = 0; c < combs.size(); c++) {
                subResultStatic[r][c] = new ArrayList<>();
                if (r == c) {
                    subResultStatic[r][c].add(combs.get(r).num);
                }
            }

        diffNumbersCombCalibRecur(combs, 0, combs.size());
    }

    private void diffNumbersCombCalibRecur(List<NumbOpComb> combs, int startIdx, int endXcludeIdx) {
        if (subResultStatic[startIdx][endXcludeIdx-1].size() > 0)
            return;

        // When idx @ last 2nd, return only [last 2 element comb], return up stack
        if(startIdx + 1 == endXcludeIdx -1 ) {
            if (subResultStatic[startIdx][endXcludeIdx-1].isEmpty()) {
                Integer nbrComb = op2ImplStatic.get(combs.get(startIdx).op).apply(combs.get(startIdx).num, combs.get(endXcludeIdx - 1).num);
                subResultStatic[startIdx][endXcludeIdx-1].add(nbrComb);
            }
            return;
        }


        // As of now, should guarantee: starIdx + 1 < endXcludeIdx -1, indicating not next to each other
        for(int splitCur =startIdx; splitCur < endXcludeIdx -1; splitCur++ ) {
            BiFunction<Integer, Integer, Integer> ops = op2ImplStatic.get(combs.get(splitCur).op);

            if (subResultStatic[startIdx][splitCur].size() == 0) { // i+1 meaning from startIdx + 2, xcludued
                diffNumbersCombCalibRecur(combs, startIdx, splitCur+1);
            }

            if (subResultStatic[splitCur + 1][endXcludeIdx-1].size() == 0) {
                diffNumbersCombCalibRecur(combs, splitCur+1, endXcludeIdx);
            }

            List<Integer> ll = subResultStatic[startIdx][splitCur];
            List<Integer> rr = subResultStatic[splitCur+1][endXcludeIdx - 1];

            for(Integer sl: ll) {
                for (Integer sr: rr) {
                    subResultStatic[startIdx][endXcludeIdx -1 ].add(  ops.apply(sl, sr) );
                }
            }

        }
    }

    private List<NumbOpComb> convert2NumOpCombs(String s) {
        List<NumbOpComb> r = new ArrayList<>();
        Pattern pattern = Pattern.compile("\\d+");
        Matcher matcher = pattern.matcher(s);
        while(matcher.find()) {
            System.out.println( matcher.start()+ ":" + matcher.end());
            if (matcher.end() < s.length())
                r.add(new NumbOpComb(Integer.parseInt(s.substring(matcher.start(), matcher.end())), s.substring(matcher.end(), matcher.end()+1).toCharArray()[0]));
            else
                r.add(new NumbOpComb(Integer.parseInt(s.substring(matcher.start(), matcher.end()))));

        }


        return r;
    }

    @Test
    public void testConvert2NumOpCombs() {
        List<NumbOpComb> combs = convert2NumOpCombs("8+2*6-3");
        combs.stream().forEach(x->System.out.println(x));
    }

    class NumbOpComb{
        Integer num;
        Character op;

        public NumbOpComb(int num, char c) {
            this.num = num;
            this.op = c;
        }

        public NumbOpComb(int num) {
            this.num = num;
        }

    }

}
