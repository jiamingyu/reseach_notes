package main

import (
	"fmt"
	// "slices"
)

func solveNQueens(n int) [][]string {
	rtn := make([][]string, 0)

	for c := 0; c < n; c++ {
		r := make([]rune, n)
		for c2 := 0; c2 < n; c2++ {
			if c == c2 {
				r[c2] = 'Q'
			} else {
				r[c2] = '.'
			}
		}

		toAdd := [][]rune{r}
		colBasedR := findNextRowQnComb(n-1, toAdd) //length carried by r
		rtn = append(rtn, colBasedR...)

	}

	return rtn

}

func findNextRowQnComb(ct int, toAdd [][]rune) [][]string {
	if ct == 0 {
		rtn := make([]string, len(toAdd[0]))
		for idx, r := range toAdd[0] {
			st := string(r)
			rtn[idx] = st
		}
		// fmt.Println(" ... ... ... ending as:", toAdd)

		mtx := make([]string, len(toAdd))
		for ridx, row := range toAdd {
			mtx[ridx] = string(row)
			// fmt.Println(ridx, string(row))
		}

		return [][]string{mtx}
	}

	rtn2 := make([][]string, 0)

	for c := 0; c < len(toAdd[0]); c++ {
		if qualified(toAdd, c) {
			newAdded := make([]rune, len(toAdd[0]))
			for i := 0; i < len(toAdd[0]); i++ {
				newAdded[i] = '.'
			}
			newAdded[c] = 'Q'

			toAdd2 := append(toAdd, newAdded)
			stackUps := findNextRowQnComb(ct-1, toAdd2)
			rtn2 = append(rtn2, stackUps...)
		}
	}

	return rtn2
}

func qualified(toAdd [][]rune, c int) bool {
	for ridx := 0; ridx < len(toAdd); ridx++ {
		if toAdd[ridx][c] == 'Q' {
			return false
		}
	}

	// upper left
	for delta := 1; delta < c+1; delta++ { // pay attention on delta < c+1, NOT delta < c
		row := len(toAdd) - delta
		if row > -1 && toAdd[row][c-delta] == 'Q' {
			return false
		}

	}

	// upper right
	for delta := 1; delta < len(toAdd[0])-c; delta++ {
		row := len(toAdd) - delta
		if row > -1 && toAdd[row][c+delta] == 'Q' {
			return false
		}

	}

	return true
}

func main() {
	count := 4

	fmt.Println(solveNQueens(count))

}
