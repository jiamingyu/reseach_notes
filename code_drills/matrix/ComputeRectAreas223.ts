/**
 * https://leetcode.wang/leetcode-223-Rectangle-Area.html
 * 
 * Key practice features: 
 * 
 * function apply() call
 * TS has no case pattern match although type define provided. This drill on impl a way to achieve like
 * 
 * ```
 *     doMatch(lrRects)
    .with({type: "connected"}, connectedFunc)
    .with({type: "tobe"}, ()=>{}))
 * 
 * ```
 *  This is referenced to, but impl DSL function instead of using https://github.com/gvergnaud/ts-pattern
 * 
 * Problem itself without above will take lots of if/else
 * 
 * https://leetcode.wang/leetcode-223-Rectangle-Area.html
 * 
 */

    class Rect{
        lwlftx: number
        lwlfty: number
        uprtx: number
        uprty: number
        
        constructor(ax1: number, ay1: number, ax2: number, ay2: number){
            this.lwlftx = ax1
            this.lwlfty = ay1
            this.uprtx = ax2
            this.uprty = ay2
        }
        
        compare(o : Rect) : boolean {
            return this.lwlftx <= o.lwlftx
        }
    }
    class LeftRightRects<T extends Rect> {
        l : T
        r: T
    
        constructor(l: T, r: T) {
            this.doSort(l, r)
        }
        protected doSort(lft : T , rt: T) {
            if ( lft.compare(rt) ) {
                this.l = lft; this.r=rt;
            } else {
                this.r = lft; this.l=rt;
            }
    
        }
    
        isConnected(): boolean {
            if (this.l.uprtx >= this.r.lwlftx )
                return true
    
            if (this.l.uprty >= this.r.lwlfty || this.l.lwlfty <= this.r.uprty)
                return true
    
            return false
        }
    
        areaSum(): number {
            return Math.abs(this.l.uprty - this.l.lwlfty) * (this.l.uprtx - this.l.lwlftx) 
                + Math.abs(this.r.uprty - this.r.lwlfty) * (this.r.uprtx - this.r.lwlftx)
        }
    }
    
    type RectangleRelation = {
        type: "none-connected" | "connected" | "tobe"
    }
    
    class RectangleRelationMatchCase {
        leftRightRects: LeftRightRects<Rect>
        rectangleRelation: RectangleRelation = {type:  "tobe"}
        checkdone = false
        
        constructor(sl: LeftRightRects<Rect>, t?: RectangleRelation) {
            this.leftRightRects = sl
            if(t != undefined) {
                this.rectangleRelation = t
                this.checkdone = false
            }
    
    
            console.log(`rectangleRelation :   ${this.rectangleRelation.type}`)
        }
    
        with(t: RectangleRelation, f: (sl: LeftRightRects<Rect>)=> void) : RectangleRelationMatchCase{
            if (this.checkdone)
                return this
    
            if (t.type == this.rectangleRelation.type) {
                f.apply(this, [this.leftRightRects])
                this.checkdone = true
            }
    
            return this
        }
    
    }
    
    function doMatch(sl: LeftRightRects<Rect>): RectangleRelationMatchCase {
        return sl.isConnected() ? new RectangleRelationMatchCase(sl, {type: "connected"}) : new RectangleRelationMatchCase(sl, {type: "none-connected"})
    }
    
    function computeArea(ax1: number, ay1: number, ax2: number, ay2: number, bx1: number, by1: number, bx2: number, by2: number): number {
        const lrRects: LeftRightRects<Rect> = new LeftRightRects(new Rect(ax1, ay1, ax2, ay2), new Rect(bx1, by1, bx2, by2))
    
        const connectedF = (funcleftRightRects: LeftRightRects<Rect>) => {
            console.log(`dealing with connected rectangles  `)
            const overlap = Math.abs(lrRects.l.uprty - lrRects.r.lwlfty) * (lrRects.l.uprtx - lrRects.r.lwlftx)
            console.log(`area is ${(lrRects.areaSum() - overlap)}`)
    
        }
    
        doMatch(lrRects)
        .with({type: "connected"}, connectedF)
        .with({type: "tobe"}, (sl: LeftRightRects<Rect>) => { //TODO: exhaustive
            console.log("dealing with none connected rectangles ")
        })
    
        return 0
    };
    
    
    computeArea( -3, 0,  3, 4, 0, -1, 9, 2) // Output: 45
    computeArea(  0, -1, 9, 2, -3, 0,  3, 4,) // Output: 45, test l/r swap
    computeArea( -2,  -2,  2,  2,  -2,  -2, 2, 2)  //Output: 16
    