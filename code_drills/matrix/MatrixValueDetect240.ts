function searchMatrix(matrix: number[][], target: number): boolean {
    let visited :  boolean[][] = Array.from({length: matrix.length},()=> Array.from({length: matrix.length},()=> false))
    return searchMatrixRecur(visited,  matrix, target, Math.floor( matrix.length/2 ), Math.floor(matrix.length/2), Math.floor( matrix.length/2), 0)
};


/**
 * 
 * @param visited : matrix[boolean] tracking visited, default all false on init

 * @param matrix : 
 
 * @param target 
 
 * @param curRowIdx : current function cursor row index
 
 * @param curColIdx 
 
 * @param distance:   Assuming square, the distance from starting to end indexes. TODO: For rect, just split into rowDistance & colDistance
 
 * @param stack : For debug usage considering its recursive
 
 * @returns : True for found, False for not found or other validation failure
 */

function searchMatrixRecur(visited :  boolean[][], matrix: number[][], target: number, curRowIdx: number, curColIdx: number, distance: number, stack: number): boolean {
    if (curRowIdx < 0 || curRowIdx >= matrix.length || curColIdx < 0 || curColIdx >= matrix.length)
        return false

    // if (stack == 5) // !!!!!!, used for debug if visited cache not built yet
    //     return false;

    if (visited[curRowIdx][curColIdx]) {
        return false
    }
    else {
        visited[curRowIdx][curColIdx]=true
    }

    if (matrix[curRowIdx][curColIdx] == target){
        console.log(`found target at row ${curRowIdx},  column ${curColIdx}`)
        return true
    }

    if (distance == 0){
        console.log(`NO target exists`)
        return false
    }

    const nextDistance = Math.ceil(distance/2)
    console.log(`next distance ${nextDistance}`) //

    if (target > matrix[curRowIdx][curColIdx] && target > matrix[curRowIdx][curColIdx]) {
        console.log(`xclude upper left`)
        return searchMatrixRecur(visited, matrix, target, curRowIdx + nextDistance, curColIdx + nextDistance, nextDistance, stack + 1) ||
        searchMatrixRecur(visited, matrix, target, curRowIdx - nextDistance, curColIdx + nextDistance, nextDistance, stack + 1) ||
        searchMatrixRecur(visited, matrix, target, curRowIdx + nextDistance, curColIdx - nextDistance, nextDistance, stack + 1) || 
        searchMatrixRecur(visited, matrix, target, curRowIdx + nextDistance, curColIdx, nextDistance, stack + 1) ||//  vertical lower only
        searchMatrixRecur(visited, matrix, target, curRowIdx, curColIdx + nextDistance, nextDistance, stack + 1)// horizontal right only
    }

    if (target < matrix[curRowIdx][curColIdx] && target < matrix[curRowIdx][curColIdx]) {
        console.log(`xclude lower right`)
        return searchMatrixRecur(visited, matrix, target, curRowIdx - nextDistance, curColIdx - nextDistance, nextDistance, stack + 1) ||
        searchMatrixRecur(visited, matrix, target, curRowIdx - nextDistance, curColIdx + nextDistance, nextDistance, stack + 1) ||
        searchMatrixRecur(visited, matrix, target, curRowIdx - nextDistance, curColIdx - nextDistance, nextDistance, stack + 1) ||
        searchMatrixRecur(visited, matrix, target, curRowIdx, curColIdx - nextDistance, nextDistance, stack + 1) ||    // horizontal left only
        searchMatrixRecur(visited, matrix, target, curRowIdx, curColIdx - nextDistance, nextDistance, stack + 1)   //vertical upper only
    }

}

const m2 = [[1,4,7,11,15],[2,5,8,12,19],[3,6,9,16,22],[10,13,14,17,24],[18,21,23,26,30]]
searchMatrix(m2, 9)
// searchMatrix(m2, 30)
// searchMatrix(m2, 1)

// searchMatrix(m2, 15)
// searchMatrix(m2, 18)


// searchMatrix(m2, 16) // test same row
searchMatrix(m2, 23) // test same col

// searchMatrix(m2, 18)

// bitwise drill
// console.log((1+6) >> 1) // >>>, vs divide
