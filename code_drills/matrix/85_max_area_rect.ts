/**
 * 
 * https://leetcode.wang/leetCode-85-Maximal-Rectangle.html
 * 
 * This solution is NOT using dynamic programming
 * Instead cached maxArea value is used to avoid dup computaions
 * 
 * @param 
 * 
 * @returns 
 */
function maximalRectangle(matrix: string[][]): number {
    let rtn = 0

    const widthMtrx = matrix.map(r => {
            let w = []
            let cum = 0
            for(const c of r) {
                if (c === '1') {
                    cum +=1;
                }else{
                    cum = 0
                }
                w.push(cum)
            }
            return w;
        }
    )

    // widthMtrx.forEach(r=>console.log(r))
    const areaMaxAsLowerRightEnd: number[][] = []
    widthMtrx.forEach((r,ridx )=> {
            let tmp = [] as Array<number>
            
            for(const cidx in r) {
                
                if (r[cidx]==0) {
                    tmp.push(0)
                    continue
                }

                let z : number = r[cidx]

                if (ridx > 0 && widthMtrx[ridx-1][cidx] > 0 ) {

                    if (widthMtrx[ridx][cidx] >= widthMtrx[ridx-1][cidx]){
                        /**
                         * USE CACHE: 
                         * When current width > above width, rec area should be just (CHCHED ABOVE CELL MAX_AREA + above CELL length size)
                         */
                        z= widthMtrx[ridx-1][cidx] + areaMaxAsLowerRightEnd[ridx-1][cidx]
                    } else {
                        // console.log(`ddddbug: ${ridx}, ${cidx}`) 

                        let h = 1
                        let minW = widthMtrx[ridx][cidx]
    
                        for(let ri =ridx-1 ; ri >= 0; ri--) {
                            if (ridx==1 ) {
                                // console.log(`widthMtrx[ri][cidx] : ${widthMtrx[ri][cidx]}`)
                            }
                            if (widthMtrx[ri][cidx] == 0)
                                break;
    
                            
                            
                            h+=1
                            minW =  widthMtrx[ri][cidx] < minW ? widthMtrx[ri][cidx] : minW
    
                            z = z < minW * h ? minW * h : z 
                        }
                    }

                } else {
                    // first row
                    // above row width is 0


                }
                tmp.push(z)
                rtn = rtn > z ? rtn : z
            }
            areaMaxAsLowerRightEnd.push(tmp)
        }// end of `widthMtrx.forEach`
    )

    areaMaxAsLowerRightEnd.forEach(r=>console.log(r))
    
    return rtn
};

const matrixRec = [["1","0","1","0","0"],["1","0","1","1","1"],["1","1","1","1","1"],["1","0","0","1","0"]]

console.log( maximalRectangle(matrixRec) )



