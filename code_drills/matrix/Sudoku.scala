import scala.collection.mutable._
import scala.util.boundary, boundary.break


// Scala3
// Features: Boundary and new RuntimeException typed break($value) used here
//https://dotty.epfl.ch/api/scala/util/boundary$.html

def isValidSudoku(board: Array[Array[String]]): Boolean = {
	//Hash collision:  r-$(0-8)|c-$(0-8)|rc-${r%3}-${c%3}, then ":{1~9}", value as boolean
	val m = Set.empty[String]

	boundary:
	// view: the same collection, but with all transformers implemented lazily
		for ( (r, ridx) <- board.view.zipWithIndex ) 
			for ((c, cidx) <- r.view.zipWithIndex if c != ".") {
				if ( m.contains(s"r-${ridx}:$c") || (m contains s"-${cidx}:$c") || (m contains s"rc-${ridx/3}-${cidx/3}:$c")  )
					then break(false)
					// then return false

				m+=(s"r-${ridx}:$c")
				m+=(s"c-${cidx}:$c")

				m+=(s"rc-${ridx/3}-${cidx/3}:$c")
				println
			}

	//Note: If true place one indent left here, it is out of boundary scope & true will be returned even break(false) is called within boundary 
		true       
}

// should be false
val board = 
Array(
 Array("8","3",".",".","7",".",".",".",".")
,Array("6",".",".","1","9","5",".",".",".")
,Array(".","9","8",".",".",".",".","6",".")
,Array("8",".",".",".","6",".",".",".","3")
,Array("4",".",".","8",".","3",".",".","1")
,Array("7",".",".",".","2",".",".",".","6")
,Array(".","6",".",".",".",".","2","8",".")
,Array(".",".",".","4","1","9",".",".","5")
,Array(".",".",".",".","8",".",".","7","9")
)

// should be true
// val board = 
// Array(
// Array("5","3",".",".","7",".",".",".",".")
// ,Array("6",".",".","1","9","5",".",".",".")
// ,Array(".","9","8",".",".",".",".","6",".")
// ,Array("8",".",".",".","6",".",".",".","3")
// ,Array("4",".",".","8",".","3",".",".","1")
// ,Array("7",".",".",".","2",".",".",".","6")
// ,Array(".","6",".",".",".",".","2","8",".")
// ,Array(".",".",".","4","1","9",".",".","5")
// ,Array	(".",".",".",".","8",".",".","7","9"))


println(isValidSudoku(board))
