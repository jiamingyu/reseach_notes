package main

/**
I leave this solution, which initially only think it 0/1 in matrix
*/

func main() {

	// matrix := [][]int{[]int{1, 1, 1}, []int{1, 0, 1}, []int{1, 1, 1}}
	// matrix := [][]int{[]int{0, 1, 2, 0}, []int{3, 4, 5, 2}, []int{1, 3, 1, 5}}
	matrix := [][]int{[]int{0, 1, 1, 0}, []int{1, 1, 1, 1}, []int{1, 1, 1, 1}}

	setZeroes(matrix)

	for _, r := range matrix {

		for _, c := range r {
			switch c {
			case 11:
				print(0, ",")
			case 10:
				print(0, ",")
			default:
				print(c, ",")
			}
		}
		println()
	}

}

func setZeroes(matrix [][]int) {
	for ridx, r := range matrix {
		for cidx, c := range r {
			if c == 0 {
				setRowOccupied(ridx, matrix)
				setColaoccupied(cidx, matrix)
			}
		}
	}

}

func setColaoccupied(cidx int, matrix [][]int) {
	for ridx := range matrix {
		if matrix[ridx][cidx] == 1 {
			matrix[ridx][cidx] = 11
		}
	}
}

func setRowOccupied(ridx int, matrix [][]int) {
	for cidx := range matrix[ridx] {

		if matrix[ridx][cidx] == 1 {
			matrix[ridx][cidx] = 11
		}

	}
}
