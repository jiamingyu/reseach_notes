/**
 * https://leetcode.wang/leetCode-85-Maximal-Rectangle.html
 * 
 * https://leetcode.wang/leetcode-221-Maximal-Square.html
 * 
 * 
 */


function maximalSquare2(matrix: string[][]): number {
    let rtn : number = 0
    
    //
    const maxAreasMatrix : number[][] = matrix.map(r => r.map(c=> -1))
    const matrixInt = matrix.map(r => r.map(c=> +c))

    for(let cidx in maxAreasMatrix[0]) {
        maxAreasMatrix[0][cidx] = matrixInt[0][cidx]   
    }
    for (let ridx in maxAreasMatrix) {
        maxAreasMatrix[ridx][0] = matrixInt[ridx][0]
    }

    // console.log(maxAreasMatrix)

    doCalibMaxSqrDownRight(matrixInt, maxAreasMatrix, 1,1)

    console.log(maxAreasMatrix)

    rtn = maxAreasMatrix.reduce((flattened, v)=>flattened.concat(v), []).reduceRight((l, r)=> Math.max(l,r)) // ?? why not push



    return rtn
};


const matrix221 = [
    ["1","0","1","0","0"],
    ["1","0","1","1","1"],
    ["1","1","1","1","1"],
    ["1","0","0","1","0"],
]
// const matrix221 = [
//     ["1","0","1"],
//     ["1","0","1"],
//     ["1","1","1"],
// ]
// const matrix221 = [
//     ["1","1","1"],
//     ["1","1","1"],
//     ["1","1","1"],
// ]

// const matrix221 = [
//     ["1","1",],
//     ["1","1",],
//     ["1","1"],
// ]

console.log(maximalSquare2(matrix221))




function doCalibMaxSqrDownRight(matrixInt: number[][], maxAreasMatrix: number[][], row: number, col: number) {
    // end crawl
    if (row >= matrixInt.length || col >= matrixInt[0].length)
        return
    if (maxAreasMatrix[row][col] != -1)
        return

    // console.log(`xyz: ${row}, ${col}`)
    // console.log(maxAreasMatrix)

    checkNExe(matrixInt, maxAreasMatrix,row-1, col-1)
    checkNExe(matrixInt, maxAreasMatrix,row, col-1)
    checkNExe(matrixInt, maxAreasMatrix,row-1, col)

    maxAreasMatrix[row][col] = Math.min(maxAreasMatrix[row-1][col-1],
         Math.min(maxAreasMatrix[row][col-1], maxAreasMatrix[row-1][col]))
         +1

    //
    doCalibMaxSqrDownRight(matrixInt, maxAreasMatrix, row+1, col)
    doCalibMaxSqrDownRight(matrixInt, maxAreasMatrix, row, col+1)
    doCalibMaxSqrDownRight(matrixInt, maxAreasMatrix, row+1, col+1)

}

function checkNExe(matrixInt: number[][], maxAreasMatrix: number[][], row: number, col: number) {
    if (maxAreasMatrix[row][col] == -1)
        doCalibMaxSqrDownRight(matrixInt, maxAreasMatrix, row, col)
}
// const numberMatrix : number[][] = matrix221.map(r => r.map(c=>+c))
// const z = matrix221.map(r=>r.map(c=> -1))