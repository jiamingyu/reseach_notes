
function solveNQueens3(n: number): string[][] {

    let cAtRowOne = -1
    let rtn = new Array<string[]>()
    
    while( ++cAtRowOne < n) {

        let m : string[][] = new Array(n).fill("").map(()=>new Array(n).fill(".") as string[]) as string[][]

        fillQnPath(0, cAtRowOne,m, rtn)

        recurNQ(1, m, rtn)


        
    }
    return rtn

    // Below as private functions

    function recurNQ(row: number, m: string[][], rtn:  Array<string[]>) {
        let cidx = -1

        while(++cidx < m.length) {
            if (m[row][cidx] == ".") {
                const nextM : string[][] = m.map(rv=>rv.map(cv => cv))
                fillQnPath(row, cidx, nextM, rtn)

                if (row+1 < m.length)
                    recurNQ(row+1, nextM, rtn)
            }
        }
    }   


    function fillQnPath(ridx: number, cidx: number, m : string[][], rtn:  Array<string[]>) {

        let r =-1
        const n = m.length

        while(++r < n) {
            if (m[r][cidx] == ".")
                    m[r][cidx] = "-" //column

            let c = -1
            while(++c < n) {
                if (m[r][c] != ".")
                    continue

                if(r==ridx && m[r][c] == ".") //row
                        m[r][c] = "-"
                        
                    
                if (r == ridx && c == cidx && m[r][c] == ".") { //fill with X
                    // console.log(`coming here 1`)
                    m[r][c] = "x"
                    continue
                }

                if (Math.abs(r-ridx) == Math.abs(c-cidx) ) // diagonal
                m[r][c] = "-"
            }

        }

        m[ridx][cidx] = "x"

        if (ridx == m.length-1) {
            const tmp = m.map(r=>r.join(","))
            rtn.push(tmp)
        }
        return true //
    }
}

console.log( solveNQueens3(4) )



