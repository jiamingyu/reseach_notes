
import org.scalatest.flatspec.AnyFlatSpec

import scala.collection.mutable.{ArrayBuffer, ListBuffer}

class CollectionDrill extends  AnyFlatSpec{

  "187 dna seq repeat detect: AAAAACCCCCAAAAACCCCCCAAAAAGGGTTT" should " List(AAAAACCCCC, CCCCCAAAAA)" in {
//    val seq = "AABCAADA"
    val seq = "AAAAACCCCCAAAAACCCCCCAAAAAGGGTTT"
    println(findRepeatedDnaSequences(seq, 10))

  }

  def findRepeatedDnaSequences(s: String, limit: Int): List[String] = {
    val lb: ListBuffer[String] = ListBuffer()

    val m = Array.ofDim[Boolean](s.length, s.length)

    m.zipWithIndex.foreach { case (r, ridx) =>
      r.zipWithIndex.foreach{
        case (c, cidx)  if(ridx <= cidx && s(ridx) == s(cidx)) => { m(ridx)(cidx) = true}
        case _ => {}
      }
    }

    for( (r, ridx) <- m.zipWithIndex) {
      for((c, cidx) <- r.zipWithIndex) {
        (ridx,cidx) match {
          case _ if ridx < cidx && m(ridx)(cidx) => {

            val lead = ListBuffer[Char]()
            var increment = 0
            while(ridx + increment < m.length && cidx + increment < m.length
              && m(ridx + increment)(cidx + increment)) {
              lead.append(s.charAt(ridx+increment))
              increment+=1
            }
            if (lead.length >= 1 && lead.length >= limit)
              lb.addOne(lead.mkString)
          }

          case _ => {}
        }

      }
    }

    lb.toList
  }
}