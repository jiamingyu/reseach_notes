package net.jyu.scalademo

import org.scalatest.flatspec.AnyFlatSpec

import scala.collection.mutable.ArrayBuffer

class MatrixSpec extends  AnyFlatSpec{

  "130 fill surrounded   " should "    " in {
    val m = Array(Array("X","X","X","X"),Array("X","O","O","X"),
      Array("X","X","O","X"), Array("X","O","X","X"))

    fillSurroundedRegions(m)
  }


  def fillSurroundedRegions(board: Array[Array[String]]): Unit = {
    val x = ArrayBuffer.fill(board.length)(ArrayBuffer.fill(board(0).length)("X"))

    for(ridx <- 0 until board.length) {
      for (cidx <- 0 until board(0).length) {
        (ridx, cidx) match {

          case (r, c) if board(r)(c) == "O"
            && (r== board.length-1 || r==0) ||(c== board(0).length-1 || c==0)
          => {
            x(r)(c) = board(r)(c) // ?? x(r)(c) = "O"
            crawlNbr(ridx, cidx, x)
          }

          case _  => {}
        }
      }
    }

    x.foreach(r=> {r.foreach(print);println()})

    def crawlNbr(ridx: Int, cidx: Int, x: ArrayBuffer[ArrayBuffer[String]]): Unit = {
      (ridx, cidx) match {
        case _ if (ridx+1 < x.length && board(ridx+1)(cidx) == "0") => x(ridx+1)(cidx) == "O";crawlNbr(ridx+1,cidx, x)
        case _ if (ridx-1 >=0 && board(ridx-1)(cidx) == "0") => x(ridx-1)(cidx) == "O";crawlNbr(ridx-1,cidx, x)

        case _ if (cidx + 1 < x(0).length && board(ridx)(cidx+1) == "0") => x(ridx)(cidx + 1) == "O";crawlNbr(ridx,cidx + 1, x)
        case _ if (cidx - 1 >= 0 && board(ridx)(cidx - 1) == "0") => x(ridx)(cidx - 1) == "O";crawlNbr(ridx,cidx - 1, x)
        case _ =>
      }
    }



  }

}
