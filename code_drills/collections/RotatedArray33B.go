package main

import "fmt"

func main() {
	nums := []int{4, 5, 6, 7, 0, 1, 2}
	target := 0
	idx := search(nums, target)
	fmt.Println(idx)

}
func search(nums []int, target int) int {
	l := 0
	r := len(nums) - 1
	if l == r {
		panic("empty input")
	}

	for l < r-1 {
		m := l + (r-l)/2
		if nums[m] == target {
			return m
		}
		switch {
		// l switch order
		case nums[m] < nums[r]:
			{
				if nums[m] < target && target < nums[r] {
					l = m + 1
				} else {
					r = m - 1
				}
			}

		case nums[l] < nums[m]:
			{ // l switch order, fall in right
				if nums[l] < target && target < nums[m] {
					r = m - 1
				} else {
					l = m + 1
				}
			}
			fmt.Println("l, r", l, r)

		}
	}

	//post process
	m := l + (r-l)/2
	switch {
	case target == nums[l]:
		return l

	case target == nums[m]:
		return m
	case target == nums[r]:
		return r
	default:
		return -1
	}

}
