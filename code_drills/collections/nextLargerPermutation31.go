// tech features:

package main

import (
	"fmt"
	"slices"
)

func main() {
	//nums := []int{1, 2, 3}
	//nums := []int{1, 5, 8, 4, 7, 6, 5, 3, 1}
	nums := []int{3, 2, 1}
	fmt.Println(nextPermutation(nums))
}

func nextPermutation(nums []int) []int {
	ctSort := make([]int, 10)
	lastV := -100
	keyIdx := -1

	for i, v := range slices.Backward(nums) {

		// assert v is (0 - 9)

		if v > lastV {
			lastV = v
			ctSort[v] += 1
		} else {
			ctSort[v] = 1 //for sure only one
			keyIdx = i
			break
		}

	}
	//fmt.Printf("Now keyIdx : %d\n", keyIdx)

	if keyIdx == -1 {
		rtn := make([]int, len(nums))
		for i, v := range slices.Backward(nums) {
			rtn[len(nums)-1-i] = v
		}
		return rtn
	}

	v2 := nums[keyIdx]
	for i := v2 + 1; i < 10; i++ {
		if ctSort[i] > 0 {
			nums[keyIdx] = i
			ctSort[i] -= 1
			break
		}
	}

	idxCt := keyIdx + 1
	for digit, digitCt := range ctSort {
		tmp := digitCt
		for tmp > 0 {
			nums[idxCt] = digit
			idxCt++
			tmp -= 1
		}
	}

	return nums

}
