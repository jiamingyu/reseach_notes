package main

import "fmt"

func main() {
	nums := []int{4, 5, 6, 7, 0, 1, 2}
	target := 0
	idx := search(nums, target)
	fmt.Println(idx)

}
func search(nums []int, target int) int {
	// switch idx
	switchIdx := -1

	for idx, _ := range nums {
		if idx+1 < len(nums) && nums[idx] > nums[idx+1] {
			switchIdx = idx
			break
		}
	}

	switch {
	case switchIdx == -1:
		return -1
	case nums[0] <= target && target <= nums[switchIdx]:
		idx, _ := doSearch(nums, 0, switchIdx, target)
		return idx
	case nums[switchIdx+1] <= target && target < nums[len(nums)-1]:

		idx, _ := doSearch(nums, switchIdx, len(nums)-1, target)
		return idx
	default:

	}

	return -1
}

func doSearch(nums []int, startIdx, endIdx int, target int) (int, bool) {
	fmt.Println("start, end", startIdx, endIdx)
	if startIdx > endIdx {
		return -1, false
	}

	midIdx := startIdx + (endIdx-startIdx)/2

	switch {
	case nums[midIdx] == target:
		return midIdx, true
	case nums[midIdx] < target:
		return doSearch(nums, midIdx+1, endIdx, target)
	case target < nums[midIdx]:
		return doSearch(nums, startIdx, midIdx-1, target)
	}
	return -1, false
}
