package net.jyu.scalademo

import org.scalatest.flatspec.AnyFlatSpec


class CollectionDrill extends  AnyFlatSpec{

  /**
   *
   * https://leetcode.com/problems/delete-node-in-a-linked-list/
   * case class /trait featured on immutable fields which is not applicable in this approach
   * @tparam T
   */
  class IntListNode(var v: Int, var next: Option[IntListNode]) {

  }

  "237 " should "Using Node to delete to build after del structure" in  {
    val ds = Array(4, 5, 1,9)
    val pickedIdx = 0

    var headCur: IntListNode = new IntListNode(ds(ds.length - 1), None)
    var entrCurOpt: Option[IntListNode] = None

    for (d <- 1 until ds.length) {
      val ridx = ds.length - 1 - d
      val x = new IntListNode(ds(ridx), Some(headCur))
      if (pickedIdx == ridx)
        entrCurOpt = Some(x)
      headCur = x
    }

    println(s"before delete: ${headCur}")
    printLL(headCur)

    //TODO: match
    deleteNode(entrCurOpt.get)

    println(s"After delete: $headCur")
    printLL(headCur)
  }

  def printLL(l: IntListNode): Unit = {
    var c = l
    while(c.next != None){
      println(c.v)
      c = c.next.get
    }
    println(c.v)
  }

  def deleteNode(node: IntListNode ): Unit = {
    var cur = node
    var cur2 : Option[IntListNode] = cur.next

    cur.v = cur2.get.v

    cur2 match {
      case x if x.get.next == None => {
        cur.next = None
        return
      }
      case x => {
        deleteNode(x.get)
      }
    }
  }
}
