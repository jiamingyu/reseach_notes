// tech features:
// A. panic call for excepiton
// B. switch /case GT/LT check, with good to know No need to break (unlike JAVA)

package main

import (
	"fmt"
)

func main() {
	nums := []int{5, 6, 7, 8, 8, 10}
	target := 8

	fmt.Println(searchRange(nums, target))
}

/*
*
Given an array of integers nums sorted in non-decreasing order,

find the starting and ending position of a given target value.
*/
func searchRange(nums []int, target int) []int {
	// nums s/e cover target
	if target < nums[0] || target > nums[len(nums)-1] {
		panic("Target is out of data range")
	}

	// identify start
	s := 0
	e := len(nums) - 1

	dbg := 0
	for s < e && dbg < 6 {
		fmt.Printf("s, e is %d, %d:\n", s, e)
		m := (e + s) / 2

		switch {
		case nums[m] == target && nums[m-1] == target:
			e = m - 1
		case nums[m] == target && nums[m-1] != target:
			e = m
		case target < nums[m]:
			e = m - 1
		case nums[m] < target:
			s = m + 1
		}
		//fmt.Printf("Two:s, e is %d, %d:\n", s, e)
		dbg += 1

	}

	return []int{e}
}
