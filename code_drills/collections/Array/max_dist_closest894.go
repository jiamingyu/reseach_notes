package main

import (
	"fmt"
	"math"
)

/**

 */

func main() {

	nums := []int{1, 0, 0, 0, 1, 0, 1}
	fmt.Println("idx is: ", maxDistToClosest(nums))

}

func maxDistToClosest(seats []int) int {
	r := math.MinInt
	sz := len(seats)
	l2r := make([]int, sz)
	r2l := make([]int, sz)

	trk := 0
	for idx, v := range seats {
		trk += 1

		switch v {
		case 0:
			{
				l2r[idx] = trk
			}
		case 1:
			l2r[idx] = trk

			for k := 0; k < trk; k++ {
				r2l[idx-k] = k
			}
			trk = 0
		}
	}

	fmt.Println("dist to left:",l2r)
	fmt.Println("dist to right",r2l)

	for i := 0; i < sz; i++ {

		if math.Min(float64(l2r[i]), float64(r2l[i])) > float64(r) {
			r = i
		}
	}

	return r

}
