package main

import (
	"fmt"
)

/**
Two way of build sum based on pre-calib result cache (tweak between impl)

Although dynamic programming is doable, it is hard to carry cache between stack.

The third one I last impl: a Var to track bigger value of cur value vs {cur value + existing biger track}
Another var to track final max

I see greedy approach mentioned, but don't think that fits.

*/

func main() {

	nums := []int{-2, 1, -3, 4, -1, 2, 1, -5, 4}
	fmt.Println("max value is: ", maxSumSubArray(nums))

}

func checkMax(v int, c int) int {
	// fmt.Println(v, c)
	if v < c {
		return c
	} else {
		return v
	}

}

func maxSumSubArray(nums []int) int {

	var r int
	crw := 0
	for _, v := range nums {
		if crw+v < v {
			crw = v
		} else {
			crw += v
		}

		if crw > r {
			r = crw
		}
	}

	return r

}

func maxSumSubArray2(nums []int) int {
	maxSum := -200
	lrSum := make([][]int, len(nums))
	for idx := range lrSum {
		lrSum[idx] = make([]int, len(nums))
	}

	// 0 idx based
	buf := 0
	for idx, v := range nums {
		buf += v
		lrSum[0][idx] = buf //[start][end]
		maxSum = checkMax(maxSum, lrSum[0][idx])

	}

	// left shrink
	rend := len(nums) - 1
	for l := 1; l < len(nums); l++ {
		lrSum[l][rend] = lrSum[l-1][rend] - nums[l-1]
		maxSum = checkMax(maxSum, lrSum[l][rend])
		for r := rend - 1; r >= l; r-- {
			lrSum[l][r] = lrSum[l][r+1] - nums[r+1]
			maxSum = checkMax(maxSum, lrSum[l][r])
		}

	}

	return maxSum
}

func maxSumSubArrayAlt(nums []int) int {
	maxSum := -1

	for idx, vstart := range nums {
		if vstart < 0 {
			continue
		}

		maxSumStartIdx := idx
		maxSumEndIdx := idx

		sumBuf := vstart
		sumMaxLoc := 0
		for endIdx := idx + 1; endIdx < len(nums); endIdx++ {

			if nums[endIdx] > 0 {
				sumBuf += nums[endIdx]
				if sumMaxLoc < sumBuf {
					sumMaxLoc = sumBuf
				}
			} else if sumBuf+nums[endIdx] >= 0 {
				sumBuf += nums[endIdx]
			} else {
				maxSumEndIdx = endIdx - 1
				fmt.Println("local sum buf:", sumBuf, "max buf", sumMaxLoc, "start:", maxSumStartIdx, "end:", maxSumEndIdx)
				// prepare for next round
				sumBuf = 0
				maxSumStartIdx = endIdx + 1
			}
		}

		fmt.Println(sumMaxLoc, "## round max check, for ", idx)

		if maxSum < sumMaxLoc {
			maxSum = sumMaxLoc
			fmt.Println("Updating max sum:", maxSum)
		}

	}

	return maxSum
}
