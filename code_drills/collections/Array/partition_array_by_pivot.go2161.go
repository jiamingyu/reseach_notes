package main

import (
	"fmt"
)

func main() {

	nums := []int{9, 12, 5, 10, 14, 3, 10}
	pivot := 10
	fmt.Println(pivotArray(nums, pivot))

}

func pivotArray(nums []int, pivot int) []int {
	cur := 0
	rcur := len(nums) - 1
	hasMore := true
	for hasMore {
		pivotRecon := 0
		for pivotRecon < len(nums) {
			if nums[pivotRecon] != pivot {
				pivotRecon += 1
			} else {
				break
			}
		}

		curMoved := false
		for nums[cur] < pivot {
			curMoved = true
			cur += 1
		}
		rcurMoved := false
		for nums[rcur] >= pivot {
			rcurMoved = true
			rcur -= 1
		}

		switch {
		case cur < rcur && curMoved && rcurMoved:
			{
				tmp := nums[cur]
				nums[cur] = nums[rcur]
				nums[rcur] = tmp
			}
		case cur < rcur && curMoved:
			{
				tmp := nums[cur]
				nums[cur] = nums[pivotRecon]
				nums[pivotRecon] = tmp
			}
		case cur < rcur && rcurMoved:
			{
				tmp := nums[rcur]
				nums[rcur] = nums[pivotRecon]
				nums[pivotRecon] = tmp
			}

		default:
			hasMore = false
		}

	}

	return nums
}
