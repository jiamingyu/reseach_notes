package main

import (
	"fmt"
)

/**
https://leetcode.com/problems/remove-element/description/

Proposal

A. L/R cur
B. counting sort 
 */

func main() {
	
	// nums := []int{3, 2, 2, 3}
	// v := 3

	nums := []int{0, 1, 2, 2, 3, 0, 4, 2}
	v := 2

	fmt.Println("idx is: ", removeElement(nums, v))

}

func removeElement(nums []int, v int) int {
	r := len(nums) - 1
	for nums[r] == v && r > -1 {
		r -= 1
	}
	for l := 0; l < r; l++ {
		if nums[l] == v {
			nums[l] = nums[r]
			nums[r] = v
			for nums[r] == v {
				r -= 1
			}
			fmt.Println("swap happening:", nums)

		}
	}

	return r + 1

}
