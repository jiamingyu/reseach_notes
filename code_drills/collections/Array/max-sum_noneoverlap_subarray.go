package main

import (
	"fmt"
	"math"
)

/**
Given an array of integers, find two non-overlapping subarrays which have the largest sum.
The number in each subarray should be contiguous.
Return the largest sum.

For given[1, 3, -1, 2, -1, 2], should be 7: the two subarrays are[1, 3] sum as 4 and[2, -1, 2] sum as 3,
or[1, 3, -1, 2] sum as 5 and[2], they both have the largest sum7.

O(n) for time complexity

Challenge is on how to understand none-overlap: none-overlapping definition: could neighbour, as long as no shared elements
*/

func main() {

	nums := []int{1, 3, -1, 2, -1, 2}
	fmt.Println("max sum of none overlapping arrays is: ", maxSumSubArray(nums))

}

func maxSumSubArray(nums []int) int {

	var r int = math.MinInt
	// left sum

	// var lbuf int = 0
	maxrs := make([]int, len(nums))
	maxrs[len(nums)-1] = nums[len(nums)-1]
	for i := len(nums) - 2; i >= 0; i-- {
		v := nums[i]
		if maxrs[i+1]+v < v {
			maxrs[i] = v
		} else {
			maxrs[i] = maxrs[i+1] + v
		}
	}

	maxls := make([]int, len(nums))
	maxls[0] = nums[0]
	for idx, v := range nums {
		if idx == 0 {
			continue
		}
		v = nums[idx]
		if v > v+maxls[idx-1] {
			maxls[idx] = v
		} else {
			maxls[idx] = maxls[idx-1] + v
		}

	}

	fmt.Println("r to l sum:", maxrs)
	fmt.Println("l to r sum:", maxls)

	for idx := 1; idx < len(nums); idx++ {
		buf := maxrs[idx-1] + maxls[idx]
		fmt.Println(buf)
		if buf > r {
			r = buf
		}
	}

	return r

}
