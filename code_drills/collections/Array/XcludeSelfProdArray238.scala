package net.jyu.scalademo

import org.scalatest.flatspec.AnyFlatSpec


class CollectionDrill extends  AnyFlatSpec{

  "238 Prod of Array except Self" should " be O(n)" in {
    val nums = Array(1,2,3,4)

    productExceptSelf(nums).foreach(println)
  }

  def productExceptSelf(nums: Array[Int]): Array[Int] = {
    var l = 1

    val multicumL = nums.zipWithIndex.map{ case (d,i)=> if (i==0) 1 else {l = l * nums(i-1); l} }
    val rnums = nums.reverse
    var r = 1
    val multicumR = rnums.zipWithIndex.map{ case (d,i)=> if (i==0) 1 else r=r * rnums( i - 1 );r }

    nums.zipWithIndex.map{
      case (d, i) => multicumL(i) * multicumR(nums.length-1-i)
    }
  }
}
