package main

import "fmt"

func main() {
	nums := []int{2, 5, 6, 0, 0, 1, 2}
	// nums := []int{2, 4, 5, 5, 6, 7, 0, 0, 1, 2, 2}
	//            0  1  2  3  4  5  6  7  8  9
	target := 0

	//Test case 2: should return -1
	// nums := []int{2, 5, 6, 0, 0, 1, 2}
	// target = 3

	idx := search(nums, target)
	fmt.Println(idx)

}
func search(nums []int, target int) int {
	l := 0
	r := len(nums) - 1
	if l == r {
		panic("empty input")
	}

	for l < r-1 {
		if nums[l] == nums[l+1] {
			l += 1
			continue
		}
		if nums[r-1] == nums[r] {
			r -= 1
			continue
		}

		m := l + (r-l)/2
		if nums[m] == target {
			return m
		}
		switch {
		// l switch order
		case nums[m] < nums[r]:
			{
				if nums[m] < target && target < nums[r] {
					l = m + 1
				} else {
					r = m - 1
				}
			}

		case nums[l] < nums[m]:
			{ // l switch order, fall in right
				if nums[l] < target && target < nums[m] {
					r = m - 1
				} else {
					l = m + 1
				}
			}
			fmt.Println("l, r", l, r)

		}
	}

	//post process
	m := l + (r-l)/2
	switch {
	case target == nums[l]:
		return l

	case target == nums[m]:
		return m
	case target == nums[r]:
		return r
	default:
		return -1
	}

}
