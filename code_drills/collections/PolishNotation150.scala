import org.scalatest.flatspec.AnyFlatSpec
import scala.annotation.tailrec
import scala.collection.mutable.ListBuffer
import scala.collection.mutable.ArrayBuffer


class MyTest extends AnyFlatSpec{
  "150 reverse polish notation tokens = [\"4\",\"13\",\"5\",\"/\",\"+\" " should "6" in {
    //    evalReversePolishNotation(Array.apply("2","1","+","3","*")).get
    val tokens = Array("4","13","5","/","+")
    recurPolishNotationWOFixedPositions(tokens) match {
      case None => println("Something wrong")
      case Some(r) => println(r)
    }

  }

  "150 reverse polish notation tokens = [\"10\",\"6\",\"9\",\"3\",\"+\",\"-11\",\"*\",\"/\",\"*\",\"17\",\"+\",\"5\",\"+\"] " should "22" in {
    //    evalReversePolishNotation(Array.apply("2","1","+","3","*")).get
    val tokens = Array("10","6","9","3","+","-11","*","/","*","17","+","5","+")
    recurPolishNotationWOFixedPositions(tokens) match {
      case None => println("Something wrong")
      case Some(r) => println(r)
    }

  }


  "150 reverse polish notation tokens = [\"2\",\"1\",\"+\",\"3\",\"*\"] " should "9" in {
    //    evalReversePolishNotation(Array.apply("2","1","+","3","*")).get
    val tokens = Array("2", "1", "+", "3", "*")
    recurPolishNotationWOFixedPositions(tokens) match {
      case None => println("Something wrong")
      case Some(r) => println(r)
    }

  }

  @tailrec
  final def recurPolishNotationWOFixedPositions(tokens: Array[String]): Option[Int] = {

    if (tokens.length == 1)
      return Some(Integer.parseInt(tokens(0)))

    var idx = -1
    breakable {
      for (t2i <- tokens.zipWithIndex) {
        if (t2i._1 == "+" || t2i._1 == "-" || t2i._1 == "*" || t2i._1 == "/") {
          idx = t2i._2
          break
        }
      }
    }

    val op = tokens(idx);
    val a = Integer.parseInt(tokens(idx-2));
    val b = Integer.parseInt(tokens(idx-1))

    val calib = op match {
      case "+" => {
        Some(a + b)
      }
      case "-" => {
        Some(a - b)
      }
      case "*" => {
        Some(a * b)
      }
      case "/" => {
        Some(a / b)
      }
      case _ => {
        return None
      }
    }

    val ab = ArrayBuffer[String]()
    ab.addAll(tokens.slice(0, idx-2))
    ab.addOne(calib.get.toString)
    ab.addAll(tokens.slice(idx+1, tokens.length))

    recurPolishNotationWOFixedPositions(ab.toArray)

  }
}