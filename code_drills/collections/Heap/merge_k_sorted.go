package main

import (
	"fmt"
	"math"
)

type MinHeap []int

func (h *MinHeap) peek() int {
	if h.isEmpty() {
		return math.MinInt
	}
	return (*h)[0]
}
func (h *MinHeap) isEmpty() bool {
	return len(*h) == 0
}

func (h *MinHeap) Insert(v int) {
	*h = append(*h, v)
	h.heapifyUp(len(*h) - 1)
}

func (h *MinHeap) Extract() int {
	if len(*h) == 0 {
		return math.MinInt
	}

	v := (*h)[0]

	(*h)[0] = (*h)[len(*h)-1]
	*h = (*h)[:len(*h)-1]
	(*h).heapifyDown()

	return v
}

func (h *MinHeap) heapifyUp(index int) {
	if len(*h) <= 1 {
		return
	}

	idxCur := index
	for {
		pidx := (idxCur - 1) / 2
		if (*h)[pidx] > (*h)[idxCur] {
			(*h)[pidx], (*h)[idxCur] = (*h)[idxCur], (*h)[pidx]
			idxCur = pidx
		} else {
			break
		}
	}

}

func (h *MinHeap) heapifyDown() {
	if len(*h) == 0 {
		return
	}

	cur := 0

	for {

		if cur == len(*h)-1 { // cur last element
			break
		}
		l := cur*2 + 1
		r := cur*2 + 2

		if l >= len(*h) {
			break
		}

		if r == len(*h) { // only l as last element, no r
			if (*h)[cur] > (*h)[l] {
				(*h)[cur], (*h)[l] = (*h)[l], (*h)[cur]
			}
			break
		}

		switch {

		case (*h)[cur] < (*h)[l] && (*h)[cur] < (*h)[r]:
			{
				return
			}

		case (*h)[cur] > (*h)[l] && (*h)[cur] > (*h)[r]:
			{
				if (*h)[l] < (*h)[r] {
					(*h)[cur], (*h)[l] = (*h)[l], (*h)[cur]
				} else {
					(*h)[cur], (*h)[r] = (*h)[r], (*h)[cur]
				}
			}

		case (*h)[cur] < (*h)[l] && (*h)[cur] > (*h)[r]:
			{
				(*h)[cur], (*h)[r] = (*h)[r], (*h)[cur]
				cur = r
			}

		case (*h)[cur] > (*h)[l] && (*h)[cur] < (*h)[r]:
			{
				(*h)[cur], (*h)[l] = (*h)[l], (*h)[cur]
				cur = l
			}

		default:

		}

	}

}

type ListNode struct {
	Val  int
	Next *ListNode
}

func mergeKLists(lists [][]int) []int {
	if len(lists) == 0 || len(lists[0]) == 0 {
		return make([]int, 0)
	}

	ptrs := make([]*ListNode, len(lists))
	for rid, rowV := range lists {
		var n *ListNode
		var oldN *ListNode
		var headN *ListNode

		for cid, v := range rowV {

			if cid == 0 {
				headN = &ListNode{Val: v}
				oldN = headN

			} else {
				n = &ListNode{Val: v}
				oldN.Next = n
				oldN = n
			}

		}
		if headN != nil {
			ptrs[rid] = headN
		}
	}

	r := make([]int, 0)

	hp := &MinHeap{}

	v2Nodes := make(map[int][]*ListNode)

	for _, ptr := range ptrs {
		hp.Insert(ptr.Val)
		if v2Nodes[ptr.Val] == nil {
			v2Nodes[ptr.Val] = []*ListNode{ptr}
		} else {
			v2Nodes[ptr.Val] = append(v2Nodes[ptr.Val], ptr)
		}
	}

	// take out
	for !hp.isEmpty() {
		e := hp.Extract()
		r = append(r, e)

		for {
			if hp.peek() == e {
				e := hp.Extract()
				r = append(r, e)
			} else {
				break
			}
		}

		for _, singleP := range v2Nodes[e] {
			if singleP.Next != nil {
				nd := singleP.Next

				hp.Insert(nd.Val)
				if v2Nodes[nd.Val] == nil {
					v2Nodes[nd.Val] = []*ListNode{nd}
				} else {
					v2Nodes[nd.Val] = append(v2Nodes[nd.Val], nd)
				}
			}

		}

	}

	return r
}

func main() {
	// lists := [][]int{{}}
	lists := [][]int{{1, 4, 5}, {1, 3, 4}, {2, 6}}
	// lists := [][]int{{1, 4, 9}, {2, 3, 8}, {6, 11}} // for demo purpose, we use unique data

	fmt.Println(mergeKLists(lists))
}
