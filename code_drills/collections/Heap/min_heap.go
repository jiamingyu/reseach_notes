package main

import (
	"fmt"
	"math"
)

type MinHeap []int

func (h *MinHeap) Insert(v int) {
	*h = append(*h, v)
	h.heapifyUp(len(*h) - 1)
}

func (h *MinHeap) Extract() int {
	if len(*h) == 0 {
		return math.MinInt
	}

	v := (*h)[0]

	(*h)[0] = (*h)[len(*h)-1]
	*h = (*h)[:len(*h)-1]
	(*h).heapifyDown()

	return v
}

func (h *MinHeap) heapifyUp(index int) {
	if len(*h) <= 1 {
		return
	}

	idxCur := index
	for {
		pidx := (idxCur - 1) / 2
		if (*h)[pidx] > (*h)[idxCur] {
			(*h)[pidx], (*h)[idxCur] = (*h)[idxCur], (*h)[pidx]
			idxCur = pidx
		} else {
			break
		}
	}

}

func (h *MinHeap) heapifyDown() {
	if len(*h) == 0 {
		fmt.Println("*** reaching end ***")
		return
	}

	cur := 0

	for {

		if cur == len(*h)-1 { // cur last element
			break
		}
		l := cur*2 + 1
		r := cur*2 + 2

		if l >= len(*h) {
			break
		}

		if r == len(*h) { // only l as last element, no r
			if (*h)[cur] > (*h)[l] {
				(*h)[cur], (*h)[l] = (*h)[l], (*h)[cur]
			}
			break
		}

		switch {

		case (*h)[cur] < (*h)[l] && (*h)[cur] < (*h)[r]:
			{
				return
			}

		case (*h)[cur] > (*h)[l] && (*h)[cur] > (*h)[r]:
			{
				if (*h)[l] < (*h)[r] {
					(*h)[cur], (*h)[l] = (*h)[l], (*h)[cur]
				} else {
					(*h)[cur], (*h)[r] = (*h)[r], (*h)[cur]
				}
			}

		case (*h)[cur] < (*h)[l] && (*h)[cur] > (*h)[r]:
			{
				(*h)[cur], (*h)[r] = (*h)[r], (*h)[cur]
				cur = r
			}

		case (*h)[cur] > (*h)[l] && (*h)[cur] < (*h)[r]:
			{
				(*h)[cur], (*h)[l] = (*h)[l], (*h)[cur]
				cur = l
			}

		default:
			// fmt.Println((*h)[cur], (*h)[l], (*h)[r])

		}

	}

}

// func main2() {
// 	heap := &MinHeap{}

// 	heap.Insert(82)
// 	heap.Insert(13)

// 	heap.Insert(2)
// 	heap.Insert(1)
// 	heap.Insert(12)

// 	fmt.Println(heap)
// 	for len(*heap) > 0 {
// 		fmt.Println("Extracted:", heap.Extract())
// 	}

// fmt.Println("Extracted:", heap.Extract())
// fmt.Println(*heap)

// fmt.Println("Extracted:", heap.Extract())
// fmt.Println("Extracted:", heap.Extract())
// fmt.Println("Extracted:", heap.Extract())
// fmt.Println("Extracted:", heap.Extract())
// fmt.Println("Extracted:", heap.Extract())

// fmt.Println(heap)

// }

func main() {
	heap := &MinHeap{}
	heap.Insert(45)
	heap.Insert(20)
	heap.Insert(14)
	heap.Insert(12)
	heap.Insert(31)
	heap.Insert(70)
	heap.Insert(11)
	heap.Insert(15)

	fmt.Println("Heap:", *heap) // Output: Heap: [11 12 14 20 31 70 45 15]

	for len(*heap) > 0 {
		fmt.Println("Extracted:", heap.Extract())
		fmt.Println(*heap)
	}
	//Extracted: 11
	//Extracted: 12
	//Extracted: 14
	//Extracted: 15
	//Extracted: 20
	//Extracted: 31
	//Extracted: 45
	//Extracted: 70
}
