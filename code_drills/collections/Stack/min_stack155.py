# python 3 format


class StackNode(object):
    def __init__(self, v, minV, lowerNode):
        self.v = v
        self.minV = minV
        self.lowerNode = lowerNode
        

class MinStack(object):
    
    def __init__(self):
        self.node = None
    
    def __str__(self):
        # return "value is {0}, min is {}".format(self.node.v, self.node.minV)
        return "value is "
        
        
    def push(self, val):
        if (self.node == None):
            self.node = StackNode(val, val, None)
        else:
            self.node = StackNode(val, self.node.v,self.node) if self.node.v < val else StackNode(val, val,self.node)
        # print(self.node.minV)
        
    def getMin(self):
        return self.node.minV
        
    def pop(self):
        buf = self.node
        self.node = self.node.lowerNode
        return buf
        
    def peek(self):
        return self.node
    

stack = MinStack()
stack.push(3)
stack.push(5)



print(stack.getMin())
stack.push(2)
stack.push(1)
print( stack.getMin() )
stack.pop()
print( stack.getMin() )
stack.pop()

print("now look@ peek")
print(stack.peek().v)
print(stack.peek().minV)
